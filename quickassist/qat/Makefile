########################################################################
#  SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only)
#  Copyright(c) 2014 - 2021 Intel Corporation
########################################################################
export CONFIG_CRYPTO_DEV_QAT=m
export CONFIG_CRYPTO_DEV_CCAT_C1XXX=m
export CONFIG_CRYPTO_DEV_CCAT_C1XXX_DEVICE=m
export QAT_UIO?=y
export ICP_HB_FAIL_SIM?=n

ifeq ($(ICP_HB_FAIL_SIM),y)
$(info Compiling with Heartbeat Failure Simulation feature)
endif

ifeq ($(KERNELRELEASE),)
KDIR ?= $(INSTALL_MOD_PATH)/lib/modules/$(shell uname -r)/build
ifneq ($(shell if [ -e $(KDIR)/include/config/auto.conf ]; then echo 1; fi),1)
  $(error ERROR: Kernel header files not found.  Install the appropriate \
    kernel development package necessary for building external kernel modules \
    or run 'make oldconfig && make modules_prepare' on kernel src to fix it)
endif
include $(KDIR)/include/config/auto.conf
INSTALL_FW_PATH ?= $(INSTALL_MOD_PATH)/lib/firmware

default: modules

# Error out on missing kernel config dependencies
CONFIG_%:
	$(if $($@), , $(error $@ not enabled in kernel configuration))

ifeq ($(QAT_UIO),y)
KCONFIG_DEP += CONFIG_UIO
endif

modules: $(KCONFIG_DEP)
	$(MAKE) -C $(KDIR) M=$(CURDIR) $@

modules_install: modules
	@# Install the external/out-of-tree modules under the
	@# $(INSTALL_MOD_PATH)/lib/modules/$(KERNELRELEASE)/updates/
	@# directory in order to supersede any in-kernel modules with the
	@# same name.  Do not remove the latter modules as that could corrupt
	@# the package manager (e.g. rpm) database.
	$(MAKE) -C $(KDIR) M=$(CURDIR) INSTALL_MOD_DIR=updates $@

clean:
	$(MAKE) -C $(KDIR) M=$(CURDIR) $@

help:
	@echo ''
	@echo '  Building external/out-of-tree Intel(R) QuickAssist Technology modules.'
	@echo ''
	@echo '  Syntax: make [install-location-options] [target]'
	@echo ''
	@echo '  install-location-options'
	@echo '    [KDIR=<path>]             - path to kernel source (default: running kernel)'
	@echo '    [INSTALL_MOD_PATH=<path>] - optional prefix to the default /lib/modules/...'
	@echo '    [INSTALL_FW_PATH=<path>]  - default: $$(INSTALL_MOD_PATH)/lib/firmware/'
	@echo ''
	@echo '  target'
	@echo '    [modules]                 - default target, build the module(s)'
	@echo '    {modules_install}         - install the module(s) and firmware'
	@echo '    {clean}                   - remove generated files'
	@echo ''

.PHONY: default modules modules_install clean help
else
subdir-ccflags-y += -include $(src)/compat/qat_compat.h
obj-m := drivers/crypto/qat/
endif
