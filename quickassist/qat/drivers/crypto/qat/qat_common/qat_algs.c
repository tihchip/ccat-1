// SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only)
/* Copyright(c) 2014 - 2021 Intel Corporation */
#ifdef QAT_SKCIPHER_SUPPORTED
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/crypto.h>
#include <linux/workqueue.h>
#include <crypto/internal/aead.h>
#include <crypto/internal/skcipher.h>
#include <crypto/aes.h>
#include <crypto/sha.h>
#include <crypto/hash.h>
#include <crypto/algapi.h>
#include <crypto/authenc.h>
#include <crypto/sm3.h>
#include <crypto/sm4.h>
#include <crypto/des.h>
#include <crypto/md5.h>
#include <crypto/ctr.h>
#include <crypto/gcm.h>
#include <crypto/hmac.h>
#include <crypto/rng.h>
#include <crypto/internal/des.h>
#include <crypto/internal/hash.h>
#include <crypto/scatterwalk.h>
#include <linux/dma-mapping.h>
#include "adf_accel_devices.h"
#include "adf_transport.h"
#include "adf_common_drv.h"
#include "qat_crypto.h"
#include "icp_qat_hw.h"
#include "icp_qat_fw.h"
#include "icp_qat_fw_la.h"

#ifdef ICP_DEBUG
#define ccat_log(...)                                  \
	do {                                                  \
		printk(KERN_INFO __VA_ARGS__);                    \
	} while (0)
#define ccat_hexdump(msg, var, var_len)                \
	do {                                                  \
		print_hex_dump(KERN_INFO, msg, DUMP_PREFIX_OFFSET,  \
				16, 1, var, var_len, false);              \
	} while (0)
#else /* !DEBUG_ON */
#define ccat_log(...) do {} while (0)
#define ccat_hexdump(msg, var, var_len) do {} while (0)
#endif /* DEBUG_ON */

static DEFINE_MUTEX(algs_lock);
static unsigned int active_devs;

struct ccat_auth_info {
	enum icp_hsm_sym_auth_mode mode;
	enum icp_hsm_sym_cipher_alg cipher_alg;
	enum icp_hsm_sym_hash_alg hash_alg;
};

struct ccat_hash_info {
	enum icp_hsm_sym_hash_alg alg;
};

struct ccat_cipher_info {
	enum icp_hsm_sym_cipher_alg alg;
	enum icp_hsm_sym_cipher_mode mode;
};

struct ccat_aead_info {
	enum icp_hsm_sym_aead_mode aead_mode;
	enum icp_hsm_sym_aead_tag aead_tag;
	enum icp_hsm_sym_cipher_alg cipher_alg;
	enum icp_hsm_sym_cipher_mode cipher_mode;
	enum icp_hsm_sym_hash_alg hash_alg;
	enum icp_hsm_sym_auth_mode auth_mode;
};

struct ccat_alg_template {
	u32 type;
	union {
		struct skcipher_alg acipher;
		struct ahash_alg ahash;
		struct aead_alg aead;
	} alg;
	enum icp_hsm_type hsm_type;
	union {
		struct ccat_cipher_info cipher_info;
		struct ccat_hash_info hash_info;
		struct ccat_auth_info auth_info;
		struct ccat_aead_info aead_info;
	} alg_info;
	bool is_rfc4106;
	bool is_rfc4309;
	bool is_rfc3686;
	bool is_echainiv;
};

struct qat_alg_aead_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};
	u8 cipher_key[ICP_HSM_MAX_CIPHER_KEY_SIZE];
	u32 cipher_key_sz;
	u8 auth_key[ICP_HSM_MAX_AUTH_KEY_SIZE];
	u32 auth_key_sz;
	union {
		u8 salt[16]; /* rfc4106/4309 */
		u8 nonce[16]; /* rfc3686 */
	};
	union {
		u32 salt_sz; /* rfc4106/4309 */
		u32 nonce_sz; /* rfc3686 */
	};
	u32 auth_tag_sz;
	bool is_rfc4106;
	bool is_rfc4309;
	bool is_rfc4543;
	bool is_rfc3686;
	bool is_echainiv;

	void *sw_tfm;
	struct qat_crypto_instance *inst;
};

struct qat_alg_skcipher_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};

	u8 cipher_key[ICP_HSM_MAX_CIPHER_KEY_SIZE];
	u32 cipher_key_sz;

	u8   nonce[16];    /* rfc3686 */
	u32  nonce_sz;    /* rfc3686 */

	bool is_rfc3686;

	struct qat_crypto_instance *inst;
	void *sw_tfm;
};

struct ccat_ahash_export_state {
	bool is_auth;
	bool is_init;
	bool is_finish;
	u8   cache_next[SHA512_BLOCK_SIZE];
	u32  cache_sz;
	u32  total_len;
	u8   digest[SHA512_DIGEST_SIZE];
};

struct qat_alg_ahash_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};
	u8 auth_key[ICP_QAT_HW_AUTH_KEY_SIZE];
	u32 auth_key_sz;

	struct qat_crypto_instance *inst;
	void *sw_tfm;
};

#define CCAT_AEAD
#define CCAT_CIPHER
#define CCAT_HASH

#ifdef CCAT_AEAD
static int ccat_aead_req_map(struct qat_crypto_instance *inst,
			       struct qat_alg_aead_ctx *qat_ctx,
			       struct scatterlist *sglin,
			       struct scatterlist *sglout,
				   struct ccat_aead_request *qat_req)
{
	u32 nr_src, nr_dst, src_num, dst_num, clen, i;
	struct scatterlist *sg = NULL;
	struct icp_hsm_addr_list *in_list = qat_req->in_list;
	struct icp_hsm_addr_list *out_list = qat_req->out_list;
	u32 crypt_len = qat_req->crypt_len;

	if (unlikely(!sg_nents(sglin)))
		return -EINVAL;

	src_num = 0;
	dst_num = 0;

	qat_req->aad_dma = dma_map_single(&GET_DEV(inst->accel_dev), qat_req->aad, ICP_HSM_AAD_MAX_SIZE, DMA_TO_DEVICE);
	if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->aad_dma))) {
		goto err_aad;
	}

	if (sglin == sglout) {
		nr_src = dma_map_sg(&GET_DEV(inst->accel_dev), sglin, sg_nents(sglin), DMA_BIDIRECTIONAL);

		if (unlikely(!nr_src))
			goto err_in;

		nr_src = sg_nents(sglin);
		for_each_sg(sglin, sg, nr_src, i) {
			if ((!sg->length) || (!crypt_len))
				continue;

			if (crypt_len < sg->length)
				clen = crypt_len;
			else
				clen = sg->length;

			in_list[src_num].addr = sg_dma_address(sg);
			in_list[src_num].length = clen;
			out_list[src_num].addr = in_list[src_num].addr;
			out_list[src_num].length = in_list[src_num].length;
			crypt_len -= clen;
			src_num++;
		}
		qat_req->in_num = qat_req->out_num = src_num;

		qat_req->in_list_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)in_list,
						sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
						DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)))
			goto err_in;

		qat_req->out_list_dma = qat_req->in_list_dma;

		qat_req->is_same_addr = true;
	} else {
		nr_src = dma_map_sg(&GET_DEV(inst->accel_dev), sglin, sg_nents(sglin), DMA_BIDIRECTIONAL);

		if (unlikely(!nr_src))
			goto err_in;

		for_each_sg(sglin, sg, nr_src, i) {
			if ((!sg->length) || (!crypt_len))
				continue;

			if (crypt_len < sg->length)
				clen = crypt_len;
			else
				clen = sg->length;

			in_list[src_num].addr = sg_dma_address(sg);
			in_list[src_num].length = clen;
			crypt_len -= clen;
			src_num++;
		}
		qat_req->in_num = src_num;

		crypt_len = qat_req->crypt_len;
		nr_dst = dma_map_sg(&GET_DEV(inst->accel_dev), sglout, sg_nents(sglout), DMA_BIDIRECTIONAL);
		if (unlikely(!nr_dst))
			goto err_out;

		for_each_sg(sglout, sg, nr_dst, i) {
			if ((!sg->length) || (!crypt_len))
				continue;

			if (crypt_len < sg->length)
				clen = crypt_len;
			else
				clen = sg->length;

			out_list[dst_num].addr = sg_dma_address(sg);
			out_list[dst_num].length = clen;
			crypt_len -= clen;
			dst_num++;
		}
		qat_req->out_num = dst_num;

		qat_req->in_list_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)in_list,
						sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
						DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)))
			goto err_out;

		qat_req->out_list_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)out_list,
						sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
						DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->out_list_dma)))
			goto err_out;

		qat_req->is_same_addr = false;
	}

	/* HW CTX need flush cache after AAD setting */
	qat_req->req_ctx.ctx_hw.aead_ctx_hw.aad_addr = qat_req->aad_dma;
	qat_req->req_ctx.ctx_hw.aead_ctx_hw.aad_nbits = (qat_req->aad_len)*8;
	if (qat_ctx->is_rfc4106 || qat_ctx->is_rfc4309)
		qat_req->req_ctx.ctx_hw.aead_ctx_hw.aad_nbits -= qat_req->iv_len*8;
	if (qat_ctx->is_rfc4543) {
		qat_req->req_ctx.ctx_hw.aead_ctx_hw.aad_nbits = (qat_req->aad_len + qat_req->crypt_len) * 8;
		qat_ctx->msg.hdr.sym_hdr.total_len = 0;
	}

	qat_req->req_ctx.ctx_hw_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)&qat_req->req_ctx.ctx_hw,
							sizeof(struct icp_hsm_sym_aead_ctx_hw),
							DMA_BIDIRECTIONAL);
	if (dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
		goto err_ctx;
	}

	return 0;

err_ctx:
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
				sizeof(struct icp_hsm_sym_cipher_ctx_hw),
				DMA_BIDIRECTIONAL);
	}
err_out:
	dst_num = sg_nents(sglout);
	for (i = 0; i < dst_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), out_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev), out_list[i].addr, out_list[i].length,
							DMA_BIDIRECTIONAL);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->out_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->out_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
	}

err_in:
	src_num = sg_nents(sglin);
	for (i = 0; i < src_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), in_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev), in_list[i].addr, in_list[i].length,
							DMA_BIDIRECTIONAL);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->in_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
	}

err_aad:
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->aad_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->aad_dma,
				ICP_HSM_AAD_MAX_SIZE,
				DMA_TO_DEVICE);
	}

	dev_err(&GET_DEV(inst->accel_dev), "ERR[AEAD]# Failed to map buf of HSM for dma \n");
	return -ENOMEM;
}

static int ccat_aead_req_unmap(struct qat_crypto_instance *inst,
				   struct ccat_aead_request *qat_req)
{
	u32 i;
	struct icp_hsm_addr_list *in_list = qat_req->in_list;
	struct icp_hsm_addr_list *out_list = qat_req->out_list;

	if(qat_req->is_same_addr == false) {
		for (i = 0; i < qat_req->out_num; i++) {
			if (!dma_mapping_error(&GET_DEV(inst->accel_dev), out_list[i].addr)) {
				dma_unmap_single(&GET_DEV(inst->accel_dev), out_list[i].addr, out_list[i].length,
								DMA_BIDIRECTIONAL);
			}
		}
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->out_list_dma)) {
			dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->out_list_dma,
					sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
					DMA_TO_DEVICE);
			qat_req->out_list_dma = 0;
		}
	}

	for (i = 0; i < qat_req->in_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), in_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev), in_list[i].addr, in_list[i].length,
							DMA_BIDIRECTIONAL);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->in_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
		qat_req->in_list_dma = 0;
	}

	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
				sizeof(struct icp_hsm_sym_aead_ctx_hw),
				DMA_BIDIRECTIONAL);
		qat_req->req_ctx.ctx_hw_dma = 0;
	}

	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->aad_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->aad_dma,
				ICP_HSM_AAD_MAX_SIZE,
				DMA_TO_DEVICE);
		qat_req->aad_dma = 0;
	}
	return 0;
}

/**
 * ccat_aead_alg_callback() - HSM aead result handler callback function.
 * @banks: HSM private data.
 * @ring: HSM descriptor ring ID.
 * @areq: Crypto API request data.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static void ccat_aead_alg_callback(struct icp_qat_fw_la_resp *qat_resp,
	struct qat_crypto_request *qat_req)
{
	struct qat_alg_aead_ctx *ctx = qat_req->aead_ctx;
	struct qat_crypto_instance *inst = ctx->inst;
	struct aead_request *req = qat_req->aead_req;
	struct crypto_aead *aead = crypto_aead_reqtfm(req);
	u32 macsize = crypto_aead_authsize(aead);
	u8 mac_cmp[32];
	int ret = 0;

	ccat_aead_req_unmap(inst, &qat_req->ccat_aead_req);
	/* copy aad and mac */
	if (req->src != req->dst)
		scatterwalk_map_and_copy(qat_req->ccat_aead_req.aad, req->dst,
				0, req->assoclen, 1);

	/* Dec */
	if (qat_req->ccat_aead_req.op) {
		scatterwalk_map_and_copy(mac_cmp, req->src,
				req->assoclen + req->cryptlen - macsize, macsize, 0);
		if (crypto_memneq(mac_cmp, qat_req->ccat_aead_req.req_ctx.ctx_hw.aead_ctx_hw.digest, macsize))
			ret = -EBADMSG;
	} else { //Enc
		scatterwalk_map_and_copy(qat_req->ccat_aead_req.req_ctx.ctx_hw.aead_ctx_hw.digest, req->dst,
				req->assoclen + req->cryptlen, macsize, 1);
	}

	req->base.complete(&req->base, ret);
}

/**
 * ccat_aead_sw() - AEAD Soft algorithm.
 * @areq: aead request data.
 * @ctx:  ccat sym context address.
 * @enc:  encrypt flag.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_aead_sw(struct aead_request *areq,
						   struct qat_alg_aead_ctx *ctx, int enc)
{
	struct crypto_aead *tfm = (struct crypto_aead *)(ctx->sw_tfm);
	struct aead_request *req;
	struct crypto_wait wait;
	u32 c_alg = ctx->msg.hdr.sym_hdr.cipher_alg;
	u32 authsize;
	int ret;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (c_alg == ICP_HSM_SYM_CIPHER_ALG_SM4)
		return -ESRCH;

	authsize = ctx->auth_tag_sz;

	if (NULL == tfm || IS_ERR(tfm))
		return -ENOMEM;

	crypto_init_wait(&wait);

	req = aead_request_alloc(tfm, GFP_ATOMIC);
	if (!req)
		return -ENOMEM;

	aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
				  crypto_req_done, &wait);

	ret = crypto_aead_setauthsize(tfm, authsize);
	if (ret)
		return ret;

	aead_request_set_crypt(req, areq->src, areq->dst, areq->cryptlen, areq->iv);

	aead_request_set_ad(req, areq->assoclen);

	ret = crypto_wait_req(enc ? crypto_aead_encrypt(req)
				  : crypto_aead_decrypt(req), &wait);

	aead_request_free(req);

	return ret;
}

/**
 * ccat_aead_gcm_ccm_setkey() - HSM aead GCM/CCM setkey function.
 * @aead:   User-instantiated objects which encapsulate.
 * @key:    key address.
 * @keylen: key length.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_aead_gcm_ccm_setkey(struct crypto_aead *aead,
								const u8 *key, unsigned int keylen)
{
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	struct crypto_tfm *tfm = crypto_aead_tfm(aead);
	struct qat_alg_aead_ctx *ctx = crypto_tfm_ctx(tfm);
#ifdef CCAT_SW_OP
	struct crypto_aead *sw_tfm = (struct crypto_aead *)(ctx->sw_tfm);
	u32 c_alg = ctx->msg.hdr.sym_hdr.cipher_alg;
	u32 c_mode = ctx->msg.hdr.sym_hdr.cipher_mode;
	u8 sw_key[128];
	u32 sw_klen;
#endif

	ccat_log("[CCAT] aead gcm/ccm setkey, keylen: %d\n", keylen);

	switch (ctx->msg.hdr.sym_hdr.cipher_alg) {
	case ICP_HSM_SYM_CIPHER_ALG_AES128:
	case ICP_HSM_SYM_CIPHER_ALG_AES192:
	case ICP_HSM_SYM_CIPHER_ALG_AES256:
		if (keylen == AES_KEYSIZE_128) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128;
		} else if (keylen == AES_KEYSIZE_256) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES256;
		} else if (keylen == AES_KEYSIZE_192) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES192;
		}
		break;
	case ICP_HSM_SYM_CIPHER_ALG_SM4:
		break;
	default:
		return -EINVAL;
	}

	memcpy(ctx->cipher_key, key, keylen);
	ctx->cipher_key_sz = keylen;

#ifdef CCAT_SW_OP
	if (NULL == sw_tfm || IS_ERR(sw_tfm))
		return 0;

	if (c_alg == ICP_HSM_SYM_CIPHER_ALG_SM4)
		return 0;

	switch (c_mode) {
	case ICP_HSM_SYM_CIPHER_MODE_GCM:
		if (ctx->is_rfc4106) {
			memcpy(sw_key, ctx->cipher_key, ctx->cipher_key_sz);
			memcpy(sw_key + ctx->cipher_key_sz, ctx->salt, ICP_GCM_SALT_SIZE);
			sw_klen = ctx->cipher_key_sz + ICP_GCM_SALT_SIZE;
		} else {
			memcpy(sw_key, ctx->cipher_key, ctx->cipher_key_sz);
			sw_klen = ctx->cipher_key_sz;
		}
		break;
	case ICP_HSM_SYM_CIPHER_MODE_CCM:
		if (ctx->is_rfc4309) {
			memcpy(sw_key, ctx->cipher_key, ctx->cipher_key_sz);
			memcpy(sw_key + ctx->cipher_key_sz, ctx->salt, ICP_CCM_SALT_SIZE);
			sw_klen = ctx->cipher_key_sz + ICP_CCM_SALT_SIZE;
		} else {
			memcpy(sw_key, ctx->cipher_key, ctx->cipher_key_sz);
			sw_klen = ctx->cipher_key_sz;
		}
		break;
	default:
		return 0;
	}
	crypto_aead_clear_flags(sw_tfm, ~0);
	if (crypto_aead_setkey(sw_tfm, sw_key, sw_klen))
		crypto_free_aead(sw_tfm);
#endif

	return 0;
}

/**
 * ccat_aead_rfc4106_gcm_setkey() - HSM aead GCM(rfc4106) setkey function.
 * @aead:   User-instantiated objects which encapsulate.
 * @key:    key address.
 * @keylen: key length.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_aead_rfc4106_gcm_setkey(struct crypto_aead *aead,
									const u8 *key, unsigned int keylen)
{
	struct crypto_tfm *tfm = crypto_aead_tfm(aead);
	struct qat_alg_aead_ctx *ctx = crypto_tfm_ctx(tfm);

	ccat_log("[CCAT] aead rfc4106-gcm setkey, keylen: %d\n", keylen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (keylen < ICP_GCM_SALT_SIZE)
		return -EINVAL;

	ctx->salt_sz = ICP_GCM_SALT_SIZE;
	memcpy(ctx->salt, key + keylen - ICP_GCM_SALT_SIZE, ICP_GCM_SALT_SIZE);
	keylen -= ICP_GCM_SALT_SIZE;
	ctx->is_rfc4106 = true;

	return ccat_aead_gcm_ccm_setkey(aead, key, keylen);
}

/**
 * ccat_aead_rfc4309_ccm_setkey() - HSM aead CCM(rfc4309) setkey function.
 * @aead:   User-instantiated objects which encapsulate.
 * @key:    key address.
 * @keylen: key length.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_aead_rfc4309_ccm_setkey(struct crypto_aead *aead,
									const u8 *key, unsigned int keylen)
{
	struct crypto_tfm *tfm = crypto_aead_tfm(aead);
	struct qat_alg_aead_ctx *ctx = crypto_tfm_ctx(tfm);

	ccat_log("[CCAT] aead rfc4306-ccm setkey, keylen: %d\n", keylen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (keylen < ICP_CCM_SALT_SIZE)
		return -EINVAL;

	ctx->salt_sz = ICP_CCM_SALT_SIZE;
	memcpy(ctx->salt, key + keylen - ICP_CCM_SALT_SIZE, ICP_CCM_SALT_SIZE);
	keylen -= ICP_CCM_SALT_SIZE;
	ctx->is_rfc4309 = true;

	return ccat_aead_gcm_ccm_setkey(aead, key, keylen);
}

/**
 * ccat_aead_authenc_setkey() - HSM aead authenc setkey function.
 * @aead:   User-instantiated objects which encapsulate.
 * @key:    key address.
 * @keylen: key length.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_aead_authenc_setkey(struct crypto_aead *aead,
								const u8 *key, unsigned int keylen)
{
	struct crypto_tfm *tfm = crypto_aead_tfm(aead);
	struct qat_alg_aead_ctx *ctx = crypto_tfm_ctx(tfm);
	struct crypto_authenc_keys keys;
#ifdef CCAT_SW_OP
	struct crypto_aead *sw_tfm = (struct crypto_aead *)(ctx->sw_tfm);
	u32 c_alg = ctx->msg.hdr.sym_hdr.cipher_alg;
	u32 c_mode = ctx->msg.hdr.sym_hdr.cipher_mode;
	u8 sw_key[128];
	u32 sw_klen;
#endif

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (crypto_authenc_extractkeys(&keys, key, keylen) != 0) {
		crypto_aead_set_flags(aead, CRYPTO_TFM_REQ_MASK);
		return -EINVAL;
	}

	ccat_log("[CCAT] aead authenc setkey, enckeysize: %d, authkeysize: %d\n", keys.enckeylen, keys.authkeylen);

	if (keys.authkeylen > 64) {
		crypto_aead_set_flags(aead, CRYPTO_TFM_REQ_MASK);
		return -EINVAL;
	}

	switch (ctx->msg.hdr.sym_hdr.cipher_alg) {
	case ICP_HSM_SYM_CIPHER_ALG_AES128:
	case ICP_HSM_SYM_CIPHER_ALG_AES192:
	case ICP_HSM_SYM_CIPHER_ALG_AES256:
		if (keys.enckeylen == AES_KEYSIZE_128) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128;
		} else if (keys.enckeylen == AES_KEYSIZE_256) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES256;
		} else if (keys.enckeylen == AES_KEYSIZE_192) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES192;
			if (ctx->is_echainiv)
				return -ESRCH;
		}
		break;
	case ICP_HSM_SYM_CIPHER_ALG_SM4:
		break;
	default:
		return -EINVAL;
	}

	memcpy(ctx->cipher_key, keys.enckey, keys.enckeylen);
	ctx->cipher_key_sz = keys.enckeylen;
	memset(ctx->auth_key, 0, SHA512_DIGEST_SIZE);
	memcpy(ctx->auth_key, keys.authkey, keys.authkeylen);
	ctx->auth_key_sz = keys.authkeylen;

#ifdef CCAT_SW_OP
	if (NULL == sw_tfm || IS_ERR(sw_tfm))
		return 0;

	if (c_alg == ICP_HSM_SYM_CIPHER_ALG_SM4)
		return 0;

	switch (c_mode) {
	case ICP_HSM_SYM_CIPHER_MODE_CBC:
#ifdef __LITTLE_ENDIAN
		sw_key[0] = 0x08;
		sw_key[1] = 0x00;
		sw_key[2] = 0x01;
		sw_key[3] = 0x00;
#else
		sw_key[0] = 0x00;
		sw_key[1] = 0x08;
		sw_key[2] = 0x00;
		sw_key[3] = 0x01;
#endif
		sw_key[4] = 0x00;
		sw_key[5] = 0x00;
		sw_key[6] = 0x00;
		sw_key[7] = ctx->cipher_key_sz;
		memcpy(sw_key + 8, ctx->auth_key, ctx->auth_key_sz);
		memcpy(sw_key + 8 + ctx->auth_key_sz, ctx->cipher_key, ctx->cipher_key_sz);
		sw_klen = 8 + ctx->auth_key_sz + ctx->cipher_key_sz;
		break;

	default:
		return 0;
	}
	crypto_aead_clear_flags(sw_tfm, ~0);
	if (crypto_aead_setkey(sw_tfm, sw_key, sw_klen))
		crypto_free_aead(sw_tfm);
#endif

	return 0;
}

/**
 * ccat_aead_setauthsize() - HSM aead setauthsize function.
 * @aead:     User-instantiated objects which encapsulate.
 * @authsize: auth mac size.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_aead_setauthsize(struct crypto_aead *aead, unsigned int authsize)
{
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	struct crypto_tfm *tfm = crypto_aead_tfm(aead);
	struct qat_alg_aead_ctx *ctx = crypto_tfm_ctx(tfm);

	ccat_log("[CCAT] aead setauthsize, authsize: %d\n", authsize);

	ctx->auth_tag_sz = authsize;
	if (ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_CCM) {
		ctx->msg.hdr.sym_hdr.aead_tag = authsize == 16 ? ICP_HSM_SYM_AEAD_TAG_16 :
								(authsize == 12 ? ICP_HSM_SYM_AEAD_TAG_12 :
								(authsize == 8  ? ICP_HSM_SYM_AEAD_TAG_8 :
								(authsize == 4  ? ICP_HSM_SYM_AEAD_TAG_4 :
												  ICP_HSM_SYM_AEAD_TAG_16)));
	} else
		ctx->msg.hdr.sym_hdr.aead_tag = ICP_HSM_SYM_AEAD_TAG_16;

	return 0;
}

/**
 * ccat_aead_encrypt() - HSM aead encrypt function.
 * @req: aead request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_aead_encrypt(struct aead_request *req)
{
	struct crypto_aead *aead = crypto_aead_reqtfm(req);
	struct qat_alg_aead_ctx *ctx = crypto_aead_ctx(aead);
	struct qat_crypto_request *qat_req = aead_request_ctx(req);
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_aead_req.in_list;
	struct icp_hsm_addr_list *out_list = qat_req->ccat_aead_req.out_list;
	struct icp_hsm_sym_aead_ctx_hw *ctx_hw = &(qat_req->ccat_aead_req.req_ctx.ctx_hw.aead_ctx_hw);
	u32 ivsize = crypto_aead_ivsize(aead);
	u32 aad_len = req->assoclen;
	u32 crypt_len = req->cryptlen;
	struct scatterlist *src, sg_src[2];
	struct scatterlist *dst, sg_dst[2];
	int i = 0;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] aead enc, aad len: %d, msg len: %d\n", req->assoclen, req->cryptlen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	/**
	 * When an algorithm is not supported by HSM, use a soft algorithm instead.
	 * 1. src/dst addrlist > 32
	 * 2. cipher algorithm is AES192
	 * 3. crypt len == 0
	 * 4. CCM mode taglen == 10
	 * 5. AAD length > ICP_HSM_AAD_MAX_SIZE=128
	 */
	if (sg_nents_for_len(req->src, aad_len + crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		sg_nents_for_len(req->dst, aad_len + crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		ctx->msg.hdr.sym_hdr.cipher_alg == ICP_HSM_SYM_CIPHER_ALG_AES192 ||
		req->assoclen > ICP_HSM_AAD_MAX_SIZE ||
		!crypt_len ||
		(ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_CCM && ctx->auth_tag_sz == 10)) {
#ifdef CCAT_SW_OP
		return ccat_aead_sw(req, ctx, 1);
#else
		return -ESRCH;
#endif
	}

	qat_req->ccat_aead_req.aad_len = req->assoclen;
	qat_req->ccat_aead_req.crypt_len = crypt_len;
	qat_req->ccat_aead_req.op = ICP_HSM_SYM_OP_ENC;
	qat_req->ccat_aead_req.iv_len = ivsize;

	memset(ctx_hw, 0, sizeof(struct icp_hsm_sym_aead_ctx_hw));

	memcpy(ctx_hw->cipher_key, ctx->cipher_key, ctx->cipher_key_sz);
	memcpy(ctx_hw->auth_key, ctx->auth_key, ctx->auth_key_sz);

	memset(ctx_hw->u.iv, 0, 16);
	if (ctx->is_rfc4106 || ctx->is_rfc4309 || ctx->is_rfc4543)
		memcpy(ctx_hw->u.iv, ctx->salt, ctx->salt_sz);
	if (ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_GCM) {
		memcpy(ctx_hw->u.j0 + ctx->salt_sz, req->iv, ivsize);
		ctx_hw->u.j0[15] = 1;
	} else if (ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_CCM) {
		if (!ctx->is_rfc4309) {
			while (req->iv[ivsize-1] == 0)
				ivsize--;
			ivsize--;
			memcpy(ctx_hw->u.ccm_n.nonce + ctx->salt_sz, req->iv+1, ivsize);
		} else
			memcpy(ctx_hw->u.ccm_n.nonce + ctx->salt_sz, req->iv, ivsize);
		ctx_hw->u.ccm_n.nonce_nbytes = ivsize + ctx->salt_sz;
	} else
		memcpy(ctx_hw->u.iv, req->iv, ivsize);

	/* First, decompose the source buffer into AAD & PT,
	 * and the destination buffer into AAD, CT , or
	 * the input into CT .
	 * It is expected that the input and output SGs will
	 * be valid, even if the AAD and input lengths are 0.
	 */
	scatterwalk_map_and_copy(qat_req->ccat_aead_req.aad, req->src, 0, req->assoclen, 0);
	src = scatterwalk_ffwd(sg_src, req->src, aad_len);
	dst = scatterwalk_ffwd(sg_dst, req->dst, aad_len);

	ret = ccat_aead_req_map(ctx->inst, ctx, src, dst, &qat_req->ccat_aead_req);
	if (unlikely(ret))
		return ret;

	msg = &qat_req->ccat_aead_req.req_ctx.msg;
	memset((uint8_t *)(msg), 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	msg->hdr.sym_hdr.operation = ICP_HSM_SYM_OP_ENC;
	msg->hdr.sym_hdr.total_len = req->cryptlen;
	msg->input_addr = qat_req->ccat_aead_req.in_list_dma;
	msg->input_length = qat_req->ccat_aead_req.in_num;
	msg->output_addr = qat_req->ccat_aead_req.out_list_dma;
	msg->output_length = qat_req->ccat_aead_req.out_num;
	msg->ctx_addr = qat_req->ccat_aead_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_aead_req.req_ctx.alg = ICP_HSM_TYPE_SYM_AEAD;
	qat_req->aead_req = req;
	qat_req->aead_ctx = ctx;
	qat_req->cb = ccat_aead_alg_callback;

	do {
		ret = adf_send_message(qat_req->aead_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		ccat_aead_req_unmap(ctx->inst, &qat_req->ccat_aead_req);
		return -EAGAIN;
	}


#ifdef ICP_DEBUG
	u32 in_total_len, out_total_len;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	for (i = 0; i < msg->output_length; i++)
		out_total_len += out_list[i].length;
	printk("in len: %d, out len: %d\n", in_total_len, out_total_len);
	printk("ring_id: %d\n", qat_req->aead_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%x\n", qat_req->ccat_aead_req.in_list_dma);
	printk("out_list addr: 0x%x\n", qat_req->ccat_aead_req.out_list_dma);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < msg->output_length; i++)
		printk("out[%d] addr: 0x%llx, len: %d\n", i, out_list[i].addr, out_list[i].length);
	for (i = 0; i < 64; i++)
		printk("ctx[%d]: 0x%x\n", i, ((u32 *)ctx_hw)[i]);
#endif

	return -EINPROGRESS;
}

/**
 * ccat_aead_echainiv_encrypt() - HSM aead echainiv encrypt function.
 * @req: aead request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_aead_echainiv_encrypt(struct aead_request *req)
{
	struct crypto_aead *aead = crypto_aead_reqtfm(req);
	struct qat_alg_aead_ctx *ctx = crypto_aead_ctx(aead);
	u8 *info;
	__be64 nseqno;
	u64 seqno;
	unsigned int ivsize = crypto_aead_ivsize(aead);

	ccat_log("[CCAT] aead echainiv enc\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	info = req->iv;

	memcpy(&nseqno, info + ivsize - 8, 8);
	seqno = be64_to_cpu(nseqno);
	memset(info, 0, ivsize);

	scatterwalk_map_and_copy(info, req->dst, req->assoclen, ivsize, 1);

	do {
		u64 a;

		memcpy(&a, ctx->salt + ivsize - 8, 8);

		a |= 1;
		a *= seqno;

		memcpy(info + ivsize - 8, &a, 8);
	} while ((ivsize -= 8));

	return ccat_aead_encrypt(req);
}

/**
 * ccat_aead_decrypt() - HSM aead decrypt function.
 * @req: aead request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_aead_decrypt(struct aead_request *req)
{
	struct crypto_aead *aead = crypto_aead_reqtfm(req);
	struct qat_alg_aead_ctx *ctx = crypto_aead_ctx(aead);
	struct qat_crypto_request *qat_req = aead_request_ctx(req);
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_aead_req.in_list;
	struct icp_hsm_addr_list *out_list = qat_req->ccat_aead_req.out_list;
	struct icp_hsm_sym_aead_ctx_hw *ctx_hw = &(qat_req->ccat_aead_req.req_ctx.ctx_hw.aead_ctx_hw);
	u32 ivsize = crypto_aead_ivsize(aead);
	u32 aad_len = req->assoclen;
	u32 crypt_len = req->cryptlen - aead->authsize;
	struct scatterlist *src, sg_src[2];
	struct scatterlist *dst, sg_dst[2];
	int i = 0;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] aead dec, aad len: %d, msg len: %d\n", req->assoclen, req->cryptlen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	/**
	 * When an algorithm is not supported by HSM, use a soft algorithm instead.
	 * 1. src/dst addrlist > 32
	 * 2. cipher algorithm is AES192
	 * 3. crypt len == 0
	 * 4. CCM mode taglen == 10
	 * 5. AAD length > ICP_HSM_AAD_MAX_SIZE=128
	 */
	if (sg_nents_for_len(req->src, aad_len + crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		sg_nents_for_len(req->dst, aad_len + crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		ctx->msg.hdr.sym_hdr.cipher_alg == ICP_HSM_SYM_CIPHER_ALG_AES192 ||
		req->assoclen > ICP_HSM_AAD_MAX_SIZE ||
		!crypt_len ||
		(ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_CCM && ctx->auth_tag_sz == 10)) {
#ifdef CCAT_SW_OP
		return ccat_aead_sw(req, ctx, 0);
#else
		return -ESRCH;
#endif
	}

	qat_req->ccat_aead_req.aad_len = req->assoclen;
	qat_req->ccat_aead_req.crypt_len = crypt_len;
	qat_req->ccat_aead_req.op = ICP_HSM_SYM_OP_DEC;
	qat_req->ccat_aead_req.iv_len = ivsize;

	memset(ctx_hw, 0, sizeof(struct icp_hsm_sym_aead_ctx_hw));

	memcpy(ctx_hw->cipher_key, ctx->cipher_key, ctx->cipher_key_sz);
	memcpy(ctx_hw->auth_key, ctx->auth_key, ctx->auth_key_sz);

	memset(ctx_hw->u.iv, 0, 16);
	if (ctx->is_rfc4106 || ctx->is_rfc4309 || ctx->is_rfc4543)
		memcpy(ctx_hw->u.iv, ctx->salt, ctx->salt_sz);
	if (ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_GCM) {
		memcpy(ctx_hw->u.j0 + ctx->salt_sz, req->iv, ivsize);
		ctx_hw->u.j0[15] = 1;
	} else if (ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_CCM) {
		if (!ctx->is_rfc4309) {
			while (req->iv[ivsize-1] == 0)
				ivsize--;
			ivsize--;
			memcpy(ctx_hw->u.ccm_n.nonce + ctx->salt_sz, req->iv+1, ivsize);
		} else
			memcpy(ctx_hw->u.ccm_n.nonce + ctx->salt_sz, req->iv, ivsize);
		ctx_hw->u.ccm_n.nonce_nbytes = ivsize + ctx->salt_sz;
	} else
		memcpy(ctx_hw->u.iv, req->iv, ivsize);

	/* First, decompose the source buffer into AAD & PT,
	 * and the destination buffer into AAD, CT , or
	 * the input into CT .
	 * It is expected that the input and output SGs will
	 * be valid, even if the AAD and input lengths are 0.
	 */
	scatterwalk_map_and_copy(qat_req->ccat_aead_req.aad, req->src, 0, req->assoclen, 0);
	src = scatterwalk_ffwd(sg_src, req->src, aad_len);
	dst = scatterwalk_ffwd(sg_dst, req->dst, aad_len);

	ret = ccat_aead_req_map(ctx->inst, ctx, src, dst, &qat_req->ccat_aead_req);
	if (unlikely(ret))
		return ret;

	msg = &qat_req->ccat_aead_req.req_ctx.msg;
	memset((uint8_t *)(msg), 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	msg->hdr.sym_hdr.operation = ICP_HSM_SYM_OP_DEC;
	msg->hdr.sym_hdr.total_len = req->cryptlen - aead->authsize;
	msg->input_addr = qat_req->ccat_aead_req.in_list_dma;
	msg->input_length = qat_req->ccat_aead_req.in_num;
	msg->output_addr = qat_req->ccat_aead_req.out_list_dma;
	msg->output_length = qat_req->ccat_aead_req.out_num;
	msg->ctx_addr = qat_req->ccat_aead_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_aead_req.req_ctx.alg = ICP_HSM_TYPE_SYM_AEAD;
	qat_req->aead_req = req;
	qat_req->aead_ctx = ctx;
	qat_req->cb = ccat_aead_alg_callback;

	do {
		ret = adf_send_message(qat_req->aead_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		ccat_aead_req_unmap(ctx->inst, &qat_req->ccat_aead_req);
		return -EAGAIN;
	}

#ifdef ICP_DEBUG
	u32 in_total_len, out_total_len;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	for (i = 0; i < msg->output_length; i++)
		out_total_len += out_list[i].length;
	printk("in len: %d, out len: %d\n", in_total_len, out_total_len);
	printk("ring_id: %d\n", qat_req->aead_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%x\n", qat_req->ccat_aead_req.in_list_dma);
	printk("out_list addr: 0x%x\n", qat_req->ccat_aead_req.out_list_dma);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < msg->output_length; i++)
		printk("out[%d] addr: 0x%llx, len: %d\n", i, out_list[i].addr, out_list[i].length);
	for (i = 0; i < 64; i++)
		printk("ctx[%d]: 0x%x\n", i, ((u32 *)ctx_hw)[i]);
#endif

	return -EINPROGRESS;
}

/**
 * ccat_aead_echainiv_decrypt() - HSM aead echainiv decrypt function.
 * @req: aead request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_aead_echainiv_decrypt(struct aead_request *req)
{
	struct crypto_aead *aead = crypto_aead_reqtfm(req);
	unsigned int ivsize = crypto_aead_ivsize(aead);

	ccat_log("[CCAT] aead echainiv dec\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	scatterwalk_map_and_copy(req->iv, req->src, req->assoclen, ivsize, 0);

	aead_request_set_crypt(req, req->src, req->dst,
			       req->cryptlen - ivsize, req->iv);
	aead_request_set_ad(req, req->assoclen + ivsize);

	return ccat_aead_decrypt(req);
}

/**
 * ccat_aead_init() - HSM aead init function.
 * @aead: User-instantiated objects which encapsulate.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_aead_init(struct crypto_aead *aead)
{
	struct crypto_tfm *tfm = crypto_aead_tfm(aead);
	struct qat_alg_aead_ctx *ctx = crypto_tfm_ctx(tfm);
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();
	struct ccat_alg_template *tmpl =
		container_of(crypto_aead_alg(aead), struct ccat_alg_template, alg.aead);
#ifdef CCAT_SW_OP
	struct crypto_aead *sw_tfm = NULL;
	char aead_name[CRYPTO_MAX_ALG_NAME];
	u32 c_alg, c_mode, a_alg;
#endif

	ccat_log("[CCAT] aead init\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return -EINVAL;

	ctx->inst = inst;
	ctx->msg.hdr.sym_hdr.service_type = ICP_HSM_TYPE_AEAD;
	ctx->msg.hdr.sym_hdr.first = 1;
	ctx->msg.hdr.sym_hdr.last = 1;
	ctx->msg.hdr.sym_hdr.addr_list = 1;
	ctx->msg.hdr.sym_hdr.init = ICP_HSM_SYM_INIT;
	ctx->msg.hdr.sym_hdr.finish = ICP_HSM_SYM_FINISH;
	ctx->msg.hdr.sym_hdr.aead_mode = tmpl->alg_info.aead_info.aead_mode;
	ctx->msg.hdr.sym_hdr.aead_tag = tmpl->alg_info.aead_info.aead_tag;
	ctx->msg.hdr.sym_hdr.cipher_alg = tmpl->alg_info.aead_info.cipher_alg;
	ctx->msg.hdr.sym_hdr.cipher_mode = tmpl->alg_info.aead_info.cipher_mode;
	ctx->msg.hdr.sym_hdr.auth_mode = tmpl->alg_info.aead_info.auth_mode;
	ctx->msg.hdr.sym_hdr.hash_alg = tmpl->alg_info.aead_info.hash_alg;
	ctx->msg.hdr.sym_hdr.key_mode = ICP_HSM_KEY_MODE_PLAIN;
	ctx->salt_sz = 0;
	ctx->cipher_key_sz = 0;
	ctx->auth_key_sz = 0;

	crypto_aead_set_reqsize(aead, ALIGN(sizeof(struct qat_crypto_request), L1_CACHE_BYTES));

#ifdef CCAT_SW_OP
	c_alg = ctx->msg.hdr.sym_hdr.cipher_alg;
	c_mode = ctx->msg.hdr.sym_hdr.cipher_mode;
	a_alg = ctx->msg.hdr.sym_hdr.hash_alg;

	if (c_alg == ICP_HSM_SYM_CIPHER_ALG_SM4)
		return 0;

	switch (c_mode) {
	case ICP_HSM_SYM_CIPHER_MODE_GCM:
		if (tmpl->is_rfc4106) {
			snprintf(aead_name, CRYPTO_MAX_ALG_NAME, "%s",
				"rfc4106(gcm_base(ctr(aes-generic),ghash-generic))");
		} else {
			snprintf(aead_name, CRYPTO_MAX_ALG_NAME, "%s",
				"gcm_base(ctr(aes-generic),ghash-generic)");
		}
		break;
	case ICP_HSM_SYM_CIPHER_MODE_CCM:
		if (tmpl->is_rfc4309) {
			snprintf(aead_name, CRYPTO_MAX_ALG_NAME, "%s",
				"rfc4309(ccm_base(ctr(aes-generic),cbcmac(aes-generic)))");
		} else {
			snprintf(aead_name, CRYPTO_MAX_ALG_NAME, "%s",
				"ccm_base(ctr(aes-generic),cbcmac(aes-generic))");
		}
		break;
	case ICP_HSM_SYM_CIPHER_MODE_CBC:
		if (a_alg == ICP_HSM_SYM_HASH_ALG_SHA1)
			snprintf(aead_name, CRYPTO_MAX_ALG_NAME, "%s",
				"authenc(hmac(sha1-generic),cbc(aes-generic))");
		else if (a_alg == ICP_HSM_SYM_HASH_ALG_SHA256)
			snprintf(aead_name, CRYPTO_MAX_ALG_NAME, "%s",
				"authenc(hmac(sha256-generic),cbc(aes-generic))");
		else
			return 0;
		break;

	default:
		return 0;
	}
	ccat_log("[CCAT] aead sw alg: %s\n", aead_name);
	sw_tfm = (void *)crypto_alloc_aead(aead_name, 0, 0);
	ctx->sw_tfm = (void *)sw_tfm;
#endif

	return 0;
}

/**
 * ccat_aead_echainiv_init() - HSM aead echainiv init function.
 * @aead: User-instantiated objects which encapsulate.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_aead_echainiv_init(struct crypto_aead *aead)
{
	struct qat_alg_aead_ctx *ctx = crypto_aead_ctx(aead);
	int err;

	err = crypto_get_default_rng();
	if (err)
		return err;

	ctx->salt_sz = crypto_aead_ivsize(aead);
	err = crypto_rng_get_bytes(crypto_default_rng, ctx->salt,
				   crypto_aead_ivsize(aead));
	crypto_put_default_rng();

	ctx->is_echainiv = true;

	return ccat_aead_init(aead);
}

/**
 * ccat_aead_exit() - HSM aead exit function.
 * @aead: User-instantiated objects which encapsulate.
 */
static void ccat_aead_exit(struct crypto_aead *aead)
{
	struct crypto_tfm *tfm = crypto_aead_tfm(aead);
	struct qat_alg_aead_ctx *ctx = crypto_tfm_ctx(tfm);
#ifdef CCAT_SW_OP
	struct crypto_aead *sw_tfm = (struct crypto_aead *)(ctx->sw_tfm);
#endif

	ccat_log("[CCAT] aead exit\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

#ifdef CCAT_SW_OP
	if (sw_tfm)
		crypto_free_aead(sw_tfm);
#endif
}
#endif /* CCAT_AEAD */

#ifdef CCAT_CIPHER
static int ccat_skcipher_req_unmap(struct qat_crypto_instance *inst,
				   struct ccat_cipher_request *qat_req)
{
	u32 i;
	struct icp_hsm_addr_list *in_list = qat_req->in_list;
	struct icp_hsm_addr_list *out_list = qat_req->out_list;

	if(qat_req->is_same_addr == false) {
		for (i = 0; i < qat_req->out_num; i++) {
			if (!dma_mapping_error(&GET_DEV(inst->accel_dev), out_list[i].addr))
				dma_unmap_single(&GET_DEV(inst->accel_dev), out_list[i].addr, out_list[i].length,
								DMA_BIDIRECTIONAL);
		}
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->out_list_dma)) {
			dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->out_list_dma,
					sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
					DMA_TO_DEVICE);
		}
	}

	for (i = 0; i < qat_req->in_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), in_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev), in_list[i].addr, in_list[i].length,
							DMA_BIDIRECTIONAL);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->in_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
	}

	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
				sizeof(struct icp_hsm_sym_cipher_ctx_hw),
				DMA_BIDIRECTIONAL);
	}
	return 0;
}

static int ccat_skcipher_req_map(struct qat_crypto_instance *inst,
			       struct scatterlist *sglin,
			       struct scatterlist *sglout,
				   struct ccat_cipher_request *qat_req)
{
	u32 nr_src, nr_dst, src_num, dst_num, clen, i;
	struct scatterlist *sg = NULL;
	struct icp_hsm_addr_list *in_list = qat_req->in_list;
	struct icp_hsm_addr_list *out_list = qat_req->out_list;
	u32 crypt_len = qat_req->crypt_len;

	if (unlikely(!sg_nents(sglin)))
		return -EINVAL;


	qat_req->req_ctx.ctx_hw_dma = dma_map_single(&GET_DEV(inst->accel_dev),
							(void *)&qat_req->req_ctx.ctx_hw,
							sizeof(struct icp_hsm_sym_cipher_ctx_hw),
							DMA_BIDIRECTIONAL);
	if (dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma))
		goto err_ctx;

	src_num = 0;
	dst_num = 0;
	if (sglin == sglout) {
		nr_src = dma_map_sg(&GET_DEV(inst->accel_dev), sglin, sg_nents(sglin), DMA_BIDIRECTIONAL);

		if (unlikely(!nr_src))
			goto err_in;

		nr_src = sg_nents(sglin);
		for_each_sg(sglin, sg, nr_src, i) {
			if ((!sg->length) || (!crypt_len))
				continue;

			if (crypt_len < sg->length)
				clen = crypt_len;
			else
				clen = sg->length;

			in_list[src_num].addr = sg_dma_address(sg);
			in_list[src_num].length = clen;
			out_list[src_num].addr = in_list[src_num].addr;
			out_list[src_num].length = in_list[src_num].length;
			crypt_len -= clen;
			src_num++;
		}
		qat_req->in_num = qat_req->out_num = src_num;

		qat_req->in_list_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)in_list,
						sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
						DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)))
			goto err_in;

		qat_req->out_list_dma = qat_req->in_list_dma;
		qat_req->is_same_addr = true;
	} else {
		nr_src = dma_map_sg(&GET_DEV(inst->accel_dev), sglin, sg_nents(sglin), DMA_BIDIRECTIONAL);

		if (unlikely(!nr_src))
			goto err_in;

		for_each_sg(sglin, sg, nr_src, i) {
			if ((!sg->length) || (!crypt_len))
				continue;

			if (crypt_len < sg->length)
				clen = crypt_len;
			else
				clen = sg->length;

			in_list[src_num].addr = sg_dma_address(sg);
			in_list[src_num].length = clen;
			crypt_len -= clen;
			src_num++;
		}
		qat_req->in_num = src_num;

		crypt_len = qat_req->crypt_len;
		nr_dst = dma_map_sg(&GET_DEV(inst->accel_dev), sglout, sg_nents(sglout), DMA_BIDIRECTIONAL);
		if (unlikely(!nr_dst))
			goto err_out;

		for_each_sg(sglout, sg, nr_dst, i) {
			if ((!sg->length) || (!crypt_len))
				continue;

			if (crypt_len < sg->length)
				clen = crypt_len;
			else
				clen = sg->length;

			out_list[dst_num].addr = sg_dma_address(sg);
			out_list[dst_num].length = clen;
			crypt_len -= clen;
			dst_num++;
		}
		qat_req->out_num = dst_num;

		qat_req->in_list_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)in_list,
						sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
						DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)))
			goto err_out;

		qat_req->out_list_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)out_list,
						sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
						DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->out_list_dma)))
			goto err_out;

		qat_req->is_same_addr = false;
	}

	return 0;

err_out:
	dst_num = sg_nents(sglout);
	for (i = 0; i < dst_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), out_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev), out_list[i].addr, out_list[i].length,
							DMA_BIDIRECTIONAL);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->out_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->out_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
	}

err_in:
	src_num = sg_nents(sglin);
	for (i = 0; i < src_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), in_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev), in_list[i].addr, in_list[i].length,
							DMA_BIDIRECTIONAL);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->in_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
	}

err_ctx:
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
				sizeof(struct icp_hsm_sym_cipher_ctx_hw),
				DMA_BIDIRECTIONAL);
	}

	dev_err(&GET_DEV(inst->accel_dev), "ERR[CIPHER]# Failed to map buf of HSM for dma \n");
	return -ENOMEM;
}

/**
 * ccat_skcipher_alg_callback() - HSM cipher callback function.
 * @qat_resp: HSM response msg.
 * @qat_req: QAT request.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static void ccat_skcipher_alg_callback(struct icp_qat_fw_la_resp *qat_resp,
	struct qat_crypto_request *qat_req)
{
	struct skcipher_request *req = qat_req->skcipher_req;
	struct crypto_skcipher *acipher = crypto_skcipher_reqtfm(req);
	struct qat_alg_skcipher_ctx *ctx = qat_req->skcipher_ctx;
	int iv_len = 0;
	int ret = 0;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	ccat_log("[CCAT] cipher result\n");

	iv_len = crypto_skcipher_ivsize(acipher);

	ccat_skcipher_req_unmap(ctx->inst, &qat_req->ccat_cipher_req);

	 if (!ctx->is_rfc3686)
	 	memcpy(req->iv,
	 			qat_req->ccat_cipher_req.req_ctx.ctx_hw.cipher_ctx_hw.u2.iv,
				iv_len);


	req->base.complete(&req->base, ret);
}

/**
 * ccat_skcipher_sw() - Cipher Soft algorithm.
 * @areq: skcipher request data.
 * @ctx:  ccat sym context address.
 * @enc:  encrypt flag.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_skcipher_sw(struct skcipher_request *areq,
								 struct qat_alg_skcipher_ctx *ctx, int enc)
{
	struct crypto_skcipher *tfm = (struct crypto_skcipher *)(ctx->sw_tfm);
	struct skcipher_request *req;
	struct crypto_wait wait;
	int ret;

	if (NULL == tfm || IS_ERR(tfm))
		return -ENOMEM;

	crypto_init_wait(&wait);

	req = skcipher_request_alloc(tfm, GFP_ATOMIC);
	if (!req)
		return -ENOMEM;

	skcipher_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
						crypto_req_done, &wait);

	skcipher_request_set_crypt(req, areq->src, areq->dst, areq->cryptlen, areq->iv);
	ret = crypto_wait_req(enc ? crypto_skcipher_encrypt(req) :
					crypto_skcipher_decrypt(req), &wait);

	skcipher_request_free(req);

	return ret;
}

/**
 * ccat_skcipher_setkey() - HSM cipher setkey function.
 * @acipher: User-instantiated objects which encapsulate.
 * @key:     key address.
 * @keylen:  key length.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_skcipher_setkey(struct crypto_skcipher *acipher,
								const u8 *key, unsigned int keylen)
{
	struct crypto_tfm *tfm = crypto_skcipher_tfm(acipher);
	struct qat_alg_skcipher_ctx *ctx = crypto_skcipher_ctx(acipher);
	u32 tmp[DES_EXPKEY_WORDS];
	int ret = 0;
#ifdef CCAT_SW_OP
	struct crypto_skcipher *sw_tfm = (struct crypto_skcipher *)(ctx->sw_tfm);
	u8 sw_key[36];
	u32 sw_klen;
#endif

	ccat_log("[CCAT] cipher setkey, alg: %d, keylen: %d\n", ctx->msg.hdr.sym_hdr.cipher_alg, keylen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	switch (ctx->msg.hdr.sym_hdr.cipher_alg) {
	case ICP_HSM_SYM_CIPHER_ALG_DES:
		if (keylen != DES_KEY_SIZE) {
			crypto_skcipher_set_flags(acipher, CRYPTO_TFM_REQ_MASK);
			return -EINVAL;
		}
		ret = verify_skcipher_des_key(acipher, key);
		if (ret)
			return ret;
		break;
	case ICP_HSM_SYM_CIPHER_ALG_3DES:
		if (keylen != DES3_EDE_KEY_SIZE) {
			crypto_skcipher_set_flags(acipher, CRYPTO_TFM_REQ_MASK);
			return -EINVAL;
		}
		break;
	case ICP_HSM_SYM_CIPHER_ALG_AES128:
	case ICP_HSM_SYM_CIPHER_ALG_AES192:
	case ICP_HSM_SYM_CIPHER_ALG_AES256:
		if (keylen == AES_KEYSIZE_128) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128;
		} else if (keylen == AES_KEYSIZE_256) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES256;
		} else if (keylen == AES_KEYSIZE_192) {
			ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES192;
		} else {
			crypto_skcipher_set_flags(acipher, CRYPTO_TFM_REQ_MASK);
			return -EINVAL;
		}
		break;
	case ICP_HSM_SYM_CIPHER_ALG_SM4:
		break;
	default:
		return -EINVAL;
	}

	memcpy(ctx->cipher_key, key, keylen);
	ctx->cipher_key_sz = keylen;

#ifdef CCAT_SW_OP
	if (NULL == sw_tfm || IS_ERR(sw_tfm))
		return 0;

	memcpy(sw_key, ctx->cipher_key, ctx->cipher_key_sz);
	sw_klen = ctx->cipher_key_sz;
	if (ctx->is_rfc3686) {
		memcpy(sw_key + ctx->cipher_key_sz, ctx->nonce, CTR_RFC3686_NONCE_SIZE);
		sw_klen += CTR_RFC3686_NONCE_SIZE;
	}

	crypto_skcipher_clear_flags(sw_tfm, ~0);
	if (crypto_skcipher_setkey(sw_tfm, sw_key, sw_klen))
		crypto_free_skcipher(sw_tfm);
#endif

	return 0;
}

/**
 * ccat_skcipher_rfc3686_setkey() - HSM cipher(rfc3686) setkey function.
 * @acipher: User-instantiated objects which encapsulate.
 * @key:     key address.
 * @keylen:  key length.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_skcipher_rfc3686_setkey(struct crypto_skcipher *acipher,
										const u8 *key, unsigned int keylen)
{
	struct qat_alg_skcipher_ctx *ctx = crypto_skcipher_ctx(acipher);

	ccat_log("[CCAT] rfc3686 cipher setkey, alg: %d, keylen: %d\n",
			ctx->msg.hdr.sym_hdr.cipher_alg, keylen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (keylen < CTR_RFC3686_NONCE_SIZE)
		return -EINVAL;

	ctx->nonce_sz = CTR_RFC3686_NONCE_SIZE;
	memcpy(ctx->nonce, key + keylen - CTR_RFC3686_NONCE_SIZE,
			CTR_RFC3686_NONCE_SIZE);
	keylen -= CTR_RFC3686_NONCE_SIZE;
	ctx->is_rfc3686 = true;

	return ccat_skcipher_setkey(acipher, key, keylen);
}

/**
 * ccat_skcipher_encrypt() - HSM cipher encrypt function.
 * @req: cipher request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_skcipher_encrypt(struct skcipher_request *req)
{
	struct crypto_skcipher *acipher = crypto_skcipher_reqtfm(req);
	struct crypto_tfm *tfm = crypto_skcipher_tfm(acipher);
	struct qat_alg_skcipher_ctx *ctx = crypto_tfm_ctx(tfm);
	struct qat_crypto_request *qat_req = skcipher_request_ctx(req);
	u32 ivsize = crypto_skcipher_ivsize(acipher);
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_cipher_req.in_list;
	struct icp_hsm_addr_list *out_list = qat_req->ccat_cipher_req.out_list;
	struct icp_hsm_sym_cipher_ctx_hw *ctx_hw =
				&(qat_req->ccat_cipher_req.req_ctx.ctx_hw.cipher_ctx_hw);
	u32 crypt_len = req->cryptlen;
	int ret = 0;
	int ctr = 0;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	ccat_log("[CCAT] cipher enc, len: %d\n", req->cryptlen);

	/**
	 * When an algorithm is not supported by HSM, use a soft algorithm instead.
	 * 1. src/dst addrlist > 32
	 * 2. cipher algorithm is AES192
	 */
	if (sg_nents_for_len(req->src, crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		sg_nents_for_len(req->dst, crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		ctx->msg.hdr.sym_hdr.cipher_alg == ICP_HSM_SYM_CIPHER_ALG_AES192) {
#ifdef CCAT_SW_OP
		return ccat_skcipher_sw(req, ctx, 1);
#else
		return -ESRCH;
#endif
	}

	qat_req->ccat_cipher_req.crypt_len = crypt_len;
	memset(ctx_hw, 0, sizeof(struct icp_hsm_sym_cipher_ctx_hw));

	if (ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_CTR)
		ctx_hw->u1.step_size = 1;

	memcpy(ctx_hw->key, ctx->cipher_key, ctx->cipher_key_sz);

	if (ctx->msg.hdr.sym_hdr.cipher_mode != ICP_HSM_SYM_CIPHER_MODE_ECB) {
		memset(ctx_hw->u2.iv, 0, 16);
		if (ctx->is_rfc3686) {
			memcpy(ctx_hw->u2.iv, ctx->nonce, ctx->nonce_sz);
			ctx_hw->u2.iv[15] = 1;
		}
		memcpy(ctx_hw->u2.iv + ctx->nonce_sz, req->iv, ivsize);
	}

	ret = ccat_skcipher_req_map(ctx->inst, req->src, req->dst, &qat_req->ccat_cipher_req);
	if (unlikely(ret))
		return ret;

	msg = &qat_req->ccat_cipher_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	msg->hdr.sym_hdr.operation = ICP_HSM_SYM_OP_ENC;
	msg->input_addr = qat_req->ccat_cipher_req.in_list_dma;
	msg->input_length = qat_req->ccat_cipher_req.in_num;
	msg->output_addr = qat_req->ccat_cipher_req.out_list_dma;
	msg->output_length = qat_req->ccat_cipher_req.out_num;
	msg->ctx_addr = qat_req->ccat_cipher_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_cipher_req.req_ctx.alg = ICP_HSM_TYPE_SYM_CIPHER;
	qat_req->skcipher_req = req;
	qat_req->skcipher_ctx = ctx;
	qat_req->cb = ccat_skcipher_alg_callback;

	do {
		ret = adf_send_message(qat_req->skcipher_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		ccat_skcipher_req_unmap(ctx->inst, &qat_req->ccat_cipher_req);
		return -EAGAIN;
	}

#ifdef ICP_DEBUG
	int i;
	u32 in_total_len = 0, out_total_len = 0;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	for (i = 0; i < msg->output_length; i++)
		out_total_len += out_list[i].length;
	printk("in len: %d, out len: %d\n", in_total_len, out_total_len);
	printk("ring_id: %d\n", qat_req->skcipher_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%llx\n", qat_req->ccat_cipher_req.in_list_dma);
	printk("out_list addr: 0x%llx\n", qat_req->ccat_cipher_req.out_list_dma);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < msg->output_length; i++)
		printk("out[%d] addr: 0x%llx, len: %d\n", i, out_list[i].addr, out_list[i].length);
	for (i = 0; i < 20; i++)
		printk("ctx[%d]: 0x%x\n", i, ((u32 *)(ctx_hw))[i]);
#endif

	return -EINPROGRESS;
}

/**
 * ccat_skcipher_decrypt() - HSM cipher decrypt function.
 * @req: cipher request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_skcipher_decrypt(struct skcipher_request *req)
{
	struct crypto_skcipher *acipher = crypto_skcipher_reqtfm(req);
	struct crypto_tfm *tfm = crypto_skcipher_tfm(acipher);
	struct qat_alg_skcipher_ctx *ctx = crypto_tfm_ctx(tfm);
	struct qat_crypto_request *qat_req = skcipher_request_ctx(req);
	int ivsize = crypto_skcipher_ivsize(acipher);
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_cipher_req.in_list;
	struct icp_hsm_addr_list *out_list = qat_req->ccat_cipher_req.out_list;
	struct icp_hsm_sym_cipher_ctx_hw *ctx_hw =
				&(qat_req->ccat_cipher_req.req_ctx.ctx_hw.cipher_ctx_hw);
	u32 crypt_len = req->cryptlen;
	int ret = 0, i;
	int ctr = 0;

	ccat_log("[CCAT] cipher dec, len: %d\n", req->cryptlen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	/**
	 * When an algorithm is not supported by TIH, use a soft algorithm instead.
	 * 1. src/dst addrlist > 32
	 * 2. cipher algorithm is AES192
	 */
	if (sg_nents_for_len(req->src, crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		sg_nents_for_len(req->dst, crypt_len) > ICP_HSM_MAX_ADDR_LIST ||
		ctx->msg.hdr.sym_hdr.cipher_alg == ICP_HSM_SYM_CIPHER_ALG_AES192) {
#ifdef CCAT_SW_OP
		return ccat_skcipher_sw(req, ctx, 0);
#else
		return -ESRCH;
#endif
	}

	qat_req->ccat_cipher_req.crypt_len = crypt_len;
	memset(ctx_hw, 0, sizeof(struct icp_hsm_sym_cipher_ctx_hw));

	if (ctx->msg.hdr.sym_hdr.cipher_mode == ICP_HSM_SYM_CIPHER_MODE_CTR)
		ctx_hw->u1.step_size = 1;

	memcpy(ctx_hw->key, ctx->cipher_key, ctx->cipher_key_sz);

	if (ctx->msg.hdr.sym_hdr.cipher_mode != ICP_HSM_SYM_CIPHER_MODE_ECB) {
		memset(ctx_hw->u2.iv, 0, 16);
		if (ctx->is_rfc3686) {
			memcpy(ctx_hw->u2.iv, ctx->nonce, ctx->nonce_sz);
			ctx_hw->u2.iv[15] = 1;
		}
		memcpy(ctx_hw->u2.iv + ctx->nonce_sz, req->iv, ivsize);
	}

	ret = ccat_skcipher_req_map(ctx->inst, req->src, req->dst, &qat_req->ccat_cipher_req);
	if (unlikely(ret))
		return ret;

	msg = &qat_req->ccat_cipher_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	msg->hdr.sym_hdr.operation = ICP_HSM_SYM_OP_DEC;
	msg->input_addr = qat_req->ccat_cipher_req.in_list_dma;
	msg->input_length = qat_req->ccat_cipher_req.in_num;
	msg->output_addr = qat_req->ccat_cipher_req.out_list_dma;
	msg->output_length = qat_req->ccat_cipher_req.out_num;
	msg->ctx_addr = qat_req->ccat_cipher_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_cipher_req.req_ctx.alg = ICP_HSM_TYPE_SYM_CIPHER;
	qat_req->skcipher_req = req;
	qat_req->skcipher_ctx = ctx;
	qat_req->cb = ccat_skcipher_alg_callback;

	do {
		ret = adf_send_message(qat_req->skcipher_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		ccat_skcipher_req_unmap(ctx->inst, &qat_req->ccat_cipher_req);
		return -EAGAIN;
	}

#ifdef ICP_DEBUG
	u32 in_total_len = 0, out_total_len = 0;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	for (i = 0; i < msg->output_length; i++)
		out_total_len += out_list[i].length;
	printk("in len: %d, out len: %d\n", in_total_len, out_total_len);
	printk("ring_id: %d\n", qat_req->skcipher_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%llx\n", qat_req->ccat_cipher_req.in_list_dma);
	printk("out_list addr: 0x%llx\n", qat_req->ccat_cipher_req.out_list_dma);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < msg->output_length; i++)
		printk("out[%d] addr: 0x%llx, len: %d\n", i, out_list[i].addr, out_list[i].length);
	for (i = 0; i < 20; i++)
		printk("ctx[%d]: 0x%x\n", i, ((u32 *)(ctx_hw))[i]);
#endif

	return -EINPROGRESS;
}

/**
 * ccat_skcipher_init() - HSM cipher init function.
 * @tfm: User-instantiated objects which encapsulate.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_skcipher_init(struct crypto_skcipher *acipher)
{
	struct qat_alg_skcipher_ctx *ctx = crypto_skcipher_ctx(acipher);
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();
	struct ccat_alg_template *tmpl =
		container_of(crypto_skcipher_alg(acipher), struct ccat_alg_template, alg.acipher);
#ifdef CCAT_SW_OP
	struct crypto_skcipher *sw_tfm = NULL;
	char skcipher_name[CRYPTO_MAX_ALG_NAME];
	char cmp_name[CRYPTO_MAX_ALG_NAME];
	u32 alg, mode;
#endif

	ccat_log("[CCAT] acipher init\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return -EINVAL;

	ctx->inst = inst;
	ctx->msg.hdr.sym_hdr.service_type = ICP_HSM_TYPE_CIPHER;
	ctx->msg.hdr.sym_hdr.first = 1;
	ctx->msg.hdr.sym_hdr.last = 1;
	ctx->msg.hdr.sym_hdr.addr_list = 1;
	ctx->msg.hdr.sym_hdr.init = ICP_HSM_SYM_INIT;
	ctx->msg.hdr.sym_hdr.finish = ICP_HSM_SYM_FINISH;
	ctx->msg.hdr.sym_hdr.cipher_alg = tmpl->alg_info.cipher_info.alg;
	ctx->msg.hdr.sym_hdr.cipher_mode = tmpl->alg_info.cipher_info.mode;
	ctx->msg.hdr.sym_hdr.key_mode = ICP_HSM_KEY_MODE_PLAIN;
	ctx->nonce_sz = 0;

	crypto_skcipher_set_reqsize(acipher, ALIGN(sizeof(struct qat_crypto_request), L1_CACHE_BYTES));

#ifdef CCAT_SW_OP
	alg = ctx->msg.hdr.sym_hdr.cipher_alg;
	mode = ctx->msg.hdr.sym_hdr.cipher_mode;

	switch (alg) {
	case ICP_HSM_SYM_CIPHER_ALG_AES128:
	case ICP_HSM_SYM_CIPHER_ALG_AES192:
	case ICP_HSM_SYM_CIPHER_ALG_AES256:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "%s", "aes-generic");
		break;
	case ICP_HSM_SYM_CIPHER_ALG_DES:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "%s", "des-generic");
		break;
	case ICP_HSM_SYM_CIPHER_ALG_3DES:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "%s", "des3_ede-generic");
		break;
	case ICP_HSM_SYM_CIPHER_ALG_SM4:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "%s", "sm4-generic");
		break;

	default:
		return 0;
	}

	memcpy(cmp_name, skcipher_name, CRYPTO_MAX_ALG_NAME);
	switch (mode) {
	case ICP_HSM_SYM_CIPHER_MODE_ECB:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "ecb(%s)", cmp_name);
		break;
	case ICP_HSM_SYM_CIPHER_MODE_CBC:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "cbc(%s)", cmp_name);
		break;
	case ICP_HSM_SYM_CIPHER_MODE_CFB:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "cfb(%s)", cmp_name);
		break;
	case ICP_HSM_SYM_CIPHER_MODE_OFB:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "ofb(%s)", cmp_name);
		break;
	case ICP_HSM_SYM_CIPHER_MODE_CTR:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "ctr(%s)", cmp_name);
		break;
	case ICP_HSM_SYM_CIPHER_MODE_XTS:
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "xts(%s)", cmp_name);
		break;

	default:
		return 0;
	}
	if (tmpl->is_rfc3686) {
		memcpy(cmp_name, skcipher_name, CRYPTO_MAX_ALG_NAME);
		snprintf(skcipher_name, CRYPTO_MAX_ALG_NAME, "rfc3686(%s)", cmp_name);
	}
	ccat_log("[CCAT] skcipher sw alg: %s\n", skcipher_name);
	sw_tfm = (void *)crypto_alloc_skcipher(skcipher_name, 0, 0);
	ctx->sw_tfm = (void *)sw_tfm;
#endif

	return 0;
}

/**
 * ccat_skcipher_exit() - HSM cipher exit function.
 * @tfm: User-instantiated objects which encapsulate.
 */
static void ccat_skcipher_exit(struct crypto_skcipher *acipher)
{
	struct qat_alg_skcipher_ctx *ctx = crypto_skcipher_ctx(acipher);

#ifdef CCAT_SW_OP
	struct crypto_skcipher *sw_tfm = (struct crypto_skcipher *)(ctx->sw_tfm);
#endif

	ccat_log("[CCAT] acipher exit\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

#ifdef CCAT_SW_OP
	if (sw_tfm)
		crypto_free_skcipher(sw_tfm);
#endif
}

#endif /* CCAT_CIPHER */

#ifdef CCAT_HASH
static int ccat_ahash_req_reset(struct qat_alg_ahash_ctx *ctx,
								struct ccat_ahash_request *qat_req)
{

	if(!ctx || !qat_req) {
		printk(KERN_ERR "ERR[HSM-HASH]: CTX or REQ is NULL when reset.\n");
		return -EINVAL;
	}

	qat_req->cache_sz = 0;
	qat_req->cache_dma_sz = 0;
	qat_req->cache_dma = 0;
	qat_req->total_len = 0;
	qat_req->is_init = true;
	qat_req->is_finish = false;
	if (ctx->msg.hdr.sym_hdr.service_type == ICP_HSM_TYPE_AUTH) {
		qat_req->is_auth = true;
		qat_req->req_ctx.alg = ICP_HSM_TYPE_SYM_AUTH;
		memset(&(qat_req->req_ctx.ctx_hw.auth_ctx_hw), 0,
				sizeof(struct icp_hsm_sym_auth_ctx_hw));
		memcpy(qat_req->req_ctx.ctx_hw.auth_ctx_hw.key, ctx->auth_key,
				ctx->auth_key_sz);
	} else {
		qat_req->is_auth = false;
		qat_req->req_ctx.alg = ICP_HSM_TYPE_SYM_HASH;
		memset(&(qat_req->req_ctx.ctx_hw.hash_ctx_hw), 0,
				sizeof(struct icp_hsm_sym_hash_ctx_hw));
	}

	return 0;
}

static int ccat_ahash_req_unmap(struct qat_crypto_instance *inst,
				   struct ccat_ahash_request *qat_req)
{
	u32 i;
	for (i = 0; i < qat_req->in_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev),
							qat_req->in_list[i].addr, qat_req->in_list[i].length,
							DMA_TO_DEVICE);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->in_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
	}

	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->cache_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->cache_dma,
						qat_req->block_sz,
						DMA_TO_DEVICE);
	}

	if (qat_req->is_auth) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
			dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
					sizeof(struct icp_hsm_sym_auth_ctx_hw),
					DMA_BIDIRECTIONAL);
		}
	} else {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
			dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
					sizeof(struct icp_hsm_sym_hash_ctx_hw),
					DMA_BIDIRECTIONAL);
		}
	}

	return 0;
}

static int ccat_ahash_req_map(struct qat_crypto_instance *inst,
			       struct scatterlist *sglin,
				   struct ccat_ahash_request *qat_req,
				   bool is_final)
{
	u32 block_sz, req_nbytes, i, n, nr_src, sg_len, src_num, index;
	bool is_auth;
	struct scatterlist *sg = NULL;
	int old_cache_bytes = 0;
	u32 hash_len = 0;
	u32 hash_rem = 0;
	u32 copy_len = 0;

	i = 0;
	block_sz = qat_req->block_sz;
	req_nbytes = qat_req->req_nbytes;
	is_auth = qat_req->is_auth;


	/* Map ctx */
	if (is_auth) {
		qat_req->req_ctx.ctx_hw_dma = dma_map_single(&GET_DEV(inst->accel_dev),
								(u8 *)(&qat_req->req_ctx.ctx_hw.auth_ctx_hw),
								sizeof(struct icp_hsm_sym_auth_ctx_hw),
								DMA_BIDIRECTIONAL);
	} else {
		qat_req->req_ctx.ctx_hw_dma = dma_map_single(&GET_DEV(inst->accel_dev),
								(u8 *)(&qat_req->req_ctx.ctx_hw.hash_ctx_hw),
								sizeof(struct icp_hsm_sym_hash_ctx_hw),
								DMA_BIDIRECTIONAL);
	}
	if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)))
		goto err_ctx;

	memcpy(qat_req->cache, qat_req->cache_next, qat_req->cache_sz);
	/* Transfer sgl to address list and map */

	/* Handle hash small buffer and fill address list */
	old_cache_bytes = qat_req->cache_sz;
	if (qat_req->cache_sz) {
		qat_req->cache_dma = dma_map_single(&GET_DEV(inst->accel_dev), qat_req->cache,
						qat_req->cache_sz, DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->cache_dma)))
			goto err_cache;
		qat_req->cache_dma_sz = qat_req->cache_sz;

		qat_req->in_list[i].addr = qat_req->cache_dma;
		qat_req->in_list[i].length = qat_req->cache_sz;
		qat_req->total_len += qat_req->in_list[i].length;
		i++;
	}

	/*
	 * n = (hash_len - block_sz) or  hash_len & (block_sz - 1)
	 * HSM may not do zero length final, so keep some data around
	 */
	hash_len = req_nbytes + qat_req->cache_sz;
	hash_rem = is_final ? 0 : (hash_len & (block_sz - 1));
	n = hash_len - hash_rem;
	if(!is_final && !hash_rem) {
		hash_rem = block_sz;
		n = hash_len - hash_rem;
	}

	/* Fix n value to sg list data count */
	n -= qat_req->cache_sz;

	if(sglin && req_nbytes) {
		nr_src = dma_map_sg(&GET_DEV(inst->accel_dev), sglin,
					sg_nents_for_len(sglin, req_nbytes), DMA_TO_DEVICE);
		if (unlikely(!nr_src))
			goto err_in;
	}

	qat_req->cache_sz = 0;
	nr_src = sg_nents(sglin);
	for_each_sg(sglin, sg, nr_src, index) {
		if ((!sg->length) || ((!hash_rem) && (!n)))
			continue;

		if (n < sg->length)
			sg_len = n;
		else
			sg_len = sg->length;

		if (n) {
			qat_req->in_list[i].addr = sg_dma_address(sg);
			qat_req->in_list[i].length = sg_len;

			qat_req->total_len += qat_req->in_list[i].length;
			i++;
		}

		/* Copy hash remain data to small buffer */
		/* When final, sg_len do not always equal to sg->length, as AEAD tag */
		if (sg_len != sg->length) {
			copy_len = (((sg->length - sg_len) > hash_rem) ? hash_rem : (sg->length - sg_len));
			scatterwalk_map_and_copy(&(qat_req->cache_next[qat_req->cache_sz]),
						sg, sg_len, copy_len, 0);
			qat_req->cache_sz += copy_len;
			hash_rem -= copy_len;
		}

		if (n != 0) {
			n -= sg_len;
		}
	}

	qat_req->in_list_dma = dma_map_single(&GET_DEV(inst->accel_dev), (void *)qat_req->in_list,
					sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
					DMA_TO_DEVICE);
	if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)))
		goto err_list;

	qat_req->in_num = i;

	return 0;

err_in:
err_list:
	src_num = sg_nents(sglin);
	for (i = 0; i < src_num; i++) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list[i].addr))
			dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->in_list[i].addr,
							qat_req->in_list[i].length,
							DMA_TO_DEVICE);
	}
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->in_list_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->in_list_dma,
				sizeof(struct icp_hsm_addr_list) * ICP_HSM_MAX_ADDR_LIST,
				DMA_TO_DEVICE);
	}

err_cache:
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->cache_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->cache_dma, old_cache_bytes,
						DMA_TO_DEVICE);
	}

err_ctx:
	if (is_auth) {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
			dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
					sizeof(struct icp_hsm_sym_auth_ctx_hw),
					DMA_BIDIRECTIONAL);
		}
	} else {
		if (!dma_mapping_error(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma)) {
			dma_unmap_single(&GET_DEV(inst->accel_dev), qat_req->req_ctx.ctx_hw_dma,
					sizeof(struct icp_hsm_sym_hash_ctx_hw),
					DMA_BIDIRECTIONAL);
		}
	}

	dev_err(&GET_DEV(inst->accel_dev), "ERR[hsm]:Failed to map buffer of hash for dma\n");
	return -ENOMEM;
}

/**
 * ccat_ahash_alg_callback() - HSM hash/auth callback function.
 * @qat_resp: HSM message data.
 * @qat_req: HSM response info.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static void ccat_ahash_alg_callback(struct icp_qat_fw_la_resp *qat_resp,
	struct qat_crypto_request *qat_req)
{
	struct ahash_request *req = qat_req->ahash_req;
	struct crypto_ahash *ahash = crypto_ahash_reqtfm(req);
	struct qat_alg_ahash_ctx *ctx = qat_req->ahash_ctx;
	u32 result_size = crypto_ahash_digestsize(ahash);
	int ret = 0;

	ccat_log("[CCAT] hash result\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	ret = ccat_ahash_req_unmap(ctx->inst, &qat_req->ccat_ahash_req);

	if (qat_req->ccat_ahash_req.is_finish) {
		/* Copy hash result to APP buffer */
		if (qat_req->ccat_ahash_req.is_auth) {
			memcpy(req->result, qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw.u.digest,
					result_size);
		}
		else {
			memcpy(req->result, qat_req->ccat_ahash_req.req_ctx.ctx_hw.hash_ctx_hw.digest,
					result_size);
		}

		/* Reset AHASH request */
		ret = ccat_ahash_req_reset(ctx, &qat_req->ccat_ahash_req);
	}

	req->base.complete(&req->base, ret);
}

/**
 * ccat_ahash_sw() - AHASH Soft algorithm.
 * @areq: ahash request data.
 * @ctx:  tih sym context address.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_ahash_digest_sw(struct ahash_request *areq,
								  struct qat_alg_ahash_ctx *ctx)
{
	struct crypto_ahash *tfm = (struct crypto_ahash *)(ctx->sw_tfm);
	struct ahash_request *req;
	struct crypto_wait wait;
	int ret;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	if (NULL == tfm || IS_ERR(tfm))
		return -ENOMEM;

	crypto_init_wait(&wait);

	req = ahash_request_alloc(tfm, GFP_ATOMIC);
	if (!req)
		return -ENOMEM;

	ahash_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
						crypto_req_done, &wait);

	ahash_request_set_crypt(req, areq->src, areq->result, areq->nbytes);
	ret = crypto_wait_req(crypto_ahash_digest(req), &wait);

	ahash_request_free(req);

	return ret;
}

/**
 * ccat_ahash_init() - HSM hash/auth init function.
 * @req: hash/auth request data.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_ahash_init(struct ahash_request *req)
{
	struct qat_crypto_request *qat_req = ahash_request_ctx(req);
	struct qat_alg_ahash_ctx *ctx = crypto_ahash_ctx(crypto_ahash_reqtfm(req));
	int ret = 0;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	ret = ccat_ahash_req_reset(ctx, &qat_req->ccat_ahash_req);

	return ret;
}

/**
 * ccat_ahash_update() - HSM hash/auth update function.
 * @req: hash/auth request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_ahash_update(struct ahash_request *req)
{
	struct crypto_ahash *ahash = crypto_ahash_reqtfm(req);
	struct crypto_tfm *tfm = crypto_ahash_tfm(ahash);
	struct qat_crypto_request *qat_req = ahash_request_ctx(req);
	struct qat_alg_ahash_ctx *ctx = crypto_ahash_ctx(crypto_ahash_reqtfm(req));
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_ahash_req.in_list;
	int ret = 0, i = 0;
	int ctr = 0;

	ccat_log("[CCAT] hash update, len: %d\n", req->nbytes);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (sg_nents_for_len(req->src, req->nbytes) > ICP_HSM_MAX_ADDR_LIST ||
		ctx->msg.hdr.sym_hdr.cipher_alg == ICP_HSM_SYM_CIPHER_ALG_AES192) {
		return -ESRCH;
	}

	qat_req->ccat_ahash_req.block_sz = crypto_tfm_alg_blocksize(tfm);
	qat_req->ccat_ahash_req.req_nbytes = req->nbytes;

	/* Small pkt copied to local buffer */
	if (((qat_req->ccat_ahash_req.cache_sz + qat_req->ccat_ahash_req.req_nbytes) <= qat_req->ccat_ahash_req.block_sz)) {
		scatterwalk_map_and_copy(&(qat_req->ccat_ahash_req.cache_next[qat_req->ccat_ahash_req.cache_sz]),
				req->src, 0, qat_req->ccat_ahash_req.req_nbytes, 0);
		qat_req->ccat_ahash_req.cache_sz += qat_req->ccat_ahash_req.req_nbytes;
		return 0;
	}

	ret = ccat_ahash_req_map(ctx->inst, req->src, &qat_req->ccat_ahash_req, 0);
	if (unlikely(ret))
		return ret;

	msg = &qat_req->ccat_ahash_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	if (qat_req->ccat_ahash_req.is_init)
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_INIT;
	else
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_NOT_INIT;
	msg->hdr.sym_hdr.finish = ICP_HSM_SYM_NOT_FINISH;
	msg->hdr.sym_hdr.total_len = 0;
	msg->input_addr = qat_req->ccat_ahash_req.in_list_dma;
	msg->input_length = qat_req->ccat_ahash_req.in_num;
	msg->ctx_addr = qat_req->ccat_ahash_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_ahash_req.is_finish = false;
	qat_req->ccat_ahash_req.is_init = false;
	qat_req->ahash_req = req;
	qat_req->ahash_ctx = ctx;
	qat_req->cb  = ccat_ahash_alg_callback;

	do {
		ret = adf_send_message(qat_req->ahash_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		ccat_ahash_req_unmap(ctx->inst , &qat_req->ccat_ahash_req);
		return -EAGAIN;
	}

#ifdef ICP_DEBUG
	u32 in_total_len;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	printk("in len: %d\n", in_total_len);
	printk("ring_id: %d\n", qat_req->ahash_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%llx\n", in_list);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < 16; i++)
		printk("ctx[%d]: 0x%x\n", i,
				((u32 *)(&(qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw)))[i]);
#endif


	return -EINPROGRESS;
}

/**
 * ccat_ahash_cmac_update() - HSM cmac update function.
 * @req: cmac request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_ahash_cmac_update(struct ahash_request *req)
{
	struct crypto_ahash *ahash = crypto_ahash_reqtfm(req);
	struct crypto_tfm *tfm = crypto_ahash_tfm(ahash);
	struct qat_crypto_request *qat_req = ahash_request_ctx(req);
	struct qat_alg_ahash_ctx *ctx = crypto_ahash_ctx(crypto_ahash_reqtfm(req));
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_ahash_req.in_list;
	int i = 0;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] hash cmac update, len: %d\n", req->nbytes);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (sg_nents_for_len(req->src, req->nbytes) > ICP_HSM_MAX_ADDR_LIST ||
		ctx->msg.hdr.sym_hdr.cipher_alg == ICP_HSM_SYM_CIPHER_ALG_AES192) {
		return -ESRCH;
	}

	qat_req->ccat_ahash_req.block_sz = crypto_tfm_alg_blocksize(tfm);
	qat_req->ccat_ahash_req.req_nbytes = req->nbytes;

	/* Small pkt copied to local buffer */
	if (((qat_req->ccat_ahash_req.cache_sz + qat_req->ccat_ahash_req.req_nbytes)
			<= qat_req->ccat_ahash_req.block_sz)) {
		scatterwalk_map_and_copy(&(qat_req->ccat_ahash_req.cache_next[qat_req->ccat_ahash_req.cache_sz]),
				req->src, 0, qat_req->ccat_ahash_req.req_nbytes, 0);
		qat_req->ccat_ahash_req.cache_sz += qat_req->ccat_ahash_req.req_nbytes;
		return 0;
	}

	ret = ccat_ahash_req_map(ctx->inst, req->src, &qat_req->ccat_ahash_req, 0);
	if (unlikely(ret))
		return ret;

	msg = &qat_req->ccat_ahash_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	if (qat_req->ccat_ahash_req.is_init)
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_INIT;
	else
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_NOT_INIT;
	msg->hdr.sym_hdr.finish = ICP_HSM_SYM_NOT_FINISH;
	msg->hdr.sym_hdr.total_len = 0;
	msg->input_addr = qat_req->ccat_ahash_req.in_list_dma;
	msg->input_length = qat_req->ccat_ahash_req.in_num;
	msg->ctx_addr = qat_req->ccat_ahash_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	do {
		ret = adf_send_message(qat_req->ahash_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	qat_req->ccat_ahash_req.is_init = false;
	qat_req->ccat_ahash_req.is_finish = false;
	qat_req->ahash_req = req;
	qat_req->ahash_ctx = ctx;
	qat_req->cb  = ccat_ahash_alg_callback;

	if (ret == -EAGAIN) {
		ccat_ahash_req_unmap(ctx->inst , &qat_req->ccat_ahash_req);
		return -EAGAIN;
	}

#ifdef ICP_DEBUG
	u32 in_total_len;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	printk("in len: %d\n", in_total_len);
	printk("ring_id: %d\n", qat_req->ahash_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%llx\n", in_list);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < 16; i++)
		printk("ctx[%d]: 0x%x\n", i,
				((u32 *)(&(qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw)))[i]);
#endif


	return -EINPROGRESS;
}

/**
 * ccat_ahash_final() - HSM hash/auth final function.
 * @req: hash/auth request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_ahash_final(struct ahash_request *req)
{
	struct qat_crypto_request *qat_req = ahash_request_ctx(req);
	struct qat_alg_ahash_ctx *ctx = crypto_ahash_ctx(crypto_ahash_reqtfm(req));
	struct crypto_ahash *ahash = crypto_ahash_reqtfm(req);
	struct crypto_tfm *tfm = crypto_ahash_tfm(ahash);
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_ahash_req.in_list;
	int i = 0;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] hash final\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	/* If we have an overall 0 length request */
	if ((qat_req->ccat_ahash_req.total_len + req->nbytes + qat_req->ccat_ahash_req.cache_sz) == 0) {
		if (ctx->msg.hdr.sym_hdr.auth_mode != ICP_HSM_SYM_AUTH_MODE_NULL) {
#ifdef CCAT_SW_OP
			return ccat_ahash_digest_sw(req, ctx);
#else
			return -ESRCH;
#endif
		}

		if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_MD5)
			memcpy(req->result, md5_zero_message_hash, MD5_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA1)
			memcpy(req->result, sha1_zero_message_hash, SHA1_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA224)
			memcpy(req->result, sha224_zero_message_hash, SHA224_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA256)
			memcpy(req->result, sha256_zero_message_hash, SHA256_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA384)
			memcpy(req->result, sha384_zero_message_hash, SHA384_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA512)
			memcpy(req->result, sha512_zero_message_hash, SHA512_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SM3)
			memcpy(req->result, sm3_zero_message_hash, SM3_DIGEST_SIZE);

		return 0;
	}

	qat_req->ccat_ahash_req.block_sz = crypto_tfm_alg_blocksize(tfm);
	qat_req->ccat_ahash_req.req_nbytes = req->nbytes;

	ret = ccat_ahash_req_map(ctx->inst, req->src, &qat_req->ccat_ahash_req, 1);
	if (unlikely(ret))
		return ret;

	qat_req->ccat_ahash_req.is_finish = true;

	msg = &qat_req->ccat_ahash_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	if (qat_req->ccat_ahash_req.is_init)
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_INIT;
	else
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_NOT_INIT;
	msg->hdr.sym_hdr.finish = ICP_HSM_SYM_FINISH;
	msg->hdr.sym_hdr.total_len = qat_req->ccat_ahash_req.total_len;
	msg->input_addr = qat_req->ccat_ahash_req.in_list_dma;
	msg->input_length = qat_req->ccat_ahash_req.in_num;
	msg->ctx_addr = qat_req->ccat_ahash_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ahash_req = req;
	qat_req->ahash_ctx = ctx;
	qat_req->cb  = ccat_ahash_alg_callback;

	do {
		ret = adf_send_message(qat_req->ahash_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		ccat_ahash_req_unmap(ctx->inst , &qat_req->ccat_ahash_req);
		return -EAGAIN;
	}

#ifdef ICP_DEBUG
	u32 in_total_len;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	printk("in len: %d\n", in_total_len);
	printk("ring_id: %d\n", qat_req->ahash_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%llx\n", in_list);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < 16; i++)
		printk("ctx[%d]: 0x%x\n", i,
				((u32 *)(&(qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw)))[i]);
#endif

	return -EINPROGRESS;
}

/**
 * ccat_ahash_finup() - HSM hash/auth finup function.
 * @req: hash/auth request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_ahash_finup(struct ahash_request *req)
{
	struct qat_crypto_request *qat_req = ahash_request_ctx(req);
	struct qat_alg_ahash_ctx *ctx = crypto_ahash_ctx(crypto_ahash_reqtfm(req));
	struct crypto_ahash *ahash = crypto_ahash_reqtfm(req);
	struct crypto_tfm *tfm = crypto_ahash_tfm(ahash);
	struct icp_hsm_msg_req *msg;
	struct icp_hsm_addr_list *in_list = qat_req->ccat_ahash_req.in_list;
	int i = 0;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] hash finup, len: %d\n", req->nbytes);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	/* If we have an overall 0 length request */
	if ((qat_req->ccat_ahash_req.total_len + req->nbytes + qat_req->ccat_ahash_req.cache_sz ) == 0) {
		if (ctx->msg.hdr.sym_hdr.auth_mode != ICP_HSM_SYM_AUTH_MODE_NULL) {
#ifdef CCAT_SW_OP
			return ccat_ahash_digest_sw(req, ctx);
#else
			return -ESRCH;
#endif
		}

		if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_MD5)
			memcpy(req->result, md5_zero_message_hash, MD5_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA1)
			memcpy(req->result, sha1_zero_message_hash, SHA1_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA224)
			memcpy(req->result, sha224_zero_message_hash, SHA224_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA256)
			memcpy(req->result, sha256_zero_message_hash, SHA256_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA384)
			memcpy(req->result, sha384_zero_message_hash, SHA384_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SHA512)
			memcpy(req->result, sha512_zero_message_hash, SHA512_DIGEST_SIZE);
		else if (ctx->msg.hdr.sym_hdr.hash_alg == ICP_HSM_SYM_HASH_ALG_SM3)
			memcpy(req->result, sm3_zero_message_hash, SM3_DIGEST_SIZE);

		return 0;
	}

	qat_req->ccat_ahash_req.block_sz = crypto_tfm_alg_blocksize(tfm);
	qat_req->ccat_ahash_req.req_nbytes = req->nbytes;

	ret = ccat_ahash_req_map(ctx->inst, req->src, &qat_req->ccat_ahash_req, 1);
	if (unlikely(ret))
		return ret;

	qat_req->ccat_ahash_req.is_finish = true;

	msg = &qat_req->ccat_ahash_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.sym_hdr, &ctx->msg.hdr.sym_hdr, sizeof(struct icp_hsm_sym_header));
	if (qat_req->ccat_ahash_req.is_init)
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_INIT;
	else
		msg->hdr.sym_hdr.init = ICP_HSM_SYM_NOT_INIT;
	msg->hdr.sym_hdr.finish = ICP_HSM_SYM_FINISH;
	msg->hdr.sym_hdr.total_len = qat_req->ccat_ahash_req.total_len;
	msg->input_addr = qat_req->ccat_ahash_req.in_list_dma;
	msg->input_length = qat_req->ccat_ahash_req.in_num;
	msg->ctx_addr = qat_req->ccat_ahash_req.req_ctx.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ahash_req = req;
	qat_req->ahash_ctx = ctx;
	qat_req->cb  = ccat_ahash_alg_callback;

	do {
		ret = adf_send_message(qat_req->ahash_ctx->inst->sym_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		ccat_ahash_req_unmap(ctx->inst , &qat_req->ccat_ahash_req);
		return -EAGAIN;
	}

#ifdef ICP_DEBUG
	u32 in_total_len;
	for (i = 0; i < msg->input_length; i++)
		in_total_len += in_list[i].length;
	printk("in len: %d\n", in_total_len);
	printk("ring_id: %d\n", qat_req->ahash_ctx->inst->sym_tx->ring_number);
	printk("in_list addr: 0x%llx\n", in_list);
	for (i = 0; i < msg->input_length; i++)
		printk("in[%d] addr: 0x%llx, len: %d\n", i, in_list[i].addr, in_list[i].length);
	for (i = 0; i < 16; i++)
		printk("ctx[%d]: 0x%x\n", i,
				((u32 *)(&(qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw)))[i]);
#endif

	return -EINPROGRESS;
}

/**
 * ccat_ahash_digest() - HSM hash/auth digest function.
 * @req: hash/auth request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_ahash_digest(struct ahash_request *req)
{
	struct qat_alg_ahash_ctx *ctx = crypto_ahash_ctx(crypto_ahash_reqtfm(req));

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	/**
	 * When an algorithm is not supported by HSM, use a soft algorithm instead.
	 * 1. src addrlist > 32
	 * 2. cipher(cmac) algorithm is AES192
	 */
	if (sg_nents_for_len(req->src, req->nbytes) > ICP_HSM_MAX_ADDR_LIST ||
		ctx->msg.hdr.sym_hdr.cipher_alg == ICP_HSM_SYM_CIPHER_ALG_AES192 ||
		(ctx->msg.hdr.sym_hdr.auth_mode == ICP_HSM_SYM_AUTH_MODE_CMAC
		&& req->nbytes == 0)) {
#ifdef CCAT_SW_OP
		return ccat_ahash_digest_sw(req, ctx);
#else
		return -ESRCH;
#endif
	}

	ccat_ahash_init(req);
	return ccat_ahash_finup(req);
}

/**
 * ccat_ahash_precompute() - HSM hmac key hash operation.
 * @alg:      hmac algorithm selection.
 * @key:      key address.
 * @keylen:   key length.
 * @hash_key: hash key address.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_ahash_precompute(u32 alg, const u8 *key, u32 keylen, u8 *hash_key)
{
	struct crypto_ahash *tfm;
	struct ahash_request *req;
	struct crypto_wait wait;
	struct scatterlist sg;
	u32 digest_sz;
	int ret = 0;

	switch (alg) {
	case ICP_HSM_SYM_HASH_ALG_MD5:
		digest_sz = MD5_DIGEST_SIZE;
		tfm = crypto_alloc_ahash("md5", 0, 0);
		break;
	case ICP_HSM_SYM_HASH_ALG_SHA1:
		digest_sz = SHA1_DIGEST_SIZE;
		tfm = crypto_alloc_ahash("sha1", 0, 0);
		break;
	case ICP_HSM_SYM_HASH_ALG_SHA224:
		digest_sz = SHA224_DIGEST_SIZE;
		tfm = crypto_alloc_ahash("sha224", 0, 0);
		break;
	case ICP_HSM_SYM_HASH_ALG_SHA256:
		digest_sz = SHA256_DIGEST_SIZE;
		tfm = crypto_alloc_ahash("sha256", 0, 0);
		break;
	case ICP_HSM_SYM_HASH_ALG_SHA384:
		digest_sz = SHA384_DIGEST_SIZE;
		tfm = crypto_alloc_ahash("sha384", 0, 0);
		break;
	case ICP_HSM_SYM_HASH_ALG_SHA512:
		digest_sz = SHA512_DIGEST_SIZE;
		tfm = crypto_alloc_ahash("sha512", 0, 0);
		break;
	case ICP_HSM_SYM_HASH_ALG_SM3:
		digest_sz = SM3_DIGEST_SIZE;
		tfm = crypto_alloc_ahash("sm3", 0, 0);
		break;
	default:
		return -EINVAL;
	}
	if (IS_ERR(tfm))
		return PTR_ERR(tfm);

	crypto_init_wait(&wait);

	req = ahash_request_alloc(tfm, GFP_KERNEL);
	if (!req) {
		ret = -ENOMEM;
		goto free_ahash;
	}

	ahash_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
					crypto_req_done, &wait);

	sg_init_one(&sg, key, keylen);
	ahash_request_set_crypt(req, &sg, hash_key, keylen);

	ret = crypto_wait_req(crypto_ahash_digest(req), &wait);
	if (ret)
		goto free_request;

free_request:
	ahash_request_free(req);
free_ahash:
	crypto_free_ahash(tfm);

	return ret;
}

/**
 * ccat_ahash_setkey() - HSM auth setkey function.
 * @ahash:  User-instantiated objects which encapsulate.
 * @key:    key address.
 * @keylen: key length.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_ahash_setkey(struct crypto_ahash *ahash, const u8 *key,
								unsigned int keylen)
{
	struct crypto_tfm *tfm = crypto_ahash_tfm(ahash);
	struct qat_alg_ahash_ctx *ctx = crypto_tfm_ctx(tfm);
	u8 hash_key[SHA512_DIGEST_SIZE];
	int ret = 0;
#ifdef CCAT_SW_OP
	struct crypto_ahash *sw_tfm = (struct crypto_ahash *)(ctx->sw_tfm);
	u8 sw_key[64];
	u32 sw_klen;
#endif

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	ccat_log("[CCAT] hash setkey, keylen: %d\n", keylen);

	if (ctx->msg.hdr.sym_hdr.auth_mode ==
			ICP_HSM_SYM_AUTH_MODE_HMAC && keylen > SHA512_DIGEST_SIZE) {
		memset(hash_key, 0, SHA512_DIGEST_SIZE);
		ret = ccat_ahash_precompute(ctx->msg.hdr.sym_hdr.hash_alg, key, keylen, hash_key);
		if (ret) {
			crypto_ahash_set_flags(ahash, CRYPTO_TFM_REQ_MASK);
			return -EINVAL;
		}

		memset(ctx->auth_key, 0, SHA512_DIGEST_SIZE);
		memcpy(ctx->auth_key, hash_key, SHA512_DIGEST_SIZE);
		ctx->auth_key_sz = SHA512_DIGEST_SIZE;
		goto end;
	}
	if (ctx->msg.hdr.sym_hdr.auth_mode == ICP_HSM_SYM_AUTH_MODE_CMAC) {
		switch (ctx->msg.hdr.sym_hdr.cipher_alg) {
		case ICP_HSM_SYM_CIPHER_ALG_AES128:
		case ICP_HSM_SYM_CIPHER_ALG_AES192:
		case ICP_HSM_SYM_CIPHER_ALG_AES256:
			if (keylen == AES_KEYSIZE_128) {
				ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128;
			} else if (keylen == AES_KEYSIZE_256) {
				ctx->msg.hdr.sym_hdr.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES256;
			} else if (keylen == AES_KEYSIZE_192) {
				crypto_ahash_set_flags(ahash, CRYPTO_TFM_REQ_MASK);
				return -EINVAL;
			}
			break;
		case ICP_HSM_SYM_CIPHER_ALG_SM4:
			break;
		default:
			return -EINVAL;
		}
	}

	memset(ctx->auth_key, 0, SHA512_DIGEST_SIZE);
	memcpy(ctx->auth_key, key, keylen);
	ctx->auth_key_sz = keylen;

end:
#ifdef CCAT_SW_OP
	if (NULL == sw_tfm || IS_ERR(sw_tfm))
		return 0;

	if (ctx->msg.hdr.sym_hdr.auth_mode != ICP_HSM_SYM_AUTH_MODE_NULL) {
		memcpy(sw_key, ctx->auth_key, ctx->auth_key_sz);
		sw_klen = ctx->auth_key_sz;
	}

	if (ctx->msg.hdr.sym_hdr.auth_mode != ICP_HSM_SYM_AUTH_MODE_NULL) {
		crypto_ahash_clear_flags(sw_tfm, ~0);

		if (crypto_ahash_setkey(sw_tfm, sw_key, sw_klen))
			crypto_free_ahash(sw_tfm);
	}
#endif

	return ret;
}

/**
 * ccat_ahash_export() - HSM hash/auth export data function.
 * @req: hash/auth request data.
 * @out: out data address.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_ahash_export(struct ahash_request *req, void *out)
{
	struct qat_crypto_request *qat_req = ahash_request_ctx(req);
	struct ccat_ahash_export_state *export = (struct ccat_ahash_export_state *)out;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	export->is_auth = qat_req->ccat_ahash_req.is_auth;
	export->is_init = qat_req->ccat_ahash_req.is_init;
	export->is_finish = qat_req->ccat_ahash_req.is_finish;
	memcpy(export->cache_next, qat_req->ccat_ahash_req.cache_next, SHA512_BLOCK_SIZE);
	export->cache_sz = qat_req->ccat_ahash_req.cache_sz;
	export->total_len = qat_req->ccat_ahash_req.total_len;

	if (qat_req->ccat_ahash_req.is_auth) {
		memcpy(export->digest, qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw.u.digest,
					SHA512_DIGEST_SIZE);
	} else {
		memcpy(export->digest, qat_req->ccat_ahash_req.req_ctx.ctx_hw.hash_ctx_hw.digest,
					SHA512_DIGEST_SIZE);
	}

	return 0;
}

/**
 * ccat_ahash_import() - HSM hash/auth import data function.
 * @req: hash/auth request data.
 * @in:  in data address.
 *
 * Return:
 * * 0       - Success.
 * * -EINVAL - Failure.
 */
static int ccat_ahash_import(struct ahash_request *req, const void *in)
{
	struct crypto_ahash *ahash = crypto_ahash_reqtfm(req);
	struct qat_crypto_request *qat_req = ahash_request_ctx(req);
	struct qat_alg_ahash_ctx *ctx = crypto_ahash_ctx(ahash);
	struct ccat_ahash_export_state *export = (struct ccat_ahash_export_state *)in;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	qat_req->ccat_ahash_req.is_auth = export->is_auth;
	qat_req->ccat_ahash_req.is_init = export->is_init;
	qat_req->ccat_ahash_req.is_finish = export->is_finish;
	memcpy(qat_req->ccat_ahash_req.cache_next, export->cache_next, SHA512_BLOCK_SIZE);
	qat_req->ccat_ahash_req.cache_sz = export->cache_sz;
	qat_req->ccat_ahash_req.total_len = export->total_len;
	qat_req->ccat_ahash_req.cache_dma = 0;
	qat_req->ccat_ahash_req.cache_dma_sz = 0;

	if (qat_req->ccat_ahash_req.is_auth) {
		memset(&(qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw), 0,
				sizeof(struct icp_hsm_sym_auth_ctx_hw));
		memcpy(qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw.key, ctx->auth_key,
				ctx->auth_key_sz);
		memcpy(qat_req->ccat_ahash_req.req_ctx.ctx_hw.auth_ctx_hw.u.digest, export->digest,
				SHA512_DIGEST_SIZE);
	} else {
		memset(&(qat_req->ccat_ahash_req.req_ctx.ctx_hw.hash_ctx_hw), 0,
				sizeof(struct icp_hsm_sym_hash_ctx_hw));
		memcpy(qat_req->ccat_ahash_req.req_ctx.ctx_hw.hash_ctx_hw.digest, export->digest,
				SHA512_DIGEST_SIZE);
	}

	return 0;
}

/**
 * ccat_ahash_cra_init() - HSM hash/auth init function.
 * @tfm: User-instantiated objects which encapsulate.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_ahash_cra_init(struct crypto_tfm *tfm)
{
	struct qat_alg_ahash_ctx *ctx = crypto_tfm_ctx(tfm);
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();
	struct ccat_alg_template *tmpl =
		container_of(__crypto_ahash_alg(tfm->__crt_alg), struct ccat_alg_template, alg.ahash);
#ifdef CCAT_SW_OP
	struct crypto_ahash *sw_tfm = NULL;
	char ahash_name[CRYPTO_MAX_ALG_NAME];
	char cmp_name[CRYPTO_MAX_ALG_NAME];
	u32 alg;
#endif

	ccat_log("[CCAT] ahash init\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return -EINVAL;

	ctx->inst = inst;
	ctx->msg.hdr.sym_hdr.first = 1;
	ctx->msg.hdr.sym_hdr.last = 1;
	ctx->msg.hdr.sym_hdr.addr_list = 1;
	if (tmpl->hsm_type == ICP_HSM_TYPE_HASH) {
		ctx->msg.hdr.sym_hdr.service_type = ICP_HSM_TYPE_HASH;
		ctx->msg.hdr.sym_hdr.hash_alg = tmpl->alg_info.hash_info.alg;
	} else {
		ctx->msg.hdr.sym_hdr.service_type = ICP_HSM_TYPE_AUTH;
		ctx->msg.hdr.sym_hdr.auth_mode = tmpl->alg_info.auth_info.mode;
		if (ctx->msg.hdr.sym_hdr.auth_mode == ICP_HSM_SYM_AUTH_MODE_HMAC)
			ctx->msg.hdr.sym_hdr.hash_alg = tmpl->alg_info.auth_info.hash_alg;
		else
			ctx->msg.hdr.sym_hdr.cipher_alg = tmpl->alg_info.auth_info.cipher_alg;
		ctx->msg.hdr.sym_hdr.key_mode = ICP_HSM_KEY_MODE_PLAIN;
	}

	crypto_ahash_set_reqsize(__crypto_ahash_cast(tfm),
			ALIGN(sizeof(struct qat_crypto_request), L1_CACHE_BYTES));

#ifdef CCAT_SW_OP
	alg = ctx->msg.hdr.sym_hdr.auth_mode == ICP_HSM_SYM_AUTH_MODE_CMAC ?
				ctx->msg.hdr.sym_hdr.cipher_alg : ctx->msg.hdr.sym_hdr.hash_alg;

	if (ctx->msg.hdr.sym_hdr.auth_mode == ICP_HSM_SYM_AUTH_MODE_CMAC) {
		if (alg == ICP_HSM_SYM_CIPHER_ALG_SM4)
			return 0;
		snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "cmac(aes-generic)");
	} else {
		switch (alg) {
		case ICP_HSM_SYM_HASH_ALG_SM3:
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "sm3-generic");
			break;
		case ICP_HSM_SYM_HASH_ALG_MD5:
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "md5-generic");
			break;
		case ICP_HSM_SYM_HASH_ALG_SHA1:
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "sha1-generic");
			break;
		case ICP_HSM_SYM_HASH_ALG_SHA224:
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "sha224-generic");
			break;
		case ICP_HSM_SYM_HASH_ALG_SHA256:
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "sha256-generic");
			break;
		case ICP_HSM_SYM_HASH_ALG_SHA384:
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "sha384-generic");
			break;
		case ICP_HSM_SYM_HASH_ALG_SHA512:
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "%s", "sha512-generic");
			break;

		default:
			return 0;
		}

		if (ctx->msg.hdr.sym_hdr.auth_mode == ICP_HSM_SYM_AUTH_MODE_HMAC) {
			if (alg == ICP_HSM_SYM_HASH_ALG_SM3)
				return 0;
			memcpy(cmp_name, ahash_name, CRYPTO_MAX_ALG_NAME);
			snprintf(ahash_name, CRYPTO_MAX_ALG_NAME, "hmac(%s)", cmp_name);
		}
	}
	ccat_log("[CCAT] ahash sw alg: %s\n", ahash_name);
	sw_tfm = (void *)crypto_alloc_ahash(ahash_name, 0, 0);
	ctx->sw_tfm = (void *)sw_tfm;
#endif

	return 0;
}

/**
 * ccat_ahash_cra_exit() - HSM hash/auth exit function.
 * @tfm: User-instantiated objects which encapsulate.
 */
static void ccat_ahash_cra_exit(struct crypto_tfm *tfm)
{
	struct qat_alg_ahash_ctx *ctx = crypto_tfm_ctx(tfm);
#ifdef CCAT_SW_OP
	struct crypto_ahash *sw_tfm = (struct crypto_ahash *)(ctx->sw_tfm);
#endif

	ccat_log("[CCAT] ahash exit\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

#ifdef CCAT_SW_OP
	if (sw_tfm)
		crypto_free_ahash(sw_tfm);
#endif
}

#endif /* CCAT_HASH */

/* HSM AES-GCM algorithms registration information */
static struct ccat_alg_template ccat_alg_gcm_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_gcm_ccm_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = GCM_AES_IV_SIZE,
		.maxauthsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "gcm(aes)",
			.cra_driver_name = "ccat-gcm-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 1,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.aead_tag = ICP_HSM_SYM_AEAD_TAG_16,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_GCM,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_NULL,
	},
};

/* HSM AES-GCM(rfc4106) algorithms registration information */
static struct ccat_alg_template ccat_alg_rfc4106_gcm_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_rfc4106_gcm_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = GCM_RFC4106_IV_SIZE,
		.maxauthsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "rfc4106(gcm(aes))",
			.cra_driver_name = "ccat-rfc4106-gcm-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 1,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.aead_tag = ICP_HSM_SYM_AEAD_TAG_16,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_GCM,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_NULL,
	},
	.is_rfc4106 = true,
};

/* HSM SM4-GCM algorithms registration information */
static struct ccat_alg_template ccat_alg_gcm_sm4 = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_gcm_ccm_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = GCM_AES_IV_SIZE,
		.maxauthsize = SM4_BLOCK_SIZE,
		.base = {
			.cra_name = "gcm(sm4)",
			.cra_driver_name = "ccat-gcm-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 1,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.aead_tag = ICP_HSM_SYM_AEAD_TAG_16,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_GCM,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_NULL,
	},
};

/* HSM AES-CCM algorithms registration information */
static struct ccat_alg_template ccat_alg_ccm_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_gcm_ccm_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = AES_BLOCK_SIZE,
		.maxauthsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "ccm(aes)",
			.cra_driver_name = "ccat-ccm-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 1,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_MTE,
		.aead_tag = ICP_HSM_SYM_AEAD_TAG_16,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CCM,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_NULL,
	},
};

/* HSM AES-CCM(rfc4309) algorithms registration information */
static struct ccat_alg_template ccat_alg_rfc4309_ccm_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_rfc4309_ccm_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = 8,
		.maxauthsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "rfc4309(ccm(aes))",
			.cra_driver_name = "ccat-rfc4309_ccm-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 1,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_MTE,
		.aead_tag = ICP_HSM_SYM_AEAD_TAG_16,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CCM,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_NULL,
	},
	.is_rfc4309 = true,
};

/* HSM AEAD(AES-CBC-SHA1-HMAC) algorithms registration information */
static struct ccat_alg_template ccat_alg_authenc_hmac_sha1_cbc_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_authenc_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = AES_BLOCK_SIZE,
		.maxauthsize = SHA1_DIGEST_SIZE,
		.base = {
			.cra_name = "authenc(hmac(sha1),cbc(aes))",
			.cra_driver_name = "ccat-authenc-hmac-sha1-cbc-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 16,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA1,
	},
};

/* HSM echainiv AEAD(AES-CBC-SHA1-HMAC) algorithms registration information */
static struct ccat_alg_template ccat_alg_echainiv_authenc_hmac_sha1_cbc_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_echainiv_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_authenc_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_echainiv_encrypt,
		.decrypt = ccat_aead_echainiv_decrypt,
		.ivsize = AES_BLOCK_SIZE,
		.maxauthsize = SHA1_DIGEST_SIZE,
		.base = {
			.cra_name = "echainiv(authenc(hmac(sha1),cbc(aes)))",
			.cra_driver_name = "ccat-echainiv-authenc-hmac-sha1-cbc-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 16,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA1,
	},
	.is_echainiv = true,
};

/* HSM AEAD(AES-CBC-SHA256-HMAC) algorithms registration information */
static struct ccat_alg_template ccat_alg_authenc_hmac_sha256_cbc_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_authenc_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = AES_BLOCK_SIZE,
		.maxauthsize = SHA256_DIGEST_SIZE,
		.base = {
			.cra_name = "authenc(hmac(sha256),cbc(aes))",
			.cra_driver_name = "ccat-authenc-hmac-sha256-cbc-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 16,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA256,
	},
};

/* HSM echainiv AEAD(AES-CBC-SHA256-HMAC) algorithms registration information */
static struct ccat_alg_template ccat_alg_echainiv_authenc_hmac_sha256_cbc_aes = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_echainiv_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_authenc_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_echainiv_encrypt,
		.decrypt = ccat_aead_echainiv_decrypt,
		.ivsize = AES_BLOCK_SIZE,
		.maxauthsize = SHA256_DIGEST_SIZE,
		.base = {
			.cra_name = "echainiv(authenc(hmac(sha256),cbc(aes)))",
			.cra_driver_name = "ccat-echainiv-authenc-hmac-sha256-cbc-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 16,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA256,
	},
	.is_echainiv = true,
};

/* HSM AEAD(SM4-CBC-SM3-HMAC) algorithms registration information */
static struct ccat_alg_template ccat_alg_authenc_hmac_sm3_cbc_sm4 = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_authenc_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = SM4_BLOCK_SIZE,
		.maxauthsize = SM3_DIGEST_SIZE,
		.base = {
			.cra_name = "authenc(hmac(sm3),cbc(sm4))",
			.cra_driver_name = "ccat-authenc-hmac-sm3-cbc-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 16,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SM3,
	},
};

/* HSM AEAD(SM4-CBC-SHA1-HMAC) algorithms registration information */
static struct ccat_alg_template ccat_alg_authenc_hmac_sha1_cbc_sm4 = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_authenc_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = SM4_BLOCK_SIZE,
		.maxauthsize = SHA1_DIGEST_SIZE,
		.base = {
			.cra_name = "authenc(hmac(sha1),cbc(sm4))",
			.cra_driver_name = "ccat-authenc-hmac-sha1-cbc-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 16,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA1,
	},
};

/* HSM AEAD(SM4-CBC-SHA256-HMAC) algorithms registration information */
static struct ccat_alg_template ccat_alg_authenc_hmac_sha256_cbc_sm4 = {
	.type = CRYPTO_ALG_TYPE_AEAD,
	.alg.aead = {
		.init = ccat_aead_init,
		.exit = ccat_aead_exit,
		.setkey = ccat_aead_authenc_setkey,
		.setauthsize = ccat_aead_setauthsize,
		.encrypt = ccat_aead_encrypt,
		.decrypt = ccat_aead_decrypt,
		.ivsize = SM4_BLOCK_SIZE,
		.maxauthsize = SHA256_DIGEST_SIZE,
		.base = {
			.cra_name = "authenc(hmac(sha256),cbc(sm4))",
			.cra_driver_name = "ccat-authenc-hmac-sha256-cbc-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 16,
			.cra_ctxsize = sizeof(struct qat_alg_aead_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_AEAD,
	.alg_info.aead_info = {
		.aead_mode = ICP_HSM_SYM_AEAD_MODE_ETM,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.cipher_mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
		.auth_mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA256,
	},
};

/* HSM DES algorithms ECB mode registration information */
static struct ccat_alg_template ccat_alg_ecb_des = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = DES_KEY_SIZE,
		.max_keysize = DES_KEY_SIZE,
		.ivsize = DES_BLOCK_SIZE,
		.base = {
			.cra_name = "ecb(des)",
			.cra_driver_name = "ccat-ecb-des",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = DES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_DES,
		.mode = ICP_HSM_SYM_CIPHER_MODE_ECB,
	},
};

/* HSM DES algorithms CBC mode registration information */
static struct ccat_alg_template ccat_alg_cbc_des = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = DES_KEY_SIZE,
		.max_keysize = DES_KEY_SIZE,
		.ivsize = DES_BLOCK_SIZE,
		.base = {
			.cra_name = "cbc(des)",
			.cra_driver_name = "ccat-cbc-des",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = DES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_DES,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
	},
};

/* HSM 3DES algorithms ECB mode registration information */
static struct ccat_alg_template ccat_alg_ecb_des3_ede = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = DES3_EDE_KEY_SIZE,
		.max_keysize = DES3_EDE_KEY_SIZE,
		.ivsize = DES3_EDE_BLOCK_SIZE,
		.base = {
			.cra_name = "ecb(des3_ede)",
			.cra_driver_name = "ccat-ecb-des3_ede",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = DES3_EDE_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_3DES,
		.mode = ICP_HSM_SYM_CIPHER_MODE_ECB,
	},
};

/* HSM 3DES algorithms CBC mode registration information */
static struct ccat_alg_template ccat_alg_cbc_des3_ede = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = DES3_EDE_KEY_SIZE,
		.max_keysize = DES3_EDE_KEY_SIZE,
		.ivsize = DES3_EDE_BLOCK_SIZE,
		.base = {
			.cra_name = "cbc(des3_ede)",
			.cra_driver_name = "ccat-cbc-des3_ede",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = DES3_EDE_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_3DES,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
	},
};

/* HSM AES algorithms ECB mode registration information */
static struct ccat_alg_template ccat_alg_ecb_aes = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = AES_MIN_KEY_SIZE,
		.max_keysize = AES_MAX_KEY_SIZE,
		.ivsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "ecb(aes)",
			.cra_driver_name = "ccat-ecb-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = AES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.mode = ICP_HSM_SYM_CIPHER_MODE_ECB,
	},
};

/* HSM AES algorithms CBC mode registration information */
static struct ccat_alg_template ccat_alg_cbc_aes = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = AES_MIN_KEY_SIZE,
		.max_keysize = AES_MAX_KEY_SIZE,
		.ivsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "cbc(aes)",
			.cra_driver_name = "ccat-cbc-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = AES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
	},
};

/* HSM AES algorithms CFB mode registration information */
static struct ccat_alg_template ccat_alg_cfb_aes = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = AES_MIN_KEY_SIZE,
		.max_keysize = AES_MAX_KEY_SIZE,
		.ivsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "cfb(aes)",
			.cra_driver_name = "ccat-cfb-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = AES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CFB,
	},
};

/* HSM AES algorithms OFB mode registration information */
static struct ccat_alg_template ccat_alg_ofb_aes = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = AES_MIN_KEY_SIZE,
		.max_keysize = AES_MAX_KEY_SIZE,
		.ivsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "ofb(aes)",
			.cra_driver_name = "ccat-ofb-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = AES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.mode = ICP_HSM_SYM_CIPHER_MODE_OFB,
	},
};

/* HSM AES algorithms CTR mode registration information */
static struct ccat_alg_template ccat_alg_ctr_aes = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = AES_MIN_KEY_SIZE,
		.max_keysize = AES_MAX_KEY_SIZE,
		.ivsize = AES_BLOCK_SIZE,
		.base = {
			.cra_name = "ctr(aes)",
			.cra_driver_name = "ccat-ctr-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = AES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CTR,
	},
};

/* HSM AES algorithms CTR mode (rfc3686) registration information */
static struct ccat_alg_template ccat_alg_rfc3686_ctr_aes = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_rfc3686_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = AES_MIN_KEY_SIZE + CTR_RFC3686_NONCE_SIZE,
		.max_keysize = AES_MAX_KEY_SIZE + CTR_RFC3686_NONCE_SIZE,
		.ivsize = CTR_RFC3686_IV_SIZE,
		.base = {
			.cra_name = "rfc3686(ctr(aes))",
			.cra_driver_name = "ccat-rfc3686-ctr-aes",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = AES_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CTR,
	},
	.is_rfc3686 = true,
};

/* HSM SM4 algorithms ECB mode registration information */
static struct ccat_alg_template ccat_alg_ecb_sm4 = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = SM4_KEY_SIZE,
		.max_keysize = SM4_KEY_SIZE,
		.ivsize = SM4_BLOCK_SIZE,
		.base = {
			.cra_name = "ecb(sm4)",
			.cra_driver_name = "ccat-ecb-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.mode = ICP_HSM_SYM_CIPHER_MODE_ECB,
	},
};

/* HSM SM4 algorithms CBC mode registration information */
static struct ccat_alg_template ccat_alg_cbc_sm4 = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = SM4_KEY_SIZE,
		.max_keysize = SM4_KEY_SIZE,
		.ivsize = SM4_BLOCK_SIZE,
		.base = {
			.cra_name = "cbc(sm4)",
			.cra_driver_name = "ccat-cbc-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = SM4_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CBC,
	},
};

/* HSM SM4 algorithms CFB mode registration information */
static struct ccat_alg_template ccat_alg_cfb_sm4 = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = SM4_KEY_SIZE,
		.max_keysize = SM4_KEY_SIZE,
		.ivsize = SM4_BLOCK_SIZE,
		.base = {
			.cra_name = "cfb(sm4)",
			.cra_driver_name = "ccat-cfb-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = SM4_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CFB,
	},
};

/* HSM SM4 algorithms OFB mode registration information */
static struct ccat_alg_template ccat_alg_ofb_sm4 = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = SM4_KEY_SIZE,
		.max_keysize = SM4_KEY_SIZE,
		.ivsize = SM4_BLOCK_SIZE,
		.base = {
			.cra_name = "ofb(sm4)",
			.cra_driver_name = "ccat-ofb-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = SM4_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.mode = ICP_HSM_SYM_CIPHER_MODE_OFB,
	},
};

/* HSM SM4 algorithms CTR mode registration information */
static struct ccat_alg_template ccat_alg_ctr_sm4 = {
	.type = CRYPTO_ALG_TYPE_SKCIPHER,
	.alg.acipher = {
		.init = ccat_skcipher_init,
		.exit = ccat_skcipher_exit,
		.setkey = ccat_skcipher_setkey,
		.encrypt = ccat_skcipher_encrypt,
		.decrypt = ccat_skcipher_decrypt,
		.min_keysize = SM4_KEY_SIZE,
		.max_keysize = SM4_KEY_SIZE,
		.ivsize = SM4_BLOCK_SIZE,
		.base = {
			.cra_name = "ctr(sm4)",
			.cra_driver_name = "ccat-ctr-sm4",
			.cra_priority = 600,
			.cra_flags = CRYPTO_ALG_TYPE_SKCIPHER | CRYPTO_ALG_ASYNC,
			.cra_blocksize = SM4_BLOCK_SIZE,
			.cra_ctxsize = sizeof(struct qat_alg_skcipher_ctx),
			.cra_alignmask = 0,
			.cra_module = THIS_MODULE,
		},
	},
	.hsm_type = ICP_HSM_TYPE_CIPHER,
	.alg_info.cipher_info = {
		.alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.mode = ICP_HSM_SYM_CIPHER_MODE_CTR,
	},
};

/* HSM MD5 algorithms registration information */
static struct ccat_alg_template ccat_alg_md5 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = MD5_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "md5",
				.cra_driver_name = "ccat-md5",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = MD5_HMAC_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_HASH,
	.alg_info.hash_info = {
		.alg = ICP_HSM_SYM_HASH_ALG_MD5,
	},
};

/* HSM SHA1 algorithms registration information */
static struct ccat_alg_template ccat_alg_sha1 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA1_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "sha1",
				.cra_driver_name = "ccat-sha1",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA1_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_HASH,
	.alg_info.hash_info = {
		.alg = ICP_HSM_SYM_HASH_ALG_SHA1,
	},
};

/* HSM SHA224 algorithms registration information */
static struct ccat_alg_template ccat_alg_sha224 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA224_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "sha224",
				.cra_driver_name = "ccat-sha224",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA224_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_HASH,
	.alg_info.hash_info = {
		.alg = ICP_HSM_SYM_HASH_ALG_SHA224,
	},
};

/* HSM SHA256 algorithms registration information */
static struct ccat_alg_template ccat_alg_sha256 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA256_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "sha256",
				.cra_driver_name = "ccat-sha256",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA256_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_HASH,
	.alg_info.hash_info = {
		.alg = ICP_HSM_SYM_HASH_ALG_SHA256,
	},
};

/* HSM SHA384 algorithms registration information */
static struct ccat_alg_template ccat_alg_sha384 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA384_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "sha384",
				.cra_driver_name = "ccat-sha384",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA384_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_HASH,
	.alg_info.hash_info = {
		.alg = ICP_HSM_SYM_HASH_ALG_SHA384,
	},
};

/* HSM SHA512 algorithms registration information */
static struct ccat_alg_template ccat_alg_sha512 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA512_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "sha512",
				.cra_driver_name = "ccat-sha512",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA512_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_HASH,
	.alg_info.hash_info = {
		.alg = ICP_HSM_SYM_HASH_ALG_SHA512,
	},
};

/* HSM SM3 algorithms registration information */
static struct ccat_alg_template ccat_alg_sm3 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SM3_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "sm3",
				.cra_driver_name = "ccat-sm3",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SM3_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_HASH,
	.alg_info.hash_info = {
		.alg = ICP_HSM_SYM_HASH_ALG_SM3,
	},
};

/* HSM MD5-HMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_hmac_md5 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = MD5_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "hmac(md5)",
				.cra_driver_name = "ccat-hmac-md5",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = MD5_HMAC_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_MD5,
	},
};

/* HSM SHA1-HMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_hmac_sha1 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA1_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "hmac(sha1)",
				.cra_driver_name = "ccat-hmac-sha1",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA1_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA1,
	},
};

/* HSM SHA224-HMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_hmac_sha224 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA224_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "hmac(sha224)",
				.cra_driver_name = "ccat-hmac-sha224",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA224_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA224,
	},
};

/* HSM SHA256-HMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_hmac_sha256 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA256_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "hmac(sha256)",
				.cra_driver_name = "ccat-hmac-sha256",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA256_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA256,
	},
};

/* HSM SHA384-HMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_hmac_sha384 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA384_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "hmac(sha384)",
				.cra_driver_name = "ccat-hmac-sha384",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA384_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA384,
	},
};

/* HSM SHA512-HMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_hmac_sha512 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SHA512_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "hmac(sha512)",
				.cra_driver_name = "ccat-hmac-sha512",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SHA512_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SHA512,
	},
};

/* HSM SM3-HMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_hmac_sm3 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SM3_DIGEST_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "hmac(sm3)",
				.cra_driver_name = "ccat-hmac-sm3",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SM3_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_HMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_NULL,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_SM3,
	},
};

/* HSM AES-CMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_cmac_aes = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_cmac_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = AES_BLOCK_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "cmac(aes)",
				.cra_driver_name = "ccat-cmac-aes",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = AES_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_CMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_AES128,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_NULL,
	},
};

/* HSM SM4-CMAC algorithms registration information */
static struct ccat_alg_template ccat_alg_cmac_sm4 = {
	.type = CRYPTO_ALG_TYPE_AHASH,
	.alg.ahash = {
		.init = ccat_ahash_init,
		.update = ccat_ahash_cmac_update,
		.final = ccat_ahash_final,
		.finup = ccat_ahash_finup,
		.digest = ccat_ahash_digest,
		.setkey = ccat_ahash_setkey,
		.export = ccat_ahash_export,
		.import = ccat_ahash_import,
		.halg = {
			.digestsize = SM4_BLOCK_SIZE,
			.statesize = sizeof(struct ccat_ahash_export_state),
			.base = {
				.cra_name = "cmac(sm4)",
				.cra_driver_name = "ccat-cmac-sm4",
				.cra_priority = 600,
				.cra_flags = CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
				.cra_blocksize = SM4_BLOCK_SIZE,
				.cra_ctxsize = sizeof(struct qat_alg_ahash_ctx),
				.cra_init = ccat_ahash_cra_init,
				.cra_exit = ccat_ahash_cra_exit,
				.cra_module = THIS_MODULE,
			},
		},
	},
	.hsm_type = ICP_HSM_TYPE_AUTH,
	.alg_info.auth_info = {
		.mode = ICP_HSM_SYM_AUTH_MODE_CMAC,
		.cipher_alg = ICP_HSM_SYM_CIPHER_ALG_SM4,
		.hash_alg = ICP_HSM_SYM_HASH_ALG_NULL,
	},
};

/* HSM supported algorithms */
static struct ccat_alg_template *ccat_algs[] = {
	/* cipher */
	&ccat_alg_ecb_des,  // PASS
	&ccat_alg_cbc_des,  // PASS
	&ccat_alg_ecb_des3_ede,  // PASS
	&ccat_alg_cbc_des3_ede,  // PASS
	&ccat_alg_ecb_aes,  // PASS
	&ccat_alg_cbc_aes,  // PASS
	&ccat_alg_cfb_aes,  // PASS
	&ccat_alg_ofb_aes,  // PASS
	&ccat_alg_ctr_aes,  // PASS
	&ccat_alg_rfc3686_ctr_aes,  // PASS (no AES192)
	&ccat_alg_ecb_sm4,  // PASS
	&ccat_alg_cbc_sm4,  // no test
	&ccat_alg_cfb_sm4,  // no test
	&ccat_alg_ofb_sm4,  // no test
	&ccat_alg_ctr_sm4,  // no test

	/* hash */
	&ccat_alg_md5,  // PASS
	&ccat_alg_sha1,  // PASS
	&ccat_alg_sha224,  // PASS
	&ccat_alg_sha256,  // PASS
	&ccat_alg_sha384,  // PASS
	&ccat_alg_sha512,  // PASS
	&ccat_alg_sm3,  // PASS

	/* auth */
	&ccat_alg_hmac_md5,  // PASS
	&ccat_alg_hmac_sha1,  // PASS
	&ccat_alg_hmac_sha224,  // PASS
	&ccat_alg_hmac_sha256,  // PASS
	&ccat_alg_hmac_sha384,  // PASS
	&ccat_alg_hmac_sha512,  // PASS
	&ccat_alg_hmac_sm3,  // no test
	&ccat_alg_cmac_aes,  // PASS (no AES192, no len=0)
	&ccat_alg_cmac_sm4,  // no test

	/* aead */
	&ccat_alg_gcm_aes,  // PASS (no AES192, no len=0)
	&ccat_alg_rfc4106_gcm_aes,  // PASS (no AES192, no len=0)
	&ccat_alg_gcm_sm4,  // no test
	&ccat_alg_ccm_aes,  // PASS (no AES192, no len=0, no taglen = 10)
	&ccat_alg_rfc4309_ccm_aes,  // PASS (no AES192, no len=0)
	&ccat_alg_authenc_hmac_sha1_cbc_aes,  // PASS (no AES192, no len=0)
	&ccat_alg_echainiv_authenc_hmac_sha1_cbc_aes,  // no test
	&ccat_alg_authenc_hmac_sha256_cbc_aes,  // PASS (no AES192, no len=0)
	&ccat_alg_echainiv_authenc_hmac_sha256_cbc_aes,  // no test
	&ccat_alg_authenc_hmac_sm3_cbc_sm4,  // no test
	&ccat_alg_authenc_hmac_sha1_cbc_sm4,  // no test
	&ccat_alg_authenc_hmac_sha256_cbc_sm4,  // no test
};

void qat_alg_callback(void *resp)
{
	struct icp_qat_fw_la_resp *qat_resp = resp;
	struct qat_crypto_request *qat_req =
		(void *)(__force long)qat_resp->opaque_data;

#ifdef ICP_DEBUG
	if(!qat_req) {
		printk("[CCAT] QAT sym callback = NULL \n");
	}
#endif

	qat_req->cb(qat_resp, qat_req);
}

int qat_algs_register(void)
{
	int i, j, ret = 0;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	mutex_lock(&algs_lock);
	if (++active_devs != 1)
		goto unlock;

	for (i = 0; i < ARRAY_SIZE(ccat_algs); i++) {
		if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_SKCIPHER) {
			ret = crypto_register_skcipher(&(ccat_algs[i]->alg.acipher));
		} else if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_AHASH) {
			ret = crypto_register_ahash(&(ccat_algs[i]->alg.ahash));
		} else if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_AEAD) {
			ret = crypto_register_aead(&(ccat_algs[i]->alg.aead));
		} else
			ret = -EINVAL;

		if (ret)
			goto fail;
	}

unlock:
	mutex_unlock(&algs_lock);
	return ret;

fail:
	for (j = 0; j < i; j++) {
		if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_SKCIPHER)
			crypto_unregister_skcipher(&(ccat_algs[i]->alg.acipher));
		else if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_AHASH)
			crypto_unregister_ahash(&(ccat_algs[i]->alg.ahash));
		else if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_AEAD)
			crypto_unregister_aead(&(ccat_algs[i]->alg.aead));
	}

	goto unlock;
}

void qat_algs_unregister(void)
{
	int i, ret = 0;
	mutex_lock(&algs_lock);
	if (--active_devs != 0)
		goto unlock;

	for (i = 0; i < ARRAY_SIZE(ccat_algs); i++) {
		if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_SKCIPHER)
			crypto_unregister_skcipher(&(ccat_algs[i]->alg.acipher));
		else if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_AHASH)
			crypto_unregister_ahash(&(ccat_algs[i]->alg.ahash));
		else if (ccat_algs[i]->type == CRYPTO_ALG_TYPE_AEAD)
			crypto_unregister_aead(&(ccat_algs[i]->alg.aead));
	}

unlock:
	mutex_unlock(&algs_lock);
}

#endif /* QAT_SKCIPHER_SUPPORTED */
