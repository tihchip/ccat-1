/* SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only) */
/* Copyright(c) 2014 - 2021 Intel Corporation */
#ifndef ADF_CFG_DEVICE_H_
#define ADF_CFG_DEVICE_H_

#include "adf_cfg_bundle.h"
#include "adf_cfg.h"
#include "adf_cfg_instance.h"
#include "adf_cfg_section.h"

#define ADF_CFG_FW_STRING_TO_ID(str, acc, id)	\
	do {	\
		typeof(id) id_ = (id);	\
		typeof(str) str_;	\
		memcpy(str_, (str), sizeof(str_));	\
		if (!strncmp(str_, ADF_SERVICES_DEFAULT,	\
				sizeof(ADF_SERVICES_DEFAULT)))	\
			*id_ = ADF_FW_IMAGE_DEFAULT;	\
		else if (!strncmp(str_, ADF_SERVICES_CRYPTO,	\
				sizeof(ADF_SERVICES_CRYPTO)))	\
			*id_ = ADF_FW_IMAGE_CRYPTO;	\
		else if (!strncmp(str_, ADF_SERVICES_COMPRESSION,	\
				sizeof(ADF_SERVICES_COMPRESSION)))	\
			*id_ = ADF_FW_IMAGE_COMPRESSION;	\
		else if (!strncmp(str_, ADF_SERVICES_CUSTOM1,	\
				sizeof(ADF_SERVICES_CUSTOM1)))	\
			*id_ = ADF_FW_IMAGE_CUSTOM1;	\
		else {	\
			*id_ = ADF_FW_IMAGE_DEFAULT;	\
			dev_warn(&GET_DEV(acc),	\
				"Invalid SerivesProfile: %s,"	\
				"Using DEFAULT image\n", str_);	\
		}	\
	} while (0)

struct adf_cfg_device {
	/* contains all the bundles info */
	struct adf_cfg_bundle **bundles;
	/* contains all the instances info */
	struct adf_cfg_instance **instances;
	int bundle_num;
	int instance_index;
	char name[ADF_CFG_MAX_STR_LEN];
	int dev_id;
	int max_kernel_bundle_nr;
	u16 total_num_inst;
};

int adf_cfg_get_ring_pairs(struct adf_cfg_device *device,
	struct adf_cfg_instance *inst,
	const char *process_name,
	struct adf_accel_dev *accel_dev);

int adf_cfg_device_init(struct adf_cfg_device *device,
	struct adf_accel_dev *accel_dev);

void adf_cfg_device_clear(struct adf_cfg_device *device,
	struct adf_accel_dev *accel_dev);

#endif
