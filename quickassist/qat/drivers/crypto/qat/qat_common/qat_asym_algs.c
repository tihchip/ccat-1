// SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only)
/* Copyright(c) 2014 - 2021 Intel Corporation */

#ifndef QAT_PKE_OLD_SUPPORTED
#include <linux/module.h>
#include <crypto/internal/rsa.h>
#include <crypto/internal/akcipher.h>
#include <crypto/akcipher.h>
#include <crypto/kpp.h>
#include <crypto/internal/kpp.h>
#include <crypto/dh.h>
#include <linux/dma-mapping.h>
#include <linux/fips.h>
#include <crypto/scatterwalk.h>
#include "icp_qat_fw_pke.h"
#include "adf_accel_devices.h"
#include "adf_transport.h"
#include "adf_transport_internal.h"
#include "adf_common_drv.h"
#include "qat_crypto.h"

#ifdef ICP_DEBUG
#define ccat_log(...)                                  \
	do {                                                  \
		printk(KERN_INFO __VA_ARGS__);                    \
	} while (0)
#define ccat_hexdump(msg, var, var_len)                \
	do {                                                  \
		print_hex_dump(KERN_INFO, msg, DUMP_PREFIX_OFFSET,  \
				16, 1, var, var_len, false);              \
	} while (0)
#else /* !DEBUG_ON */
#define ccat_log(...) do {} while (0)
#define ccat_hexdump(msg, var, var_len) do {} while (0)
#endif /* DEBUG_ON */


static DEFINE_MUTEX(algs_lock);
static unsigned int active_devs;

struct qat_asym_request;
struct qat_alg_random_ctx;

/* HWRNG Open/Close ctrl macro */
//#define CCAT_HWRNG
/* hwrng */
static struct qat_alg_random_ctx *ccat_rng_ctx;
/* Only support single request */
static struct qat_asym_request   *ccat_rng_req;

struct qat_rsa_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};

	char *n;
	char *e;
	char *d;
	char *p;
	char *q;
	char *dp;
	char *dq;
	char *qinv;
	dma_addr_t dma_n;
	dma_addr_t dma_e;
	dma_addr_t dma_d;
	dma_addr_t dma_p;
	dma_addr_t dma_q;
	dma_addr_t dma_dp;
	dma_addr_t dma_dq;
	dma_addr_t dma_qinv;
	unsigned int key_sz;
	bool crt_mode;
	unsigned int ebits;

	struct qat_crypto_instance *inst;
} __packed ____cacheline_aligned;

struct qat_dh_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};

	char *g;
	char *xa;
	char *p;
	dma_addr_t dma_g;
	dma_addr_t dma_xa;
	dma_addr_t dma_p;
	unsigned int p_size;
	bool g2;

	struct qat_crypto_instance *inst;
} __packed ____cacheline_aligned;

struct qat_ecdh_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};

	//...TODO

	struct qat_crypto_instance *inst;
} __packed ____cacheline_aligned;

struct qat_sm2_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};

	//...TODO

	struct qat_crypto_instance *inst;
} __packed ____cacheline_aligned;

struct qat_sm2ex_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};

	//...TODO

	struct qat_crypto_instance *inst;
} __packed ____cacheline_aligned;

struct ccat_rsa_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	u8                      in[512] ____cacheline_aligned;
	u8                      out[512] ____cacheline_aligned;;
	dma_addr_t              in_dma ____cacheline_aligned;;
	dma_addr_t              out_dma;
	dma_addr_t              ctx_hw_dma;
	u32                     rsa_op;
};

struct ccat_dh_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	//... TODO
};

struct ccat_sm2_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	//... TODO
};

struct ccat_ecdh_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	//... TODO
};

struct ccat_sm2ex_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	//... TODO
};

struct ccat_random_request {
	struct ccat_alg_req_ctx req_ctx;
	u32 rng_len;
};

struct qat_asym_request {
	union {
		struct ccat_rsa_request   ccat_rsa_req;
		struct ccat_dh_request    ccat_dh_req;
		struct ccat_sm2_request   ccat_sm2_req;
		struct ccat_ecdh_request  ccat_ecdh_req;
		struct ccat_sm2ex_request ccat_sm2ex_req;
		struct ccat_random_request ccat_rng_req;
	};

	union {
		struct qat_rsa_ctx *rsa_ctx;
		struct qat_dh_ctx *dh_ctx;
		struct qat_ecdh_ctx *ecdh_ctx;
		struct qat_sm2_ctx *sm2_ctx;
		struct qat_sm2ex_ctx *sm2ex_ctx;
		struct qat_alg_random_ctx *rng_ctx;
	};
	union {
		struct akcipher_request *rsa_req;
		struct akcipher_request *sm2_req;
		struct kpp_request *dh_req;
		struct kpp_request *ecdh_req;
		struct kpp_request *sm2ex_req;
	};

	char *src_align;
	char *dst_align;
	int err;
	void (*cb)(struct icp_qat_fw_pke_resp *resp);
} __aligned(64);


struct qat_alg_hwrng_ctx {
#define CCAT_RNG_DATA_MAX_SIZE         1024
	u8 buffer[CCAT_RNG_DATA_MAX_SIZE] ____cacheline_aligned;
	dma_addr_t buffer_dma ____cacheline_aligned;

	struct completion filled;

#define CCAT_RNG_BUF_NOT_EMPTY 0
#define CCAT_RNG_BUF_EMPTY     1
#define CCAT_RNG_BUF_PENDING   2
	atomic_t empty;
	u32 index;

	struct hwrng *rng;
};

struct qat_alg_random_ctx {
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	};

	struct qat_alg_hwrng_ctx hwrng_ctx;
	struct qat_crypto_instance *inst;
};

static inline struct akcipher_request *akcipher_request_cast(
	struct crypto_async_request *areq)
{
	return container_of(areq, struct akcipher_request, base);
}


/**
 * ccat_rsa_callback() - HSM RSA result handler callback function.
 * @resp: HSM message.
 *
 * Return: none
 */
static void ccat_rsa_callback(struct icp_qat_fw_pke_resp *resp)
{
	struct qat_asym_request *qat_req = (void *)(__force long)resp->opaque;
	struct akcipher_request *areq = qat_req->rsa_req;
	struct qat_rsa_ctx *rsa_ctx = qat_req->rsa_ctx;
	struct qat_crypto_instance *inst = qat_req->rsa_ctx->inst;
	struct device *dev = &GET_DEV(inst->accel_dev);
	u32 err =  ((struct icp_hsm_msg_resp *)resp)->err_code;
	u32 ctx_sz = 0;

	ccat_log("[CCAT] RSA result\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	switch (qat_req->ccat_rsa_req.rsa_op) {
	case ICP_HSM_PK_OP_RSA_ENCRYPT:
		ctx_sz = sizeof(struct icp_hsm_pk_rsa_encrypt_ctx_hw);
		break;
	case ICP_HSM_PK_OP_RSA_DECRYPT:
		ctx_sz = sizeof(struct icp_hsm_pk_rsa_decrypt_ctx_hw);
		break;
	case ICP_HSM_PK_OP_RSA_CRT_DECRYPT:
		ctx_sz = sizeof(struct icp_hsm_pk_rsa_crt_decrypt_ctx_hw);
		break;
	default:
		ctx_sz = 0;
		break;
	}

	if (qat_req->ccat_rsa_req.in_dma) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.in_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
		qat_req->ccat_rsa_req.in_dma = 0;
	}
	if (qat_req->ccat_rsa_req.out_dma) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.out_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
		qat_req->ccat_rsa_req.out_dma = 0;
	}
	if (qat_req->ccat_rsa_req.ctx_hw_dma) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.ctx_hw_dma, ctx_sz,
				DMA_BIDIRECTIONAL);
		qat_req->ccat_rsa_req.ctx_hw_dma = 0;
	}

	scatterwalk_map_and_copy(qat_req->ccat_rsa_req.out, areq->dst,
				0, areq->dst_len, 1);

	akcipher_request_complete(areq, err);
}



/**
 * ccat_rsa_enc() - HSM rsa encrypt function.
 * @req: Public key request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_rsa_enc(struct akcipher_request *req)
{
	struct crypto_akcipher *tfm = crypto_akcipher_reqtfm(req);
	struct qat_rsa_ctx *rsa_ctx = akcipher_tfm_ctx(tfm);
	struct qat_asym_request *qat_req = akcipher_request_ctx(req);
	struct icp_hsm_pk_rsa_encrypt_ctx_hw *ctx_hw =
			                     &(qat_req->ccat_rsa_req.req_ctx.ctx_hw.rsa_encrypt);
	struct device *dev = &GET_DEV(rsa_ctx->inst->accel_dev);
	struct icp_hsm_msg_req *msg;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] rsa enc, srclen: %d, dstlen: %d\n", req->src_len, req->dst_len);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (req->src_len > rsa_ctx->key_sz)
		return -EOVERFLOW;

	if (req->dst_len < rsa_ctx->key_sz) {
		req->dst_len = rsa_ctx->key_sz;
		return -EOVERFLOW;
	}

	qat_req->ccat_rsa_req.rsa_op = ICP_HSM_PK_OP_RSA_ENCRYPT;
	memset(qat_req->ccat_rsa_req.in, 0, rsa_ctx->key_sz);
	scatterwalk_map_and_copy(qat_req->ccat_rsa_req.in + (rsa_ctx->key_sz - req->src_len),
				req->src, 0, req->src_len, 0);

	qat_req->ccat_rsa_req.in_dma = dma_map_single(dev, qat_req->ccat_rsa_req.in,
			rsa_ctx->key_sz, DMA_BIDIRECTIONAL);
	if (unlikely(dma_mapping_error(dev, qat_req->ccat_rsa_req.in_dma))) {
		ret = -ENOMEM;
		goto err_in;
	}

	qat_req->ccat_rsa_req.out_dma = dma_map_single(dev, qat_req->ccat_rsa_req.out,
			rsa_ctx->key_sz, DMA_BIDIRECTIONAL);
	if (unlikely(dma_mapping_error(dev, qat_req->ccat_rsa_req.out_dma))) {
		ret = -ENOMEM;
		goto err_out;
	}

	ctx_hw->n_bits = rsa_ctx->key_sz * 8;
	ctx_hw->n_addr = rsa_ctx->dma_n;
	ctx_hw->e_bits = rsa_ctx->ebits;
	ctx_hw->e_addr = rsa_ctx->dma_e +
		(rsa_ctx->key_sz - ((rsa_ctx->ebits+7)/8));
	ctx_hw->m_addr = qat_req->ccat_rsa_req.in_dma;
	ctx_hw->c_addr = qat_req->ccat_rsa_req.out_dma;

	qat_req->ccat_rsa_req.ctx_hw_dma = dma_map_single(dev,
			&(qat_req->ccat_rsa_req.req_ctx.ctx_hw.rsa_encrypt),
			sizeof(struct icp_hsm_pk_rsa_encrypt_ctx_hw), DMA_BIDIRECTIONAL);
	if (unlikely(dma_mapping_error(dev, qat_req->ccat_rsa_req.ctx_hw_dma))) {
		ret = -ENOMEM;
		goto err_ctx;
	}

	msg = &qat_req->ccat_rsa_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.pk_hdr, &rsa_ctx->msg.hdr.pk_hdr,
			sizeof(struct icp_hsm_pk_header));
	msg->hdr.pk_hdr.pk_op = ICP_HSM_PK_OP_RSA_ENCRYPT;
	msg->ctx_addr = qat_req->ccat_rsa_req.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_rsa_req.req_ctx.alg = ICP_HSM_TYPE_PK_RSA_ENCRYPT;
	qat_req->rsa_req = req;
	qat_req->rsa_ctx = rsa_ctx;
	qat_req->cb = ccat_rsa_callback;

	do {
		ret = adf_send_message(rsa_ctx->inst->pke_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		goto err_ctx;
	}

#ifdef ICP_DEBUG
	int i;
	printk("ring_id: %d\n", rsa_ctx->inst->pke_tx->ring_number);
	for (i = 0; i < 16; i++)
		printk("ctx[%d]: 0x%x\n", i, ((u32 *)ctx_hw)[i]);
#endif
	return -EINPROGRESS;

err_ctx:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.ctx_hw_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.ctx_hw_dma,
						sizeof(struct icp_hsm_pk_rsa_encrypt_ctx_hw),
						DMA_BIDIRECTIONAL);
	}
err_out:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.out_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.out_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
	}

err_in:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.in_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.in_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
	}

	dev_err(dev, "ERR[RSA]# Failed to map buf of HSM for dma \n");
     return ret;

}

/**
 * ccat_rsa_crt_dec() - HSM rsa-crt decrypt function.
 * @req: Public key request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_rsa_crt_dec(struct akcipher_request *req)
{
	struct crypto_akcipher *tfm = crypto_akcipher_reqtfm(req);
	struct qat_rsa_ctx *rsa_ctx = akcipher_tfm_ctx(tfm);
	struct qat_asym_request *qat_req = akcipher_request_ctx(req);
	struct icp_hsm_pk_rsa_crt_decrypt_ctx_hw *ctx_hw =
			&(qat_req->ccat_rsa_req.req_ctx.ctx_hw.rsa_crt_decrypt);
	struct device *dev = &GET_DEV(rsa_ctx->inst->accel_dev);
	struct icp_hsm_msg_req *msg;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] rsa-crt dec, srclen: %d, dstlen: %d\n", req->src_len, req->dst_len);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	qat_req->ccat_rsa_req.rsa_op = ICP_HSM_PK_OP_RSA_CRT_DECRYPT;
	memset(qat_req->ccat_rsa_req.in, 0, rsa_ctx->key_sz);
	scatterwalk_map_and_copy(qat_req->ccat_rsa_req.in + (rsa_ctx->key_sz - req->src_len),
				req->src, 0, req->src_len, 0);

	qat_req->ccat_rsa_req.in_dma = dma_map_single(dev, qat_req->ccat_rsa_req.in,
			rsa_ctx->key_sz, DMA_BIDIRECTIONAL);
	if (dma_mapping_error(dev, qat_req->ccat_rsa_req.in_dma)) {
		ret = -ENOMEM;
		goto err_in;
	}
	qat_req->ccat_rsa_req.out_dma = dma_map_single(dev, qat_req->ccat_rsa_req.out,
			rsa_ctx->key_sz, DMA_BIDIRECTIONAL);
	if (dma_mapping_error(dev, qat_req->ccat_rsa_req.out_dma)) {
		ret = -ENOMEM;
		goto err_out;
	}

	ctx_hw->n_bits = rsa_ctx->key_sz * 8;
	ctx_hw->p_addr = rsa_ctx->dma_p;
	ctx_hw->q_addr = rsa_ctx->dma_q;
	ctx_hw->dp_addr = rsa_ctx->dma_dp;
	ctx_hw->dq_addr = rsa_ctx->dma_dq;
	ctx_hw->u_addr = rsa_ctx->dma_qinv;
	ctx_hw->c_addr = qat_req->ccat_rsa_req.in_dma;
	ctx_hw->m_addr = qat_req->ccat_rsa_req.out_dma;

	qat_req->ccat_rsa_req.ctx_hw_dma = dma_map_single(dev,
			&(qat_req->ccat_rsa_req.req_ctx.ctx_hw.rsa_crt_decrypt),
			sizeof(struct icp_hsm_pk_rsa_crt_decrypt_ctx_hw), DMA_BIDIRECTIONAL);
	if (dma_mapping_error(dev, qat_req->ccat_rsa_req.ctx_hw_dma)) {
		ret = -ENOMEM;
		goto err_ctx;
	}

	msg = &qat_req->ccat_rsa_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.pk_hdr, &msg->hdr.pk_hdr, sizeof(struct icp_hsm_pk_header));
	msg->hdr.pk_hdr.pk_op = ICP_HSM_PK_OP_RSA_CRT_DECRYPT;
	msg->ctx_addr = qat_req->ccat_rsa_req.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_rsa_req.req_ctx.alg = ICP_HSM_TYPE_PK_RSA_CRT_DECRYPT;
	qat_req->rsa_req = req;
	qat_req->rsa_ctx = rsa_ctx;
	qat_req->cb = ccat_rsa_callback;

	do {
		ret = adf_send_message(rsa_ctx->inst->pke_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		goto err_ctx;
	}

#ifdef ICP_DEBUG
	int i;
	printk("ring_id: %d\n", rsa_ctx->inst->pke_tx->ring_number);
	for (i = 0; i < 16; i++)
		printk("ctx[%d]: 0x%x\n", i, ((u32 *)ctx_hw)[i]);
#endif
	return -EINPROGRESS;

err_ctx:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.ctx_hw_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.ctx_hw_dma,
				sizeof(struct icp_hsm_pk_rsa_crt_decrypt_ctx_hw),
				DMA_BIDIRECTIONAL);
	}
err_out:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.out_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.out_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
	}

err_in:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.in_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.in_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
	}

	dev_err(dev, "ERR[RSA]# Failed to map buf of HSM for dma \n");
	return ret;
}

/**
 * ccat_rsa_dec() - HSM rsa decrypt function.
 * @req: Public key request data.
 *
 * Return:
 * * 0            - Success.
 * * -EINPROGRESS - Operation now in progress(Asynchronous operation succeeded).
 * * -ESRCH       - No such process.
 * * -EINVAL      - Failure.
 */
static int ccat_rsa_dec(struct akcipher_request *req)
{
	struct crypto_akcipher *tfm = crypto_akcipher_reqtfm(req);
	struct qat_rsa_ctx *rsa_ctx = akcipher_tfm_ctx(tfm);
	struct qat_asym_request *qat_req = akcipher_request_ctx(req);
	struct icp_hsm_pk_rsa_decrypt_ctx_hw *ctx_hw =
			&(qat_req->ccat_rsa_req.req_ctx.ctx_hw.rsa_decrypt);
	struct device *dev = &GET_DEV(rsa_ctx->inst->accel_dev);
	struct icp_hsm_msg_req *msg;
	int ret = 0;
	int ctr = 0;

	ccat_log("[CCAT] rsa dec, srclen: %d, dstlen: %d\n", req->src_len, req->dst_len);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if (req->src_len > rsa_ctx->key_sz)
		return -EOVERFLOW;

	if (req->dst_len < rsa_ctx->key_sz) {
		req->dst_len = rsa_ctx->key_sz;
		return -EOVERFLOW;
	}

	if (rsa_ctx->crt_mode)
		return ccat_rsa_crt_dec(req);

	qat_req->ccat_rsa_req.rsa_op = ICP_HSM_PK_OP_RSA_DECRYPT;
	memset(qat_req->ccat_rsa_req.in, 0, rsa_ctx->key_sz);
	scatterwalk_map_and_copy(qat_req->ccat_rsa_req.in + (rsa_ctx->key_sz - req->src_len),
				req->src, 0, req->src_len, 0);

	qat_req->ccat_rsa_req.in_dma = dma_map_single(dev, qat_req->ccat_rsa_req.in,
			rsa_ctx->key_sz, DMA_BIDIRECTIONAL);
	if (unlikely(dma_mapping_error(dev, qat_req->ccat_rsa_req.in_dma))) {
		ret = -ENOMEM;
		goto err_in;
	}
	qat_req->ccat_rsa_req.out_dma = dma_map_single(dev, qat_req->ccat_rsa_req.out,
			rsa_ctx->key_sz, DMA_BIDIRECTIONAL);
	if (unlikely(dma_mapping_error(dev, qat_req->ccat_rsa_req.out_dma))) {
		ret = -ENOMEM;
		goto err_out;
	}

	ctx_hw->n_bits = rsa_ctx->key_sz * 8;
	ctx_hw->n_addr = rsa_ctx->dma_n;
	ctx_hw->d_addr = rsa_ctx->dma_d;
	ctx_hw->c_addr = qat_req->ccat_rsa_req.in_dma;
	ctx_hw->m_addr = qat_req->ccat_rsa_req.out_dma;

	qat_req->ccat_rsa_req.ctx_hw_dma = dma_map_single(dev,
			&(qat_req->ccat_rsa_req.req_ctx.ctx_hw.rsa_decrypt),
			sizeof(struct icp_hsm_pk_rsa_decrypt_ctx_hw), DMA_BIDIRECTIONAL);
	if (unlikely(dma_mapping_error(dev, qat_req->ccat_rsa_req.ctx_hw_dma))) {
		ret = -ENOMEM;
		goto err_ctx;
	}

	msg = &qat_req->ccat_rsa_req.req_ctx.msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	memcpy(&msg->hdr.pk_hdr, &rsa_ctx->msg.hdr.pk_hdr, sizeof(struct icp_hsm_pk_header));
	msg->hdr.pk_hdr.pk_op = ICP_HSM_PK_OP_RSA_DECRYPT;
	msg->ctx_addr = qat_req->ccat_rsa_req.ctx_hw_dma;
	msg->s.opaque_data = (u64)qat_req;

	qat_req->ccat_rsa_req.req_ctx.alg = ICP_HSM_TYPE_PK_RSA_DECRYPT;
	qat_req->rsa_req = req;
	qat_req->rsa_ctx = rsa_ctx;
	qat_req->cb = ccat_rsa_callback;

	do {
		ret = adf_send_message(rsa_ctx->inst->pke_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	if (ret == -EAGAIN) {
		goto err_ctx;
	}

#ifdef ICP_DEBUG
	int i;
	printk("ring_id: %d\n", rsa_ctx->inst->pke_tx->ring_number);
	for (i = 0; i < 16; i++)
		printk("ctx[%d]: 0x%x\n", i, ((u32 *)ctx_hw)[i]);
#endif
	return -EINPROGRESS;

err_ctx:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.ctx_hw_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.ctx_hw_dma,
				sizeof(struct icp_hsm_pk_rsa_decrypt_ctx_hw),
				DMA_BIDIRECTIONAL);
	}
err_out:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.out_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.out_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
	}

err_in:
	if (!dma_mapping_error(dev, qat_req->ccat_rsa_req.in_dma)) {
		dma_unmap_single(dev, qat_req->ccat_rsa_req.in_dma, rsa_ctx->key_sz,
				DMA_BIDIRECTIONAL);
	}

	dev_err(dev, "ERR[RSA]# Failed to map buf of HSM for dma \n");
	return ret;
}

/**
 * ccat_rsa_ctx_clear() - Clear rsa context.
 * @dev: Device structure pointer.
 * @ctx: HSM rsa algorithm context.
 */
static void ccat_rsa_ctx_clear(struct device *dev, struct qat_rsa_ctx *ctx)
{
	u32 half_key_sz = ctx->key_sz / 2;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	/* Free the old key if any */
	if (ctx->n)
		dma_free_coherent(dev, ctx->key_sz, ctx->n, ctx->dma_n);
	if (ctx->e)
		dma_free_coherent(dev, ctx->key_sz, ctx->e, ctx->dma_e);
	if (ctx->d) {
		memset(ctx->d, '\0', ctx->key_sz);
		dma_free_coherent(dev, ctx->key_sz, ctx->d, ctx->dma_d);
	}
	if (ctx->p) {
		memset(ctx->p, '\0', half_key_sz);
		dma_free_coherent(dev, half_key_sz, ctx->p, ctx->dma_p);
	}
	if (ctx->q) {
		memset(ctx->q, '\0', half_key_sz);
		dma_free_coherent(dev, half_key_sz, ctx->q, ctx->dma_q);
	}
	if (ctx->dp) {
		memset(ctx->dp, '\0', half_key_sz);
		dma_free_coherent(dev, half_key_sz, ctx->dp, ctx->dma_dp);
	}
	if (ctx->dq) {
		memset(ctx->dq, '\0', half_key_sz);
		dma_free_coherent(dev, half_key_sz, ctx->dq, ctx->dma_dq);
	}
	if (ctx->qinv) {
		memset(ctx->qinv, '\0', half_key_sz);
		dma_free_coherent(dev, half_key_sz, ctx->qinv, ctx->dma_qinv);
	}

	ctx->n = NULL;
	ctx->e = NULL;
	ctx->d = NULL;
	ctx->p = NULL;
	ctx->q = NULL;
	ctx->dp = NULL;
	ctx->dq = NULL;
	ctx->qinv = NULL;
	ctx->crt_mode = false;
	ctx->key_sz = 0;
}

/**
 * ccat_rsa_n_set() - Set n in rsa context.
 * @dev:   Device structure pointer.
 * @ctx:   HSM rsa algorithm context.
 * @value: Value.
 * @vlen:  Value length.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_rsa_n_set(struct device *dev, struct qat_rsa_ctx *ctx,
			const char *value, size_t vlen)
{
	const char *ptr = value;
	u32 bitslen;
	int ret;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	while (!*ptr && vlen) {
		ptr++;
		vlen--;
	}

	ctx->key_sz = vlen;
	ret = -EINVAL;
	/* invalid key size provided */
	bitslen = ctx->key_sz << 3;  // TODO
	if (bitslen != 1024 && bitslen != 2048 && bitslen != 4096)
		goto err;

	ret = -ENOMEM;
	ctx->n = dma_alloc_coherent(dev, ctx->key_sz, &ctx->dma_n, GFP_KERNEL);
	if (!ctx->n)
		goto err;

	memcpy(ctx->n, ptr, ctx->key_sz);
	return 0;
err:
	ctx->key_sz = 0;
	ctx->n = NULL;
	return ret;
}

/**
 * ccat_rsa_e_set() - Set e in rsa context.
 * @dev:   Device structure pointer.
 * @ctx:   HSM rsa algorithm context.
 * @value: Value.
 * @vlen:  Value length.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_rsa_e_set(struct device *dev, struct qat_rsa_ctx *ctx,
			const char *value, size_t vlen)
{
	const char *ptr = value;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	while (!*ptr && vlen) {
		ptr++;
		vlen--;
	}

	if (!ctx->key_sz || !vlen || vlen > ctx->key_sz) {
		ctx->e = NULL;
		return -EINVAL;
	}

	ctx->e = dma_alloc_coherent(dev, ctx->key_sz, &ctx->dma_e, GFP_KERNEL);
	if (!ctx->e)
		return -ENOMEM;

	ctx->ebits = vlen*8;
	memcpy(ctx->e + (ctx->key_sz - vlen), ptr, vlen);
	return 0;
}

/**
 * ccat_rsa_d_set() - Set d in rsa context.
 * @dev:   Device structure pointer.
 * @ctx:   HSM rsa algorithm context.
 * @value: Value.
 * @vlen:  Value length.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_rsa_d_set(struct device *dev, struct qat_rsa_ctx *ctx,
			const char *value, size_t vlen)
{
	const char *ptr = value;
	int ret;

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	while (!*ptr && vlen) {
		ptr++;
		vlen--;
	}

	ret = -EINVAL;
	if (!ctx->key_sz || !vlen || vlen > ctx->key_sz)
		goto err;

	ret = -ENOMEM;
	ctx->d = dma_alloc_coherent(dev, ctx->key_sz, &ctx->dma_d, GFP_KERNEL);
	if (!ctx->d)
		goto err;

	memcpy(ctx->d + (ctx->key_sz - vlen), ptr, vlen);
	return 0;
err:
	ctx->d = NULL;
	return ret;
}

/**
 * ccat_rsa_crt_setkey() - Set rsa-crt key in rsa context.
 * @dev:     Device structure pointer.
 * @ctx:     HSM rsa algorithm context.
 * @rsa_key: rsa key.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static void ccat_rsa_crt_setkey(struct device *dev, struct qat_rsa_ctx *ctx,
								struct rsa_key *rsa_key)
{
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	const char *ptr;
	u32 len;
	u32 half_key_sz = ctx->key_sz / 2;

	/* p */
	ptr = rsa_key->p;
	len = rsa_key->p_sz;
	while (!*ptr && len) {
		ptr++;
		len--;
	}
	if (!len)
		goto err;
	ctx->p = dma_alloc_coherent(dev, half_key_sz, &ctx->dma_p, GFP_KERNEL);
	if (!ctx->p)
		goto err;
	memcpy(ctx->p + (half_key_sz - len), ptr, len);

	/* q */
	ptr = rsa_key->q;
	len = rsa_key->q_sz;
	while (!*ptr && len) {
		ptr++;
		len--;
	}
	if (!len)
		goto free_p;
	ctx->q = dma_alloc_coherent(dev, half_key_sz, &ctx->dma_q, GFP_KERNEL);
	if (!ctx->q)
		goto free_p;
	memcpy(ctx->q + (half_key_sz - len), ptr, len);

	/* dp */
	ptr = rsa_key->dp;
	len = rsa_key->dp_sz;
	while (!*ptr && len) {
		ptr++;
		len--;
	}
	if (!len)
		goto free_q;
	ctx->dp = dma_alloc_coherent(dev, half_key_sz, &ctx->dma_dp, GFP_KERNEL);
	if (!ctx->dp)
		goto free_q;
	memcpy(ctx->dp + (half_key_sz - len), ptr, len);

	/* dq */
	ptr = rsa_key->dq;
	len = rsa_key->dq_sz;
	while (!*ptr && len) {
		ptr++;
		len--;
	}
	if (!len)
		goto free_dp;
	ctx->dq = dma_alloc_coherent(dev, half_key_sz, &ctx->dma_dq, GFP_KERNEL);
	if (!ctx->dq)
		goto free_dp;
	memcpy(ctx->dq + (half_key_sz - len), ptr, len);

	/* qinv */
	ptr = rsa_key->qinv;
	len = rsa_key->qinv_sz;
	while (!*ptr && len) {
		ptr++;
		len--;
	}
	if (!len)
		goto free_dq;
	ctx->qinv = dma_alloc_coherent(dev, half_key_sz, &ctx->dma_qinv,
					GFP_KERNEL);
	if (!ctx->qinv)
		goto free_dq;
	memcpy(ctx->qinv + (half_key_sz - len), ptr, len);

	ctx->crt_mode = true;
	return;

free_dq:
	memset(ctx->dq, '\0', half_key_sz);
	dma_free_coherent(dev, half_key_sz, ctx->dq, ctx->dma_dq);
	ctx->dq = NULL;
free_dp:
	memset(ctx->dp, '\0', half_key_sz);
	dma_free_coherent(dev, half_key_sz, ctx->dp, ctx->dma_dp);
	ctx->dp = NULL;
free_q:
	memset(ctx->q, '\0', half_key_sz);
	dma_free_coherent(dev, half_key_sz, ctx->q, ctx->dma_q);
	ctx->q = NULL;
free_p:
	memset(ctx->p, '\0', half_key_sz);
	dma_free_coherent(dev, half_key_sz, ctx->p, ctx->dma_p);
	ctx->p = NULL;
err:
	ctx->crt_mode = false;
}

/**
 * ccat_rsa_setkey() - Set rsa key in rsa context.
 * @tfm:     User-instantiated objects which encapsulate.
 * @key:     rsa key.
 * @keylen:  rsa key length.
 * @private: prikey flag.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_rsa_setkey(struct crypto_akcipher *tfm, const void *key,
				unsigned int keylen, bool private)
{
	struct qat_rsa_ctx *rsa_ctx = akcipher_tfm_ctx(tfm);
	struct device *dev = &GET_DEV(rsa_ctx->inst->accel_dev);
	struct rsa_key rsa_key;
	int ret = 0;

	ccat_log("[CCAT] rsa set key(%d) len: %d\n", private, keylen);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	ccat_rsa_ctx_clear(dev, rsa_ctx);

	if (private)
		ret = rsa_parse_priv_key(&rsa_key, key, keylen);
	else
		ret = rsa_parse_pub_key(&rsa_key, key, keylen);
	if (ret < 0)
		goto free;

	ret = ccat_rsa_n_set(dev, rsa_ctx, rsa_key.n, rsa_key.n_sz);
	if (ret < 0)
		goto free;
	ret = ccat_rsa_e_set(dev, rsa_ctx, rsa_key.e, rsa_key.e_sz);
	if (ret < 0)
		goto free;
	if (private) {
		ret = ccat_rsa_d_set(dev, rsa_ctx, rsa_key.d, rsa_key.d_sz);
		if (ret < 0)
			goto free;
		ccat_rsa_crt_setkey(dev, rsa_ctx, &rsa_key);
	}

	return 0;
free:
	ccat_rsa_ctx_clear(dev, rsa_ctx);
	return ret;
}

/**
 * ccat_rsa_setpubkey() - HSM rsa set pubkey function.
 * @tfm:    User-instantiated objects which encapsulate.
 * @key:    Key address.
 * @keylen: Key length.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_rsa_setpubkey(struct crypto_akcipher *tfm, const void *key,
				unsigned int keylen)
{
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
	return ccat_rsa_setkey(tfm, key, keylen, false);
}

/**
 * ccat_rsa_setprivkey() - HSM rsa set prikey function.
 * @tfm:    User-instantiated objects which encapsulate.
 * @key:    Key address.
 * @keylen: Key length.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_rsa_setprivkey(struct crypto_akcipher *tfm, const void *key,
				unsigned int keylen)
{
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	return ccat_rsa_setkey(tfm, key, keylen, true);
}

/**
 * ccat_rsa_size_max() - HSM rsa set max size function.
 * @tfm:    User-instantiated objects which encapsulate.
 *
 * Return:
 * * unsigned int - rsa max key size.
 */
static unsigned int ccat_rsa_size_max(struct crypto_akcipher *tfm)
{
	struct qat_rsa_ctx *ctx = akcipher_tfm_ctx(tfm);

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	return ctx->key_sz;
}

/**
 * ccat_rsa_init() - HSM rsa init function.
 * @tfm:    User-instantiated objects which encapsulate.
 *
 * Return:
 * * 0       - Success.
 * * -ENOMEM - Out of memory
 * * -EINVAL - Failure.
 */
static int ccat_rsa_init(struct crypto_akcipher *tfm)
{
	struct qat_rsa_ctx *ctx = akcipher_tfm_ctx(tfm);
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	ccat_log("[CCAT] rsa init\n");
	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return -EINVAL;

	ctx->inst = inst;
	ctx->msg.hdr.pk_hdr.service_type = ICP_HSM_TYPE_PK;
	ctx->msg.hdr.pk_hdr.first = 1;
	ctx->msg.hdr.pk_hdr.last = 1;

	return 0;
}

/**
 * ccat_rsa_exit() - HSM rsa exit function.
 * @tfm:    User-instantiated objects which encapsulate.
 */
static void ccat_rsa_exit(struct crypto_akcipher *tfm)
{
	struct qat_rsa_ctx *ctx = akcipher_tfm_ctx(tfm);

	ccat_log("[CCAT] rsa exit\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	memzero_explicit(ctx, sizeof(struct qat_rsa_ctx));
}

static void ccat_rng_callback(struct icp_qat_fw_pke_resp *qat_resp)
{
	struct qat_asym_request *qat_req = (void *)(__force long)qat_resp->opaque;
	struct qat_alg_random_ctx *rng_ctx = qat_req->rng_ctx;

	ccat_log("[CCAT] rng result\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	/* Buffer refilled, invalidate cache */
	dma_sync_single_for_cpu(&GET_DEV(rng_ctx->inst->accel_dev), rng_ctx->hwrng_ctx.buffer_dma,
			CCAT_RNG_DATA_MAX_SIZE, DMA_FROM_DEVICE);

	atomic_set(&rng_ctx->hwrng_ctx.empty, CCAT_RNG_BUF_NOT_EMPTY);
	complete(&rng_ctx->hwrng_ctx.filled);
}

static int ccat_rng_data_get(struct qat_alg_random_ctx *rng_ctx)
{
	int ret = 0;
	int ctr = 0;
	struct icp_hsm_msg_req *msg;

	msg = &rng_ctx->msg;
	memset((uint8_t *)msg, 0, sizeof(struct icp_hsm_msg_req));
	msg->hdr.ctrl_hdr.service_type = ICP_HSM_TYPE_CTRL;
	msg->hdr.ctrl_hdr.first = 1;
	msg->hdr.ctrl_hdr.last = 1;
	msg->hdr.ctrl_hdr.cmd_id = ICP_HSM_CTRL_CMD_RAND_GET;
	msg->output_addr = rng_ctx->hwrng_ctx.buffer_dma;
	msg->output_length = CCAT_RNG_DATA_MAX_SIZE;
	msg->s.opaque_data = (u64)ccat_rng_req;

	/* Rng don't need hardware ctx, and only callback for software */
	ccat_rng_req->rng_ctx = rng_ctx;
	ccat_rng_req->cb = ccat_rng_callback;

	do {
		ret = adf_send_message(ccat_rng_req->rng_ctx->inst->pke_tx, (uint32_t *)msg);
	} while (ret == -EAGAIN && ctr++ < 10);

	return ret;
}

static int ccat_rng_req_summit(struct qat_alg_random_ctx *rng_ctx)
{
	int ret = 0;

	init_completion(&rng_ctx->hwrng_ctx.filled);

	ret = ccat_rng_data_get(rng_ctx);

	if(ret) {
		complete(&rng_ctx->hwrng_ctx.filled); /* don't wait on failed job*/
	} else {
		atomic_set(&rng_ctx->hwrng_ctx.empty, CCAT_RNG_BUF_PENDING); /* note if pending */
	}

	return ret;
}

static int ccat_rng_read(struct hwrng *rng, void *data, size_t max, bool wait)
{
	u32 copy_len = 0;
	ccat_log("[CCAT] rng read max: %d\n", (u32)max);
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	/* Enough data > max */
	if ((atomic_read(&ccat_rng_ctx->hwrng_ctx.empty) == CCAT_RNG_BUF_NOT_EMPTY)
			&& (max < (CCAT_RNG_DATA_MAX_SIZE - ccat_rng_ctx->hwrng_ctx.index))) {
		memcpy(data, ccat_rng_ctx->hwrng_ctx.buffer + ccat_rng_ctx->hwrng_ctx.index, max);
		ccat_rng_ctx->hwrng_ctx.index += max;

		return max;
	}

	/* Little Data <= max */
	if((atomic_read(&ccat_rng_ctx->hwrng_ctx.empty) == CCAT_RNG_BUF_NOT_EMPTY)
			&& ((CCAT_RNG_DATA_MAX_SIZE - ccat_rng_ctx->hwrng_ctx.index) > 0)) {

		copy_len = CCAT_RNG_DATA_MAX_SIZE - ccat_rng_ctx->hwrng_ctx.index;
		memcpy(data, ccat_rng_ctx->hwrng_ctx.buffer + ccat_rng_ctx->hwrng_ctx.index, copy_len);
	}

	/* Send new command and buffer empty */
	ccat_rng_ctx->hwrng_ctx.index = 0;
	atomic_set(&ccat_rng_ctx->hwrng_ctx.empty, CCAT_RNG_BUF_EMPTY);
	ccat_rng_req_summit(ccat_rng_ctx);

	/* is wait? */
	if (!wait) {
		return copy_len;
	}

	wait_for_completion(&ccat_rng_ctx->hwrng_ctx.filled);

	return copy_len + ccat_rng_read(rng, (u8 *)data + copy_len, max - copy_len, wait);
}

static int ccat_rng_init(struct hwrng *rng)
{
	int ret = 0;
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();

	ccat_log("[CCAT] rng init\n");
#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return -EINVAL;

	ccat_rng_ctx = NULL;
	ccat_rng_ctx = kzalloc(sizeof(struct qat_alg_random_ctx), GFP_DMA | GFP_KERNEL);
	if (unlikely(!ccat_rng_ctx)) {
		ret = -ENOMEM;
		goto err_buffer;
	}

	ccat_rng_req = NULL;
	ccat_rng_req = kzalloc(sizeof(struct qat_crypto_request), GFP_DMA | GFP_KERNEL);
	if (unlikely(!ccat_rng_req)) {
		ret = -ENOMEM;
		goto err_buffer;
	}

	atomic_set(&ccat_rng_ctx->hwrng_ctx.empty, CCAT_RNG_BUF_EMPTY);

	ccat_rng_ctx->inst = inst;
	ccat_rng_ctx->hwrng_ctx.rng = rng;

	/* Map buffer */
	ccat_rng_ctx->hwrng_ctx.buffer_dma = dma_map_single(&GET_DEV(inst->accel_dev),
									(u8 *)ccat_rng_ctx->hwrng_ctx.buffer,
									CCAT_RNG_DATA_MAX_SIZE,
									DMA_FROM_DEVICE);
	if (unlikely(dma_mapping_error(&GET_DEV(inst->accel_dev), ccat_rng_ctx->hwrng_ctx.buffer_dma))) {
		ret = -EINVAL;
		goto err_buffer;
	}

	/* Init RNG request */
	ccat_rng_req->ccat_rng_req.rng_len = CCAT_RNG_DATA_MAX_SIZE;
	ccat_rng_req->ccat_rng_req.req_ctx.alg = ICP_HSM_TYPE_CTRL_RNG;
	ccat_rng_req->cb = ccat_rng_callback;
	ccat_rng_req->rng_ctx = ccat_rng_ctx;

	ret = ccat_rng_req_summit(ccat_rng_ctx);
	if(ret) {
		ret = -EAGAIN;
		goto err_req;
	}
	wait_for_completion(&ccat_rng_ctx->hwrng_ctx.filled);

	return 0;

err_req:
	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), ccat_rng_ctx->hwrng_ctx.buffer_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), ccat_rng_ctx->hwrng_ctx.buffer_dma,
				CCAT_RNG_DATA_MAX_SIZE,
				DMA_FROM_DEVICE);
	}
err_buffer:
	if(ccat_rng_ctx) {
		kfree(ccat_rng_ctx);
	}
	if(ccat_rng_req) {
		kfree(ccat_rng_req);
	}

	return ret;
}

static void ccat_rng_cleanup(struct hwrng *rng)
{
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return;

	ccat_log("[CCAT] rng cleanup\n");
	if (atomic_read(&ccat_rng_ctx->hwrng_ctx.empty) == CCAT_RNG_BUF_PENDING)
		wait_for_completion(&ccat_rng_ctx->hwrng_ctx.filled);

	if (!dma_mapping_error(&GET_DEV(inst->accel_dev), ccat_rng_ctx->hwrng_ctx.buffer_dma)) {
		dma_unmap_single(&GET_DEV(inst->accel_dev), ccat_rng_ctx->hwrng_ctx.buffer_dma,
				CCAT_RNG_DATA_MAX_SIZE, DMA_FROM_DEVICE);
	}

	if(ccat_rng_ctx) {
		kfree(ccat_rng_ctx);
	}
	if(ccat_rng_req) {
		kfree(ccat_rng_req);
	}
}

static int ccat_hwrng_register(void)
{
	int ret = 0;
	struct hwrng *p_rng = NULL;
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return -EINVAL;

	p_rng = &inst->accel_dev->accel_platform_dev.rng;


	p_rng->name = "ccat-rng",
	p_rng->init = ccat_rng_init,
	p_rng->cleanup = ccat_rng_cleanup,
	p_rng->read = ccat_rng_read,
	p_rng->quality = 1024;

	ret = devm_hwrng_register(&GET_DEV(inst->accel_dev), p_rng);

	return ret;
}

static int ccat_hwrng_unregister(void)
{
	struct hwrng *p_rng = NULL;
	struct qat_crypto_instance *inst = NULL;
	int node = get_current_node();

#ifdef ICP_DEBUG
	printk("%s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif

	inst = qat_crypto_get_instance_node(node);
	if (!inst)
		return -EINVAL;

	devm_hwrng_unregister(&GET_DEV(inst->accel_dev), ccat_rng_ctx->hwrng_ctx.rng);

	return 0;
}

/* HSM rsa algorithms registration information */
static struct akcipher_alg ccat_alg_rsa = {
	.encrypt = ccat_rsa_enc,
	.decrypt = ccat_rsa_dec,
	.sign = ccat_rsa_dec,
	.verify = ccat_rsa_enc,
	.set_pub_key = ccat_rsa_setpubkey,
	.set_priv_key = ccat_rsa_setprivkey,
	.max_size = ccat_rsa_size_max,
	.init = ccat_rsa_init,
	.exit = ccat_rsa_exit,
	.reqsize = ALIGN(sizeof(struct qat_asym_request), L1_CACHE_BYTES),
	.base = {
		.cra_name = "rsa",
		.cra_driver_name = "ccat-rsa",
		.cra_priority = 600,
		.cra_module = THIS_MODULE,
		.cra_ctxsize = sizeof(struct qat_rsa_ctx),
	},
};

void qat_alg_asym_callback(void *_resp)
{
	struct icp_qat_fw_pke_resp *resp = _resp;
	struct qat_asym_request *areq = (void *)(__force long)resp->opaque;

#ifdef ICP_DEBUG
	if(!areq) {
		printk("[CCAT] QAT asym callback = NULL \n");
	}
#endif

	areq->cb(resp);
}

int qat_asym_algs_register(void)
{
	int ret = 0;

	mutex_lock(&algs_lock);
	if (++active_devs == 1) {
		ccat_alg_rsa.base.cra_flags = 0;
		ret = crypto_register_akcipher(&ccat_alg_rsa);
		if (ret)
			goto unlock;
//		dh.base.cra_flags = 0;
//		ret = crypto_register_kpp(&dh);

#ifdef CCAT_HWRNG
#ifdef CONFIG_HW_RANDOM
		ret = ccat_hwrng_register();
		if (ret) {
#ifdef ICP_DEBUG
			printk("CCAT-HWRNG register fail. %s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
		}
#endif /* CONFIG_HW_RANDOM */
#endif /* CCAT_HWRNG */
	}
unlock:
	mutex_unlock(&algs_lock);
	return ret;
}

void qat_asym_algs_unregister(void)
{
	int ret = 0;

	mutex_lock(&algs_lock);
	if (--active_devs == 0) {
		crypto_unregister_akcipher(&ccat_alg_rsa);
//		crypto_unregister_kpp(&dh);

#ifdef CCAT_HWRNG
#ifdef CONFIG_HW_RANDOM
		ret = ccat_hwrng_unregister();
		if (ret) {
#ifdef ICP_DEBUG
			printk("CCAT-HWRNG unregister fail. %s %s %d \n", __FILE__, __FUNCTION__, __LINE__);
#endif
		}
#endif
#endif
	}
	mutex_unlock(&algs_lock);
}

#endif

