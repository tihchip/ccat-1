/* SPDX-License-Identifier: GPL-2.0 */
/*
 * This file is part of HSM driver.
 *
 * Copyright (C) 2021, TIH MicroElectronic Technology - All Rights Reserved
 * Author: Chad <weichao.lu@tihchip.com>
 */

#ifndef __ICP_HSM_HW_H__
#define __ICP_HSM_HW_H__
#include <linux/crypto.h>

/* The hash 0-length input is support? */
#define ICP_HSM_HASH_NO_INPUT_SUPPORT         0

/* The maximum length of aad */
#define ICP_HSM_AAD_MAX_SIZE                  128

/* The maximum number of addr_list */
#define ICP_HSM_MAX_ADDR_LIST                 32

/* The max length of the HSM hw context */
#define ICP_HSM_MAX_CTX_LENGTH                256

/* The max size of the HSM hw cipher key */
#define ICP_HSM_MAX_CIPHER_KEY_SIZE           64

/* The max size of the HSM hw auth key */
#define ICP_HSM_MAX_AUTH_KEY_SIZE             64

/**
 * enum icp_hsm_type - HSM Descriptor Header Service Type Bits enumeration.
 * @ICP_HSM_TYPE_CIPHER:   Cipher descriptor.
 * @ICP_HSM_TYPE_HASH:     Hash descriptor.
 * @ICP_HSM_TYPE_AUTH:     Auth descriptor.
 * @ICP_HSM_TYPE_AEAD:     Aead descriptor.
 * @ICP_HSM_TYPE_RESERVED: Reserved.
 * @ICP_HSM_TYPE_PK:       Pk descriptor.
 * @ICP_HSM_TYPE_CTRL:     Ctrl descriptor.
 * @ICP_HSM_TYPE_BYPASS:   Bypass descriptor.
 */
enum icp_hsm_type {
	ICP_HSM_TYPE_CIPHER,
	ICP_HSM_TYPE_HASH,
	ICP_HSM_TYPE_AUTH,
	ICP_HSM_TYPE_AEAD,
	ICP_HSM_TYPE_RESERVED,
	ICP_HSM_TYPE_PK,
	ICP_HSM_TYPE_CTRL,
	ICP_HSM_TYPE_BYPASS
};

/**
 * enum icp_hsm_alg_type - HSM algorithm operate type.
 */
enum icp_hsm_alg_type {
	ICP_HSM_TYPE_SYM_CIPHER,
	ICP_HSM_TYPE_SYM_HASH,
	ICP_HSM_TYPE_SYM_AUTH,
	ICP_HSM_TYPE_SYM_AEAD,
	ICP_HSM_TYPE_PK_RSA_KEYGEN,
	ICP_HSM_TYPE_PK_RSA_CRT_KEYGEN,
	ICP_HSM_TYPE_PK_RSA_CRT_DECRYPT,
	ICP_HSM_TYPE_PK_RSA_ENCRYPT,
	ICP_HSM_TYPE_PK_RSA_DECRYPT,
	ICP_HSM_TYPE_PK_ECDH,
	ICP_HSM_TYPE_PK_ECDSA,
	ICP_HSM_TYPE_PK_DH,
	ICP_HSM_TYPE_PK_DSA,
	ICP_HSM_TYPE_PK_SM2_ENCRYPT,
	ICP_HSM_TYPE_PK_SM2_DECRYPT,
	ICP_HSM_TYPE_PK_SM2_E_GET,
	ICP_HSM_TYPE_PK_SM2_Z_GET,
	ICP_HSM_TYPE_PK_SM2_SIGN,
	ICP_HSM_TYPE_PK_SM2_VERIFY,
	ICP_HSM_TYPE_PK_SM2_KEY_EXCHANGE,
	ICP_HSM_TYPE_CTRL_RNG
};

/**
 * enum icp_hsm_sym_cipher_alg - HSM-SYM Descriptor Header Cipher Algorithm Bits enumeration.
 * @ICP_HSM_SYM_CIPHER_ALG_NULL:     Not cipher algorithm.
 * @ICP_HSM_SYM_CIPHER_ALG_SM4:      SM4 algorithm.
 * @ICP_HSM_SYM_CIPHER_ALG_RESERVED: Reserved.
 * @ICP_HSM_SYM_CIPHER_ALG_AES128:   AES128 algorithm.
 * @ICP_HSM_SYM_CIPHER_ALG_AES192:   AES192 algorithm.
 * @ICP_HSM_SYM_CIPHER_ALG_AES256:   AES256 algorithm.
 * @ICP_HSM_SYM_CIPHER_ALG_DES:      DES algorithm.
 * @ICP_HSM_SYM_CIPHER_ALG_3DES:     3DES algorithm.
 */
enum icp_hsm_sym_cipher_alg {
	ICP_HSM_SYM_CIPHER_ALG_NULL,
	ICP_HSM_SYM_CIPHER_ALG_SM4,
	ICP_HSM_SYM_CIPHER_ALG_RESERVED,
	ICP_HSM_SYM_CIPHER_ALG_AES128,
	ICP_HSM_SYM_CIPHER_ALG_AES192,
	ICP_HSM_SYM_CIPHER_ALG_AES256,
	ICP_HSM_SYM_CIPHER_ALG_DES,
	ICP_HSM_SYM_CIPHER_ALG_3DES
};

/**
 * enum icp_hsm_sym_cipher_mode - HSM-SYM Descriptor Header Cipher Mode Bits enumeration.
 * @ICP_HSM_SYM_CIPHER_MODE_NULL: Not cipher mode.
 * @ICP_HSM_SYM_CIPHER_MODE_ECB:  ECB mode.
 * @ICP_HSM_SYM_CIPHER_MODE_CBC:  CBC mode.
 * @ICP_HSM_SYM_CIPHER_MODE_CFB:  CFB mode.
 * @ICP_HSM_SYM_CIPHER_MODE_OFB:  OFB mode.
 * @ICP_HSM_SYM_CIPHER_MODE_CTR:  CTR mode.
 * @ICP_HSM_SYM_CIPHER_MODE_XTS:  XTS mode.
 * @ICP_HSM_SYM_CIPHER_MODE_GCM:  GCM mode.
 * @ICP_HSM_SYM_CIPHER_MODE_CCM:  CCM mode.
 */
enum icp_hsm_sym_cipher_mode {
	ICP_HSM_SYM_CIPHER_MODE_NULL,
	ICP_HSM_SYM_CIPHER_MODE_ECB,
	ICP_HSM_SYM_CIPHER_MODE_CBC,
	ICP_HSM_SYM_CIPHER_MODE_CFB,
	ICP_HSM_SYM_CIPHER_MODE_OFB,
	ICP_HSM_SYM_CIPHER_MODE_CTR,
	ICP_HSM_SYM_CIPHER_MODE_XTS,
	ICP_HSM_SYM_CIPHER_MODE_GCM,
	ICP_HSM_SYM_CIPHER_MODE_CCM
};

/**
 * enum icp_hsm_sym_hash_alg - HSM-SYM Descriptor Header Hash Algorithm Bits enumeration.
 * @ICP_HSM_SYM_HASH_ALG_NULL:   Not hash algorithm.
 * @ICP_HSM_SYM_HASH_ALG_SM3:    SM3 algorithm.
 * @ICP_HSM_SYM_HASH_ALG_MD5:    MD5 algorithm.
 * @ICP_HSM_SYM_HASH_ALG_SHA1:   SHA1 algorithm.
 * @ICP_HSM_SYM_HASH_ALG_SHA224: SHA224 algorithm.
 * @ICP_HSM_SYM_HASH_ALG_SHA256: SHA256 algorithm.
 * @ICP_HSM_SYM_HASH_ALG_SHA384: SHA384 algorithm.
 * @ICP_HSM_SYM_HASH_ALG_SHA512: SHA512 algorithm.
 */
enum icp_hsm_sym_hash_alg {
	ICP_HSM_SYM_HASH_ALG_NULL,
	ICP_HSM_SYM_HASH_ALG_SM3,
	ICP_HSM_SYM_HASH_ALG_MD5,
	ICP_HSM_SYM_HASH_ALG_SHA1,
	ICP_HSM_SYM_HASH_ALG_SHA224,
	ICP_HSM_SYM_HASH_ALG_SHA256,
	ICP_HSM_SYM_HASH_ALG_SHA384,
	ICP_HSM_SYM_HASH_ALG_SHA512
};

/**
 * enum icp_hsm_sym_auth_mode - HSM-SYM Descriptor Header Auth Mode Bits enumeration.
 * @ICP_HSM_SYM_AUTH_MODE_NULL: Not auth mode.
 * @ICP_HSM_SYM_AUTH_MODE_HMAC: HMAC mode.
 * @ICP_HSM_SYM_AUTH_MODE_CMAC: CMAC mode.
 */
enum icp_hsm_sym_auth_mode {
	ICP_HSM_SYM_AUTH_MODE_NULL,
	ICP_HSM_SYM_AUTH_MODE_HMAC,
	ICP_HSM_SYM_AUTH_MODE_CMAC
};

/**
 * enum icp_hsm_sym_init - HSM-SYM Descriptor Header Init Flag Bits enumeration.
 * @ICP_HSM_SYM_NOT_INIT: Not init operation.
 * @ICP_HSM_SYM_INIT:     Init operation.
 */
enum icp_hsm_sym_init {
	ICP_HSM_SYM_NOT_INIT,
	ICP_HSM_SYM_INIT
};

/**
 * enum icp_hsm_sym_finish - HSM-SYM Descriptor Header Finish Flag Bits enumeration.
 * @ICP_HSM_SYM_NOT_FINISH: Not finfish operation.
 * @ICP_HSM_SYM_FINISH:     Finish operation.
 */
enum icp_hsm_sym_finish {
	ICP_HSM_SYM_NOT_FINISH,
	ICP_HSM_SYM_FINISH
};

/**
 * enum icp_hsm_sym_aead_mode - HSM-SYM Descriptor Header Aead Mode Bits enumeration.
 * @ICP_HSM_SYM_AEAD_MODE_NULL: Not aead mode.
 * @ICP_HSM_SYM_AEAD_MODE_ETM:  ETM mode.
 * @ICP_HSM_SYM_AEAD_MODE_MTE:  MTE mode.
 */
enum icp_hsm_sym_aead_mode {
	ICP_HSM_SYM_AEAD_MODE_NULL,
	ICP_HSM_SYM_AEAD_MODE_ETM,
	ICP_HSM_SYM_AEAD_MODE_MTE
};

/**
 * enum icp_hsm_sym_aead_tag - HSM-SYM Descriptor Header Aead Tag Size Bits enumeration.
 * @ICP_HSM_SYM_AEAD_TAG_16: Tag size is 16 Bytes.
 * @ICP_HSM_SYM_AEAD_TAG_12: Tag size is 12 Bytes.
 * @ICP_HSM_SYM_AEAD_TAG_8:  Tag size is 8 Bytes.
 * @ICP_HSM_SYM_AEAD_TAG_4:  Tag size is 4 Bytes.
 */
enum icp_hsm_sym_aead_tag {
	ICP_HSM_SYM_AEAD_TAG_16,
	ICP_HSM_SYM_AEAD_TAG_12,
	ICP_HSM_SYM_AEAD_TAG_8,
	ICP_HSM_SYM_AEAD_TAG_4
};

/**
 * enum icp_hsm_sym_op - HSM-SYM Descriptor Header Operation Bits enumeration.
 * @ICP_HSM_SYM_OP_ENC: Encrypt operation.
 * @ICP_HSM_SYM_OP_DEC: Decrypt operation.
 */
enum icp_hsm_sym_op {
	ICP_HSM_SYM_OP_ENC,
	ICP_HSM_SYM_OP_DEC
};

/**
 * enum icp_hsm_pk_op - HSM-PK Descriptor Header Algorithm Bits enumeration.
 * @ICP_HSM_PK_ALG_SM2_KEYPAIR_GEN:             SM2 keypair generate operation.
 * @ICP_HSM_PK_ALG_SM2_PUBKEY_GEN_FROM_PRIKEY:  SM2 pubkey generate from prikey operation.
 * @ICP_HSM_PK_ALG_SM2_Z_GET:                   SM2 z get operation.
 * @ICP_HSM_PK_ALG_SM2_E_GET:                   SM2 e get operation.
 * @ICP_HSM_PK_ALG_SM2_SIGN:                    SM2 signature operation.
 * @ICP_HSM_PK_ALG_SM2_VERIFY:                  SM2 verify operation.
 * @ICP_HSM_PK_ALG_SM2_ENCRYPT:                 SM2 encrypt operation.
 * @ICP_HSM_PK_ALG_SM2_DECRYPT:                 SM2 decrypt operation.
 * @ICP_HSM_PK_ALG_SM2_KEY_EXCHANGE:            SM2 key exchange operation.
 * @ICP_HSM_PK_ALG_ECCP_KEYPAIR_GEN:            ECCP keypair generate operation.
 * @ICP_HSM_PK_ALG_ECCP_PUBKEY_GEN_FROM_PRIKEY: ECCP pubkey generate from prikey operation.
 * @ICP_HSM_PK_ALG_ECDSA_SIGN:                  ECDSA signature operation.
 * @ICP_HSM_PK_ALG_ECDSA_VERIFY:                ECDSA verify operation.
 * @ICP_HSM_PK_ALG_ECDH_COMPUTE_KEY:            ECDH compute key operation.
 * @ICP_HSM_PK_ALG_RSA_KEYPAIR_GEN:             RSA keypair generate operation.
 * @ICP_HSM_PK_ALG_RSA_CRT_KEYPAIR_GEN:         RSA-crt keypair generate operation.
 * @ICP_HSM_PK_ALG_RSA_ENCRYPT:                 RSA encrypt operation.
 * @ICP_HSM_PK_ALG_RSA_DECRYPT:                 RSA decrypt operation.
 * @ICP_HSM_PK_ALG_RSA_CRT_DECRYPT:             RSA-crt decrypt operation.
 */
enum icp_hsm_pk_op {
    ICP_HSM_PK_OP_RSA_KEYPAIR_GEN                 = 0x20,
    ICP_HSM_PK_OP_RSA_CRT_KEYPAIR_GEN             = 0x21,
    ICP_HSM_PK_OP_RSA_ENCRYPT                     = 0x22,
    ICP_HSM_PK_OP_RSA_DECRYPT                     = 0x23,
    ICP_HSM_PK_OP_RSA_CRT_DECRYPT                 = 0x24,
    ICP_HSM_PK_OP_RSA_KEYPAIR_GEN_FROM_PQ         = 0x25,
    ICP_HSM_PK_OP_RSA_KEYPAIR_GEN_FROM_PQE        = 0x26,
    ICP_HSM_PK_OP_RSA_CRT_KEYPAIR_GEN_FROM_PQ     = 0x27,
    ICP_HSM_PK_OP_RSA_CRT_KEYPAIR_GEN_FROM_PQE    = 0x28,
};

/**
 * enum icp_hsm_pk_rsa_bit - HSM-PK Descriptor Header Rsa-bit Bits enumeration.
 * @ICP_HSM_PK_RSA_BIT_1024: RSA nbits is 1024.
 * @ICP_HSM_PK_RSA_BIT_2048: RSA nbits is 2048.
 * @ICP_HSM_PK_RSA_BIT_4096: RSA nbits is 4096.
 */
enum icp_hsm_pk_rsa_bit {
	ICP_HSM_PK_RSA_BIT_1024,
	ICP_HSM_PK_RSA_BIT_2048,
	ICP_HSM_PK_RSA_BIT_4096
};

/**
 * enum icp_hsm_ctrl_cmd_id - HSM-CTRL Descriptor Header Cmd_id Bits enumeration.
 * @ICP_HSM_CTRL_CMD_RAND_GET: Get rand data operation.
 */
enum icp_hsm_ctrl_cmd_id {
    ICP_HSM_CTRL_CMD_RAND_GET,
	ICP_HSM_CTRL_CMD_KEY_GEN,
	ICP_HSM_CTRL_CMD_KEY_INPUT,
	ICP_HSM_CTRL_CMD_KEY_OUTPUT,
	ICP_HSM_CTRL_CMD_KEY_UPDATE,
	ICP_HSM_CTRL_CMD_KEY_CLEAR,
	ICP_HSM_CTRL_CMD_KEY_BASE_ADDR_SET,
	ICP_HSM_CTRL_CMD_OTP_PROG,
	ICP_HSM_CTRL_CMD_OTP_READ,
	ICP_HSM_CTRL_CMD_SYM_RESET,
	ICP_HSM_CTRL_CMD_SLEEP,
	ICP_HSM_CTRL_CMD_WAKE,
	ICP_HSM_CTRL_CMD_TDC_TEMP_GET
};

/**
 * enum icp_hsm_key_mode - HSM Descriptor Header Key Mode Bits enumeration.
 * @ICP_HSM_KEY_MODE_PLAIN:  Plain key mode.
 * @ICP_HSM_KEY_MODE_CIPHER: Cipher key mode.
 * @ICP_HSM_KEY_MODE_ID:     Key id mode.
 */
enum icp_hsm_key_mode {
	ICP_HSM_KEY_MODE_PLAIN,
	ICP_HSM_KEY_MODE_CIPHER,
	ICP_HSM_KEY_MODE_ID
};

/**
 * struct icp_hsm_sym_cipher_ctx_hw - HSM-SYM Cipher HW Context structure definition.
 * @key:       cipher key, for all cipher algorithm
 * @step_size: step size, for cipher CTR mode.
 * @xts_key:   xts key, for cipher XTS mode.
 * @iv:        iv, for cipher CBC/CFB/OFB mode.
 * @count:     count, for cipher CTR mode.
 * @tw:        tw, for cipher XTS mode.
 */
struct icp_hsm_sym_cipher_ctx_hw {
	u8      key[32];
	union {
		u32 step_size;
		u8  xts_key[32];
	} u1;
	union {
		u8  iv[16];
		u8  count[16];
		u8  tw[16];
	} u2;
} __aligned(8);

/**
 * struct icp_hsm_sym_hash_ctx_hw - HSM-SYM Hash HW Context structure definition.
 * @digest: hash digest value.
 */
struct icp_hsm_sym_hash_ctx_hw {
	uint8_t digest[64];
} __aligned(8);

/**
 * struct icp_hsm_sym_auth_ctx_hw - HSM-SYM Auth HW Context structure definition.
 * @key:    auth key, for all auth mode.
 * @digest: auth digest value, for HMAC mode.
 * @iv:     auth iv value, for CMAC mode.
 */
struct icp_hsm_sym_auth_ctx_hw {
	u8     key[64];
	union {
		u8 digest[64];
		u8 iv[16];
	} u;
} __aligned(8);

/**
 * struct icp_hsm_sym_aead_ctx_hw - HSM-SYM Aead HW Context structure definition.
 * @cipher_key:   cipher key, for cipher algorithm.
 * @resrvd0:      Reserved.
 * @iv:           iv, for cipher CBC mode.
 * @j0:           j0, for cipher GCM mode.
 * @nonce:        nonce, for cipher CCM mode.
 * @nonce_nbytes: nonce bytes length.
 * @auth_key:     auth key, for HMAC mode.
 * @digest:       digest value, for HMAC mode.
 * @pad_type:     padding type, for AEAD MTE mode.
 * @pad_nbytes:   padding bytes length.
 * @aad_nbits:    aad bits length, for all AEAD algorithm.
 * @aad_addr:     aad data address, for all AEAD algorithm.
 * @ghash_value:  ghash value, for GCM mode, software does not need to be configured.
 * @count0_ek:    count0 ek, for GCM mode, software does not need to be configured.
 */
struct icp_hsm_sym_aead_ctx_hw {
	u8         cipher_key[32];
	u8         resrvd0[32];
	union {
		u8     iv[16];
		u8     j0[16];
		struct {
			u8 nonce[15];
			u8 nonce_nbytes;
		} ccm_n;
	} u;
	u8         auth_key[64];
	u8         digest[56];
	u32        pad_type;
	u32        pad_nbytes;
	u64        aad_nbits;
	u64        aad_addr;
	u8         ghash_value[16];
	u8         count0_ek[16];
} __aligned(8);

/**
 * struct icp_hsm_pk_rsa_key_gen_ctx_hw - HSM-PK Rsa key Generate HW Context structure definition.
 * @ebits:  RSA e bits length.
 * @e_addr: RSA e address
 * @d_addr: RSA d address
 * @n_addr: RSA n address
 */
struct icp_hsm_pk_rsa_key_gen_ctx_hw {
	u32 n_bits;
	u64 n_addr;
	u32 e_bits;
	u64 e_addr;
	u64 d_addr;
} __packed __aligned(8);

/**
 * struct icp_hsm_pk_rsa_crt_key_gen_ctx_hw - HSM-PK Rsa-crt key Generate HW Context structure definition.
 * @ebits:  RSA e bits length.
 * @e_addr: RSA e address
 * @p_addr: RSA p address
 * @q_addr: RSA q address
 * @dp_addr: RSA dp address
 * @dq_addr: RSA dq address
 * @u_addr: RSA u address
 * @n_addr: RSA n address
 */
struct icp_hsm_pk_rsa_crt_key_gen_ctx_hw {
	u32 n_bits;
	u64 n_addr;
	u32 e_bits;
	u64 e_addr;
	u64 p_addr;
	u64 q_addr;
	u64 dp_addr;
	u64 dq_addr;
	u64 u_addr;
} __packed __aligned(8);

/**
 * struct icp_hsm_pk_rsa_encrypt_ctx_hw - HSM-PK Rsa Encrypt HW Context structure definition.
 * @ebits:  RSA e bits length.
 * @e_addr: RSA e address
 * @n_addr: RSA n address
 */
struct icp_hsm_pk_rsa_encrypt_ctx_hw {
	u32 n_bits;
	u64 n_addr;
	u32 e_bits;
	u64 e_addr;
	u64 m_addr;
	u64 c_addr;
} __packed __aligned(8);

/**
 * struct icp_hsm_pk_rsa_decrypt_ctx_hw - HSM-PK Rsa Decrypt HW Context structure definition.
 * @d_addr: RSA d address
 * @e_addr: RSA n address
 */
struct icp_hsm_pk_rsa_decrypt_ctx_hw {
	u32 n_bits;
	u64 n_addr;
	u64 d_addr;
	u64 c_addr;
	u64 m_addr;
} __packed __aligned(8);

/**
 * struct icp_hsm_pk_rsa_crt_decrypt_ctx_hw - HSM-PK Rsa-crt Decrypt HW Context structure definition.
 * @p_addr:  RSA p address
 * @q_addr:  RSA q address
 * @dp_addr: RSA dp address
 * @dq_addr: RSA dq address
 * @u_addr:  RSA u address
 */
struct icp_hsm_pk_rsa_crt_decrypt_ctx_hw {
	u32 n_bits;
	u64 p_addr;
	u64 q_addr;
	u64 dp_addr;
	u64 dq_addr;
	u64 u_addr;
	u64 c_addr;
	u64 m_addr;
} __packed __aligned(8);

/**
 * struct icp_hsm_sym_header - HSM-SYM Descriptor Header structure definition.
 * @service_type: service_type bits.
 * @last:         last bits, The bits must be set to 1.
 * @first:        first bits, The bits must be set to 1.
 * @addr_list:    addr_list bits, The bits must be set to 1.
 * @cipher_alg:   Cipher algorithm selection.
 * @cipher_mode:  Cipher mode selection.
 * @hash_alg:     Hash algorithm selection.
 * @auth_mode:    Auth mode selection.
 * @init:         init flag bit.
 * @finish:       finish flag bit.
 * @aead_mode:    Aead mode selection.
 * @aead_tag:     Aead tag size selection.
 * @operation:    Operation selection.
 * @resrvd0:      Reserved.
 * @key_mode:     Key mode selection.
 * @total_len:    Total length.
 */
struct icp_hsm_sym_header {
	u32 service_type:3;
	u32 last:1;
	u32 first:1;
	u32 addr_list:1;
	u32 cipher_alg:4;
	u32 cipher_mode:4;
	u32 hash_alg:4;
	u32 auth_mode:3;
	u32 init:1;
	u32 finish:1;
	u32 aead_mode:2;
	u32 aead_tag:2;
	u32 operation:1;
	u32 resrvd0:2;
	u32 key_mode:2;

	u32 total_len;
} __packed __aligned(8);

/**
 * struct icp_hsm_pk_header - HSM-PK Descriptor Header structure definition.
 * @service_type:     service_type bits.
 * @last:             last bits, The bits must be set to 1.
 * @first:            first bits, The bits must be set to 1.
 * @pk_alg:           pk algorithm selection.
 * @rsa_nbits:        rsa nbits length selection.
 * @sm2_sm9_role:     sm2/sm9 role selection, for key exchange operation.
 * @ecc_binary:       ecc binary falg.
 * @sm2_order:        sm2 order selection.
 * @sm9_enc_type:     sm9 Encrypt type selection.
 * @sm9_padding_type: sm9 padding type selection.
 * @resrvd0:          Reserved.
 * @security:         security selection.
 * @resrvd1:          Reserved.
 * @key_mode:         Key mode selection.
 * @resrvd2:          Reserved.
 */
struct icp_hsm_pk_header {
	u32 service_type:3;
	u32 last:1;
	u32 first:1;
	u32 pk_op:8;
	u32 ecc_bin:1;
	u32 sm2_order:1;
	u32 sm2_sm9_role:1;
	u32 sm9_enc_type:1;
	u32 sm9_padding_type:1;
	u32 resrvd0:10;
	u32 security:1;
	u32 resrvd1:1;
	u32 key_mode:2;

	u32 resrvd2;
} __packed __aligned(8);

/**
 * struct icp_hsm_ctrl_header - HSM-CTRL Descriptor Header structure definition.
 * @service_type:     service_type bits.
 * @last:             last bits, The bits must be set to 1.
 * @first:            first bits, The bits must be set to 1.
 * @resrvd0:          Reserved.
 * @cmd_id:           commond id selection.
 */
struct icp_hsm_ctrl_header {
	u32 service_type:3;
	u32 last:1;
	u32 first:1;
	u32 resrvd0:19;
	u32 cmd_id:8;

	u32 resrvd2;
} __packed __aligned(8);

/**
 * struct icp_hsm_addr_list - HSM Address list structure definition.
 * @addr:   Data address.
 * @length: Data length.
 */
struct icp_hsm_addr_list {
	u64 addr:48;
	u64 length:16;
} __packed __aligned(8);

/**
 * struct icp_hsm_cdesc - HSM Command Descriptor message request structure definition.
 * @sym_hdr:       Sym header.
 * @pk_hdr:        Pk header.
 * @ctrl_hdr:      Ctrl header.
 * @input_addr:    Input data address.
 * @input_length:  Input Data length.
 * @output_addr:   Output Data address.
 * @output_length: Output Data length.
 * @ctx_addr:      Context addr.
 * @resrvd:        Reserved.
 */
struct icp_hsm_msg_req {
	union {
		struct icp_hsm_sym_header  sym_hdr;
		struct icp_hsm_pk_header   pk_hdr;
		struct icp_hsm_ctrl_header ctrl_hdr;
	} hdr;
	u64                           input_addr:48;
	u64                           input_length:16;
	u64                           output_addr:48;
	u64                           output_length:16;
	u64                           ctx_addr:48;
	u64                           resrvd:16;

	/**< Software only, LWs 8-15 */
	struct
	{
		u64 opaque_data;
		/**< Opaque data passed unmodified from the request to
		 * response messages by firmware (fw) */

		u8 service_cmd_id;
		/**< Service Command Id  - this field is service-specific
		 * Please use service-specific command Id here e.g.Crypto Command Id
		 * or Compression Command Id etc. */
		u8 resrvd1[23];
	} s;
} __packed __aligned(8);

/**
 * struct icp_hsm_rdesc - HSM Result Descriptor message response structure definition.
 * @err_code:         Error code.
 * @ac_output_length: Actual output Data length.
 * @input_addr:       Input data address.
 * @input_length:     Input Data length.
 * @output_addr:      Output Data address.
 * @output_length:    Output Data length.
 * @ctx_addr:         Context addr.
 * @resrvd:           Reserved.
 */
struct icp_hsm_msg_resp {
	u32 err_code;
	u32 ac_output_length;
	u64 input_addr:48;
	u64 input_length:16;
	u64 output_addr:48;
	u64 output_length:16;
	u64 ctx_addr:48;
	u64 resrvd:16;

	/**< Software only, LWs 8-15 */
	struct
	{
		u64 opaque_data;
		/**< Opaque data passed unmodified from the request to
		 * response messages by firmware (fw) */

		u8 service_cmd_id;
		/**< Service Command Id  - this field is service-specific
		 * Please use service-specific command Id here e.g.Crypto Command Id
		 * or Compression Command Id etc. */
		u8 resrvd1[23];
	} s;
} __packed __aligned(8);

#endif
