/* SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only) */
/* Copyright(c) 2014 - 2021 Intel Corporation */
#ifndef ADF_TRANSPORT_ACCESS_MACROS_H
#define ADF_TRANSPORT_ACCESS_MACROS_H

#include "adf_accel_devices.h"

/* C1XXX Ring CSR offsets */
#ifdef ADF_C1XXX_DEVICE_NAME
#define ADF_RING_CSR_QUEUE_CTRL           0x00
#define ADF_RING_CSR_QUEUE_IE             0x04
#define ADF_RING_CSR_QUEUE_IE_ENABLE      0x00000001
#define ADF_RING_CSR_QUEUE_ISR            0x08
#define ADF_RING_CSR_QUEUE_ISR_CLEAR      0x00000001
#define ADF_RING_CSR_QUEUE_LSR            0x0C
#define ADF_RING_CSR_INTC                 0x10
#define ADF_RING_CSR_INTC_OFFSET_TIMEOUT  0x8
#define ADF_RING_CSR_INTC_MASK_TIMEOUT    0xFFFFFF00
#define ADF_RING_CSR_INTC_OFFSET_PKG_UNIT 0x0
#define ADF_RING_CSR_INTC_MASK_PKG_UNIT   0x00000003
#define ADF_RING_CSR_INTC_OFFSET_COL_NUM  0x4
#define ADF_RING_CSR_INTC_MASK_COL_NUM    0x00000070
#define ADF_RING_CSR_DESC_CTRL            0x14
#define ADF_RING_CSR_RING_BASE_L          0x20
#define ADF_RING_CSR_RING_BASE_H          0x24
#define ADF_RING_CSR_RING_HEADER_INDEX    0x28
#define ADF_RING_CSR_RING_TAIL_INDEX      0x2C
#define ADF_RING_CSR_BUFFER_CTRL          0x30
#define ADF_RING_CSR_TPH_CTRL             0x34
#define ADF_RING_CSR_FUNC_ID              0x38


#define BUILD_RING_CSR_INTC_CONFIG(pkg_unit, col_num, timeout) \
		(((pkg_unit << ADF_RING_CSR_INTC_MASK_PKG_UNIT) &      \
			ADF_RING_CSR_INTC_MASK_PKG_UNIT)                   \
		| ((col_num << ADF_RING_CSR_INTC_OFFSET_COL_NUM) &     \
			ADF_RING_CSR_INTC_MASK_COL_NUM)                    \
		| ((timeout << ADF_RING_CSR_INTC_OFFSET_TIMEOUT) &     \
			ADF_RING_CSR_INTC_MASK_TIMEOUT)                    \
        )

#endif /* End of ADF_C1XXX_DEVICE_NAME */

#define ADF_RINGS_PER_INT_SRCSEL 8
#define ADF_BANK_INT_SRC_SEL_MASK 0x44444444UL
#define ADF_BANK_INT_FLAG_CLEAR_MASK 0xFFFF
#define ADF_RING_CSR_RING_CONFIG 0x000
#define ADF_RING_CSR_RING_LBASE 0x040
#define ADF_RING_CSR_RING_UBASE 0x080
#define ADF_RING_CSR_RING_HEAD 0x0C0
#define ADF_RING_CSR_RING_TAIL 0x100
#define ADF_RING_CSR_E_STAT 0x14C
#define ADF_RING_CSR_NE_STAT 0x150
#define ADF_RING_CSR_NF_STAT 0x154
#define ADF_RING_CSR_F_STAT 0x158
#define ADF_RING_CSR_INT_FLAG_EN 0x16C
#define ADF_RING_CSR_INT_FLAG	0x170
#define ADF_RING_CSR_INT_SRCSEL 0x174
#define ADF_RING_CSR_NEXT_INT_SRCSEL 0x4
#define ADF_RING_CSR_INT_COL_EN 0x17C
#define ADF_RING_CSR_INT_COL_CTL 0x180
#define ADF_RING_CSR_INT_FLAG_AND_COL 0x184
#define ADF_RING_CSR_INT_COL_CTL_ENABLE	0x80000000
#define ADF_RING_BUNDLE_SIZE 0x1000
#define ADF_RING_CONFIG_NEAR_FULL_WM 0x0A
#define ADF_RING_CONFIG_NEAR_EMPTY_WM 0x05
#define ADF_COALESCING_MIN_TIME 0x1FF
#define ADF_COALESCING_MAX_TIME 0xFFFFF
#define ADF_COALESCING_DEF_TIME 0x27FF
#define ADF_RING_NEAR_WATERMARK_512 0x08
#define ADF_RING_NEAR_WATERMARK_0 0x00
#define ADF_RING_EMPTY_SIG 0x7F7F7F7F

/* Valid internal ring number values */
#define ADF_RING_SIZE_64  64
#define ADF_RING_SIZE_128 128
#define ADF_RING_SIZE_256 256
#define ADF_RING_SIZE_512 512
#define ADF_RING_SIZE_4K  4096
#define ADF_RING_SIZE_16K 16384
#define ADF_RING_SIZE_4M  (4 << 20)
#define ADF_MIN_RING_SIZE ADF_RING_SIZE_64
#define ADF_MAX_RING_SIZE ADF_RING_SIZE_4K
#define ADF_DEFAULT_RING_SIZE ADF_RING_SIZE_128

/* Valid internal msg size values */
#define ADF_MSG_SIZE_SHIFT 6
#define ADF_MSG_SIZE_64    (1 << ADF_MSG_SIZE_SHIFT)

#define ADF_C1XXX_CCAT_BANK_RINGS    (ADF_ETR_MAX_RINGS_PER_BANK) //SYM/PK

/* Size to bytes conversion macros for ring and msg size values */
#define ADF_MSG_SIZE_TO_BYTES(SIZE) (SIZE)
#define ADF_BYTES_TO_MSG_SIZE(SIZE) (SIZE)
#define ADF_SIZE_TO_RING_SIZE_IN_BYTES(SIZE) (SIZE << ADF_MSG_SIZE_SHIFT)
#define ADF_RING_SIZE_IN_BYTES_TO_SIZE(SIZE) (SIZE >> ADF_MSG_SIZE_SHIFT)

/* Set the response quota to a high number */
#define ADF_NO_RESPONSE_QUOTA 0xFFFFFFFF

/* Minimum ring bufer size for memory allocation */
#define ADF_RING_SIZE_BYTES_MIN(SIZE) \
	((SIZE < ADF_SIZE_TO_RING_SIZE_IN_BYTES(ADF_RING_SIZE_64)) ? \
		ADF_SIZE_TO_RING_SIZE_IN_BYTES(ADF_RING_SIZE_64) : SIZE)


#define WRITE_CSR_RING_BASE(value)                                             \
    do                                                                         \
    {                                                                          \
        uint32_t l_base = 0, h_base = 0;                                         \
        l_base = (uint32_t)(value & 0xFFFFFFFF);                                 \
        h_base = (uint32_t)((value & 0x0000FFFF00000000ULL) >> 32);              \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_RING_BASE_L,                               \
                       l_base);                                                \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_RING_BASE_H,                               \
                       h_base);                                                \
    } while (0)

#define WRITE_CSR_RING_CTRL(size, count)                                       \
    do                                                                         \
    {                                                                          \
        uint32_t l_base = 0;                                                     \
        l_base = (uint32_t)((size << 12) | (count & 0x00000FFF));                \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_DESC_CTRL,                                 \
                       l_base);                                                \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_QUEUE_CTRL,                                \
                       1);                                                     \
    } while (0)

#define WRITE_CSR_RING_TAIL(csr_base_addr, value)                              \
    do{                                                                        \
        uint32_t l_base = 0;                                                     \
        l_base = (uint32_t)(value & 0x00000FFF);                                 \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_RING_TAIL_INDEX,                           \
                       l_base);                                                \
    } while (0)

#define WRITE_CSR_RING_ISR(csr_base_addr, value)                               \
    do{                                                                        \
        uint32_t l_base = 0;                                                     \
        l_base = (uint32_t)(value & 0x00000001);                                 \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_QUEUE_ISR,                                 \
                       l_base);                                                \
    } while (0)

#define WRITE_CSR_RING_IE(csr_base_addr, value)                               \
    do{                                                                        \
        uint32_t l_base = 0;                                                     \
        l_base = (uint32_t)(value & 0x00000001);                                 \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_QUEUE_IE,                                  \
                       l_base);                                                \
    } while (0)

#define WRITE_CSR_RING_INTC(csr_base_addr, value)                               \
    do{                                                                        \
        uint32_t l_base = 0;                                                     \
        l_base = (uint32_t)(value & 0xFFFFFFFF);                                 \
        ADF_CSR_WR(csr_base_addr,                                          \
                       ADF_RING_CSR_INTC,                                      \
                       l_base);                                                \
    } while (0)

#define READ_CSR_RING_QUEUE_CTRL                                               \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_QUEUE_CTRL)
#define READ_CSR_RING_QUEUE_IE                                                 \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_QUEUE_IE)
#define READ_CSR_RING_QUEUE_ISR                                                \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_QUEUE_ISR)
#define READ_CSR_RING_QUEUE_LSR                                                \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_QUEUE_LSR)
#define READ_CSR_RING_INTC                                                     \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_INTC)
#define READ_CSR_RING_DESC_CTRL                                                \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_DESC_CTRL)
#define READ_CSR_RING_BASE_L                                                   \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_RING_BASE_L)
#define READ_CSR_RING_BASE_H                                                   \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_RING_BASE_H)
#define READ_CSR_RING_HEADER_INDEX                                             \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_RING_HEADER_INDEX)
#define READ_CSR_RING_TAIL_INDEX                                               \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_RING_TAIL_INDEX)
#define READ_CSR_RING_BUFFER_CTRL                                              \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_BUFFER_CTRL)
#define READ_CSR_RING_TPH_CTRL                                                 \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_TPH_CTRL)
#define READ_CSR_RING_FUNC_ID                                                  \
    ADF_CSR_RD(csr_base_addr, ADF_RING_CSR_FUNC_ID)

//-------------------------old--------------------------------------
#if 0
/* Max outstanding requests */
#define BUILD_RING_BASE_ADDR(addr, size) \
	((addr >> 6) & (0xFFFFFFFFFFFFFFFFULL << size))
#define READ_CSR_RING_HEAD(csr_base_addr, bank, ring) \
	ADF_CSR_RD(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_RING_HEAD + (ring << 2))
#define READ_CSR_RING_TAIL(csr_base_addr, bank, ring) \
	ADF_CSR_RD(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_RING_TAIL + (ring << 2))
#define READ_CSR_E_STAT(csr_base_addr, bank) \
	ADF_CSR_RD(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_E_STAT)
#define WRITE_CSR_RING_CONFIG(csr_base_addr, bank, ring, value) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_RING_CONFIG + (ring << 2), value)
#define WRITE_CSR_RING_BASE(csr_base_addr, bank, ring, value) \
	do { \
		uint32_t l_base = 0, u_base = 0; \
		l_base = (uint32_t)(value & 0xFFFFFFFF); \
		u_base = (uint32_t)((value & 0xFFFFFFFF00000000ULL) >> 32); \
		ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
			ADF_RING_CSR_RING_LBASE + (ring << 2), l_base);	\
		ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
			ADF_RING_CSR_RING_UBASE + (ring << 2), u_base);	\
	} while (0)
static inline uint64_t read_base(void __iomem *csr_base_addr,
	uint32_t bank,
	uint32_t ring)
{
	uint32_t l_base, u_base;
	uint64_t addr;

	l_base = ADF_CSR_RD(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) +
			ADF_RING_CSR_RING_LBASE + (ring << 2));
	u_base = ADF_CSR_RD(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) +
			ADF_RING_CSR_RING_UBASE + (ring << 2));

	addr = (uint64_t)l_base & 0x00000000FFFFFFFFULL;
	addr |= (uint64_t)u_base << 32 & 0xFFFFFFFF00000000ULL;

	return addr;
}

#define READ_CSR_RING_BASE(csr_base_addr, bank, ring) \
	read_base(csr_base_addr, bank, ring)
#define WRITE_CSR_RING_HEAD(csr_base_addr, bank, ring, value) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_RING_HEAD + (ring << 2), value)
#define WRITE_CSR_RING_TAIL(csr_base_addr, bank, ring, value) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_RING_TAIL + (ring << 2), value)
#define WRITE_CSR_INT_FLAG(csr_base_addr, bank, value) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * (bank)) + \
		ADF_RING_CSR_INT_FLAG, value)
#define WRITE_CSR_INT_SRCSEL(csr_base_addr, bank, idx) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * (bank)) + \
		(ADF_RING_CSR_INT_SRCSEL + ((idx) * ADF_RING_CSR_NEXT_INT_SRCSEL)), \
		ADF_BANK_INT_SRC_SEL_MASK)
#define WRITE_CSR_INT_COL_EN(csr_base_addr, bank, value) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_INT_COL_EN, value)
#define WRITE_CSR_INT_COL_CTL(csr_base_addr, bank, value) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_INT_COL_CTL, \
		ADF_RING_CSR_INT_COL_CTL_ENABLE | value)
#define WRITE_CSR_INT_FLAG_AND_COL(csr_base_addr, bank, value) \
	ADF_CSR_WR(csr_base_addr, (ADF_RING_BUNDLE_SIZE * bank) + \
		ADF_RING_CSR_INT_FLAG_AND_COL, value)
#endif
#endif
