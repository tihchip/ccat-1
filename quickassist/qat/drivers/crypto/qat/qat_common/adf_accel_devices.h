/* SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only) */
/* Copyright(c) 2014 - 2021 Intel Corporation */
#ifndef ADF_ACCEL_DEVICES_H_
#define ADF_ACCEL_DEVICES_H_
#ifndef USER_SPACE
#include <linux/interrupt.h>
#include <linux/module.h>
//#include <linux/pci.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/platform_device.h>
#include <linux/list.h>
#include <linux/io.h>
#include <linux/ratelimit.h>
#include <linux/hw_random.h>
#include "adf_cfg_common.h"
#endif /* USER_SPACE */

#define ADF_DH895XCC_DEVICE_NAME "dh895xcc"
#define ADF_DH895XCCVF_DEVICE_NAME "dh895xccvf"
#define ADF_C62X_DEVICE_NAME "c6xx"
#define ADF_C62XVF_DEVICE_NAME "c6xxvf"
#define ADF_C3XXX_DEVICE_NAME "c3xxx"
#define ADF_C3XXXVF_DEVICE_NAME "c3xxxvf"
#define ADF_200XX_DEVICE_NAME "200xx"
#define ADF_200XXVF_DEVICE_NAME "200xxvf"
#define ADF_D15XX_DEVICE_NAME "d15xx"
#define ADF_D15XXVF_DEVICE_NAME "d15xxvf"
#define ADF_C1XXX_DEVICE_NAME "c1xxx"
#define ADF_DH895XCC_PCI_DEVICE_ID 0x435
#define ADF_DH895XCCIOV_PCI_DEVICE_ID 0x443
#define ADF_C62X_PCI_DEVICE_ID 0x37c8
#define ADF_C62XIOV_PCI_DEVICE_ID 0x37c9
#define ADF_C3XXX_PCI_DEVICE_ID 0x19e2
#define ADF_C3XXXIOV_PCI_DEVICE_ID 0x19e3
#define ADF_200XX_PCI_DEVICE_ID 0x18ee
#define ADF_200XXIOV_PCI_DEVICE_ID 0x18ef
#define ADF_D15XX_PCI_DEVICE_ID 0x6f54
#define ADF_D15XXIOV_PCI_DEVICE_ID 0x6f55

#if defined(CONFIG_PCI_IOV)
#define ADF_VF2PF_SET_SIZE 32
#define ADF_MAX_VF2PF_SET 4
#define ADF_VF2PF_SET_OFFSET(set_nr) ((set_nr) * ADF_VF2PF_SET_SIZE)
#define ADF_VF2PF_VFNR_TO_SET(vf_nr) ((vf_nr) / ADF_VF2PF_SET_SIZE)
#define ADF_VF2PF_VFNR_TO_MASK(vf_nr)	\
	({	\
		u32 vf_nr_ = (vf_nr);	\
		BIT((vf_nr_) - ADF_VF2PF_SET_SIZE * ADF_VF2PF_VFNR_TO_SET(vf_nr_));	\
	})
#endif

#define ADF_ADMINMSGUR_OFFSET (0x3A000 + 0x574)
#define ADF_ADMINMSGLR_OFFSET (0x3A000 + 0x578)
#define ADF_MAILBOX_BASE_OFFSET 0x20970
#define ADF_MAILBOX_STRIDE 0x1000
#define ADF_ADMINMSG_LEN 32
#define ADF_DEVICE_FUSECTL_OFFSET 0x40
#define ADF_DEVICE_LEGFUSE_OFFSET 0x4C
#define ADF_DEVICE_FUSECTL_MASK 0x80000000
#define ADF_PCI_MAX_BARS 3
#define ADF_DEVICE_NAME_LENGTH 32
#define ADF_MAX_MSIX_VECTOR_NAME 32
#define ADF_DEVICE_NAME_PREFIX "ccat_"
#define ADF_STOP_RETRY 100
#define ADF_VF_SHUTDOWN_RETRY 100
#define ADF_PF_WAIT_RESTARTING_COMPLETE_DELAY 100
#define ADF_CFG_NUM_SERVICES 4          /* [crypto | asym | sym | ctrl] | dc */
#define ADF_SRV_TYPE_BIT_LEN 3
#define ADF_SRV_TYPE_MASK 0x7
#define ADF_RINGS_PER_SRV_TYPE 2
#define ADF_THRD_ABILITY_BIT_LEN 4
#define ADF_THRD_ABILITY_MASK 0xf
#define ADF_VF_OFFSET 0x8
#define ADF_MAX_FUNC_PER_DEV 0x7
#define ADF_PCI_DEV_OFFSET 0x3

#define ADF_PLATFORM_MAX_BANKS 2
#define ADF_ETR_MAX_RINGS_PER_BANK 4  //SYM(TX/RX) PK(TX/RX)

#define ADF_DEFAULT_RING_TO_SRV_MAP \
	(CRYPTO | CRYPTO << ADF_CFG_SERV_RING_PAIR_1_SHIFT | \
		NA << ADF_CFG_SERV_RING_PAIR_2_SHIFT | \
		COMP << ADF_CFG_SERV_RING_PAIR_3_SHIFT)

enum adf_accel_capabilities {
	ADF_ACCEL_CAPABILITIES_NULL = 0,
	ADF_ACCEL_CAPABILITIES_CRYPTO_SYMMETRIC = 1,
	ADF_ACCEL_CAPABILITIES_CRYPTO_ASYMMETRIC = 2,
	ADF_ACCEL_CAPABILITIES_CIPHER = 4,
	ADF_ACCEL_CAPABILITIES_AUTHENTICATION = 8,
	ADF_ACCEL_CAPABILITIES_COMPRESSION = 32,
	ADF_ACCEL_CAPABILITIES_DEPRECATED = 64,
	ADF_ACCEL_CAPABILITIES_RANDOM_NUMBER = 128
};

enum dev_sku_info {
	DEV_SKU_1 = 0,
	DEV_SKU_2,
	DEV_SKU_3,
	DEV_SKU_4,
	DEV_SKU_VF,
	DEV_SKU_UNKNOWN,
};

#ifndef USER_SPACE

struct adf_irq_data {
	struct adf_etr_bank_data *bank;
	u32    bank_nr;
	u32    ring_nr;
	u32    virq;
	char   name[ADF_MAX_MSIX_VECTOR_NAME];
};

struct adf_bar {
	resource_size_t base_addr;
	void __iomem *virt_addr;
	resource_size_t size;
};

struct adf_hw_resource {
	struct adf_irq_data    irq_data;
	struct adf_bar         reg_data;
};

#if 1
struct adf_irq {
	bool enabled;
	char name[ADF_MAX_MSIX_VECTOR_NAME];
};

struct adf_accel_msix {
	//struct msix_entry *entries;
	struct adf_irq *irqs;
	u32 num_entries;
};

struct adf_accel_pci {
	struct pci_dev *pci_dev;
	struct adf_accel_msix msix_entries;
	struct adf_bar pci_bars[ADF_PCI_MAX_BARS];
	enum dev_sku_info sku;
	u8 revid;
};
#endif

struct adf_accel_platform {
	struct platform_device *pdev;
	struct adf_hw_resource c1xxx_hw_resource[ADF_PLATFORM_MAX_BANKS][ADF_ETR_MAX_RINGS_PER_BANK];
	u8 revid;
	struct hwrng rng;
};

#endif /* USER_SPACE */

enum dev_state {
	DEV_DOWN = 0,
	DEV_UP
};

static inline const char *get_sku_info(enum dev_sku_info info)
{
	switch (info) {
	case DEV_SKU_1:
		return "SKU1";
	case DEV_SKU_2:
		return "SKU2";
	case DEV_SKU_3:
		return "SKU3";
	case DEV_SKU_4:
		return "SKU4";
	case DEV_SKU_VF:
		return "SKUVF";
	case DEV_SKU_UNKNOWN:
	default:
		break;
	}
	return "Unknown SKU";
}

#ifndef USER_SPACE
struct adf_hw_device_class {
	const char *name;
	const enum adf_device_type type;
	uint32_t instances;
};

struct adf_cfg_device_data;
struct adf_accel_dev;
struct adf_etr_data;
struct adf_etr_ring_data;

struct sla_sku_dev {
	u32 ticks[ADF_MAX_SERVICES];
	u16 slau_supported[ADF_MAX_SERVICES];
	u8 svc_supported;
};

struct adf_dev_util_table {
	void *virt_addr;
	dma_addr_t dma_addr;
	u32 total_util;
};

struct adf_hw_device_data {
	struct adf_hw_device_class *dev_class;
	uint32_t (*get_accel_mask)(struct adf_accel_dev *accel_dev);
	uint32_t (*get_ae_mask)(struct adf_accel_dev *accel_dev);
	uint32_t (*get_sram_bar_id)(struct adf_hw_device_data *self);
	uint32_t (*get_misc_bar_id)(struct adf_hw_device_data *self);
	uint32_t (*get_etr_bar_id)(struct adf_hw_device_data *self);
	uint32_t (*get_num_aes)(struct adf_hw_device_data *self);
	uint32_t (*get_num_accels)(struct adf_hw_device_data *self);
	uint32_t (*get_pf2vf_offset)(uint32_t i);
	uint32_t (*get_vintmsk_offset)(uint32_t i);
	uint32_t (*get_clock_speed)(struct adf_hw_device_data *self);
	enum dev_sku_info(*get_sku)(struct adf_hw_device_data *self);
#if defined(CONFIG_PCI_IOV)
	void (*get_vf2pf_mask)(void __iomem *pmisc_bar_addr,
		u32 vf_mask_sets[ADF_MAX_VF2PF_SET]);
	void (*enable_vf2pf_interrupts)(void __iomem *pmisc_bar_addr,
		u32 vf_mask_sets, u8 vf2pf_set);
	void (*disable_vf2pf_interrupts)(void __iomem *pmisc_bar_addr,
		u32 vf_mask_sets, u8 vf2pf_set);
#endif
	int (*alloc_irq)(struct adf_accel_dev *accel_dev);
	void (*free_irq)(struct adf_accel_dev *accel_dev);
	void (*enable_error_correction)(struct adf_accel_dev *accel_dev);
	int (*check_uncorrectable_error)(struct adf_accel_dev *accel_dev);
	void (*print_err_registers)(struct adf_accel_dev *accel_dev);
	void (*disable_error_interrupts)(struct adf_accel_dev *accel_dev);
	int (*init_admin_comms)(struct adf_accel_dev *accel_dev);
	void (*exit_admin_comms)(struct adf_accel_dev *accel_dev);
	int (*send_admin_init)(struct adf_accel_dev *accel_dev);
	int (*get_heartbeat_status)(struct adf_accel_dev *accel_dev);
	uint32_t (*get_ae_clock)(struct adf_hw_device_data *self);
#ifdef QAT_KPT
	int (*enable_kpt)(struct adf_accel_dev *accel_dev);
#endif
	void (*set_asym_rings_mask)(struct adf_accel_dev *accel_dev);
	int (*get_ring_to_svc_map)(struct adf_accel_dev *accel_dev,
		u16 *ring_to_svc_map);
	int (*get_fw_image_type)(struct adf_accel_dev *accel_dev,
		enum adf_cfg_fw_image_type *fw_image_type);
	int (*get_fw_name)(struct adf_accel_dev *accel_dev, char *uof_name);
	int (*get_sla_units)(struct adf_accel_dev *accel_dev, u16 **sla_units);
	uint32_t (*get_accel_cap)(struct adf_accel_dev *accel_dev);
	int (*init_arb)(struct adf_accel_dev *accel_dev);
	void (*exit_arb)(struct adf_accel_dev *accel_dev);
	void (*disable_arb)(struct adf_accel_dev *accel_dev);
	void (*get_arb_mapping)(struct adf_accel_dev *accel_dev,
		const uint32_t **cfg);
	void (*disable_iov)(struct adf_accel_dev *accel_dev);
	void (*configure_iov_threads)(struct adf_accel_dev *accel_dev,
		bool enable);
	void (*enable_ints)(struct adf_accel_dev *accel_dev);
	bool (*check_slice_hang)(struct adf_accel_dev *accel_dev);
	int (*set_ssm_wdtimer)(struct adf_accel_dev *accel_dev);
	int (*enable_vf2pf_comms)(struct adf_accel_dev *accel_dev);
	int (*disable_vf2pf_comms)(struct adf_accel_dev *accel_dev);
	void (*reset_device)(struct adf_accel_dev *accel_dev);
	int (*measure_clock)(struct adf_accel_dev *accel_dev);
	void (*pre_reset)(struct adf_accel_dev *accel_dev);
	void (*post_reset)(struct adf_accel_dev *accel_dev);
	int (*get_resource)(struct adf_accel_dev *accel_dev, uint8_t bank_nr,
		uint8_t ring_nr, struct adf_bar *bar, uint32_t *irq);
	void (*set_resource)(uint8_t bank_nr, uint8_t ring_nr, uint32_t *irq);
	const char *fw_name;
	const char *fw_mmp_name;
	uint32_t fuses;
	uint32_t accel_capabilities_mask;
	uint32_t instance_id;
	u32 aerucm_mask;
	u32 ae_mask;
	uint16_t tx_rings_mask;
	uint8_t tx_rx_gap;
	uint8_t num_banks;
	uint8_t num_instances_per_bank;
	u8 num_rings_per_bank;
	uint8_t num_accel;
	uint8_t num_logical_accel;
	uint8_t num_engines;
	u32 extended_dc_capabilities;
	u32 clock_frequency;
	u16 ring_to_svc_map;
	u16 accel_mask;
	int (*config_device)(struct adf_accel_dev *accel_dev);
	u16 asym_rings_mask;
	bool is_du_supported;
	bool is_sla_supported;
#ifdef QAT_KPT
	u32 kpt_hw_capabilities;
	u32 kpt_achandle;
#endif
	u8 min_iov_compat_ver;
};

/* CSR write macro */
#define ADF_CSR_WR(csr_base, csr_offset, val)   \
	__raw_writel(val, csr_base + csr_offset)

/* CSR read macro */
#define ADF_CSR_RD(csr_base, csr_offset) __raw_readl(csr_base + csr_offset)
/* Macro applying a percentage to a value */
#define ADF_APPLY_PERCENTAGE(value, percentage) ((value * percentage) / 100)

#define GET_DEV(accel_dev) ((accel_dev)->accel_platform_dev.pdev->dev)
#define GET_BARS(accel_dev) ((accel_dev)->accel_pci_dev.pci_bars)
#define GET_HW_DATA(accel_dev) (accel_dev->hw_device)
#define GET_MAX_BANKS(accel_dev) (GET_HW_DATA(accel_dev)->num_banks)
#define GET_DEV_SKU(accel_dev) (accel_dev->accel_pci_dev.sku)
#define GET_NUM_RINGS_PER_BANK(accel_dev) (GET_HW_DATA(accel_dev)->num_rings_per_bank)
#define GET_MAX_ACCELENGINES(accel_dev) (GET_HW_DATA(accel_dev)->num_engines)
#define accel_to_pci_dev(accel_ptr) accel_ptr->accel_pci_dev.pci_dev
#define accel_to_platform_dev(accel_ptr) accel_ptr->accel_platform_dev.pdev
#define ADF_NUM_THREADS_PER_AE (8)
#define ADF_AE_ADMIN_THREAD (7)
#define ADF_NUM_PKE_STRAND (2)
#define ADF_AE_STRAND0_THREAD (8)
#define ADF_AE_STRAND1_THREAD (9)
#define ADF_NUM_HB_CNT_PER_AE (ADF_NUM_THREADS_PER_AE + ADF_NUM_PKE_STRAND)
#define GET_SRV_TYPE(ena_srv_mask, srv)                                 \
	(((ena_srv_mask) >> (ADF_SRV_TYPE_BIT_LEN * (srv))) & ADF_SRV_TYPE_MASK)
#define GET_MAX_PROCESSES(accel_dev)                                    \
	({                                                                  \
		typeof(accel_dev) dev = (accel_dev);                            \
		(GET_MAX_BANKS(dev) * (GET_NUM_RINGS_PER_BANK(dev) / 2));       \
	})
#define SET_ASYM_MASK(asym_mask, srv)                                   \
	({                                                                  \
		typeof(srv) srv_ = (srv);                                       \
		(asym_mask) |=                                                  \
			((1 << (srv_) * ADF_RINGS_PER_SRV_TYPE) | \
				(1 << ((srv_) * ADF_RINGS_PER_SRV_TYPE + 1))); \
	})
#define GET_DU_TABLE(accel_dev) (accel_dev->du_table)

static inline void adf_csr_fetch_and_and(void __iomem *csr,
	size_t offs, unsigned long mask)
{
	unsigned int val = ADF_CSR_RD(csr, offs);

	val &= mask;
	ADF_CSR_WR(csr, offs, val);
}

static inline void adf_csr_fetch_and_or(void __iomem *csr,
	size_t offs, unsigned long mask)
{
	unsigned int val = ADF_CSR_RD(csr, offs);

	val |= mask;
	ADF_CSR_WR(csr, offs, val);
}

struct pfvf_stats {
	struct dentry *stats_file;
	/* Messages put in CSR */
	unsigned int tx;
	/* Messages read from CSR */
	unsigned int rx;
	/* Interrupt fired but int bit was clear */
	unsigned int spurious;
	/* Block messages sent */
	unsigned int blk_tx;
	/* Block messages received */
	unsigned int blk_rx;
	/* Blocks received with CRC errors */
	unsigned int crc_err;
	/* CSR in use by other side */
	unsigned int busy;
	/* Receiver did not acknowledge */
	unsigned int no_ack;
	/* Collision detected */
	unsigned int collision;
	/* Couldn't send a response */
	unsigned int tx_timeout;
	/* Didn't receive a response */
	unsigned int rx_timeout;
	/* Responses received */
	unsigned int rx_rsp;
	/* Messages re-transmitted */
	unsigned int retry;
	/* Event put timeout */
	unsigned int event_timeout;
};

#define NUM_PFVF_COUNTERS 14

struct adf_admin_comms {
	dma_addr_t phy_addr;
	dma_addr_t const_tbl_addr;
	dma_addr_t phy_hb_addr;
	void *virt_addr;
	void *virt_tbl_addr;
	void *virt_hb_addr;
	void __iomem *mailbox_addr;
	struct mutex lock;  /* protects adf_admin_comms struct */
};

struct icp_qat_fw_loader_handle;
struct adf_fw_loader_data {
	struct icp_qat_fw_loader_handle *fw_loader;
	const struct firmware *uof_fw;
	const struct firmware *mmp_fw;
};

struct adf_accel_vf_info {
	struct adf_accel_dev *accel_dev;
	struct mutex pf2vf_lock; /* protect CSR access for PF2VF messages */
	struct ratelimit_state vf2pf_ratelimit;
	u32 vf_nr;
	bool init;
	u8 compat_ver;
	struct pfvf_stats pfvf_counters;
};

struct adf_fw_versions {
	u8 fw_version_major;
	u8 fw_version_minor;
	u8 fw_version_patch;
	u8 mmp_version_major;
	u8 mmp_version_minor;
	u8 mmp_version_patch;
};

#define ADF_COMPAT_CHECKER_MAX 8
typedef int (*adf_iov_compat_checker_t)(struct adf_accel_dev *accel_dev,
	u8 vf_compat_ver);
struct adf_accel_compat_manager {
	u8 num_chker;
	adf_iov_compat_checker_t iov_compat_checkers[ADF_COMPAT_CHECKER_MAX];
};

struct adf_heartbeat;
struct adf_ver;
struct adf_uio_control_accel;
struct adf_accel_dev {
	struct adf_etr_data *transport;
	struct adf_hw_device_data *hw_device;
	struct adf_cfg_device_data *cfg;
	struct adf_fw_loader_data *fw_loader;
	struct adf_admin_comms *admin;
	struct adf_heartbeat *heartbeat;
	struct adf_ver *pver;
	unsigned int autoreset_on_error;
	atomic_t ref_count;
	struct list_head crypto_list;
	unsigned long status;
	struct dentry *debugfs_dir;
	struct dentry *clock_dbgfile;
	struct dentry *fw_cntr_dbgfile;
	struct dentry *cnvnr_dbgfile;
	struct dentry *pfvf_dbgdir;
	struct list_head list;
	struct adf_accel_pci accel_pci_dev;
	struct adf_accel_platform accel_platform_dev;
	struct module *owner;
	struct adf_accel_compat_manager *cm;
	u8 compat_ver;
	struct adf_fw_versions fw_versions;
	bool is_vf;
	union {
		struct {
			/* vf_info is non-zero when SR-IOV is init'ed */
			struct adf_accel_vf_info *vf_info;
		} pf;
		struct {
			struct tasklet_struct pf2vf_bh_tasklet;
			struct mutex vf2pf_lock; /* protect CSR access */
			struct completion iov_msg_completion;
			struct pfvf_stats pfvf_counters;
			struct completion err_notified;
			char *irq_name;
			bool irq_enabled;
			bool is_err_notified;
			uint8_t compatible;
			uint8_t pf_version;
			u8 pf2vf_block_byte;
			u8 pf2vf_block_resp_type;
		} vf;
	};
	u32 accel_id;
	spinlock_t vf2pf_csr_lock; /* protects VF2PF CSR access */
	struct adf_uio_control_accel *accel;
	unsigned int num_ker_bundles;
	struct sla_sku_dev sla_sku;
	u16 *available_slau;
	u16 *sla_ids;
	struct adf_dev_util_table du_table;
	struct list_head sla_list;
#ifdef QAT_KPT
	u32 detect_kpt;
#endif
	void *lac_dev;
	bool is_drv_rm;
};

#endif
#endif
