// SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only)
/* Copyright(c) 2014 - 2021 Intel Corporation */
#include <linux/delay.h>
#include "adf_accel_devices.h"
#include "adf_transport_internal.h"
#include "adf_transport_access_macros.h"
#include "adf_cfg.h"
#include "adf_common_drv.h"

static inline uint32_t adf_modulo(uint32_t data, uint32_t shift)
{
	return data & shift;
}

static inline int adf_check_ring_alignment(uint64_t addr, uint64_t size)
{
	if (((size - 1) & addr) != 0)
		return -EFAULT;
	return 0;
}

static int adf_verify_ring_size(uint32_t msg_size, uint32_t msg_num)
{
	int i = ADF_MIN_RING_SIZE;

	for (; i <= ADF_MAX_RING_SIZE; i++)
		if ((msg_size * msg_num) == ADF_SIZE_TO_RING_SIZE_IN_BYTES(i))
			return i;

	return ADF_DEFAULT_RING_SIZE;
}

static int adf_reserve_ring(struct adf_etr_bank_data *bank, uint32_t ring)
{
	spin_lock(&bank->lock);
	if (bank->ring_mask & (1 << ring)) {
		spin_unlock(&bank->lock);
		return -EFAULT;
	}
	bank->ring_mask |= (1 << ring);
	spin_unlock(&bank->lock);
	return 0;
}

static void adf_unreserve_ring(struct adf_etr_bank_data *bank, uint32_t ring)
{
	spin_lock(&bank->lock);
	bank->ring_mask &= ~(1 << ring);
	spin_unlock(&bank->lock);
}

static void adf_enable_ring_irq(struct adf_etr_bank_data *bank, uint32_t ring_nr)
{
	struct adf_etr_ring_data *ring;
	void __iomem *csr_base_addr = NULL;

	ring = &bank->rings[ring_nr];
	csr_base_addr = ring->csr_addr;

	spin_lock_bh(&bank->lock);
	bank->irq_mask |= (1 << ring_nr);
	spin_unlock_bh(&bank->lock);

	WRITE_CSR_RING_IE(csr_base_addr, 1);
}

static void adf_disable_ring_irq(struct adf_etr_bank_data *bank, uint32_t ring_nr)
{
	struct adf_etr_ring_data *ring;
	void __iomem *csr_base_addr = NULL;

	ring = &bank->rings[ring_nr];
	csr_base_addr = ring->csr_addr;

	spin_lock_bh(&bank->lock);
	bank->irq_mask &= ~(1 << ring_nr);
	spin_unlock_bh(&bank->lock);
	WRITE_CSR_RING_IE(csr_base_addr, 0);
}

int adf_send_message(struct adf_etr_ring_data *ring, uint32_t *msg)
{
	u32 msg_size = 0;
	u8  *pmsg = NULL;
	int i = 0;
	void __iomem *csr_base_addr = NULL;

	spin_lock_bh(&ring->lock);
	if (((ring->tail + 1) & (ring->ring_size - 1)) == ring->head) {
		spin_unlock_bh(&ring->lock);
		return -EAGAIN;
	}

	atomic_add_return(1, ring->inflights);

	csr_base_addr = ring->csr_addr;
	msg_size = ring->msg_size;
	pmsg = (u8 *)ring->base_addr + ring->tail * msg_size;

	memcpy((void *)pmsg, msg, msg_size);
	ring->tail = (ring->tail + 1) & (ring->ring_size - 1);
	WRITE_CSR_RING_TAIL(csr_base_addr, ring->tail);

	ring->csr_tail_offset = ring->tail;

	spin_unlock_bh(&ring->lock);

#ifdef ICP_DEBUG
	printk("\n\nadf_send_message bank/ring info-->\n");
	printk("BANK NO=%d, RING NO=%d(tail=%d, head=%d), RING DESC(addr=0x%lx)-->\n",
			ring->bank->bank_number,
			ring->ring_number,
			ring->tail,
			ring->head,
			pmsg);
	for (i = 0; i < (msg_size >> 2); i++)
		printk("[%d]: 0x%x\n", i, ((u32 *)pmsg)[i]);

	printk("\n\n");
#endif

	return 0;
}
EXPORT_SYMBOL_GPL(adf_send_message);

int adf_handle_response(struct adf_etr_ring_data *ring, u32 quota)
{
	uint32_t *msg = NULL;
	uint32_t msg_counter = 0;
	void __iomem *csr_base_addr = ring->csr_addr;

	spin_lock_bh(&ring->lock);
    /* point to where the next message should be */
    msg = (uint32_t *)(((u8 *)ring->base_addr) + ring->head * ring->msg_size);

	if (!quota) {
		quota = ADF_NO_RESPONSE_QUOTA;
	}

	while ((ring->head != READ_CSR_RING_HEADER_INDEX)
			&& (msg_counter < quota)) {
		 /* Invoke the callback for the message */
		ring->callback((uint32_t *)msg);
		atomic_dec(ring->inflights);

        /* Advance the head offset and handle wraparound */
        ring->head += 1;
        ring->head = adf_modulo(ring->head, (ring->ring_size - 1));

		msg_counter++;

        /* Point to where the next message should be */
        msg = (uint32_t *)(((u8 *)ring->base_addr) + ring->head * ring->msg_size);
	}
	spin_unlock_bh(&ring->lock);

	return msg_counter;
}
EXPORT_SYMBOL_GPL(adf_handle_response);

int adf_poll_bank(u32 accel_id, u32 bank_num, u32 quota)
{
	int num_resp;
	struct adf_accel_dev *accel_dev;
	struct adf_etr_data *trans_data;
	struct adf_etr_bank_data *bank;
	struct adf_etr_ring_data *ring;
	u32 rings_not_empty = 0;
	u32 ring_num;
	u32 resp_total = 0;
	u32 num_rings_per_bank;

	/* Find the accel device associated with the accelId
	 * passed in.
	 */
	accel_dev = adf_devmgr_get_dev_by_id(accel_id);
	if (!accel_dev) {
		pr_err(
			"There is no accel device associated with this accel id.\n");
		return -EINVAL;
	}

	trans_data = accel_dev->transport;
	bank = &trans_data->banks[bank_num];
	spin_lock(&bank->lock);

	/* Read the ring status CSR to determine which rings are empty. */
	//rings_not_empty = READ_CSR_E_STAT(bank->csr_addr, bank->bank_number);
	/* Complement to find which rings have data to be processed. */
	rings_not_empty = (~rings_not_empty) & bank->ring_mask;

	/* Return RETRY if the bank polling rings
	 * are all empty.
	 */
	if (!(rings_not_empty & bank->ring_mask)) {
		spin_unlock(&bank->lock);
		return -EAGAIN;
	}

	/*
	 * Loop over all rings within this bank.
	 * The ring structure is global to all
	 * rings hence while we loop over all rings in the
	 * bank we use ring_number to get the global ring.
	 */
	num_rings_per_bank = accel_dev->hw_device->num_rings_per_bank;
	for (ring_num = 0; ring_num < num_rings_per_bank; ring_num++) {
		ring = &bank->rings[ring_num];
		/* If this ring has not being created move to next ring. */
		if (!ring)
			continue;

		/* And with polling ring mask.
		 * If the there is no data on this ring
		 * move to the next one.
		 */
		if (!(rings_not_empty & (1 << ring->ring_number)))
			continue;

		/* Poll the ring. */
		num_resp = adf_handle_response(ring, quota);
		resp_total += num_resp;
	}

	spin_unlock(&bank->lock);
	/* Return SUCCESS if there's any response message
	 * returned.
	 */
	if (resp_total)
		return 0;
	return -EAGAIN;
}
EXPORT_SYMBOL_GPL(adf_poll_bank);

int adf_poll_all_banks(u32 accel_id, u32 quota)
{
	int status = -EAGAIN;
	struct adf_accel_dev *accel_dev;
	struct adf_etr_data *trans_data;
	struct adf_etr_bank_data *bank;
	u32 bank_num;
	u32 stat_total = 0;

	/* Find the accel device associated with the accelId
	 * passed in.
	 */
	accel_dev = adf_devmgr_get_dev_by_id(accel_id);
	if (!accel_dev) {
		pr_err(
			"There is no accel device associated with this accel id.\n");
		return -EINVAL;
	}

	/* Loop over banks and call adf_poll_bank */
	trans_data = accel_dev->transport;
	for (bank_num = 0; bank_num < GET_MAX_BANKS(accel_dev); bank_num++) {
		bank = &trans_data->banks[bank_num];
		/* if there are no polling rings on this bank
		 * continue to the next bank number.
		 */
		if (bank->ring_mask == 0)
			continue;
		status = adf_poll_bank(accel_id, bank_num, quota);
		/* The successful status should be AGAIN or 0 */
		if (status == 0)
			stat_total++;
		else if (status != -EAGAIN)
			return status;
	}

	/* Return SUCCESS if adf_poll_bank returned SUCCESS
	 * at any stage. adf_poll_bank cannot
	 * return fail in the above case.
	 */
	if (stat_total)
		return 0;

	return -EAGAIN;
}
EXPORT_SYMBOL_GPL(adf_poll_all_banks);

/* FIXME */
#if 0
static void adf_configure_tx_ring(struct adf_etr_ring_data *ring)
{
	uint32_t ring_config = BUILD_RING_CONFIG(ring->ring_size);

	WRITE_CSR_RING_CONFIG(ring->bank->csr_addr, ring->bank->bank_number,
		ring->ring_number, ring_config);
}

static void adf_configure_rx_ring(struct adf_etr_ring_data *ring)
{
	uint32_t ring_config =
		BUILD_RESP_RING_CONFIG(ring->ring_size,
			ADF_RING_NEAR_WATERMARK_512,
			ADF_RING_NEAR_WATERMARK_0);

	WRITE_CSR_RING_CONFIG(ring->bank->csr_addr, ring->bank->bank_number,
		ring->ring_number, ring_config);
}
#endif

static int adf_init_ring(struct adf_etr_ring_data *ring)
{
	struct adf_etr_bank_data *bank = ring->bank;
	struct adf_accel_dev *accel_dev = bank->accel_dev;
	void __iomem *csr_base_addr = ring->csr_addr;
	uint32_t ring_size_bytes =
			ADF_SIZE_TO_RING_SIZE_IN_BYTES(ring->ring_size);

	ring_size_bytes = ADF_RING_SIZE_BYTES_MIN(ring_size_bytes);
	ring->base_addr = dma_alloc_coherent(&GET_DEV(accel_dev),
					     ring_size_bytes, &ring->dma_addr,
					     GFP_KERNEL);
	if (!ring->base_addr)
		return -ENOMEM;

	memset(ring->base_addr, 0x7F, ring_size_bytes);
	/* The base_addr has to be aligned to the size of the buffer */
#if 0
	if (adf_check_ring_alignment(ring->dma_addr, ring_size_bytes)) {
		dev_err(&GET_DEV(accel_dev), "Ring address not aligned\n");
		dma_free_coherent(&GET_DEV(accel_dev), ring_size_bytes,
				  ring->base_addr, ring->dma_addr);
		return -EFAULT;
	}


	if (hw_data->tx_rings_mask & (1 << ring->ring_number))
		adf_configure_tx_ring(ring);

	else
		adf_configure_rx_ring(ring);
#endif

	/* Set ring register */
    WRITE_CSR_RING_ISR(csr_base_addr, 1);
    /* Disable ring irq */
    WRITE_CSR_RING_IE(csr_base_addr, 0);
    WRITE_CSR_RING_BASE(ring->dma_addr);

    /* HSM DESC size is not same for SYM and PK, please see spec */
    if(ring->ring_number != 1) //TX PK ring=0
    {
        WRITE_CSR_RING_CTRL(15, ring->ring_size);
    }
    else //TX SYM ring = 1
    {
        WRITE_CSR_RING_CTRL(ring->msg_size >> 2,
                            ring->ring_size);
    }

    WRITE_CSR_RING_TAIL(csr_base_addr, ring->tail);

	spin_lock_init(&ring->lock);
	return 0;
}

static void adf_cleanup_ring(struct adf_etr_ring_data *ring)
{
	uint32_t ring_size_bytes =
		ADF_SIZE_TO_RING_SIZE_IN_BYTES(ring->ring_size);
	ring_size_bytes = ADF_RING_SIZE_BYTES_MIN(ring_size_bytes);

	if (ring->base_addr) {
		/* dont do that, but... */
		//memzero_explicit(ring->base_addr, ring_size_bytes);
		dma_free_coherent(&GET_DEV(ring->bank->accel_dev),
			ring_size_bytes, ring->base_addr,
			ring->dma_addr);
	}

	return;
}

int adf_create_ring(struct adf_accel_dev *accel_dev, const char *section,
	uint32_t bank_num, uint32_t num_msgs,
	uint32_t msg_size, const char *ring_name,
	adf_callback_fn callback, int poll_mode,
	struct adf_etr_ring_data **ring_ptr)
{
	struct adf_etr_data *transport_data = accel_dev->transport;
	struct adf_etr_bank_data *bank;
	struct adf_etr_ring_data *ring;
	struct adf_bar bar;
	void __iomem *csr_base_addr = NULL;
	char val[ADF_CFG_MAX_VAL_LEN_IN_BYTES];
	u32 ring_num;
	int ret;
	u8 num_rings_per_bank = accel_dev->hw_device->num_rings_per_bank;
	struct adf_hw_device_data *hw_data = accel_dev->hw_device;

	if (bank_num >= GET_MAX_BANKS(accel_dev)) {
		dev_err(&GET_DEV(accel_dev), "Invalid bank number\n");
		return -EFAULT;
	}
	if (msg_size > ADF_MSG_SIZE_TO_BYTES(ADF_MSG_SIZE_64)) {
		dev_err(&GET_DEV(accel_dev), "Invalid msg size\n");
		return -EFAULT;
	}
	if (adf_cfg_get_param_value(accel_dev, section, ring_name, val)) {
		dev_err(&GET_DEV(accel_dev), "Section %s, no such entry : %s\n",
			section, ring_name);
		return -EFAULT;
	}
	if (kstrtouint(val, 10, &ring_num)) {
		dev_err(&GET_DEV(accel_dev), "Can't get ring number\n");
		return -EFAULT;
	}
	if (ring_num >= num_rings_per_bank) {
		dev_err(&GET_DEV(accel_dev), "Invalid ring number\n");
		return -EFAULT;
	}

	bank = &transport_data->banks[bank_num];
	if (adf_reserve_ring(bank, ring_num)) {
		dev_err(&GET_DEV(accel_dev), "Ring %d, %s already exists.\n",
			ring_num, ring_name);
		return -EFAULT;
	}

	hw_data->get_resource(accel_dev, bank_num, ring_num, &bar, NULL);
	csr_base_addr = bar.virt_addr;

	ring = &bank->rings[ring_num];
	ring->ring_number = ring_num;
	ring->bank = bank;
	ring->callback = callback;
	ring->msg_size = ADF_BYTES_TO_MSG_SIZE(msg_size);
	ring->ring_size = adf_verify_ring_size(msg_size, num_msgs);
	ring->max_inflights = ring->ring_size;
	ring->head = READ_CSR_RING_HEADER_INDEX;
	ring->tail = READ_CSR_RING_TAIL_INDEX;
	ring->csr_tail_offset = 0;
	ring->csr_addr = bar.virt_addr;
	atomic_set(ring->inflights, 0);

    if (ring->head != ring->tail)
    {
    	dev_err(&GET_DEV(accel_dev), "Ring [head %d - tail %d] head tail index different\n",
                  ring->head, ring->tail);
        return -EBUSY;
    }

	ret = adf_init_ring(ring);
	if (ret)
		goto err;

	/* Enable HW arbitration for the given ring */
	//adf_update_ring_arb(ring);

	if (adf_ring_debugfs_add(ring, ring_name)) {
		dev_err(&GET_DEV(accel_dev),
			"Couldn't add ring debugfs entry\n");
		ret = -EFAULT;
		goto err;
	}

	/* Enable interrupts if needed */
	if (callback && (!poll_mode))
	adf_enable_ring_irq(bank, ring->ring_number);
	*ring_ptr = ring;
	return 0;
err:
	adf_cleanup_ring(ring);
	adf_unreserve_ring(bank, ring_num);
	//adf_update_ring_arb(ring);
	return ret;
}
EXPORT_SYMBOL_GPL(adf_create_ring);

void adf_remove_ring(struct adf_etr_ring_data *ring)
{
	/* FIXME */
	struct adf_etr_bank_data *bank = ring->bank;

	adf_ring_debugfs_rm(ring);
	adf_unreserve_ring(bank, ring->ring_number);
	/* Disable HW arbitration for the given ring */
	//adf_update_ring_arb(ring);
	adf_cleanup_ring(ring);
}
EXPORT_SYMBOL_GPL(adf_remove_ring);

#if 0
static void adf_ring_response_handler(struct adf_etr_bank_data *bank)
{
	struct adf_accel_dev *accel_dev = bank->accel_dev;
	struct adf_hw_device_data *hw_data = accel_dev->hw_device;
	u8 num_rings_per_bank = hw_data->num_rings_per_bank;
	unsigned long empty_rings;
	int i;

	empty_rings = READ_CSR_E_STAT(bank->csr_addr, bank->bank_number);
	empty_rings = ~empty_rings & bank->irq_mask;

	for_each_set_bit(i, &empty_rings, num_rings_per_bank)
	adf_handle_response(&bank->rings[i], 0);
}


void adf_response_handler(uintptr_t bank_addr)
{
	struct adf_etr_bank_data *bank = (void *)bank_addr;

	/* Handle all the responses and reenable IRQs */
	adf_ring_response_handler(bank);
	WRITE_CSR_INT_COL_EN(bank->csr_addr, bank->bank_number,
		bank->irq_mask);
}
#endif
irqreturn_t adf_response_handler(int irq, void *irq_data)
{
	struct adf_irq_data *irqd = (struct adf_irq_data *)irq_data;
	struct adf_etr_bank_data *bank = irqd->bank;
	struct adf_etr_ring_data *ring = &bank->rings[irqd->ring_nr];
	void __iomem *csr_base_addr = ring->csr_addr;

	/* Handle all the responses and reenable IRQs */
	adf_handle_response(ring, 8);

	/* Enable HSM interrupt */
	WRITE_CSR_RING_IE(csr_base_addr, 1);

	return IRQ_HANDLED;
}

static inline int adf_get_cfg_int(struct adf_accel_dev *accel_dev,
	const char *section, const char *format,
	uint32_t key, uint32_t *value)
{
	char key_buf[ADF_CFG_MAX_KEY_LEN_IN_BYTES];
	char val_buf[ADF_CFG_MAX_VAL_LEN_IN_BYTES];

	snprintf(key_buf, ADF_CFG_MAX_KEY_LEN_IN_BYTES, format, key);

	if (adf_cfg_get_param_value(accel_dev, section, key_buf, val_buf))
		return -EFAULT;

	if (kstrtouint(val_buf, 10, value))
		return -EFAULT;
	return 0;
}

static void adf_get_coalesc_timer(struct adf_etr_bank_data *bank,
	const char *section,
	uint32_t bank_num_in_accel)
{
	struct adf_accel_dev *accel_dev = bank->accel_dev;
	struct adf_hw_device_data *hw_data = accel_dev->hw_device;
	u32 coalesc_timer = ADF_COALESCING_DEF_TIME;

	adf_get_cfg_int(accel_dev, section,
		ADF_ETRMGR_COALESCE_TIMER_FORMAT,
		bank_num_in_accel, &coalesc_timer);

	if (hw_data->get_clock_speed)
		bank->irq_coalesc_timer =
			(coalesc_timer * (hw_data->get_clock_speed(hw_data) /
					USEC_PER_SEC)) /
			NSEC_PER_USEC;
	else
		bank->irq_coalesc_timer = coalesc_timer;

	if (bank->irq_coalesc_timer > ADF_COALESCING_MAX_TIME)
		bank->irq_coalesc_timer = ADF_COALESCING_MAX_TIME;
	else if (bank->irq_coalesc_timer < ADF_COALESCING_MIN_TIME)
		bank->irq_coalesc_timer = ADF_COALESCING_MIN_TIME;
}

static int adf_init_bank(struct adf_accel_dev *accel_dev,
	struct adf_etr_bank_data *bank,
	uint32_t bank_num, void __iomem *csr_addr)
{
	struct adf_hw_device_data *hw_data = accel_dev->hw_device;
	struct adf_etr_ring_data *ring;
	struct adf_etr_ring_data *tx_ring;
	uint32_t i, coalesc_enabled = 0;
	u8 num_rings_per_bank = hw_data->num_rings_per_bank;
	unsigned long ring_mask;
	u32 size;

	memset(bank, 0, sizeof(*bank));
	bank->bank_number = bank_num;
	bank->csr_addr = csr_addr;
	bank->accel_dev = accel_dev;
	spin_lock_init(&bank->lock);

	/* Allocate the rings in the bank */
	size = num_rings_per_bank * sizeof(struct adf_etr_ring_data);
	bank->rings = kzalloc_node(size, GFP_KERNEL,
			dev_to_node(&GET_DEV(accel_dev)));
	if (!bank->rings)
		return -ENOMEM;

	/* Enable IRQ coalescing always. This will allow to use
	 * the optimised flag and coalesc register.
	 * If it is disabled in the config file just use min time value
	 */
	if ((adf_get_cfg_int(accel_dev, "Accelerator0",
				ADF_ETRMGR_COALESCING_ENABLED_FORMAT, bank_num,
				&coalesc_enabled) == 0) && coalesc_enabled)
		adf_get_coalesc_timer(bank, "Accelerator0", bank_num);
	else
		bank->irq_coalesc_timer = ADF_COALESCING_MIN_TIME;

	for (i = 0; i < num_rings_per_bank; i++) {
		//WRITE_CSR_RING_CONFIG(csr_addr, bank_num, i, 0);
		//WRITE_CSR_RING_BASE(csr_addr, bank_num, i, 0);
		ring = &bank->rings[i];

		if (hw_data->tx_rings_mask & (1 << i)) {
			ring->inflights =
				kzalloc_node(sizeof(atomic_t),
					GFP_KERNEL,
					dev_to_node(&GET_DEV(accel_dev)));
			if (!ring->inflights)
				goto err;
		} else {
			if (i < hw_data->tx_rx_gap) {
				dev_err(&GET_DEV(accel_dev),
					"Invalid tx rings mask config\n");
				goto err;
			}
			tx_ring = &bank->rings[i - hw_data->tx_rx_gap];
			ring->inflights = tx_ring->inflights;
		}
	}
	if (adf_bank_debugfs_add(bank)) {
		dev_err(&GET_DEV(accel_dev),
			"Failed to add bank debugfs entry\n");
		goto err;
	}

	//WRITE_CSR_INT_FLAG(csr_addr, bank_num, ADF_BANK_INT_FLAG_CLEAR_MASK);
	//for (i = 0; i < num_rings_per_bank / ADF_RINGS_PER_INT_SRCSEL; i++)
	//WRITE_CSR_INT_SRCSEL(csr_addr, bank_num, i);
	return 0;
err:
	ring_mask = hw_data->tx_rings_mask;
	for_each_set_bit(i, &ring_mask, num_rings_per_bank) {
		ring = &bank->rings[i];
		kfree(ring->inflights);
		ring->inflights = NULL;
	}
	kfree(bank->rings);
	return -ENOMEM;
}

static int adf_create_etr_data(struct adf_accel_dev *accel_dev)
{
	/* FIXME */
	struct adf_etr_data *etr_data;
	//struct adf_hw_device_data *hw_data = accel_dev->hw_device;
	uint32_t size;
	uint32_t num_banks = 0;
	//int i, ret = 0;
	int ret = 0;

	etr_data = kzalloc_node(sizeof(*etr_data), GFP_KERNEL,
			dev_to_node(&GET_DEV(accel_dev)));
	if (!etr_data)
		return -ENOMEM;

	num_banks = GET_MAX_BANKS(accel_dev);
	size = num_banks * sizeof(struct adf_etr_bank_data);
	etr_data->banks = kzalloc_node(size, GFP_KERNEL,
			dev_to_node(&GET_DEV(accel_dev)));
	if (!etr_data->banks) {
		ret = -ENOMEM;
		goto err_bank;
	}

	accel_dev->transport = etr_data;
	/* FIXME */

	/* accel_dev->debugfs_dir should always be non-NULL here */
	etr_data->debug = debugfs_create_dir("transport",
			accel_dev->debugfs_dir);
	if (!etr_data->debug) {
		dev_err(&GET_DEV(accel_dev),
			"Unable to create transport debugfs entry\n");
		ret = -ENOENT;
		goto err_bank_debug;
	}
	return 0;

err_bank_debug:
	kfree(etr_data->banks);
err_bank:
	kfree(etr_data);
	accel_dev->transport = NULL;
	return ret;
}

static int adf_init_banks(struct adf_accel_dev *accel_dev)
{
	struct adf_etr_data *etr_data;
	//struct adf_hw_device_data *hw_data = accel_dev->hw_device;
	void __iomem *csr_addr = NULL;
	uint32_t num_banks = 0;
	int i, ret = 0;

	/* FIXME */
	etr_data = accel_dev->transport;
	num_banks = GET_MAX_BANKS(accel_dev);

	for (i = 0; i < num_banks; i++) {
		ret = adf_init_bank(accel_dev, &etr_data->banks[i], i, csr_addr);
		if (ret)
			return ret;
	}
	return ret;
}

/**
 * adf_init_etr_data() - Initialize transport rings for acceleration device
 * @accel_dev:  Pointer to acceleration device.
 *
 * Function is the initializes the communications channels (rings) to the
 * acceleration device accel_dev.
 * To be used by QAT device specific drivers.
 *
 * Return: 0 on success, error code otherwise.
 */
int adf_init_etr_data(struct adf_accel_dev *accel_dev)
{
	struct adf_etr_data *etr_data = accel_dev->transport;
	int ret = 0;

	if (!adf_devmgr_in_reset(accel_dev) || !(accel_dev->is_vf))
		ret = adf_create_etr_data(accel_dev);
	if (ret)
		goto err_create;

	ret = adf_init_banks(accel_dev);
	if (ret)
		goto err_bank_all;

	return 0;

err_bank_all:
	debugfs_remove(etr_data->debug);
	kfree(etr_data->banks);
	kfree(etr_data);
	accel_dev->transport = NULL;
err_create:
	return ret;
}
EXPORT_SYMBOL_GPL(adf_init_etr_data);

static void cleanup_bank(struct adf_etr_bank_data *bank)
{
	uint32_t i;
	struct adf_accel_dev *accel_dev = bank->accel_dev;
	struct adf_hw_device_data *hw_data = accel_dev->hw_device;
	u8 num_rings_per_bank = hw_data->num_rings_per_bank;

	for (i = 0; i < num_rings_per_bank; i++) {
		struct adf_etr_ring_data *ring = &bank->rings[i];

		if (bank->ring_mask & (1 << i))
			adf_cleanup_ring(ring);

		if (hw_data->tx_rings_mask & (1 << i)) {
			kfree(ring->inflights);
			ring->inflights = NULL;
		}
	}
	kfree(bank->rings);
	adf_bank_debugfs_rm(bank);
	memset(bank, 0, sizeof(*bank));
}

static void adf_cleanup_etr_handles(struct adf_accel_dev *accel_dev)
{
	struct adf_etr_data *etr_data = accel_dev->transport;
	uint32_t i, num_banks = GET_MAX_BANKS(accel_dev);

	if (!etr_data->banks->rings)
		return;

	for (i = 0; i < num_banks; i++)
		cleanup_bank(&etr_data->banks[i]);
}

static void adf_destroy_etr_data(struct adf_accel_dev *accel_dev)
{
	struct adf_etr_data *etr_data = accel_dev->transport;

	debugfs_remove(etr_data->debug);
	kfree(etr_data->banks->rings);
	kfree(etr_data->banks);
	kfree(etr_data);
	accel_dev->transport = NULL;
}

/**
 * adf_cleanup_etr_data() - Clear transport rings for acceleration device
 * @accel_dev:  Pointer to acceleration device.
 *
 * Function is the clears the communications channels (rings) of the
 * acceleration device accel_dev.
 * To be used by QAT device specific drivers.
 *
 * Return: void
 */
void adf_cleanup_etr_data(struct adf_accel_dev *accel_dev)
{
	struct adf_etr_data *etr_data = accel_dev->transport;

	if (!etr_data)
		return;

	adf_cleanup_etr_handles(accel_dev);

	if (!adf_devmgr_in_reset(accel_dev) || (!accel_dev->is_vf))
		adf_destroy_etr_data(accel_dev);
}
EXPORT_SYMBOL_GPL(adf_cleanup_etr_data);
