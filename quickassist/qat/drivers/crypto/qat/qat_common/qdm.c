// SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only)
/* Copyright(c) 2014 - 2021 Intel Corporation */
#ifndef LINUX_VERSION_CODE
#include <linux/version.h>
#else
#define KERNEL_VERSION(a, b, c) (((a) << 16) + ((b) << 8) + (c))
#endif

#include <linux/iommu.h>
#include <linux/module.h>
//#include <linux/pci.h>
#include <linux/platform_device.h>
#include "qdm.h"

static struct iommu_domain *domain;

/**
 * qdm_attach_device() - Attach a device to the QAT IOMMU domain
 * @dev: Device to be attached
 *
 * Function attaches the device to the QDM IOMMU domain.
 *
 * Return: 0 on success, error code otherwise.
 */
int qdm_attach_device(struct device *dev)
{
	if (!domain)
		return 0;

	if (!dev) {
		pr_err("QDM: Invalid device\n");
		return -ENODEV;
	}

	return iommu_attach_device(domain, dev);
}

/**
 * qdm_detach_device() - Detach a device from the QAT IOMMU domain
 * @dev: Device to be detached
 *
 * Function detaches the device from the QDM IOMMU domain.
 *
 * Return: 0 on success, error code otherwise.
 */
int qdm_detach_device(struct device *dev)
{
	if (!domain)
		return 0;

	if (!dev) {
		pr_err("QDM: Invalid device\n");
		return -ENODEV;
	}

	iommu_detach_device(domain, dev);
	return 0;
}

static int qdm_iommu_mem_map(dma_addr_t *iova, phys_addr_t paddr, size_t size)
{
	if (!domain)
		return 0;

#if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,34)
	return iommu_map_range(domain, *iova, paddr, size,
			IOMMU_READ | IOMMU_WRITE | IOMMU_CACHE);
#elif LINUX_VERSION_CODE <= KERNEL_VERSION(3,2,45) && \
    LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,35)
	return iommu_map(domain, *iova, paddr, get_order(size),
			IOMMU_READ | IOMMU_WRITE | IOMMU_CACHE);
#else
	return iommu_map(domain, *iova, paddr, size,
			IOMMU_READ | IOMMU_WRITE | IOMMU_CACHE);
#endif
}

/**
 * qdm_iommu_map() - Map a block of memory to the QAT IOMMU domain
 * @iova:   Device virtual address
 * @vaddr:  Kernel virtual address
 * @size:   Size (in bytes) of the memory block.
 *          Must be a multiple of PAGE_SIZE
 *
 * Function maps a block of memory to the QDM IOMMU domain.
 *
 * Return: 0 on success, error code otherwise.
 */
int qdm_iommu_map(dma_addr_t *iova, void *vaddr, size_t size)
{
	phys_addr_t paddr = (phys_addr_t)virt_to_phys(vaddr);
	*iova = (dma_addr_t)paddr;
	return qdm_iommu_mem_map(iova, paddr, size);
}
EXPORT_SYMBOL_GPL(qdm_iommu_map);

/**
 * qdm_iommu_unmap() - Unmap a block of memory from the QAT IOMMU domain
 * @iova:   Device virtual address
 * @size:   Size (in bytes) of the memory block
 *          Must be the same size as mapped.
 *
 * Function unmaps a block of memory from the QDM IOMMU domain.
 *
 * Return: 0 on success, error code otherwise.
 */
int qdm_iommu_unmap(dma_addr_t iova, size_t size)
{
	if (!domain)
		return 0;

#if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,34)
	iommu_unmap_range(domain, (unsigned long)iova, size);
#elif LINUX_VERSION_CODE <= KERNEL_VERSION(3,2,45) && \
    LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,35)
	iommu_unmap(domain, (unsigned long)iova, get_order(size));
#else
	iommu_unmap(domain, (unsigned long)iova, size);
#endif

	return 0;
}
EXPORT_SYMBOL_GPL(qdm_iommu_unmap);

/**
 * qdm_hugepage_iommu_map() - Map a hugepage block of memory to the QAT IOMMU
 * domain
 * @iova:   Device virtual address
 * @va_page:  Kernel virtual address
 * @size:   Size (in bytes) of the memory block.
 *          Must be a multiple of PAGE_SIZE
 *
 * Function maps a block of memory to the QDM IOMMU domain.
 *
 * Return: 0 on success, error code otherwise.
 */
int qdm_hugepage_iommu_map(dma_addr_t *iova, void *va_page, size_t size)
{
	phys_addr_t paddr = (phys_addr_t)page_to_phys((struct page *)va_page);
	*iova = (dma_addr_t)paddr;
	return qdm_iommu_mem_map(iova, paddr, size);
}
EXPORT_SYMBOL_GPL(qdm_hugepage_iommu_map);

int __init qdm_init(void)
{
	if (!iommu_present(&platform_bus_type))
		return 0;

#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 2, 0)
	domain = iommu_domain_alloc();
#else
	domain = iommu_domain_alloc(&platform_bus_type);
#endif
	if (!domain) {
		pr_err("QDM: Failed to allocate a domain\n");
		return -1;
	}
	return 0;
}

void __exit qdm_exit(void)
{
	if (domain)
		iommu_domain_free(domain);
	domain = NULL;
}
