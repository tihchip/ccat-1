/* SPDX-License-Identifier: (BSD-3-Clause OR GPL-2.0-only) */
/* Copyright(c) 2014 - 2021 Intel Corporation */
#ifndef _QAT_CRYPTO_INSTANCE_H_
#define _QAT_CRYPTO_INSTANCE_H_

#include <linux/list.h>
#include <linux/slab.h>
#include <crypto/sha.h>
#include "adf_accel_devices.h"
#include "icp_qat_fw_la.h"

#ifndef LINUX_VERSION_CODE
#include <linux/version.h>
#endif

struct qat_crypto_instance {
	struct adf_etr_ring_data *sym_tx;
	struct adf_etr_ring_data *sym_rx;
	struct adf_etr_ring_data *pke_tx;
	struct adf_etr_ring_data *pke_rx;
	struct adf_accel_dev *accel_dev;
	struct list_head list;
	unsigned long state;
	int id;
	atomic_t refctr;
};

struct ccat_alg_req_ctx {
	enum icp_hsm_alg_type alg;
	union {
		struct icp_qat_fw_la_bulk_req req;
		struct icp_hsm_msg_req msg;
	} ____cacheline_aligned;
	union {
		struct icp_hsm_sym_cipher_ctx_hw cipher_ctx_hw;
		struct icp_hsm_sym_hash_ctx_hw hash_ctx_hw;
		struct icp_hsm_sym_auth_ctx_hw auth_ctx_hw;
		struct icp_hsm_sym_aead_ctx_hw aead_ctx_hw;
		struct icp_hsm_pk_rsa_key_gen_ctx_hw     rsa_keygen;
		struct icp_hsm_pk_rsa_crt_key_gen_ctx_hw rsa_crt_keygen;
		struct icp_hsm_pk_rsa_encrypt_ctx_hw     rsa_encrypt;
		struct icp_hsm_pk_rsa_decrypt_ctx_hw     rsa_decrypt;
		struct icp_hsm_pk_rsa_crt_decrypt_ctx_hw rsa_crt_decrypt;
	} ctx_hw ____cacheline_aligned;
	dma_addr_t ctx_hw_dma ____cacheline_aligned;
};

struct ccat_aead_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	struct icp_hsm_addr_list in_list[ICP_HSM_MAX_ADDR_LIST] ____cacheline_aligned;
	struct icp_hsm_addr_list out_list[ICP_HSM_MAX_ADDR_LIST] ____cacheline_aligned;
	u8 aad[ICP_HSM_AAD_MAX_SIZE] ____cacheline_aligned;
	dma_addr_t in_list_dma ____cacheline_aligned;
	dma_addr_t out_list_dma;
	dma_addr_t aad_dma;
	u32 op;

	u32 aad_len;
	u32 crypt_len;
	u32 iv_len;
	u32 in_num;
	u32 out_num;
	bool is_same_addr;
};

struct ccat_cipher_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	struct icp_hsm_addr_list in_list[ICP_HSM_MAX_ADDR_LIST] ____cacheline_aligned;
	struct icp_hsm_addr_list out_list[ICP_HSM_MAX_ADDR_LIST] ____cacheline_aligned;
	dma_addr_t in_list_dma ____cacheline_aligned;
	dma_addr_t out_list_dma;
	u32 crypt_len;
	u32 in_num;
	u32 out_num;
	bool is_same_addr;
};

struct ccat_ahash_request {
	struct ccat_alg_req_ctx req_ctx ____cacheline_aligned;
	struct icp_hsm_addr_list in_list[ICP_HSM_MAX_ADDR_LIST] ____cacheline_aligned;
	u8 cache[SHA512_BLOCK_SIZE] ____cacheline_aligned;
	u8 cache_next[SHA512_BLOCK_SIZE] ____cacheline_aligned;
	dma_addr_t in_list_dma ____cacheline_aligned;
	dma_addr_t cache_dma;
	u32 cache_dma_sz;
	u32 cache_sz;
	u32 total_len;
	bool is_auth;
	bool is_init;
	bool is_finish;

	u32 req_nbytes;
	u32 block_sz;
	u32 in_num;
};

struct qat_crypto_request;

struct qat_crypto_request {
	union {
		struct ccat_aead_request   ccat_aead_req;
		struct ccat_cipher_request ccat_cipher_req;
		struct ccat_ahash_request  ccat_ahash_req;
	};

	struct list_head list;
	union {
		struct qat_alg_aead_ctx *aead_ctx;
#if KERNEL_VERSION(5, 5, 0) > LINUX_VERSION_CODE
		struct qat_alg_ablkcipher_ctx *ablkcipher_ctx;
#else
		struct qat_alg_skcipher_ctx *skcipher_ctx;
#endif
		struct qat_alg_ahash_ctx *ahash_ctx;
	};
	union {
		struct aead_request *aead_req;
#if KERNEL_VERSION(5, 5, 0) > LINUX_VERSION_CODE
		struct ablkcipher_request *ablkcipher_req;
#else
		struct skcipher_request *skcipher_req;
#endif
		struct ahash_request *ahash_req;
	};
	void (*cb)(struct icp_qat_fw_la_resp *resp,
		struct qat_crypto_request *req);
	void *iv;
	dma_addr_t iv_paddr;
	bool enqueued;
};

#endif
