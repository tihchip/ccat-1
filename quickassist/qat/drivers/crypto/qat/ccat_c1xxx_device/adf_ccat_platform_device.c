
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>

static void adf_ccat_device_release(struct device *dev)
{
	/* Nothing to do. */
}

static struct platform_device adf_sample_device = {
	.name = "tih_ccat_platform",
	.dev  = {
		.release = adf_ccat_device_release,
	},
};

static int __init adf_ccat_platform_init(void)
{
	return platform_device_register(&adf_sample_device);
}

static void __exit adf_ccat_platform_exit(void)
{
	platform_device_unregister(&adf_sample_device);
}

module_init(adf_ccat_platform_init);
module_exit(adf_ccat_platform_exit);
MODULE_LICENSE("GPL v2");

