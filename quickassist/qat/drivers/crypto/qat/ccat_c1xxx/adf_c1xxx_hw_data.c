
#include <adf_accel_devices.h>
#include <adf_common_drv.h>
#include <adf_dev_err.h>
#include "adf_c1xxx_hw_data.h"
#include "adf_heartbeat.h"
#include "adf_cfg.h"
#include "icp_qat_hw.h"

/* Register */
#define ADF_C1XXX_SYM_BASE		(0x02100000)
#define ADF_C1XXX_CTRL_BASE		(0x02200000)
#define ADF_C1XXX_ASYM_BASE		(ADF_C1XXX_CTRL_BASE + 0x100000)

#define ADF_C1XXX_ASYM0			ADF_C1XXX_ASYM_BASE
#define ADF_C1XXX_ASYM1			(ADF_C1XXX_ASYM0 + ADF_RING_BUNDLE_SIZE)
#define ADF_C1XXX_ASYM2			(ADF_C1XXX_ASYM1 + ADF_RING_BUNDLE_SIZE)

#define ADF_C1XXX_ASYM0_IRQ	    (7)
#define ADF_C1XXX_ASYM1_IRQ     (8)
#define ADF_C1XXX_ASYM2_IRQ     (9)

#define ADF_C1XXX_SYM0			ADF_C1XXX_SYM_BASE
#define ADF_C1XXX_SYM1			(ADF_C1XXX_SYM0 + ADF_RING_BUNDLE_SIZE)
#define ADF_C1XXX_SYM2			(ADF_C1XXX_SYM1 + ADF_RING_BUNDLE_SIZE)

#define ADF_C1XXX_SYM0_IRQ      (1)
#define ADF_C1XXX_SYM1_IRQ      (2)
#define ADF_C1XXX_SYM2_IRQ      (3)

#define ADF_C1XXX_CTRL0			ADF_C1XXX_CTRL_BASE
#define ADF_C1XXX_CTRL1			(ADF_C1XXX_CTRL0 + ADF_RING_BUNDLE_SIZE)
#define ADF_C1XXX_CTRL2			(ADF_C1XXX_CTRL1 + ADF_RING_BUNDLE_SIZE)

#define ADF_C1XXX_CTRL0_IRQ     (4)
#define ADF_C1XXX_CTRL1_IRQ     (5)
#define ADF_C1XXX_CTRL2_IRQ     (6)

static struct adf_hw_device_class c1xxx_class = {
	.name = ADF_C1XXX_DEVICE_NAME,
	.type = DEV_C1XXX,
	.instances = 0
};

static int resource_mem(struct adf_accel_dev *accel_dev, uint8_t bank_nr, uint8_t ring_nr,
						struct adf_bar *bar)
{
	struct adf_hw_resource (*c1xxx_hw_resource)[ADF_ETR_MAX_RINGS_PER_BANK] = NULL;

	c1xxx_hw_resource = accel_dev->accel_platform_dev.c1xxx_hw_resource;
	if ((bank_nr >= ADF_C1XXX_ETR_MAX_BANKS) ||
		(ring_nr >= ADF_ETR_MAX_RINGS_PER_BANK))
		return -1;

	if(bar) {
		bar->base_addr = c1xxx_hw_resource[bank_nr][ring_nr].reg_data.base_addr;
		bar->virt_addr = c1xxx_hw_resource[bank_nr][ring_nr].reg_data.virt_addr;
		bar->size      = c1xxx_hw_resource[bank_nr][ring_nr].reg_data.size;
	}

	return 0;
}

static int resource_irq(struct adf_accel_dev *accel_dev, uint8_t bank_nr, uint8_t ring_nr,
						uint32_t *irq)
{
	struct adf_hw_resource (*c1xxx_hw_resource)[ADF_ETR_MAX_RINGS_PER_BANK] = NULL;

	c1xxx_hw_resource = accel_dev->accel_platform_dev.c1xxx_hw_resource;
	if ((bank_nr >= ADF_C1XXX_ETR_MAX_BANKS) ||
		(ring_nr >= ADF_ETR_MAX_RINGS_PER_BANK))
		return -1;

	if(irq) {
		*irq = c1xxx_hw_resource[bank_nr][ring_nr].irq_data.virq;
	}

	return 0;
}

static int c1xxx_get_resource(struct adf_accel_dev *accel_dev,
	uint8_t bank_nr, uint8_t ring_nr, struct adf_bar *bar, uint32_t *irq)
{
	if (resource_mem(accel_dev, bank_nr, ring_nr, bar))
		return -EFAULT;

	if (resource_irq(accel_dev, bank_nr, ring_nr, irq))
		return -EFAULT;

	return 0;
}

static void c1xxx_set_resource(uint8_t bank_nr, uint8_t ring_nr, uint32_t *irq)
{
	//TODO
}

static enum dev_sku_info c1xxx_get_sku(struct adf_hw_device_data *self)
{
	return DEV_SKU_1;
}

static uint32_t c1xxx_get_hw_cap(struct adf_accel_dev *accel_dev)
{
	return (ICP_ACCEL_CAPABILITIES_CRYPTO_ASYMMETRIC +
			ICP_ACCEL_CAPABILITIES_CRYPTO_SYMMETRIC +
			ICP_ACCEL_CAPABILITIES_CRYPTO_CTRL +
			ICP_ACCEL_CAPABILITIES_CIPHER);
}

static void adf_enable_ints(struct adf_accel_dev *accel_dev)
{
	struct adf_hw_device_data *hw_data = accel_dev->hw_device;
	struct adf_etr_data *etr_data = accel_dev->transport;
	char val[ADF_CFG_MAX_VAL_LEN_IN_BYTES] = {0};
	unsigned long num_kernel_inst = hw_data->num_banks;
	struct adf_hw_resource (*c1xxx_hw_resource)[ADF_ETR_MAX_RINGS_PER_BANK] = NULL;
	u8 i = 0, j = 0;

	c1xxx_hw_resource = accel_dev->accel_platform_dev.c1xxx_hw_resource;
	if (adf_cfg_get_param_value(accel_dev, ADF_GENERAL_SEC,
			ADF_FIRST_USER_BUNDLE, val) == 0) {
		if (kstrtoul(val, 10, &num_kernel_inst))
			return;
	}

	for (i = 0; i < num_kernel_inst; i++) { // bank
		for (j = 0; j < ADF_C1XXX_CCAT_BANK_RINGS; j++) { // ring
			void __iomem *csr_base_addr =
						c1xxx_hw_resource[i][j].reg_data.virt_addr;
#ifdef ICP_DEBUG
			printk(KERN_DEBUG "[CCAT-INT]bank=%d, ring=%d, base_addr=0x%lx enable interrupt OK.\n",
					i, j, csr_base_addr);
#endif
			/* Enable interrupt line*/
			WRITE_CSR_RING_IE(csr_base_addr, 1);
		}
	}
}

int adf_map_hw_resources(struct adf_accel_dev *accel_dev)
{
	struct resource *res_irq, *res_mem;
	struct platform_device *pdev = NULL;
	struct adf_hw_resource (*c1xxx_hw_resource)[ADF_ETR_MAX_RINGS_PER_BANK] = NULL;
	int i, j, hw_ring = 0;

	pdev = accel_dev->accel_platform_dev.pdev;
	c1xxx_hw_resource = accel_dev->accel_platform_dev.c1xxx_hw_resource;

	/**
	 * As CCAT bank list:
	 * PK-TX, SYM-TX, PK-RX, SYM-RX-->4 rings
	 * CTRL ring for bank is not used
	 */
	/* HSM bank/ring init */
	for (i = 0; i < ADF_C1XXX_ETR_MAX_BANKS; i++) {
		/*  BANK_M ring1=SYM, ring0=PK, please see device tree tih-t690.dtsi */
		for (j = 0; j < ADF_C1XXX_CCAT_BANK_RINGS; j++) {
			if(j < 2) {//ring=0/1, PK-TX, SYM-TX
				hw_ring = i * ADF_C1XXX_DT_BANK_RINGS + j;

				res_mem = platform_get_resource(pdev, IORESOURCE_MEM, hw_ring);
				if (NULL == res_mem) {
					dev_err(&pdev->dev, "CCAT failed to get resource!\n");
					goto err;
				}

				c1xxx_hw_resource[i][j].reg_data.base_addr = res_mem->start;
				c1xxx_hw_resource[i][j].reg_data.virt_addr =
										devm_ioremap_resource(&pdev->dev, res_mem);
				c1xxx_hw_resource[i][j].reg_data.size      = resource_size(res_mem);
				if (IS_ERR(c1xxx_hw_resource[i][j].reg_data.virt_addr)) {
					dev_err(&pdev->dev, "CCAT[reg_data] failed to get resource!\n");
					return PTR_ERR(c1xxx_hw_resource[i][j].reg_data.virt_addr);
				}

				res_irq = platform_get_resource(pdev, IORESOURCE_IRQ, hw_ring);
				if (IS_ERR(res_irq)) {
					dev_err(&pdev->dev, "CCAT[irq] failed to get resource!\n");
					return PTR_ERR(res_irq);
				}

				c1xxx_hw_resource[i][j].irq_data.bank_nr = i;
				c1xxx_hw_resource[i][j].irq_data.ring_nr = j;
				c1xxx_hw_resource[i][j].irq_data.virq = res_irq->start;
				c1xxx_hw_resource[i][j].irq_data.bank = NULL;
				strcpy(c1xxx_hw_resource[i][j].irq_data.name, res_irq->name);
			} else { //ring=2/3, only ring2=ring0 PK-RX, ring3=ring1 SYM-RX
				c1xxx_hw_resource[i][j].reg_data.base_addr = c1xxx_hw_resource[i][j - 2].reg_data.base_addr;
				c1xxx_hw_resource[i][j].reg_data.virt_addr = c1xxx_hw_resource[i][j - 2].reg_data.virt_addr;
				c1xxx_hw_resource[i][j].reg_data.size      = c1xxx_hw_resource[i][j - 2].reg_data.size;
				c1xxx_hw_resource[i][j].irq_data.bank_nr   = c1xxx_hw_resource[i][j - 2].irq_data.bank_nr;
				c1xxx_hw_resource[i][j].irq_data.ring_nr   = c1xxx_hw_resource[i][j - 2].irq_data.ring_nr;
				c1xxx_hw_resource[i][j].irq_data.virq      = 0;//c1xxx_hw_resource[i][j - 2].irq_data.virq;
				c1xxx_hw_resource[i][j].irq_data.bank = c1xxx_hw_resource[i][j - 2].irq_data.bank;
				strcpy(c1xxx_hw_resource[i][j - 2].irq_data.name, c1xxx_hw_resource[i][j].irq_data.name);
			}
		}
	}
	return 0;

err:
	dev_err(&pdev->dev, "CCAT probe err!\n");
	return -1;
}

void adf_init_hw_data_c1xxx(struct adf_hw_device_data *hw_data)
{
	hw_data->dev_class = &c1xxx_class;
	hw_data->instance_id = c1xxx_class.instances++;
	hw_data->num_banks = ADF_C1XXX_ETR_MAX_BANKS;
	hw_data->num_rings_per_bank = ADF_ETR_MAX_RINGS_PER_BANK;
	hw_data->num_accel = ADF_C1XXX_MAX_ACCELERATORS;
	hw_data->num_logical_accel = 1;
	hw_data->tx_rx_gap = ADF_C1XXX_RX_RINGS_OFFSET;
	hw_data->num_instances_per_bank = ADF_C1XXX_MAX_INSTANCES_PER_BANK;
	hw_data->tx_rings_mask = ADF_C1XXX_TX_RINGS_MASK;
	hw_data->get_accel_cap = c1xxx_get_hw_cap;
	hw_data->ring_to_svc_map = ADF_DEFAULT_RING_TO_SRV_MAP;
	hw_data->extended_dc_capabilities = 0;
	hw_data->config_device = adf_config_device;
	hw_data->set_asym_rings_mask = adf_cfg_set_asym_rings_mask;
	hw_data->get_resource = c1xxx_get_resource;
	hw_data->set_resource = c1xxx_set_resource;
	hw_data->alloc_irq = adf_isr_resource_alloc;
	hw_data->free_irq  = adf_isr_resource_free;
	hw_data->enable_ints = adf_enable_ints;
	hw_data->get_sku = c1xxx_get_sku;

}

void adf_clean_hw_data_c1xxx(struct adf_hw_device_data *hw_data)
{
	hw_data->dev_class->instances--;
}
