
#include <linux/kernel.h>
#include <linux/module.h>
//#include <linux/pci.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/device.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <linux/workqueue.h>
#include <linux/io.h>
#include <linux/of_device.h>
#include <adf_accel_devices.h>
#include <adf_common_drv.h>
#include <adf_cfg.h>
#include "adf_c1xxx_hw_data.h"

static int adf_suspend(struct device *dev)
{
	/* FIXME */
	return 0;
}

static int adf_resume(struct device *dev)
{
	/* FIXME */
	return 0;
}

static void adf_cleanup_platform_dev(struct adf_accel_dev *accel_dev)
{
	/* FIXME */
}

static void adf_cleanup_accel(struct adf_accel_dev *accel_dev)
{
	if (accel_dev->hw_device) {
		adf_clean_hw_data_c1xxx(accel_dev->hw_device);
		kfree(accel_dev->hw_device);
		accel_dev->hw_device = NULL;
	}
	adf_cfg_dev_remove(accel_dev);
	debugfs_remove(accel_dev->clock_dbgfile);
	debugfs_remove(accel_dev->debugfs_dir);
	adf_devmgr_rm_dev(accel_dev, NULL);

	return;
}

static int adf_probe(struct platform_device *pdev)
{
	struct adf_accel_dev *accel_dev;
	struct adf_accel_platform *accel_platform_dev;
	struct adf_hw_device_data *hw_data;
	char name[ADF_DEVICE_NAME_LENGTH];
	int ret = 0;

#ifdef ICP_DEBUG
	dev_info(&pdev->dev, "c1xxx - adf_probe\n");
#endif

	/* allocate the custom structure */
	accel_dev = kzalloc_node(sizeof(*accel_dev),
			GFP_KERNEL,
			dev_to_node(&pdev->dev));
	if (!accel_dev)
		return -ENOMEM;

	INIT_LIST_HEAD(&accel_dev->crypto_list);
	accel_platform_dev = &accel_dev->accel_platform_dev;
	accel_platform_dev->pdev = pdev;

	/* Add accel device to accel table.
	 * This should be called before adf_cleanup_accel is called */
	if (adf_devmgr_add_dev(accel_dev, NULL)) {
		dev_err(&pdev->dev, "Failed to add new accelerator device.\n");
		kfree(accel_dev);
		return -EFAULT;
	}

	accel_dev->owner = THIS_MODULE;
	/* Allocate and configure device configuration structure */
	hw_data = kzalloc_node(sizeof(*hw_data),
			GFP_KERNEL,
			dev_to_node(&pdev->dev));
	if (!hw_data) {
		ret = -ENOMEM;
		goto out_err;
	}

	accel_dev->hw_device = hw_data;
	adf_init_hw_data_c1xxx(accel_dev->hw_device);

	/* Fetch reg-data and irq-data and map them from device tree */
	ret = adf_map_hw_resources(accel_dev);
	if (ret)
		goto out_err;

	accel_dev->accel_platform_dev.revid = ADF_C1XXX_REV_ID;

	/* Create dev top level debugfs entry */
	snprintf(name, sizeof(name), "%s%s_%u",
		ADF_DEVICE_NAME_PREFIX,
		hw_data->dev_class->name,
		hw_data->instance_id);

	accel_dev->debugfs_dir = debugfs_create_dir(name, NULL);
	if (!accel_dev->debugfs_dir) {
		dev_err(&pdev->dev, "Could not create debugfs dir %s\n", name);
		ret = -EINVAL;
		goto out_err;
	}
	adf_clock_debugfs_add(accel_dev);

	/* Create device configuration table */
	ret = adf_cfg_dev_add(accel_dev);
	if (ret)
		goto out_err;

	if (hw_data->get_accel_cap) {
		hw_data->accel_capabilities_mask =
			hw_data->get_accel_cap(accel_dev);
	}

	ret = qat_crypto_dev_config(accel_dev);
	if (ret)
		goto out_err;

	ret = adf_dev_init(accel_dev);
	if (ret)
		goto out_err_dev_shutdown;

	ret = adf_dev_start(accel_dev);
	if (ret)
		goto out_err_dev_stop;

	return ret;

out_err_dev_stop:
	adf_dev_stop(accel_dev);
out_err_dev_shutdown:
	adf_dev_shutdown(accel_dev);
out_err:
	adf_cleanup_accel(accel_dev);
	kfree(accel_dev);
	return ret;
}

static int adf_remove(struct platform_device *pdev)
{
	/* FIXME */
	struct adf_accel_dev *accel_dev = adf_devmgr_platform_to_accel_dev(pdev);

	if (unlikely(!accel_dev)) {
		pr_err("QAT: Driver removal failed\n");
	} else {
		adf_dev_stop(accel_dev);
		adf_dev_shutdown(accel_dev);
		adf_cleanup_accel(accel_dev);
		adf_cleanup_platform_dev(accel_dev);
		kfree(accel_dev);
	}

	return 0;
}

static const struct dev_pm_ops adf_pm_ops = {
	SET_LATE_SYSTEM_SLEEP_PM_OPS(adf_suspend, adf_resume)
};

/* ccat device of match struct members definitions */
static const struct of_device_id tih_ccat_dt_ids[] = {
	{
		.compatible = "tih,t690-ccat",
	},
	{},
};
MODULE_DEVICE_TABLE(of, tih_ccat_dt_ids);

static struct platform_driver adf_driver = {
	.driver = {
		.name = "tih,t690-ccat",
		.of_match_table = tih_ccat_dt_ids,
		.pm = &adf_pm_ops,
	},
	.probe = adf_probe,
	.remove = adf_remove,
};

module_platform_driver(adf_driver);

MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("TIH CCAT Platform Driver");

