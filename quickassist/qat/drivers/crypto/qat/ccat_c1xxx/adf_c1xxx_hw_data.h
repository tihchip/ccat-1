
#ifndef __ADF_C1XXX_HW_DATA_H__
#define __ADF_C1XXX_HW_DATA_H__

#define ADF_C1XXX_ETR_MAX_BANKS				(ADF_PLATFORM_MAX_BANKS)
#define ADF_C1XXX_CCAT_BANK_RINGS			(ADF_ETR_MAX_RINGS_PER_BANK)
#define ADF_C1XXX_DT_BANK_RINGS				2 //Only use PK/SYM ring, not CTRL ring in device tree.

/* max_inst indicates the max instance number one bank can hold */
#define ADF_C1XXX_MAX_INSTANCES_PER_BANK	2 //TODO

#define ADF_C1XXX_RX_RINGS_OFFSET			1
#define ADF_C1XXX_TX_RINGS_MASK				0xFF
#define ADF_C1XXX_MAX_ACCELERATORS			3

#define ADF_C1XXX_REV_ID                    0x11

/* function */
int adf_map_hw_resources(struct adf_accel_dev *accel_dev);
void adf_init_hw_data_c1xxx(struct adf_hw_device_data *hw_data);
void adf_clean_hw_data_c1xxx(struct adf_hw_device_data *hw_data);

#endif
