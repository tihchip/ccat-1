/***************************************************************************
 *
 *   BSD LICENSE
 * 
 *   Copyright(c) 2007-2023 Intel Corporation. All rights reserved.
 *   All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *  version: QAT.L.4.22.0-00001
 *
 ***************************************************************************/

/**
 ***************************************************************************
 *
 * @file lac_ctrl.c
 *
 * @ingroup Lac_Ctrl
 *
 * SM2 functions
 * SM2 algorithm is using a fixed EC curve.
 * The length of the params is fixed to LAC_EC_SM2_SIZE_BYTES(32 bytes).
 * More details in http://tools.ietf.org/html/draft-shen-sm2-ecdsa-02
 *
 * @lld_start
 *
 * @lld_overview
 * This file implements TIH Ctrl api functions.
 * @lld_dependencies
 * - \ref LacAsymCommonQatComms "PKE QAT Comms" : For creating and sending
 * messages to the QAT
 * - \ref LacMem "Mem" : For memory allocation and freeing, and translating
 * between scalar and pointer types
 * - OSAL : For atomics and logging
 *
 * @lld_initialisation
 * On initialization this component clears the stats.
 *
 * @lld_module_algorithms
 *
 * @lld_process_context
 *
 * @lld_end
 *
 ***************************************************************************/

/*
 * ****************************************************************************
 * * Include public/global header files
 * ****************************************************************************
 * */
/* API Includes */
#include "cpa.h"
#include "cpa_cy_im.h"
#include "cpa_cy_ctrl.h"

/* OSAL Includes */
#include "Osal.h"

/* ADF Includes */
#include "icp_adf_init.h"
#include "icp_adf_transport.h"
#include "icp_accel_devices.h"
#include "icp_adf_debug.h"

/* QAT includes */
#include "icp_qat_fw_la.h"
#include "icp_qat_fw_mmp.h"
#include "icp_qat_fw_mmp_ids.h"
#include "icp_qat_fw_pke.h"

/* Look Aside Includes */
#include "lac_log.h"
#include "lac_common.h"
#include "lac_mem.h"
#include "lac_mem_pools.h"
#include "lac_pke_utils.h"
#include "lac_pke_qat_comms.h"
#include "lac_sync.h"
#include "lac_ctrl.h"
#include "lac_list.h"
#include "sal_service_state.h"
#include "lac_sal_types_crypto.h"
#include "sal_statistics.h"
#include "qae_flushCache.h"

/**< number of CTRL statistics */
#define LAC_CTRL_NUM_STATS (sizeof(CpaCyCtrlStats64) / sizeof(Cpa64U))

#ifndef DISABLE_STATS
#define LAC_CTRL_STAT_INC(statistic, pCryptoService)                           \
    do                                                                         \
    {                                                                          \
        if (CPA_TRUE ==                                                        \
            pCryptoService->generic_service_info.stats->bEccStatsEnabled)      \
        {                                                                      \
            osalAtomicInc(&(pCryptoService)                                    \
                               ->pLacCtrlStatsArr[offsetof(CpaCyCtrlStats64,   \
                                                            statistic) /       \
                                                   sizeof(Cpa64U)]);           \
        }                                                                      \
    } while (0)
/**< @ingroup Lac_Ec
 * macro to increment a CTRL stat (derives offset into array of atomics) */
#else
#define LAC_CTRL_STAT_INC(statistic, pCryptoService)                           \
    (pCryptoService) = (pCryptoService)
#endif

#define LAC_CTRL_STATS_GET(ctrlStats, pCryptoService)                          \
    do                                                                         \
    {                                                                          \
        Cpa32U i;                                                              \
                                                                               \
        for (i = 0; i < LAC_CTRL_NUM_STATS; i++)                               \
        {                                                                      \
            ((Cpa64U *)&(ctrlStats))[i] =                                      \
                osalAtomicGet(&pCryptoService->pLacCtrlStatsArr[i]);           \
        }                                                                      \
    } while (0)
/**< @ingroup Lac_Ec
 * macro to collect a ECDSA stat in sample period of performance counters */

#if defined(COUNTERS) && !defined(DISABLE_STATS)

#define LAC_CTRL_TIMESTAMP_BEGIN(pCbData, OperationDir, instanceHandle)        \
    LacCtrl_StatsBegin(pCbData, OperationDir, instanceHandle);

#define LAC_CTRL_TIMESTAMP_END(pCbData, OperationDir, instanceHandle)          \
    LacCtrl_StatsEnd(pCbData, OperationDir, instanceHandle);

void LacCtrl_StatsBegin(void *pCbData,
                         ctrl_request_type_t OperationDir,
                         CpaInstanceHandle instanceHandle);

void LacCtrl_StatsEnd(void *pCbData,
                       ctrl_request_type_t OperationDir,
                       CpaInstanceHandle instanceHandle);

#else
#define LAC_CTRL_TIMESTAMP_BEGIN(pCbData, OperationDir, instanceHandle)
#define LAC_CTRL_TIMESTAMP_END(pCbData, OperationDir, instanceHandle)
#endif

#ifdef ICP_PARAM_CHECK
/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      CTRL Check if Ctrl is supported by
 ***************************************************************************/
STATIC CpaStatus LacCtrl_SessionParamCheck(
    const CpaInstanceHandle instanceHandle)
{
    CpaCyCapabilitiesInfo capInfo;
    cpaCyQueryCapabilities(instanceHandle, &capInfo);
    if (!capInfo.ctrlSupported)
    {
        LAC_UNSUPPORTED_PARAM_LOG("Unsupported Algorithm CTRL");
        return CPA_STATUS_UNSUPPORTED;
    }
    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl sym reset function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_SymResetBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    CpaBoolean *pResetStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pResetStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl get rand function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_RandGetBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlRandGetOpData *pRandGetOpData,
    CpaFlatBuffer *pRandOutputData)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pRandGetOpData);
    LAC_CHECK_NULL_PARAM(pRandOutputData->pData);
    LAC_CHECK_SIZE(pRandOutputData, CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl set key base addr function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyBaseAddrSetBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyBaseAddrSetOpData *pAddrOpData,
    CpaBoolean *pSetStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pAddrOpData);
    LAC_CHECK_CTRL_KEY_TYPE(pAddrOpData->keyType);
    LAC_CHECK_NULL_PARAM(pSetStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl generate key function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyGenBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyGenOpData *pKeyGenOpData,
    CpaBoolean *pUpdateStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pKeyGenOpData);
    LAC_CHECK_CTRL_KEY_TYPE(pKeyGenOpData->keyType);
    LAC_CHECK_NULL_PARAM(pUpdateStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl key input function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyInputBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyInputOpData *pKeyInputeOpData,
    CpaBoolean *pKeyInputStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pKeyInputeOpData);
    LAC_CHECK_CTRL_KEY_TYPE(pKeyInputeOpData->keyType);
    LAC_CHECK_NULL_PARAM(pKeyInputeOpData->key.pData);
    LAC_CHECK_SIZE(&(pKeyInputeOpData->key), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pKeyInputStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl key output function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyOutputBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyOutputOpData *pKeyOutputOpData,
    CpaFlatBuffer *pKeyOutputData)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pKeyOutputOpData);
    LAC_CHECK_CTRL_KEY_TYPE(pKeyOutputOpData->keyType);
    LAC_CHECK_NULL_PARAM(pKeyOutputData->pData);
    LAC_CHECK_SIZE(pKeyOutputData, CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl key update function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyUpdateBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyUpdateOpData *pKeyUpdateOpData,
    CpaBoolean *pUpdateStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pKeyUpdateOpData);
    LAC_CHECK_CTRL_KEY_TYPE(pKeyUpdateOpData->keyType);
    LAC_CHECK_NULL_PARAM(pKeyUpdateOpData->key.pData);
    LAC_CHECK_SIZE(&(pKeyUpdateOpData->key), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pUpdateStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl key clear function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyClearBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyClearOpData *pKeyChearOpData,
    CpaBoolean *pClearStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pKeyChearOpData);
    LAC_CHECK_CTRL_KEY_TYPE(pKeyChearOpData->keyType);
    LAC_CHECK_NULL_PARAM(pClearStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl program OTP function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_OtpProgBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlOtpProgOpData *pOtpProgOpData,
    CpaBoolean *pOtpProgStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pOtpProgOpData);
    LAC_CHECK_NULL_PARAM(pOtpProgOpData->otpData.pData);
    LAC_CHECK_SIZE(&(pOtpProgOpData->otpData), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOtpProgStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl read OTP function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_OtpReadBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlOtpReadOpData *pOtpReadOpData,
    CpaFlatBuffer *pOtpOutputData)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pOtpReadOpData);
    LAC_CHECK_NULL_PARAM(pOtpOutputData);
    LAC_CHECK_NULL_PARAM(pOtpOutputData->pData);
    LAC_CHECK_SIZE(pOtpOutputData, CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl sleep function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_SleepBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    CpaBoolean *pSleepStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pSleepStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl wake function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_WakeBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    CpaBoolean *pWakeStatus)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pWakeStatus);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl get tdc temperature function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacCtrl_GetTdcTemperatureBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    CpaFlatBuffer *pTempOutputData)
{
    LAC_CHECK_STATUS(LacCtrl_SessionParamCheck(instanceHandle));

    LAC_CHECK_NULL_PARAM(pTempOutputData);
    LAC_CHECK_NULL_PARAM(pTempOutputData->pData);
    LAC_CHECK_SIZE(pTempOutputData, CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}
#endif

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (sym reset) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_SymResetSyn(const CpaInstanceHandle instanceHandle,
                                     CpaBoolean *pResetStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlSymReset(instanceHandle,
                                   (CpaCyCtrlSymResetCbFunc)LacSync_GenVerifyCb,
                                   pSyncCallbackData,
                                   pResetStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlSymResetRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pResetStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pResetStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL SYM RESET FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlSymResetCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (get rand) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_RandGetSyn(const CpaInstanceHandle instanceHandle,
                                    const CpaCyCtrlRandGetOpData *pOpData,
                                    CpaFlatBuffer *pRandOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlRandGet(instanceHandle,
                                  (CpaCyCtrlRandGetCbFunc)LacSync_GenFlatBufCb,
                                  pSyncCallbackData,
                                  pOpData,
                                  pRandOutputData);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlRandGetRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            NULL);

        if (CPA_STATUS_SUCCESS != wCbStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL GET RANDOM FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlRandGetCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (internel key base addr set) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyBaseAddrSetSyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyBaseAddrSetOpData *pOpData,
    CpaBoolean *pSetStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlKeyBaseAddrSet(
                        instanceHandle,
                        (CpaCyCtrlKeyBaseAddrSetCbFunc)LacSync_GenVerifyCb,
                        pSyncCallbackData,
                        pOpData,
                        pSetStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlKeyBaseAddrSetRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pSetStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pSetStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL SET KEY BASE ADDR FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlKeyBaseAddrSetCompletedError,
                               pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (internel generation) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyGenSyn(const CpaInstanceHandle instanceHandle,
                                   const CpaCyCtrlKeyGenOpData *pOpData,
                                   CpaBoolean *pUpdateStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlKeyGen(instanceHandle,
                                 (CpaCyCtrlKeyGenCbFunc)LacSync_GenVerifyCb,
                                 pSyncCallbackData,
                                 pOpData,
                                 pUpdateStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlKeyGenRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pUpdateStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pUpdateStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL GENERATE KEY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlKeyGenCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key input) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyInputSyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyInputOpData *pOpData,
    CpaBoolean *pUpdateStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlKeyInput(
                        instanceHandle,
                        (CpaCyCtrlKeyInputCbFunc)LacSync_GenVerifyCb,
                        pSyncCallbackData,
                        pOpData,
                        pUpdateStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlKeyOutputCompletedError, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pUpdateStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pUpdateStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL INPUT KEY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlKeyInputCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key output) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyOutputSyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyOutputOpData *pOpData,
    CpaFlatBuffer *pKeyOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlKeyOutput(
                        instanceHandle,
                        (CpaCyCtrlKeyOutputCbFunc)LacSync_GenFlatBufCb,
                        pSyncCallbackData,
                        pOpData,
                        pKeyOutputData);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlKeyOutputRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            NULL);

        if (CPA_STATUS_SUCCESS != wCbStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL OUTPUT KEY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlKeyOutputCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key update) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyUpdateSyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyUpdateOpData *pOpData,
    CpaBoolean *pUpdateStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlKeyUpdate(
                        instanceHandle,
                        (CpaCyCtrlKeyUpdateCbFunc)LacSync_GenVerifyCb,
                        pSyncCallbackData,
                        pOpData,
                        pUpdateStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlKeyUpdateRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pUpdateStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pUpdateStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL UPDATE KEY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlKeyUpdateCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key clear) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_KeyClearSyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlKeyClearOpData *pOpData,
    CpaBoolean *pClearStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlKeyClear(instanceHandle,
                                   (CpaCyCtrlKeyClearCbFunc)LacSync_GenVerifyCb,
                                   pSyncCallbackData,
                                   pOpData,
                                   pClearStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlKeyClearRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pClearStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pClearStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL CLEAR KEY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlKeyClearCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (OTP program) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_OtpProgSyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlOtpProgOpData *pOpData,
    CpaBoolean *pOtpProgStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlOtpProg(instanceHandle,
                                  (CpaCyCtrlOtpProgCbFunc)LacSync_GenVerifyCb,
                                  pSyncCallbackData,
                                  pOpData,
                                  pOtpProgStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlOtpProgRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pOtpProgStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pOtpProgStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL OTP PROGRAM FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlOtpProgCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (OTP read) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_OtpReadSyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyCtrlOtpReadOpData *pOpData,
    CpaFlatBuffer *pOtpOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlOtpRead(instanceHandle,
                                  (CpaCyCtrlOtpReadCbFunc)LacSync_GenFlatBufCb,
                                  pSyncCallbackData,
                                  pOpData,
                                  pOtpOutputData);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlOtpReadRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            NULL);

        if (CPA_STATUS_SUCCESS != wCbStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL OTP READ FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlOtpReadCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (Sleep) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_SleepSyn(const CpaInstanceHandle instanceHandle,
                                  CpaBoolean *pSleepStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlSleep(instanceHandle,
                               (CpaCyCtrlSleepCbFunc)LacSync_GenVerifyCb,
                               pSyncCallbackData,
                               pSleepStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlSleepRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pSleepStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pSleepStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL SLEEP FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlSleepCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (Wake) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_WakeSyn(const CpaInstanceHandle instanceHandle,
                                 CpaBoolean *pWakeStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlWake(instanceHandle,
                               (CpaCyCtrlWakeCbFunc)LacSync_GenVerifyCb,
                               pSyncCallbackData,
                               pWakeStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlWakeRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pWakeStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pWakeStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL WAKE FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlWakeCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (Wake) operation
 *
 ***************************************************************************/
STATIC CpaStatus LacCtrl_GetTdcTemperatureSyn(
    const CpaInstanceHandle instanceHandle,
    CpaFlatBuffer *pTempOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyCtrlGetTdcTemperature(
                    instanceHandle,
                    (CpaCyCtrlTdcTempCbFunc)LacSync_GenFlatBufCb,
                    pSyncCallbackData,
                    pTempOutputData);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_CTRL_STAT_INC(numCtrlTdcTempRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            NULL);

        if (CPA_STATUS_SUCCESS != wCbStatus)
        {
            status = wCbStatus;
            LAC_LOG("CTRL GET TDC TEMPERATURE FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_CTRL_STAT_INC(numCtrlTdcTempCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl sym reset internal callback
 ***************************************************************************/
STATIC void LacCtrl_SymResetCb(CpaStatus status,
                               CpaBoolean symResetStatus,
                               CpaInstanceHandle instanceHandle,
                               lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlSymResetCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pCb = (CpaCyCtrlSymResetCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_SYM_RESET, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlSymResetCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlSymResetCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == symResetStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlSymResetCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, NULL, symResetStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl get random internal callback
 ***************************************************************************/
STATIC void LacCtrl_RandGetCb(CpaStatus status,
                              CpaBoolean randStatus,
                              CpaInstanceHandle instanceHandle,
                              lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlRandGetCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlRandGetOpData *pOpData = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlRandGetCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_RAND_GET, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlRandGetCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlRandGetCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == randStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlRandGetCompletedOutputInvalid, pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->pData,
                             pOutData->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl set key base address internal callback
 ***************************************************************************/
STATIC void LacCtrl_KeyBaseAddrSetCb(CpaStatus status,
                                     CpaBoolean keyStatus,
                                     CpaInstanceHandle instanceHandle,
                                     lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlKeyBaseAddrSetCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlKeyBaseAddrSetOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlKeyBaseAddrSetCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_KEY_BASE_ADDR_SET, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyBaseAddrSetCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyBaseAddrSetCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == keyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlKeyBaseAddrSetCompletedOutputInvalid,
                            pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, keyStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl generate key internal callback
 ***************************************************************************/
STATIC void LacCtrl_KeyGenCb(CpaStatus status,
                             CpaBoolean keyStatus,
                             CpaInstanceHandle instanceHandle,
                             lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlKeyGenCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlKeyGenOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlKeyGenCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_KEY_GEN, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyGenCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyGenCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == keyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlKeyGenCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, keyStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl input key internal callback
 ***************************************************************************/
STATIC void LacCtrl_KeyInputCb(CpaStatus status,
                               CpaBoolean keyStatus,
                               CpaInstanceHandle instanceHandle,
                               lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlKeyInputCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlKeyInputOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlKeyInputCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_KEY_INPUT, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyInputCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyInputCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == keyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlKeyInputCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, keyStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl output key internal callback
 ***************************************************************************/
STATIC void LacCtrl_KeyOutputCb(CpaStatus status,
                                CpaBoolean keyStatus,
                                CpaInstanceHandle instanceHandle,
                                lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlKeyOutputCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlKeyOutputOpData *pOpData = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlKeyOutputCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_KEY_OUTPUT, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyOutputCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyOutputCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == keyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlKeyOutputCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl update key internal callback
 ***************************************************************************/
STATIC void LacCtrl_KeyUpdateCb(CpaStatus status,
                                CpaBoolean keyStatus,
                                CpaInstanceHandle instanceHandle,
                                lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlKeyUpdateCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlKeyUpdateOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlKeyUpdateCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_KEY_UPDATE, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyUpdateCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyUpdateCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == keyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlKeyUpdateCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, keyStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl update key internal callback
 ***************************************************************************/
STATIC void LacCtrl_KeyClearCb(CpaStatus status,
                               CpaBoolean clearStatus,
                               CpaInstanceHandle instanceHandle,
                               lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlKeyClearCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlKeyClearOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlKeyClearCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_KEY_CLEAR, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyClearCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyClearCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == clearStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlKeyClearCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, clearStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl OTP program internal callback
 ***************************************************************************/
STATIC void LacCtrl_OtpProgCb(CpaStatus status,
                              CpaBoolean otpProgStatus,
                              CpaInstanceHandle instanceHandle,
                              lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlOtpProgCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlOtpProgOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlOtpProgCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_OTP_PROG, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlOtpProgCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlOtpProgCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == otpProgStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlOtpProgCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, otpProgStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl OTP program internal callback
 ***************************************************************************/
STATIC void LacCtrl_OtpReadCb(CpaStatus status,
                              CpaBoolean otpReadStatus,
                              CpaInstanceHandle instanceHandle,
                              lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlOtpReadCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyCtrlOtpReadOpData *pOpData = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyCtrlOtpReadCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_OTP_READ, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlOtpReadCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlOtpReadCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == otpReadStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlOtpReadCompletedOutputInvalid, pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->pData,
                             pOutData->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl sleep internal callback
 ***************************************************************************/
STATIC void LacCtrl_SleepCb(CpaStatus status,
                              CpaBoolean sleepStatus,
                              CpaInstanceHandle instanceHandle,
                              lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlSleepCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pCb = (CpaCyCtrlSleepCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_SLEEP, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlSleepCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlSleepCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == sleepStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlSleepCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, NULL, sleepStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl sleep internal callback
 ***************************************************************************/
STATIC void LacCtrl_WakeCb(CpaStatus status,
                           CpaBoolean wakeStatus,
                           CpaInstanceHandle instanceHandle,
                           lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlWakeCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pCb = (CpaCyCtrlWakeCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_WAKE, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlWakeCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlWakeCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == wakeStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlWakeCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, NULL, wakeStatus);
}

/***************************************************************************
 * @ingroup Lac_Ctrl
 *      Ctrl sleep internal callback
 ***************************************************************************/
STATIC void LacCtrl_GetTdcTemperatureCb(CpaStatus status,
                                        CpaBoolean tdcStatus,
                                        CpaInstanceHandle instanceHandle,
                                        lac_pke_op_cb_data_t *pCbData)
{
    CpaCyCtrlTdcTempCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pCb = (CpaCyCtrlTdcTempCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_CTRL_TIMESTAMP_END(pCbData, LAC_CTRL_TDC_TEMP_GET, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlTdcTempCompleted, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlTdcTempCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == tdcStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_CTRL_STAT_INC(numCtrlTdcTempCompletedOutputInvalid, pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, NULL, pOutData);
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (Sleep) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlSymReset(const CpaInstanceHandle instanceHandle_in,
                            const CpaCyCtrlSymResetCbFunc pSymResetCb,
                            void *pCallbackTag,
                            CpaBoolean *pSymResetStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pSymResetCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_SymResetSyn(instanceHandle, pSymResetStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_SymResetBasicParamCheck(instanceHandle, pSymResetStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_SYM_RESET);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pSymResetCb;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_SymResetCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlSymResetRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlSymResetRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (get rand) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlRandGet(const CpaInstanceHandle instanceHandle_in,
                           const CpaCyCtrlRandGetCbFunc pRandGetCb,
                           void *pCallbackTag,
                           const CpaCyCtrlRandGetOpData *pRandGetOpData,
                           CpaFlatBuffer *pRandOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }
#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_NULL_PARAM(pRandOutputData);
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pRandGetCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_RandGetSyn(
                    instanceHandle, pRandGetOpData, pRandOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_RandGetBasicParamCheck(
                    instanceHandle, pRandGetOpData, pRandOutputData);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pRandOutputData->pData,
                            pRandOutputData->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_RAND_GET);

        desc.dst_data = LAC_OS_VIRT_TO_PHYS_INTERNAL(pRandOutputData->pData);
        desc.dst_data |= (((Cpa64U)(pRandOutputData->dataLenInBytes)) << 48);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pRandGetCb;
        cbData.pClientOpData = pRandGetOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pRandOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_RandGetCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlRandGetRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlRandGetRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (internel key base addr set) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlKeyBaseAddrSet(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyCtrlKeyBaseAddrSetCbFunc pKeyAddrCb,
    void *pCallbackTag,
    const CpaCyCtrlKeyBaseAddrSetOpData *pKeyAddrOpData,
    CpaBoolean *pKeyAddrStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pKeyAddrCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_KeyBaseAddrSetSyn(
                    instanceHandle, pKeyAddrOpData, pKeyAddrStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_KeyBaseAddrSetBasicParamCheck(
                    instanceHandle, pKeyAddrOpData, pKeyAddrStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_KEY_BASE_ADDR_SET);
        ICP_CCAT_FW_CTRL_KEY_TYPE_SET(desc, pKeyAddrOpData->keyType);

        desc.src_data = LAC_OS_VIRT_TO_PHYS_INTERNAL((void*)pKeyAddrOpData->keyBaseAddr);
        desc.src_data |= (((Cpa64U)(pKeyAddrOpData->keyLen)) << 48);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pKeyAddrCb;
        cbData.pClientOpData = pKeyAddrOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_KeyBaseAddrSetCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyBaseAddrSetRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyBaseAddrSetRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (internel generation) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlKeyGen(const CpaInstanceHandle instanceHandle_in,
                          const CpaCyCtrlKeyGenCbFunc pKeyGenCb,
                          void *pCallbackTag,
                          const CpaCyCtrlKeyGenOpData *pKeyGenOpData,
                          CpaBoolean *pKeyGenStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pKeyGenCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_KeyGenSyn(
                    instanceHandle, pKeyGenOpData, pKeyGenStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_KeyGenBasicParamCheck(
                    instanceHandle, pKeyGenOpData, pKeyGenStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_KEY_GEN);
        ICP_CCAT_FW_CTRL_KEY_TYPE_SET(desc, pKeyGenOpData->keyType);
        ICP_CCAT_FW_CTRL_KEY_ID_SET(desc, pKeyGenOpData->keyId);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pKeyGenCb;
        cbData.pClientOpData = pKeyGenOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_KeyGenCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyGenRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyGenRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key input) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlKeyInput(const CpaInstanceHandle instanceHandle_in,
                            const CpaCyCtrlKeyInputCbFunc pKeyInputCb,
                            void *pCallbackTag,
                            const CpaCyCtrlKeyInputOpData *pKeyInputeOpData,
                            CpaBoolean *pInputStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pKeyInputCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_KeyInputSyn(
                    instanceHandle, pKeyInputeOpData, pInputStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_KeyInputBasicParamCheck(
                    instanceHandle, pKeyInputeOpData, pInputStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_KEY_INPUT);
        ICP_CCAT_FW_CTRL_KEY_TYPE_SET(desc, pKeyInputeOpData->keyType);
        ICP_CCAT_FW_CTRL_KEY_ID_SET(desc, pKeyInputeOpData->keyId);
        ICP_CCAT_FW_CTRL_KEK_ID_SET(desc, pKeyInputeOpData->kekId);

        desc.src_data = LAC_OS_VIRT_TO_PHYS_INTERNAL(pKeyInputeOpData->key.pData);
        desc.src_data &= 0xFFFFFFFFFFFF;

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pKeyInputCb;
        cbData.pClientOpData = pKeyInputeOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_KeyInputCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyInputRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyInputRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key output) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlKeyOutput(const CpaInstanceHandle instanceHandle_in,
                             const CpaCyCtrlKeyOutputCbFunc pKeyOutputCb,
                             void *pCallbackTag,
                             const CpaCyCtrlKeyOutputOpData *pKeyOutputOpData,
                             CpaFlatBuffer *pKeyOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pKeyOutputCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_KeyOutputSyn(
                    instanceHandle, pKeyOutputOpData, pKeyOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_KeyOutputBasicParamCheck(
                    instanceHandle, pKeyOutputOpData, pKeyOutputData);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_KEY_OUTPUT);
        ICP_CCAT_FW_CTRL_KEY_TYPE_SET(desc, pKeyOutputOpData->keyType);
        ICP_CCAT_FW_CTRL_KEY_ID_SET(desc, pKeyOutputOpData->keyId);
        ICP_CCAT_FW_CTRL_KEK_ID_SET(desc, pKeyOutputOpData->kekId);

        desc.dst_data = LAC_OS_VIRT_TO_PHYS_INTERNAL(pKeyOutputData->pData);
        desc.dst_data &= 0xFFFFFFFFFFFF;

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pKeyOutputCb;
        cbData.pClientOpData = pKeyOutputOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pKeyOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_KeyOutputCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyOutputRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyOutputRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key update) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlKeyUpdate(const CpaInstanceHandle instanceHandle_in,
                             const CpaCyCtrlKeyUpdateCbFunc pKeyUpdateCb,
                             void *pCallbackTag,
                             const CpaCyCtrlKeyUpdateOpData *pKeyUpdateOpData,
                             CpaBoolean *pUpdateStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pKeyUpdateCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_KeyUpdateSyn(
                    instanceHandle, pKeyUpdateOpData, pUpdateStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_KeyUpdateBasicParamCheck(
                    instanceHandle, pKeyUpdateOpData, pUpdateStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_KEY_UPDATE);
        ICP_CCAT_FW_CTRL_KEY_TYPE_SET(desc, pKeyUpdateOpData->keyType);
        ICP_CCAT_FW_CTRL_KEY_ID_SET(desc, pKeyUpdateOpData->keyId);
        ICP_CCAT_FW_CTRL_KEK_ID_SET(desc, pKeyUpdateOpData->kekId);

        desc.src_data = LAC_OS_VIRT_TO_PHYS_INTERNAL(pKeyUpdateOpData->key.pData);
        desc.src_data &= 0xFFFFFFFFFFFF;

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pKeyUpdateCb;
        cbData.pClientOpData = pKeyUpdateOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_KeyUpdateCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyUpdateRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyUpdateRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (key clear) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlKeyClear(const CpaInstanceHandle instanceHandle_in,
                            const CpaCyCtrlKeyClearCbFunc pKeyClearCb,
                            void *pCallbackTag,
                            const CpaCyCtrlKeyClearOpData *pKeyChearOpData,
                            CpaBoolean *pClearStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pKeyClearCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_KeyClearSyn(
                    instanceHandle, pKeyChearOpData, pClearStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_KeyClearBasicParamCheck(
                    instanceHandle, pKeyChearOpData, pClearStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_KEY_CLEAR);
        ICP_CCAT_FW_CTRL_KEY_TYPE_SET(desc, pKeyChearOpData->keyType);
        ICP_CCAT_FW_CTRL_KEY_ID_SET(desc, pKeyChearOpData->keyId);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pKeyClearCb;
        cbData.pClientOpData = pKeyChearOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_KeyClearCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlKeyClearRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlKeyClearRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (OTP program) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlOtpProg(const CpaInstanceHandle instanceHandle_in,
                           const CpaCyCtrlOtpProgCbFunc pOtpProgCb,
                           void *pCallbackTag,
                           const CpaCyCtrlOtpProgOpData *pOtpProgOpData,
                           CpaBoolean *pOtpProgStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pOtpProgCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_OtpProgSyn(
                    instanceHandle, pOtpProgOpData, pOtpProgStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_OtpProgBasicParamCheck(
                    instanceHandle, pOtpProgOpData, pOtpProgStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_OTP_PROG);
        ICP_CCAT_FW_CTRL_OTP_OFFSET_SET(desc, pOtpProgOpData->offset);

        desc.src_data = LAC_OS_VIRT_TO_PHYS_INTERNAL(pOtpProgOpData->otpData.pData);
        desc.src_data |= (((Cpa64U)pOtpProgOpData->otpData.dataLenInBytes) << 48);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pOtpProgCb;
        cbData.pClientOpData = pOtpProgOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_OtpProgCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlOtpProgRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlOtpProgRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (OTP read) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlOtpRead(const CpaInstanceHandle instanceHandle_in,
                           const CpaCyCtrlOtpReadCbFunc pOtpReadCb,
                           void *pCallbackTag,
                           const CpaCyCtrlOtpReadOpData *pOtpReadOpData,
                           CpaFlatBuffer *pOtpOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pOtpReadCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_OtpReadSyn(
                    instanceHandle, pOtpReadOpData, pOtpOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_OtpReadBasicParamCheck(
                    instanceHandle, pOtpReadOpData, pOtpOutputData);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pOtpOutputData->pData,
                            pOtpOutputData->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_OTP_READ);
        ICP_CCAT_FW_CTRL_OTP_OFFSET_SET(desc, pOtpReadOpData->offset);

        desc.dst_data = LAC_OS_VIRT_TO_PHYS_INTERNAL(pOtpOutputData->pData);
        desc.dst_data |= (((Cpa64U)pOtpOutputData->dataLenInBytes) << 48);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pOtpReadCb;
        cbData.pClientOpData = pOtpReadOpData;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pOtpOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_OtpReadCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlOtpReadRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlOtpReadRequests, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (Sleep) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlSleep(const CpaInstanceHandle instanceHandle_in,
                         const CpaCyCtrlSleepCbFunc pSleepCb,
                         void *pCallbackTag,
                         CpaBoolean *pSleepStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pSleepCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_SleepSyn(instanceHandle, pSleepStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_SleepBasicParamCheck(instanceHandle, pSleepStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_SLEEP);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pSleepCb;
        cbData.pClientOpData = NULL;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_SleepCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlSleepRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlSleepRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (Wake) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlWake(const CpaInstanceHandle instanceHandle_in,
                        const CpaCyCtrlWakeCbFunc pWakeCb,
                        void *pCallbackTag,
                        CpaBoolean *pWakeStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pWakeCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_WakeSyn(instanceHandle, pWakeStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_WakeBasicParamCheck(instanceHandle, pWakeStatus);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_WAKE);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pWakeCb;
        cbData.pClientOpData = NULL;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_WakeCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlWakeRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlWakeRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl command (Wake) operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlGetTdcTemperature(const CpaInstanceHandle instanceHandle_in,
                                     const CpaCyCtrlTdcTempCbFunc pTempCb,
                                     void *pCallbackTag,
                                     CpaFlatBuffer *pTempOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U functionalityId = 0;
    CpaInstanceHandle instanceHandle = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_NULL_PARAM(pTempOutputData);
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pTempCb)
    {
        /* Call synchronous mode function */
        return LacCtrl_GetTdcTemperatureSyn(instanceHandle, pTempOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacCtrl_GetTdcTemperatureBasicParamCheck(
                            instanceHandle, pTempOutputData);
    if (CPA_STATUS_SUCCESS != status)
    {
        LAC_INVALID_PARAM_LOG("Parameter invalid!");
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_CTRL);
        ICP_CCAT_FW_CTRL_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_CTRL_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_CTRL_CMD_ID_SET(desc, CTRL_CMD_ID_TDC_TEMP_GET);

        desc.dst_data = LAC_OS_VIRT_TO_PHYS_INTERNAL(pTempOutputData->pData);
        desc.dst_data |= (((Cpa64U)(pTempOutputData->dataLenInBytes)) << 48);

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientCb = pTempCb;
        cbData.pClientOpData = NULL;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pTempOutputData;

        /* Flush cacheline */
        qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pTempOutputData->pData,
                              pTempOutputData->dataLenInBytes);

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                NULL,
                                (lac_pke_op_cb_func_t)LacCtrl_GetTdcTemperatureCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_CTRL_STAT_INC(numCtrlTdcTempRequests, pCryptoService);
    }
    else
    {
        LAC_CTRL_STAT_INC(numCtrlTdcTempRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ctrl
 *
 * @description
 *     TIH ctrl query stats64 operation
 *
 ***************************************************************************/
CpaStatus cpaCyCtrlQueryStats64(const CpaInstanceHandle instanceHandle_in,
                                CpaCyCtrlStats64 *pCtrlStats)
{
    sal_crypto_service_t *pCryptoService = NULL;
    CpaInstanceHandle instanceHandle = NULL;

#ifdef ICP_TRACE
    LAC_LOG2("Called with params (0x%lx, 0x%lx)\n",
             (LAC_ARCH_UINT)instanceHandle_in,
             (LAC_ARCH_UINT)pCtrlStats);
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }
    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_RUNNING_CHECK(instanceHandle);
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));
    LAC_CHECK_NULL_PARAM(pCtrlStats);

    /* get stats into user supplied stats structure */
    LAC_CTRL_STATS_GET(*pCtrlStats, pCryptoService);

    return CPA_STATUS_SUCCESS;
}
