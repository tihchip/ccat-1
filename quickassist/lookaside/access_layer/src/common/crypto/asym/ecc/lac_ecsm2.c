/***************************************************************************
 *
 *   BSD LICENSE
 * 
 *   Copyright(c) 2007-2023 Intel Corporation. All rights reserved.
 *   All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *  version: QAT.L.4.22.0-00001
 *
 ***************************************************************************/

/**
 ***************************************************************************
 *
 * @file lac_ecsm2.c
 *
 * @ingroup Lac_Ecsm2
 *
 * SM2 functions
 * SM2 algorithm is using a fixed EC curve.
 * The length of the params is fixed to LAC_EC_SM2_SIZE_BYTES(32 bytes).
 * More details in http://tools.ietf.org/html/draft-shen-sm2-ecdsa-02
 *
 * @lld_start
 *
 * @lld_overview
 * This file implements SM2 api functions.
 * @lld_dependencies
 * - \ref LacAsymCommonQatComms "PKE QAT Comms" : For creating and sending
 * messages to the QAT
 * - \ref LacMem "Mem" : For memory allocation and freeing, and translating
 * between scalar and pointer types
 * - OSAL : For atomics and logging
 *
 * @lld_initialisation
 * On initialization this component clears the stats.
 *
 * @lld_module_algorithms
 *
 * @lld_process_context
 *
 * @lld_end
 *
 ***************************************************************************/

/*
 * ****************************************************************************
 * * Include public/global header files
 * ****************************************************************************
 * */
/* API Includes */
#include "cpa.h"
#include "cpa_cy_im.h"
#include "cpa_cy_ecsm2.h"
#include "cpa_cy_ecdsa.h"

/* OSAL Includes */
#include "Osal.h"

/* ADF Includes */
#include "icp_adf_init.h"
#include "icp_adf_transport.h"
#include "icp_accel_devices.h"
#include "icp_adf_debug.h"

/* QAT includes */
#include "icp_qat_fw_la.h"
#include "icp_qat_fw_mmp.h"
#include "icp_qat_fw_mmp_ids.h"
#include "icp_qat_fw_pke.h"

/* Look Aside Includes */
#include "lac_log.h"
#include "lac_common.h"
#include "lac_mem.h"
#include "lac_mem_pools.h"
#include "lac_pke_utils.h"
#include "lac_pke_qat_comms.h"
#include "lac_sync.h"
#include "lac_ec.h"
#include "lac_list.h"
#include "sal_service_state.h"
#include "lac_sal_types_crypto.h"
#include "sal_statistics.h"
#include "qae_flushCache.h"

#define LAC_EC_SM2_SIZE_BYTES LAC_BITS_TO_BYTES(LAC_256_BITS)

/**< number of ECSM2 statistics */
#define LAC_ECSM2_NUM_STATS (sizeof(CpaCyEcsm2Stats64) / sizeof(Cpa64U))

#ifndef DISABLE_STATS
#define LAC_ECSM2_STAT_INC(statistic, pCryptoService)                          \
    do                                                                         \
    {                                                                          \
        if (CPA_TRUE ==                                                        \
            pCryptoService->generic_service_info.stats->bEccStatsEnabled)      \
        {                                                                      \
            osalAtomicInc(&(pCryptoService)                                    \
                               ->pLacEcsm2StatsArr[offsetof(CpaCyEcsm2Stats64, \
                                                            statistic) /       \
                                                   sizeof(Cpa64U)]);           \
        }                                                                      \
    } while (0)
/**< @ingroup Lac_Ec
 * macro to increment a ECSM2 stat (derives offset into array of atomics) */
#else
#define LAC_ECSM2_STAT_INC(statistic, pCryptoService)                          \
    (pCryptoService) = (pCryptoService)
#endif

#define LAC_ECSM2_STATS_GET(ecsm2Stats, pCryptoService)                        \
    do                                                                         \
    {                                                                          \
        Cpa32U i;                                                              \
                                                                               \
        for (i = 0; i < LAC_ECSM2_NUM_STATS; i++)                              \
        {                                                                      \
            ((Cpa64U *)&(ecsm2Stats))[i] =                                     \
                osalAtomicGet(&pCryptoService->pLacEcsm2StatsArr[i]);          \
        }                                                                      \
    } while (0)
/**< @ingroup Lac_Ec
 * macro to collect a ECDSA stat in sample period of performance counters */

#if defined(COUNTERS) && !defined(DISABLE_STATS)

#define LAC_ECSM2_TIMESTAMP_BEGIN(pCbData, OperationDir, instanceHandle)       \
    LacEcsm2_StatsBegin(pCbData, OperationDir, instanceHandle);

#define LAC_ECSM2_TIMESTAMP_END(pCbData, OperationDir, instanceHandle)         \
    LacEcsm2_StatsEnd(pCbData, OperationDir, instanceHandle);

void LacEcsm2_StatsBegin(void *pCbData,
                         ecsm2_request_type_t OperationDir,
                         CpaInstanceHandle instanceHandle);

void LacEcsm2_StatsEnd(void *pCbData,
                       ecsm2_request_type_t OperationDir,
                       CpaInstanceHandle instanceHandle);

#else
#define LAC_ECSM2_TIMESTAMP_BEGIN(pCbData, OperationDir, instanceHandle)
#define LAC_ECSM2_TIMESTAMP_END(pCbData, OperationDir, instanceHandle)
#endif

/*
****************************************************************************
* Define static function definitions
****************************************************************************
*/
#ifdef ICP_PARAM_CHECK
/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      return the size in bytes of biggest number in
 *CpaCyEcsm2PointMultiplyOpData
 *
 * @description
 *      return the size of the biggest number in
 *      CpaCyEcsm2PointMultiplyOpData.
 *
 * @param[in]  pOpData      Pointer to a CpaCyEcsm2PointMultiplyOpData structure
 *
 * @retval max  the size in bytes of the biggest number
 *
 ***************************************************************************/
STATIC Cpa32U
LacEcsm2_PointMulOpDataSizeGetMax(const CpaCyEcsm2PointMultiplyOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->x)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->y)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->k)), max);
    return max;
}

STATIC Cpa32U LacEcsm2_GeneratorMulOpDataSizeGetMax(
    const CpaCyEcsm2GeneratorMultiplyOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->k)), max);
    return max;
}

STATIC Cpa32U
LacEcsm2_PointVerifyOpDataSizeGetMax(const CpaCyEcsm2PointVerifyOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->x)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->y)), max);
    return max;
}

STATIC Cpa32U LacEcsm2_SignOpDataSizeGetMax(const CpaCyEcsm2SignOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->k)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->e)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->d)), max);
    return max;
}

STATIC Cpa32U
LacEcsm2_VerifyOpDataSizeGetMax(const CpaCyEcsm2VerifyOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->e)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->r)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->s)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->xP)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->yP)), max);
    return max;
}

STATIC Cpa32U
LacEcsm2_EncOpDataSizeGetMax(const CpaCyEcsm2EncryptOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->k)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->xP)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->yP)), max);
    return max;
}

STATIC Cpa32U
LacEcsm2_DecOpDataSizeGetMax(const CpaCyEcsm2DecryptOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->d)), max);
    return max;
}

STATIC Cpa32U
LacEcsm2_KeyExchangeOpDataSizeGetMax(const CpaCyEcsm2KeyExOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->dA)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->PB_X)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->PB_Y)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->rA)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->RA_X)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->RA_Y)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->RB_X)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->RB_Y)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->ZA)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->ZB)), max);
    return max;
}

STATIC Cpa32U
LacEcsm2_GetZOpDataSizeGetMax(const CpaCyEcsm2GetZOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->xP)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->yP)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->id)), max);
    return max;
}

STATIC Cpa32U
LacEcsm2_GetEOpDataSizeGetMax(const CpaCyEcsm2GetEOpData *pOpData)
{
    Cpa32U max = 0;

    /* need to find max size in bytes of number in input buffers */
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->z)), max);
    max = LAC_MAX(LacPke_GetMinBytes(&(pOpData->m)), max);
    return max;
}
#endif

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Signature synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_SignSyn(const CpaInstanceHandle instanceHandle,
                                  const CpaCyEcsm2SignOpData *pEcsm2SignOpData,
                                  CpaBoolean *pSignStatus,
                                  CpaFlatBuffer *pR,
                                  CpaFlatBuffer *pS)
{
    CpaStatus status = CPA_STATUS_FAIL;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the synchronous callback function as a parameter.
         */
        status =
            cpaCyEcsm2Sign(instanceHandle,
                           (CpaCyEcsm2SignCbFunc)LacSync_GenDualFlatBufVerifyCb,
                           pSyncCallbackData,
                           pEcsm2SignOpData,
                           pSignStatus,
                           pR,
                           pS);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_ECSM2_STAT_INC(numEcsm2SignRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pSignStatus);
        if ((CPA_STATUS_SUCCESS != wCbStatus) || (CPA_TRUE != *pSignStatus))
        {
            status = wCbStatus;
            LAC_LOG("ECSM2 SIGN FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_ECSM2_STAT_INC(numEcsm2SignCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 signature verify synchronous function
 ***************************************************************************/
STATIC CpaStatus LacEcsm2_VerifySyn(const CpaInstanceHandle instanceHandle,
                                    const CpaCyEcsm2VerifyOpData *pOpData,
                                    CpaBoolean *pVerifyStatus)
{
    CpaStatus status = CPA_STATUS_FAIL;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status = cpaCyEcsm2Verify(instanceHandle,
                                  (CpaCyEcsm2VerifyCbFunc)LacSync_GenVerifyCb,
                                  pSyncCallbackData,
                                  pOpData,
                                  pVerifyStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_ECSM2_STAT_INC(numEcsm2VerifyRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pVerifyStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pVerifyStatus)
        {
            status = wCbStatus;
            LAC_LOG("ECSM2 SIGN VERIFY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_ECSM2_STAT_INC(numEcsm2VerifyCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 point multiply synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_PointMultiplySyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyEcsm2PointMultiplyOpData *pEcsm2PointMulOpData,
    CpaBoolean *pMultiplyStatus,
    CpaFlatBuffer *pXk,
    CpaFlatBuffer *pYk)
{
    CpaStatus status = CPA_STATUS_FAIL;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the synchronous callback function as a parameter.
         */
        status = cpaCyEcsm2PointMultiply(
            instanceHandle,
            (CpaCyEcPointMultiplyCbFunc)LacSync_GenDualFlatBufVerifyCb,
            pSyncCallbackData,
            pEcsm2PointMulOpData,
            pMultiplyStatus,
            pXk,
            pYk);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_ECSM2_STAT_INC(numEcsm2PointMultiplyRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pMultiplyStatus);
        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pMultiplyStatus)
        {
            status = wCbStatus;
            LAC_LOG("ECSM2 POINT MULTIPLY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_ECSM2_STAT_INC(numEcsm2PointMultiplyCompletedError,
                               pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 generator multiply synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_GenMultiplySyn(
    const CpaInstanceHandle instanceHandle,
    const CpaCyEcsm2GeneratorMultiplyOpData *pEcsm2GenMulOpData,
    CpaBoolean *pMultiplyStatus,
    CpaFlatBuffer *pXk,
    CpaFlatBuffer *pXy)
{
    CpaStatus status = CPA_STATUS_FAIL;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the synchronous callback function as a parameter.
         */
        status = cpaCyEcsm2GeneratorMultiply(
            instanceHandle,
            (CpaCyEcPointMultiplyCbFunc)LacSync_GenDualFlatBufVerifyCb,
            pSyncCallbackData,
            pEcsm2GenMulOpData,
            pMultiplyStatus,
            pXk,
            pXy);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_ECSM2_STAT_INC(numEcsm2GeneratorMultiplyRequestErrors,
                           pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pMultiplyStatus);
        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pMultiplyStatus)
        {
            status = wCbStatus;
            LAC_LOG("ECSM2 GENERATOR MULTIPLY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_ECSM2_STAT_INC(numEcsm2GeneratorMultiplyCompletedError,
                               pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 point verify synchronous function
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_PointVerifySyn(const CpaInstanceHandle instanceHandle,
                        const CpaCyEcsm2PointVerifyOpData *pOpData,
                        CpaBoolean *pVerifyStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the generic synchronous callback function as a parameter.
         */
        status =
            cpaCyEcsm2PointVerify(instanceHandle,
                                  (CpaCyEcPointVerifyCbFunc)LacSync_GenVerifyCb,
                                  pSyncCallbackData,
                                  pOpData,
                                  pVerifyStatus);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_ECSM2_STAT_INC(numEcsm2PointVerifyRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(pSyncCallbackData,
                                            LAC_PKE_SYNC_CALLBACK_TIMEOUT,
                                            &status,
                                            pVerifyStatus);

        if (CPA_STATUS_SUCCESS != wCbStatus || CPA_TRUE != *pVerifyStatus)
        {
            status = wCbStatus;
            LAC_LOG("ECSM2 POINT VERIFY FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_ECSM2_STAT_INC(numEcsm2PointVerifyCompletedError,
                               pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Encryptio synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_EncSyn(const CpaInstanceHandle instanceHandle,
                                 const CpaCyEcsm2EncryptOpData *pEcsm2EncOpData,
                                 CpaFlatBuffer *pEcsm2EncOutput)
{
    CpaStatus status = CPA_STATUS_FAIL;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the synchronous callback function as a parameter.
         */
        status = cpaCyEcsm2Encrypt(instanceHandle,
                                   (CpaCyEcsm2EncryptCbFunc)LacSync_GenFlatBufCb,
                                   pSyncCallbackData,
                                   pEcsm2EncOpData,
                                   pEcsm2EncOutput);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_ECSM2_STAT_INC(numEcsm2EncryptRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(
            pSyncCallbackData, LAC_PKE_SYNC_CALLBACK_TIMEOUT, &status, NULL);
        if (CPA_STATUS_SUCCESS != wCbStatus)
        {
            status = wCbStatus;
            LAC_LOG("ECSM2 ENCRYPTION FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_ECSM2_STAT_INC(numEcsm2EncryptCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Decryption synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_DecSyn(const CpaInstanceHandle instanceHandle,
                                 const CpaCyEcsm2DecryptOpData *pEcsm2DecOpData,
                                 CpaFlatBuffer *pEcsm2DecOutput)
{
    CpaStatus status = CPA_STATUS_FAIL;
    CpaStatus wCbStatus = CPA_STATUS_FAIL;
    lac_sync_op_data_t *pSyncCallbackData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    status = LacSync_CreateSyncCookie(&pSyncCallbackData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /*
         * Call the asynchronous version of the function
         * with the synchronous callback function as a parameter.
         */
        status = cpaCyEcsm2Decrypt(instanceHandle,
                                   (CpaCyEcsm2DecryptCbFunc)LacSync_GenFlatBufCb,
                                   pSyncCallbackData,
                                   pEcsm2DecOpData,
                                   pEcsm2DecOutput);
    }
    else
    {
#ifndef DISABLE_STATS
        LAC_ECSM2_STAT_INC(numEcsm2DecryptRequestErrors, pCryptoService);
#endif
        return status;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        wCbStatus = LacSync_WaitForCallback(
            pSyncCallbackData, LAC_PKE_SYNC_CALLBACK_TIMEOUT, &status, NULL);

        if (CPA_STATUS_SUCCESS != wCbStatus)
        {
            status = wCbStatus;
            LAC_LOG("ECSM2 DECRYPTION FAILED!!\n");
#ifndef DISABLE_STATS
            LAC_ECSM2_STAT_INC(numEcsm2DecryptCompletedError, pCryptoService);
#endif
        }
    }
    else
    {
        /* As the Request was not sent the Callback will never
         * be called, so need to indicate that we're finished
         * with cookie so it can be destroyed. */
        LacSync_SetSyncCookieComplete(pSyncCallbackData);
    }

    LacSync_DestroySyncCookie(&pSyncCallbackData);
    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 key exchange synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_KeyeExSyn(const CpaInstanceHandle instanceHandle,
            const CpaCyEcsm2KeyExOpData *pEcsm2KeyExOpData,
            CpaCyEcsm2KeyExOutputData *pEcsm2KeyExOutputData)
{
    CpaStatus status = CPA_STATUS_FAIL;

    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 get Z synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_GetZSyn(const CpaInstanceHandle instanceHandle,
            const CpaCyEcsm2GetZOpData *pEcsm2GetZOpData,
            CpaFlatBuffer *pOut)
{
    CpaStatus status = CPA_STATUS_FAIL;

    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 get E synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_GetESyn(const CpaInstanceHandle instanceHandle,
            const CpaCyEcsm2GetEOpData *pEcsm2GetEOpData,
            CpaFlatBuffer *pOut)
{
    CpaStatus status = CPA_STATUS_FAIL;

    return status;
}

/**********************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 generate key synchronous function
 *
 **********************************************************************/
STATIC CpaStatus LacEcsm2_KeyGenSyn(const CpaInstanceHandle instanceHandle,
            const CpaCyEcsm2KeyGenOpData *pEcsm2KeyGenOpData,
            CpaCyEcsm2KeyGenOutputData *pEcsm2KeyGenOutputData)
{
    CpaStatus status = CPA_STATUS_FAIL;

    return status;
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Signature internal callback
 ***************************************************************************/
STATIC void LacEcsm2_SignCb(CpaStatus status,
                            CpaBoolean signStatus,
                            CpaInstanceHandle instanceHandle,
                            lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2SignCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2SignOpData *pOpData = NULL;
    CpaFlatBuffer *pR = NULL;
    CpaFlatBuffer *pS = NULL;
    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2SignCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pR = pCbData->pOutputData1;
    pS = pCbData->pOutputData2;

#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;

    /* increment Sign stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, LAC_ECSM2_SIGN_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2SignCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2SignCompletedError, pCryptoService);
    }
    if ((CPA_FALSE == signStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2SignCompletedOutputInvalid, pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pR->pData,
                             pR->dataLenInBytes);
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pS->pData,
                             pS->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, signStatus, pR, pS);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Signature Verify internal callback
 ***************************************************************************/

STATIC void LacEcsm2_VerifyCb(CpaStatus status,
                              CpaBoolean verifyStatus,
                              CpaInstanceHandle instanceHandle,
                              lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2VerifyCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2VerifyOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2VerifyCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Verify stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, LAC_ECSM2_VERIFY_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2VerifyCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2VerifyCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == verifyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2VerifyCompletedOutputInvalid,
                           pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, verifyStatus);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Encryption internal callback
 ***************************************************************************/
STATIC void LacEcsm2_EncCb(CpaStatus status,
                           CpaBoolean encStatus,
                           CpaInstanceHandle instanceHandle,
                           lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2EncryptCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2EncryptOpData *pOpData = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2EncryptCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Enc stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, LAC_ECSM2_ENC_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2EncryptCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2EncryptCompletedError, pCryptoService);
    }
    if ((CPA_FALSE == encStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2EncryptCompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->pData,
                             pOutData->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, (void *)pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Decryption internal callback
 ***************************************************************************/
STATIC void LacEcsm2_DecCb(CpaStatus status,
                           CpaBoolean decStatus,
                           CpaInstanceHandle instanceHandle,
                           lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2DecryptCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2DecryptOpData *pOpData = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2DecryptCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Decrypt stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, LAC_ECSM2_DEC_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2DecryptCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2DecryptCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == decStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2DecryptCompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->pData,
                             pOutData->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, (void *)pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 key exchange callback
 ***************************************************************************/
STATIC void LacEcsm2_KeyExCb(CpaStatus status,
                             CpaBoolean keyExStatus,
                             CpaInstanceHandle instanceHandle,
                             lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2KeyExchangeCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2KeyExOpData *pOpData = NULL;
    CpaCyEcsm2KeyExOutputData *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2KeyExchangeCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Decrypt stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, 
                            LAC_ECSM2_KEY_EXCHANGE_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2KeyExCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2KeyExCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == keyExStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2KeyExCompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->Key.pData,
                             pOutData->Key.dataLenInBytes);
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->S1.pData,
                             pOutData->S1.dataLenInBytes);
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->SA.pData,
                             pOutData->SA.dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, (void *)pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 get Z callback
 ***************************************************************************/
STATIC void LacEcsm2_GetZCb(CpaStatus status,
                            CpaBoolean zGetStatus,
                            CpaInstanceHandle instanceHandle,
                            lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2GetZCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2GetZOpData *pOpData = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2GetZCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Decrypt stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, 
                            LAC_ECSM2_GET_Z_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2GetZCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2GetZCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == zGetStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2GetZCompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->pData,
                             pOutData->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, (void *)pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 get E callback
 ***************************************************************************/
STATIC void LacEcsm2_GetECb(CpaStatus status,
                            CpaBoolean eGetStatus,
                            CpaInstanceHandle instanceHandle,
                            lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2GetECbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2GetEOpData *pOpData = NULL;
    CpaFlatBuffer *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2GetECbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Decrypt stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, 
                            LAC_ECSM2_GET_E_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2GetECompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2GetECompletedError, pCryptoService);
    }

    if ((CPA_FALSE == eGetStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2GetECompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->pData,
                             pOutData->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, (void *)pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 get E callback
 ***************************************************************************/
STATIC void LacEcsm2_KeyGenCb(CpaStatus status,
                              CpaBoolean keyGenStatus,
                              CpaInstanceHandle instanceHandle,
                              lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcsm2KeyGenCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2KeyGenOpData *pOpData = NULL;
    CpaCyEcsm2KeyGenOutputData *pOutData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcsm2KeyGenCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pOutData = pCbData->pOutputData1;
    LAC_ASSERT_NOT_NULL(pOutData);

#ifndef DISABLE_STATS
    /* increment Decrypt stats */
    LAC_ECSM2_TIMESTAMP_END(pCbData, 
                            LAC_ECSM2_KEY_GEN_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2KeyGenCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2KeyGenCompletedError, pCryptoService);
    }

    if ((CPA_FALSE == keyGenStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2KeyGenCompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->PriKey.pData,
                             pOutData->PriKey.dataLenInBytes);
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->PubKeyX.pData,
                             pOutData->PubKeyX.dataLenInBytes);
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pOutData->PubKeyY.pData,
                             pOutData->PubKeyY.dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, (void *)pOutData);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Point Multiplication internal callback
 ***************************************************************************/
STATIC void LacEcsm2_PointMulCb(CpaStatus status,
                                CpaBoolean multiplyStatus,
                                CpaInstanceHandle instanceHandle,
                                lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcPointMultiplyCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2PointMultiplyOpData *pOpData = NULL;
    CpaFlatBuffer *pXk = NULL;
    CpaFlatBuffer *pYk = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcPointMultiplyCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pXk = pCbData->pOutputData1;
    pYk = pCbData->pOutputData2;
    LAC_ASSERT_NOT_NULL(pXk);
    LAC_ASSERT_NOT_NULL(pYk);

#ifndef DISABLE_STATS
    /* increment Point Multiply stats */
    LAC_ECSM2_TIMESTAMP_END(
        pCbData, LAC_ECSM2_POINT_MULTIPLY_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointMultiplyCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointMultiplyCompletedError, pCryptoService);
    }
    if ((CPA_FALSE == multiplyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointMultiplyCompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pXk->pData,
                             pXk->dataLenInBytes);
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pYk->pData,
                             pYk->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, multiplyStatus, pXk, pYk);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      SM2 Generator Multiplication internal callback
 ***************************************************************************/
STATIC void LacEcsm2_GenMulCb(CpaStatus status,
                              CpaBoolean multiplyStatus,
                              CpaInstanceHandle instanceHandle,
                              lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcPointMultiplyCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2GeneratorMultiplyOpData *pOpData = NULL;
    CpaFlatBuffer *pXk = NULL;
    CpaFlatBuffer *pYk = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcPointMultiplyCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);
    pXk = pCbData->pOutputData1;
    pYk = pCbData->pOutputData2;
    LAC_ASSERT_NOT_NULL(pXk);
    LAC_ASSERT_NOT_NULL(pYk);

#ifndef DISABLE_STATS
    /* increment Point Generator stats */
    LAC_ECSM2_TIMESTAMP_END(
        pCbData, LAC_ECSM2_GEN_POINT_MULTIPLY_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2GeneratorMultiplyCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2GeneratorMultiplyCompletedError,
                           pCryptoService);
    }
    if ((CPA_FALSE == multiplyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2GeneratorMultiplyCompletedOutputInvalid,
                           pCryptoService);
    }
#endif

    /* Flush cacheline */
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pXk->pData,
                             pXk->dataLenInBytes);
    qaeFlushInvalDCacheLines((LAC_ARCH_UINT)pYk->pData,
                             pYk->dataLenInBytes);

    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, multiplyStatus, pXk, pYk);
}

/***************************************************************************
 * @ingroup Lac_Ecsm2
 *      EC Point Verify internal callback
 ***************************************************************************/
STATIC void LacEcsm2_PointVerifyCb(CpaStatus status,
                                   CpaBoolean verifyStatus,
                                   CpaInstanceHandle instanceHandle,
                                   lac_pke_op_cb_data_t *pCbData)
{
    CpaCyEcPointVerifyCbFunc pCb = NULL;
    void *pCallbackTag = NULL;
    CpaCyEcsm2PointVerifyOpData *pOpData = NULL;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService =
        (sal_crypto_service_t *)instanceHandle;
#endif

    /* extract info from callback data structure */
    LAC_ASSERT_NOT_NULL(pCbData);
    pCallbackTag = pCbData->pCallbackTag;
    pOpData = (void *)LAC_CONST_PTR_CAST(pCbData->pClientOpData);
    pCb = (CpaCyEcPointVerifyCbFunc)LAC_CONST_PTR_CAST(pCbData->pClientCb);

#ifndef DISABLE_STATS
    /* increment Point Verify stats */
    LAC_ECSM2_TIMESTAMP_END(
        pCbData, LAC_ECSM2_POINT_VERIFY_REQUEST, instanceHandle);
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointVerifyCompleted, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointVerifyCompletedError, pCryptoService);
    }
    if ((CPA_FALSE == verifyStatus) && (CPA_STATUS_SUCCESS == status))
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointVerifyCompletedOutputInvalid,
                           pCryptoService);
    }
#endif
    /* invoke the user callback */
    pCb(pCallbackTag, status, pOpData, verifyStatus);
}

#ifdef ICP_PARAM_CHECK
/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Check if SM2 is supported by
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_SessionParamCheck(const CpaInstanceHandle instanceHandle)
{
    CpaCyCapabilitiesInfo capInfo;
    cpaCyQueryCapabilities(instanceHandle, &capInfo);
    if (!capInfo.ecSm2Supported)
    {
        LAC_UNSUPPORTED_PARAM_LOG("Unsupported Algorithm ECSM2");
        return CPA_STATUS_UNSUPPORTED;
    }
    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Point Multiply function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacEcsm2_PointMultiplyBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyEcsm2PointMultiplyOpData *pOpData,
    const CpaBoolean *pMultiplyStatus,
    const CpaFlatBuffer *pXk,
    const CpaFlatBuffer *pYk)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(instanceHandle);
    LAC_CHECK_NULL_PARAM(pOpData);
    LAC_CHECK_NULL_PARAM(pMultiplyStatus);
    LAC_CHECK_NULL_PARAM(pXk);
    LAC_CHECK_NULL_PARAM(pYk);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->k.pData);
    LAC_CHECK_SIZE(&(pOpData->k), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->x.pData);
    LAC_CHECK_SIZE(&(pOpData->x), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->y.pData);
    LAC_CHECK_SIZE(&(pOpData->y), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pXk->pData);
    LAC_CHECK_SIZE(pXk, CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pYk->pData);
    LAC_CHECK_SIZE(pYk, CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Generator Multiply function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus LacEcsm2_GenMultiplyBasicParamCheck(
    const CpaInstanceHandle instanceHandle,
    const CpaCyEcsm2GeneratorMultiplyOpData *pOpData,
    const CpaBoolean *pMultiplyStatus,
    const CpaFlatBuffer *pXk,
    const CpaFlatBuffer *pYk)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);
    LAC_CHECK_NULL_PARAM(pMultiplyStatus);
    LAC_CHECK_NULL_PARAM(pXk);
    LAC_CHECK_NULL_PARAM(pYk);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->k.pData);
    LAC_CHECK_SIZE(&(pOpData->k), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pXk->pData);
    LAC_CHECK_SIZE(pXk, CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pYk->pData);
    LAC_CHECK_SIZE(pYk, CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Point Verify function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_PointVerifyBasicParamCheck(const CpaInstanceHandle instanceHandle,
                                    const CpaCyEcsm2PointVerifyOpData *pOpData,
                                    const CpaBoolean *pVerifyStatus)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);
    LAC_CHECK_NULL_PARAM(pVerifyStatus);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->x.pData);
    LAC_CHECK_SIZE(&(pOpData->x), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->y.pData);
    LAC_CHECK_SIZE(&(pOpData->y), CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Signature function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_SignBasicParamCheck(const CpaInstanceHandle instanceHandle,
                             const CpaBoolean *pSignStatus,
                             const CpaCyEcsm2SignOpData *pOpData,
                             CpaFlatBuffer *pR,
                             CpaFlatBuffer *pS)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);
    LAC_CHECK_NULL_PARAM(pSignStatus);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->e.pData);
    LAC_CHECK_SIZE(&(pOpData->e), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->d.pData);
    LAC_CHECK_SIZE(&(pOpData->d), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pR);
    LAC_CHECK_NULL_PARAM(pS);
    LAC_CHECK_NULL_PARAM(pR->pData);
    LAC_CHECK_SIZE(pR, CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pS->pData);
    LAC_CHECK_SIZE(pS, CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Signature Verify function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_VerifyBasicParamCheck(const CpaInstanceHandle instanceHandle,
                               const CpaCyEcsm2VerifyOpData *pOpData,
                               const CpaBoolean *pVerifyStatus)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);
    LAC_CHECK_NULL_PARAM(pVerifyStatus);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->e.pData);
    LAC_CHECK_SIZE(&(pOpData->e), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->r.pData);
    LAC_CHECK_SIZE(&(pOpData->r), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->s.pData);
    LAC_CHECK_SIZE(&(pOpData->s), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->xP.pData);
    LAC_CHECK_SIZE(&(pOpData->xP), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->yP.pData);
    LAC_CHECK_SIZE(&(pOpData->yP), CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Encryption function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_EncBasicParamCheck(const CpaInstanceHandle instanceHandle,
                            const CpaCyEcsm2EncryptOpData *pOpData,
                            const CpaFlatBuffer *pOut)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->xP.pData);
    LAC_CHECK_SIZE(&(pOpData->xP), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->yP.pData);
    LAC_CHECK_SIZE(&(pOpData->yP), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOut);
    LAC_CHECK_NULL_PARAM(pOut->pData);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Decryption function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_DecBasicParamCheck(const CpaInstanceHandle instanceHandle,
                            const CpaCyEcsm2DecryptOpData *pOpData,
                            const CpaFlatBuffer *pOut)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->d.pData);
    LAC_CHECK_SIZE(&(pOpData->d), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->c.pData);
    LAC_CHECK_SIZE(&(pOpData->c), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOut);
    LAC_CHECK_NULL_PARAM(pOut->pData);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 Key Exchange function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_KeyExBasicParamCheck(const CpaInstanceHandle instanceHandle,
                              const CpaCyEcsm2KeyExOpData *pOpData,
                              const CpaCyEcsm2KeyExOutputData *pOut)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->dA.pData);
    LAC_CHECK_SIZE(&(pOpData->dA), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->PB_X.pData);
    LAC_CHECK_SIZE(&(pOpData->PB_X), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->PB_Y.pData);
    LAC_CHECK_SIZE(&(pOpData->PB_Y), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->rA.pData);
    LAC_CHECK_SIZE(&(pOpData->rA), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->RA_X.pData);
    LAC_CHECK_SIZE(&(pOpData->RA_X), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->RA_Y.pData);
    LAC_CHECK_SIZE(&(pOpData->RA_Y), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->RB_X.pData);
    LAC_CHECK_SIZE(&(pOpData->RB_X), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->RB_Y.pData);
    LAC_CHECK_SIZE(&(pOpData->RB_Y), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->ZA.pData);
    LAC_CHECK_SIZE(&(pOpData->ZA), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->ZB.pData);
    LAC_CHECK_SIZE(&(pOpData->ZB), CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 get z function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_GetZBasicParamCheck(const CpaInstanceHandle instanceHandle,
                             const CpaCyEcsm2GetZOpData *pOpData,
                             const CpaFlatBuffer *pOut)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->xP.pData);
    LAC_CHECK_SIZE(&(pOpData->xP), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->yP.pData);
    LAC_CHECK_SIZE(&(pOpData->yP), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->id.pData);
    LAC_CHECK_SIZE(&(pOpData->id), CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 get z function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_GetEBasicParamCheck(const CpaInstanceHandle instanceHandle,
                             const CpaCyEcsm2GetEOpData *pOpData,
                             const CpaFlatBuffer *pOut)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    /* Check flat buffers in pOpData for NULL and dataLen of 0*/
    LAC_CHECK_NULL_PARAM(pOpData->z.pData);
    LAC_CHECK_SIZE(&(pOpData->z), CHECK_NONE, 0);
    LAC_CHECK_NULL_PARAM(pOpData->m.pData);
    LAC_CHECK_SIZE(&(pOpData->m), CHECK_NONE, 0);

    return CPA_STATUS_SUCCESS;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *      ECSM2 generate key function to perform basic checks on the IN
 *      parameters (e.g. checks data buffers for NULL and 0 dataLen)
 ***************************************************************************/
STATIC CpaStatus
LacEcsm2_KeyGenBasicParamCheck(const CpaInstanceHandle instanceHandle,
                               const CpaCyEcsm2KeyGenOpData *pOpData,
                               const CpaCyEcsm2KeyGenOutputData *pOut)
{
    /* check for null parameters */
    LAC_CHECK_NULL_PARAM(pOpData);
    LAC_CHECK_NULL_PARAM(pOut);

    /* check for HW support */
    LAC_CHECK_STATUS(LacEcsm2_SessionParamCheck(instanceHandle));

    return CPA_STATUS_SUCCESS;
}
#endif

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 point multiplication operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2PointMultiply(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyEcPointMultiplyCbFunc pEcsm2PointMulCb,
    void *pCallbackTag,
    const CpaCyEcsm2PointMultiplyOpData *pEcsm2PointMulOpData,
    CpaBoolean *pMultiplyStatus,
    CpaFlatBuffer *pXk,
    CpaFlatBuffer *pYk)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
    Cpa32U dataOperationSizeBytes = 0;
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2PointMulCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_PointMultiplySyn(
            instanceHandle, pEcsm2PointMulOpData, pMultiplyStatus, pXk, pYk);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_PointMultiplyBasicParamCheck(
        instanceHandle, pEcsm2PointMulOpData, pMultiplyStatus, pXk, pYk);

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status = LacEc_GetRange(
            LacEcsm2_PointMulOpDataSizeGetMax(pEcsm2PointMulOpData),
            &dataOperationSizeBytes);
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        /* Check that output buffers are big enough
         * for SM2 algorithm, the length is fixed
         * equal to LAC_EC_SM2_SIZE_BYTES (32 bytes)
         */
        if ((pXk->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES) ||
            (pYk->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES))
        {
            LAC_INVALID_PARAM_LOG("Output buffers not big enough");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2PointMulOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    dataOperationSizeBytes = LAC_EC_SIZE_QW4_IN_BYTES;

    if (CPA_STATUS_SUCCESS == status)
    {
        Cpa8U *pMemPoolConcate = NULL;
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* clear output buffers */
        osalMemSet(pXk->pData, 0, pXk->dataLenInBytes);
        osalMemSet(pXk->pData, 0, pXk->dataLenInBytes);

        /* Need to concatenate user inputs - copy to ecc mempool memory */
        status = LacEc_GetSm2PCurve(instanceHandle_in,
                                    dataOperationSizeBytes,
                                    &pMemPoolConcate);
        if(CPA_STATUS_SUCCESS != status)
        {
            return status;
        }

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2PointMulOpData->k.pData,
                            pEcsm2PointMulOpData->k.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2PointMulOpData->x.pData,
                            pEcsm2PointMulOpData->x.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2PointMulOpData->y.pData,
                            pEcsm2PointMulOpData->y.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pXk->pData,
                            pXk->dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pYk->pData,
                            pYk->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_POINT_MUL);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.CurveAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pMemPoolConcate));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.KAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2PointMulOpData->k.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.PxAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2PointMulOpData->x.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.PyAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2PointMulOpData->y.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.QxAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pXk->pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.QyAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pYk->pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2PointMulOpData;
        cbData.pClientCb = pEcsm2PointMulCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pXk;
        cbData.pOutputData2 = pYk;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_PointMulCb,
                                &cbData,
                                instanceHandle);
        }

        if (CPA_STATUS_SUCCESS != status)
        {
            /* Free Mem Pool */
            if (NULL != pMemPoolConcate)
            {
                Lac_MemPoolEntryFree(pMemPoolConcate);
            }
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointMultiplyRequests, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointMultiplyRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 point generator multiplication operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2GeneratorMultiply(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyEcPointMultiplyCbFunc pEcsm2GenMulCb,
    void *pCallbackTag,
    const CpaCyEcsm2GeneratorMultiplyOpData *pEcsm2GenMulOpData,
    CpaBoolean *pMultiplyStatus,
    CpaFlatBuffer *pXk,
    CpaFlatBuffer *pYk)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
    Cpa32U dataOperationSizeBytes = 0;
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2GenMulCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_GenMultiplySyn(
            instanceHandle, pEcsm2GenMulOpData, pMultiplyStatus, pXk, pYk);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_GenMultiplyBasicParamCheck(
        instanceHandle, pEcsm2GenMulOpData, pMultiplyStatus, pXk, pYk);

    /* Check that output buffers are big enough
     * for SM2 algorithm, the length is fixed
     * equal to LAC_EC_SM2_SIZE_BYTES (32 bytes)
     */

    /* Determine size - based on input numbers */
    if (CPA_STATUS_SUCCESS == status)
    {
        status = LacEc_GetRange(
            LacEcsm2_GeneratorMulOpDataSizeGetMax(pEcsm2GenMulOpData),
            &dataOperationSizeBytes);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        if ((pXk->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES) ||
            (pYk->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES))
        {
            LAC_INVALID_PARAM_LOG("Output buffers not big enough");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2GenMulOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        Cpa8U *pMemPoolConcate = NULL;
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* clear output buffers */
        osalMemSet(pXk->pData, 0, pXk->dataLenInBytes);
        osalMemSet(pXk->pData, 0, pXk->dataLenInBytes);

        /* Need to concatenate user inputs - copy to ecc mempool memory */
        status = LacEc_GetSm2PCurve(instanceHandle_in,
                                    dataOperationSizeBytes,
                                    &pMemPoolConcate);
        if(CPA_STATUS_SUCCESS != status)
        {
            return status;
        }

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2GenMulOpData->k.pData,
                            pEcsm2GenMulOpData->k.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pXk->pData,
                            pXk->dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pYk->pData,
                            pYk->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_POINT_MUL);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.CurveAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pMemPoolConcate));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.KAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2GenMulOpData->k.pData));
        // LAC_MEM_SHARED_WRITE_FROM_PTR(
        //             ctx.mmp_ecc_point_mul.PxAddr,
        //             LAC_OS_VIRT_TO_PHYS_INTERNAL((char*)Sm2P256_Gx));
        // LAC_MEM_SHARED_WRITE_FROM_PTR(
        //             ctx.mmp_ecc_point_mul.PyAddr,
        //             LAC_OS_VIRT_TO_PHYS_INTERNAL((char*)Sm2P256_Gy));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.QxAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pXk->pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_mul.QyAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pYk->pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2GenMulOpData;
        cbData.pClientCb = pEcsm2GenMulCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pXk;
        cbData.pOutputData2 = pYk;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_GenMulCb,
                                &cbData,
                                instanceHandle);
        }

        if (CPA_STATUS_SUCCESS != status)
        {
            /* Free Mem Pool */
            if (NULL != pMemPoolConcate)
            {
                Lac_MemPoolEntryFree(pMemPoolConcate);
            }
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2GeneratorMultiplyRequests,
                            pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2GeneratorMultiplyRequestErrors,
                            pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 point verify operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2PointVerify(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyEcPointVerifyCbFunc pEcsm2PointVeirfyCb,
    void *pCallbackTag,
    const CpaCyEcsm2PointVerifyOpData *pEcsm2PointVerifyOpData,
    CpaBoolean *pPointVerifyStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
    Cpa32U dataOperationSizeBytes = 0;
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2PointVeirfyCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_PointVerifySyn(
            instanceHandle, pEcsm2PointVerifyOpData, pPointVerifyStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_PointVerifyBasicParamCheck(
        instanceHandle, pEcsm2PointVerifyOpData, pPointVerifyStatus);

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status = LacEc_GetRange(
            LacEcsm2_PointVerifyOpDataSizeGetMax(pEcsm2PointVerifyOpData),
            &dataOperationSizeBytes);
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2PointVerifyOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    dataOperationSizeBytes = LAC_EC_SIZE_QW4_IN_BYTES;

    if (CPA_STATUS_SUCCESS == status)
    {
        Cpa8U *pMemPoolConcate = NULL;
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Need to concatenate user inputs - copy to ecc mempool memory */
        status = LacEc_GetSm2PCurve(instanceHandle_in,
                                    dataOperationSizeBytes,
                                    &pMemPoolConcate);
        if(CPA_STATUS_SUCCESS != status)
        {
            return status;
        }

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2PointVerifyOpData->x.pData,
                            pEcsm2PointVerifyOpData->x.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2PointVerifyOpData->y.pData,
                            pEcsm2PointVerifyOpData->y.dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_POINT_VERIFY);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_verify.CurveAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pMemPoolConcate));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_verify.PxAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2PointVerifyOpData->x.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_ecc_point_verify.PyAddr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2PointVerifyOpData->y.pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2PointVerifyOpData;
        cbData.pClientCb = pEcsm2PointVeirfyCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_PointVerifyCb,
                                &cbData,
                                instanceHandle);
        }

        if (CPA_STATUS_SUCCESS != status)
        {
            /* Free Mem Pool */
            if (NULL != pMemPoolConcate)
            {
                Lac_MemPoolEntryFree(pMemPoolConcate);
            }
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointVerifyRequests, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2PointVerifyRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 signature operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2Sign(const CpaInstanceHandle instanceHandle_in,
                         const CpaCyEcsm2SignCbFunc pEcsm2SignCb,
                         void *pCallbackTag,
                         const CpaCyEcsm2SignOpData *pEcsm2SignOpData,
                         CpaBoolean *pSignStatus,
                         CpaFlatBuffer *pR,
                         CpaFlatBuffer *pS)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2SignCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_SignSyn(
            instanceHandle, pEcsm2SignOpData, pSignStatus, pR, pS);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_SignBasicParamCheck(
        instanceHandle, pSignStatus, pEcsm2SignOpData, pR, pS);

    /* Determine size - based on input numbers */
    if (CPA_STATUS_SUCCESS == status)
    {
        status = LacEc_GetRange(LacEcsm2_SignOpDataSizeGetMax(pEcsm2SignOpData),
                                &dataOperationSizeBytes);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Check that output buffers are big enough
         * for SM2 algorithm, the length is fixed
         * equal to LAC_EC_SM2_SIZE_BYTES (32 bytes)
         */
        if ((pR->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES) ||
            (pS->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES))
        {
            LAC_INVALID_PARAM_LOG("Output buffers not big enough");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2SignOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* clear output buffers */
        osalMemSet(pR->pData, 0, pR->dataLenInBytes);
        osalMemSet(pS->pData, 0, pS->dataLenInBytes);

        /* Flush cacheline */
        if(pEcsm2SignOpData->k.pData)
        {
            qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2SignOpData->k.pData,
                                pEcsm2SignOpData->k.dataLenInBytes);
        }
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2SignOpData->e.pData,
                            pEcsm2SignOpData->e.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2SignOpData->d.pData,
                            pEcsm2SignOpData->d.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pR->pData,
                            pR->dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pS->pData,
                            pS->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_SIGN);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_sign.prikey_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2SignOpData->d.pData));
        if(pEcsm2SignOpData->k.pData)
        {
            LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_sign.randkey_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2SignOpData->k.pData));
        }
        else
        {
            ctx.mmp_sm2_sign.randkey_addr = 0;
        }
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_sign.e_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2SignOpData->e.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_sign.sign_r_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pR->pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_sign.sign_s_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pS->pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2SignOpData;
        cbData.pClientCb = pEcsm2SignCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pR;
        cbData.pOutputData2 = pS;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_SignCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
        pCryptoService = (sal_crypto_service_t *)instanceHandle;
        /* increment stats */
        if (CPA_STATUS_SUCCESS == status)
        {
            LAC_ECSM2_STAT_INC(numEcsm2SignRequests, pCryptoService);
        }
        else
        {
            LAC_ECSM2_STAT_INC(numEcsm2SignRequestErrors, pCryptoService);
        }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 signature verify operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2Verify(const CpaInstanceHandle instanceHandle_in,
                           const CpaCyEcsm2VerifyCbFunc pEcsm2VerifyCb,
                           void *pCallbackTag,
                           const CpaCyEcsm2VerifyOpData *pEcsm2VerifyOpData,
                           CpaBoolean *pVerifyStatus)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2VerifyCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_VerifySyn(
            instanceHandle, pEcsm2VerifyOpData, pVerifyStatus);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_VerifyBasicParamCheck(
        instanceHandle, pEcsm2VerifyOpData, pVerifyStatus);
    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status =
            LacEc_GetRange(LacEcsm2_VerifyOpDataSizeGetMax(pEcsm2VerifyOpData),
                           &dataOperationSizeBytes);
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2VerifyOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2VerifyOpData->xP.pData,
                            pEcsm2VerifyOpData->xP.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2VerifyOpData->yP.pData,
                            pEcsm2VerifyOpData->yP.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2VerifyOpData->e.pData,
                            pEcsm2VerifyOpData->e.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2VerifyOpData->r.pData,
                            pEcsm2VerifyOpData->r.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2VerifyOpData->s.pData,
                            pEcsm2VerifyOpData->s.dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_VERIFY);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_verify.pubkey_x_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2VerifyOpData->xP.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_verify.pubkey_y_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2VerifyOpData->yP.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_verify.e_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2VerifyOpData->e.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_verify.sign_r_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2VerifyOpData->r.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_verify.sign_s_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2VerifyOpData->s.pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2VerifyOpData;
        cbData.pClientCb = pEcsm2VerifyCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = NULL;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_VerifyCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
        pCryptoService = (sal_crypto_service_t *)instanceHandle;
        /* increment stats */
        if (CPA_STATUS_SUCCESS == status)
        {
            LAC_ECSM2_STAT_INC(numEcsm2VerifyRequests, pCryptoService);
        }
        else
        {
            LAC_ECSM2_STAT_INC(numEcsm2VerifyRequestErrors, pCryptoService);
        }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 encryption operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2Encrypt(const CpaInstanceHandle instanceHandle_in,
                            const CpaCyEcsm2EncryptCbFunc pEcsm2EncCb,
                            void *pCallbackTag,
                            const CpaCyEcsm2EncryptOpData *pEcsm2EncOpData,
                            CpaFlatBuffer *pEcsm2EncOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2EncCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_EncSyn(
            instanceHandle, pEcsm2EncOpData, pEcsm2EncOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_EncBasicParamCheck(
        instanceHandle, pEcsm2EncOpData, pEcsm2EncOutputData);
    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status = LacEc_GetRange(LacEcsm2_EncOpDataSizeGetMax(pEcsm2EncOpData),
                                &dataOperationSizeBytes);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Check that output buffers are big enough
         * for SM2 algorithm, the length is fixed
         * equal to LAC_EC_SM2_SIZE_BYTES (32 bytes)
         */
        if (pEcsm2EncOutputData->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES)
        {
            LAC_INVALID_PARAM_LOG("Output buffers not big enough");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2EncOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2EncOpData->xP.pData,
                            pEcsm2EncOpData->xP.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2EncOpData->yP.pData,
                            pEcsm2EncOpData->yP.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2EncOpData->m.pData,
                            pEcsm2EncOpData->m.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2EncOpData->k.pData,
                            pEcsm2EncOpData->k.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2EncOutputData->pData,
                            pEcsm2EncOutputData->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_ENCRYPT);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_encrypt.pubkey_x_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2EncOpData->xP.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_encrypt.pubKey_y_Addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2EncOpData->yP.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_encrypt.rand_k_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2EncOpData->k.pData));
        ctx.mmp_sm2_encrypt.m_byteslen = pEcsm2EncOpData->m.dataLenInBytes;
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_encrypt.m_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2EncOpData->m.pData));
        ctx.mmp_sm2_encrypt.c_bytesLen = pEcsm2EncOutputData->dataLenInBytes;
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_encrypt.c_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2EncOutputData->pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2EncOpData;
        cbData.pClientCb = pEcsm2EncCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pEcsm2EncOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_EncCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
    pCryptoService = (sal_crypto_service_t *)instanceHandle;
    /* increment stats */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_ECSM2_STAT_INC(numEcsm2EncryptRequests, pCryptoService);
    }
    else
    {
        LAC_ECSM2_STAT_INC(numEcsm2EncryptRequestErrors, pCryptoService);
    }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 point decryption operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2Decrypt(const CpaInstanceHandle instanceHandle_in,
                            const CpaCyEcsm2DecryptCbFunc pEcsm2DecCb,
                            void *pCallbackTag,
                            const CpaCyEcsm2DecryptOpData *pEcsm2DecOpData,
                            CpaFlatBuffer *pEcsm2DecOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2DecCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_DecSyn(
            instanceHandle, pEcsm2DecOpData, pEcsm2DecOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_DecBasicParamCheck(
        instanceHandle, pEcsm2DecOpData, pEcsm2DecOutputData);

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status = LacEc_GetRange(LacEcsm2_DecOpDataSizeGetMax(pEcsm2DecOpData),
                                &dataOperationSizeBytes);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2DecOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_DECRYPT);

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2DecOpData->d.pData,
                            pEcsm2DecOpData->d.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2DecOpData->c.pData,
                            pEcsm2DecOpData->c.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2DecOutputData->pData,
                            pEcsm2DecOutputData->dataLenInBytes);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_decrypt.prikey_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2DecOpData->d.pData));
        ctx.mmp_sm2_decrypt.c_bytesLen = pEcsm2DecOpData->c.dataLenInBytes;
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_decrypt.c_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2DecOpData->c.pData));
        ctx.mmp_sm2_decrypt.m_bytesLen = pEcsm2DecOutputData->dataLenInBytes;
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                    ctx.mmp_sm2_decrypt.m_addr,
                    LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2DecOutputData->pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2DecOpData;
        cbData.pClientCb = pEcsm2DecCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pEcsm2DecOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_DecCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
        pCryptoService = (sal_crypto_service_t *)instanceHandle;
        /* increment stats */
        if (CPA_STATUS_SUCCESS == status)
        {
            LAC_ECSM2_STAT_INC(numEcsm2DecryptRequests, pCryptoService);
        }
        else
        {
            LAC_ECSM2_STAT_INC(numEcsm2DecryptRequestErrors, pCryptoService);
        }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 key exchange operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2KeyExchange(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyEcsm2KeyExchangeCbFunc pEcsm2KeyExchangeCb,
    void *pCallbackTag,
    const CpaCyEcsm2KeyExOpData *pEcsm2KeyExOpData,
    CpaCyEcsm2KeyExOutputData *pEcsm2KeyExOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2KeyExchangeCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_KeyeExSyn(instanceHandle,
                                  pEcsm2KeyExOpData,
                                  pEcsm2KeyExOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_KeyExBasicParamCheck(
        instanceHandle, pEcsm2KeyExOpData, pEcsm2KeyExOutputData);

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status = LacEc_GetRange(LacEcsm2_KeyExchangeOpDataSizeGetMax(
                                pEcsm2KeyExOpData),
                                &dataOperationSizeBytes);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2KeyExOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->dA.pData,
                            pEcsm2KeyExOpData->dA.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->PB_X.pData,
                            pEcsm2KeyExOpData->PB_X.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->PB_Y.pData,
                            pEcsm2KeyExOpData->PB_Y.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->rA.pData,
                            pEcsm2KeyExOpData->rA.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->RA_X.pData,
                            pEcsm2KeyExOpData->RA_X.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->RA_Y.pData,
                            pEcsm2KeyExOpData->RA_Y.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->RB_X.pData,
                            pEcsm2KeyExOpData->RB_X.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->RB_Y.pData,
                            pEcsm2KeyExOpData->RB_Y.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->ZA.pData,
                            pEcsm2KeyExOpData->ZA.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOpData->ZB.pData,
                            pEcsm2KeyExOpData->ZB.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOutputData->Key.pData,
                            pEcsm2KeyExOutputData->Key.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOutputData->S1.pData,
                            pEcsm2KeyExOutputData->S1.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyExOutputData->SA.pData,
                            pEcsm2KeyExOutputData->SA.dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_KEY_EXCHANGE);
        ICP_CCAT_FW_PK_SM2_SM9_ROLE_SET(desc, pEcsm2KeyExOpData->role);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.da_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->dA.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.pb_x_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->PB_X.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.pb_y_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->PB_Y.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.ra_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->rA.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.ra_x_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->RA_X.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.ra_y_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->RA_Y.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.rb_x_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->RB_X.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.rb_y_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->RB_Y.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.za_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->ZA.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.zb_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOpData->ZB.pData));
        ctx.mmp_sm2_exchange_key.key_byteslen = pEcsm2KeyExOpData->keyLenInBytes;
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.key_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOutputData->Key.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.s1_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOutputData->S1.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_exchange_key.sa_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyExOutputData->SA.pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2KeyExOpData;
        cbData.pClientCb = pEcsm2KeyExchangeCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pEcsm2KeyExOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_KeyExCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
        pCryptoService = (sal_crypto_service_t *)instanceHandle;
        /* increment stats */
        if (CPA_STATUS_SUCCESS == status)
        {
            LAC_ECSM2_STAT_INC(numEcsm2KeyExRequests, pCryptoService);
        }
        else
        {
            LAC_ECSM2_STAT_INC(numEcsm2KeyExRequestErrors, pCryptoService);
        }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 get Z operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2GetZ(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyEcsm2GetZCbFunc pEcsm2GetZCb,
    void *pCallbackTag,
    const CpaCyEcsm2GetZOpData *pEcsm2GetZOpData,
    CpaFlatBuffer *pOut)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2GetZCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_GetZSyn(instanceHandle,
                                pEcsm2GetZOpData,
                                pOut);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_GetZBasicParamCheck(instanceHandle,
                                          pEcsm2GetZOpData,
                                          pOut);
    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status = LacEc_GetRange(
                    LacEcsm2_GetZOpDataSizeGetMax(pEcsm2GetZOpData),
                                                  &dataOperationSizeBytes);
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        /* Check that output buffers are big enough
         * for SM2 algorithm, the length is fixed
         * equal to LAC_EC_SM2_SIZE_BYTES (32 bytes)
         */
        if (pOut->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES)
        {
            LAC_INVALID_PARAM_LOG("Output buffers not big enough");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2GetZOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2GetZOpData->xP.pData,
                            pEcsm2GetZOpData->xP.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2GetZOpData->yP.pData,
                            pEcsm2GetZOpData->yP.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2GetZOpData->id.pData,
                            pEcsm2GetZOpData->id.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pOut->pData,
                            pOut->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_Z_GET);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_get_z.pubkey_x_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2GetZOpData->xP.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_get_z.pubKey_y_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2GetZOpData->yP.pData));
        ctx.mmp_sm2_get_z.id_byteslen = pEcsm2GetZOpData->id.dataLenInBytes;
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_get_z.id_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2GetZOpData->id.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_get_z.z_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pOut->pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2GetZOpData;
        cbData.pClientCb = pEcsm2GetZCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pOut;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_GetZCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
        pCryptoService = (sal_crypto_service_t *)instanceHandle;
        /* increment stats */
        if (CPA_STATUS_SUCCESS == status)
        {
            LAC_ECSM2_STAT_INC(numEcsm2GetZRequests, pCryptoService);
        }
        else
        {
            LAC_ECSM2_STAT_INC(numEcsm2GetZRequestErrors, pCryptoService);
        }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 get E operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2GetE(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyEcsm2GetECbFunc pEcsm2GetECb,
    void *pCallbackTag,
    const CpaCyEcsm2GetEOpData *pEcsm2GetEOpData,
    CpaFlatBuffer *pOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2GetECb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_GetESyn(instanceHandle,
                                pEcsm2GetEOpData,
                                pOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_GetEBasicParamCheck(instanceHandle,
                                          pEcsm2GetEOpData,
                                          pOutputData);

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Determine size - based on input numbers */
        status = LacEc_GetRange(
                        LacEcsm2_GetEOpDataSizeGetMax(pEcsm2GetEOpData),
                                                      &dataOperationSizeBytes);
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        /* Check that output buffers are big enough
         * for SM2 algorithm, the length is fixed
         * equal to LAC_EC_SM2_SIZE_BYTES (32 bytes)
         */
        if (pOutputData->dataLenInBytes < LAC_EC_SM2_SIZE_BYTES)
        {
            LAC_INVALID_PARAM_LOG("Output buffers not big enough");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2GetEOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2GetEOpData->z.pData,
                            pEcsm2GetEOpData->z.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2GetEOpData->m.pData,
                            pEcsm2GetEOpData->m.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pOutputData->pData,
                            pOutputData->dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_E_GET);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_get_e.z_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2GetEOpData->z.pData));
        ctx.mmp_sm2_get_e.msg_byteslen = pEcsm2GetEOpData->m.dataLenInBytes;
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_get_e.msg_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2GetEOpData->m.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
                ctx.mmp_sm2_get_e.e_addr,
                LAC_OS_VIRT_TO_PHYS_INTERNAL(pOutputData->pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2GetEOpData;
        cbData.pClientCb = pEcsm2GetECb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_GetECb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
        pCryptoService = (sal_crypto_service_t *)instanceHandle;
        /* increment stats */
        if (CPA_STATUS_SUCCESS == status)
        {
            LAC_ECSM2_STAT_INC(numEcsm2GetERequests, pCryptoService);
        }
        else
        {
            LAC_ECSM2_STAT_INC(numEcsm2GetERequestErrors, pCryptoService);
        }
#endif

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ecsm2
 *
 * @description
 *     SM2 generrate key operation
 *
 ***************************************************************************/
CpaStatus cpaCyEcsm2KeyGen(
    const CpaInstanceHandle instanceHandle_in,
    const CpaCyEcsm2KeyGenCbFunc pEcsm2KeyGenCb,
    void *pCallbackTag,
    const CpaCyEcsm2KeyGenOpData *pEcsm2KeyGenOpData,
    CpaCyEcsm2KeyGenOutputData *pEcsm2KeyGenOutputData)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_PARAM_CHECK
    Cpa32U dataOperationSizeBytes = 0;
#endif
    Cpa32U functionalityId = 0;
#ifndef DISABLE_STATS
    sal_crypto_service_t *pCryptoService = NULL;
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

#ifdef ICP_PARAM_CHECK
    /* instance checks - if fail, no inc stats just return */
    /* check for valid acceleration handle */
    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_CHECK_ADDR_TRANS_SETUP(instanceHandle);
#endif

    /* check LAC is initialised*/
    SAL_RUNNING_CHECK(instanceHandle);

    /* Check if the API has been called in synchronous mode */
    if (NULL == pEcsm2KeyGenCb)
    {
        /* Call synchronous mode function */
        return LacEcsm2_KeyGenSyn(instanceHandle,
                                  pEcsm2KeyGenOpData,
                                  pEcsm2KeyGenOutputData);
    }

#ifdef ICP_PARAM_CHECK
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));

    /* Basic Param Checking - NULL params, buffer lengths etc. */
    status = LacEcsm2_KeyGenBasicParamCheck(instanceHandle,
                                            pEcsm2KeyGenOpData,
                                            pEcsm2KeyGenOutputData);

    dataOperationSizeBytes = LAC_EC_SIZE_QW4_IN_BYTES;

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Check that output buffers are big enough
         * for SM2 algorithm, the length is fixed
         * equal to LAC_EC_SM2_SIZE_BYTES (32 bytes)
         */
        if ((pEcsm2KeyGenOutputData->PriKey.dataLenInBytes
                                                < LAC_EC_SM2_SIZE_BYTES) ||
            (pEcsm2KeyGenOutputData->PubKeyX.dataLenInBytes
                                                < LAC_EC_SM2_SIZE_BYTES) ||
            (pEcsm2KeyGenOutputData->PubKeyY.dataLenInBytes
                                                < LAC_EC_SM2_SIZE_BYTES))

        {
            LAC_INVALID_PARAM_LOG("Output buffers not big enough");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
    if (CPA_STATUS_SUCCESS == status)
    {
        if (CPA_CY_EC_FIELD_TYPE_PRIME == pEcsm2KeyGenOpData->fieldType)
        {
            switch (dataOperationSizeBytes)
            {
                case LAC_EC_SIZE_QW4_IN_BYTES:
                    functionalityId = 0;
                    break;
                case LAC_EC_SIZE_QW8_IN_BYTES:
                case LAC_EC_SIZE_QW9_IN_BYTES:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
                default:
                    LAC_INVALID_PARAM_LOG(
                        "SM2 curver other than GFP P-256 not suppported");
                    status = CPA_STATUS_INVALID_PARAM;
                    break;
            }
        }
        else
        {
            LAC_INVALID_PARAM_LOG("SM2 curve over binary field not supported");
            status = CPA_STATUS_INVALID_PARAM;
        }
    }
#endif

    if (CPA_STATUS_SUCCESS == status)
    {
        icp_qat_fw_mmp_ctx_param_t ctx = {0};
        icp_qat_fw_pke_request_test_t desc = {0};
        lac_pke_op_cb_data_t cbData = {0};

        /* Flush cacheline */
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyGenOutputData->PriKey.pData,
                            pEcsm2KeyGenOutputData->PriKey.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyGenOutputData->PubKeyX.pData,
                            pEcsm2KeyGenOutputData->PubKeyX.dataLenInBytes);
        qaeFlushDCacheLines((LAC_ARCH_UINT)pEcsm2KeyGenOutputData->PubKeyY.pData,
                            pEcsm2KeyGenOutputData->PubKeyY.dataLenInBytes);

        /* populate desc header parameters */
        ICP_CCAT_FW_PK_SERVICE_TYPE_SET(desc, CCAT_COMN_PTR_SERVICE_TYPE_PK);
        ICP_CCAT_FW_PK_FIRST_SET(desc, CCAT_COMN_PTR_FIRST_FIRST);
        ICP_CCAT_FW_PK_LAST_SET(desc, CCAT_COMN_PTR_LAST_LAST);
        ICP_CCAT_FW_PK_ALG_SET(desc, PK_OP_SM2_KEYPAIR_GEN);

        /* populate ctx parameters */
        LAC_MEM_SHARED_WRITE_FROM_PTR(
            ctx.mmp_sm2_get_key.prikey_addr,
            LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyGenOutputData->PriKey.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
            ctx.mmp_sm2_get_key.pubkey_x_addr,
            LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyGenOutputData->PubKeyX.pData));
        LAC_MEM_SHARED_WRITE_FROM_PTR(
            ctx.mmp_sm2_get_key.pubkey_y_addr,
            LAC_OS_VIRT_TO_PHYS_INTERNAL(pEcsm2KeyGenOutputData->PubKeyY.pData));

        cbData.pCallbackTag = pCallbackTag;
        cbData.pClientOpData = pEcsm2KeyGenOpData;
        cbData.pClientCb = pEcsm2KeyGenCb;
        cbData.pOpaqueData = NULL;
        cbData.pOutputData1 = pEcsm2KeyGenOutputData;

        /* Send pke request */
        if (CPA_STATUS_SUCCESS == status)
        {
            /* send a PKE request to the QAT */
            status = LacPke_SendSingleRequestTest(functionalityId,
                                &desc,
                                &ctx,
                                (lac_pke_op_cb_func_t)LacEcsm2_KeyGenCb,
                                &cbData,
                                instanceHandle);
        }
    }

#ifndef DISABLE_STATS
        pCryptoService = (sal_crypto_service_t *)instanceHandle;
        /* increment stats */
        if (CPA_STATUS_SUCCESS == status)
        {
            LAC_ECSM2_STAT_INC(numEcsm2KeyGenRequests, pCryptoService);
        }
        else
        {
            LAC_ECSM2_STAT_INC(numEcsm2KeyGenRequestErrors, pCryptoService);
        }
#endif

    return status;
}

CpaStatus cpaCyEcsm2QueryStats64(const CpaInstanceHandle instanceHandle_in,
                                 CpaCyEcsm2Stats64 *pEcsm2Stats)
{
    sal_crypto_service_t *pCryptoService = NULL;
    CpaInstanceHandle instanceHandle = NULL;
#ifdef ICP_TRACE
    LAC_LOG2("Called with params (0x%lx, 0x%lx)\n",
             (LAC_ARCH_UINT)instanceHandle_in,
             (LAC_ARCH_UINT)pEcsm2Stats);
#endif

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }
    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    LAC_CHECK_INSTANCE_HANDLE(instanceHandle);
    SAL_RUNNING_CHECK(instanceHandle);
    /* ensure this is a crypto or asym instance with pke enabled */
    SAL_CHECK_INSTANCE_TYPE(
        instanceHandle,
        (SAL_SERVICE_TYPE_CRYPTO | SAL_SERVICE_TYPE_CRYPTO_ASYM));
    LAC_CHECK_NULL_PARAM(pEcsm2Stats);

    /* get stats into user supplied stats structure */
    LAC_ECSM2_STATS_GET(*pEcsm2Stats, pCryptoService);

    return CPA_STATUS_SUCCESS;
}

