/***************************************************************************
 *
 *   BSD LICENSE
 * 
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/

/**
 ***************************************************************************
 *
 * @file lac_ec_common.c
 *
 * @defgroup Lac_Ec Elliptic Curve
 *
 * @ingroup LacAsym
 *
 * Elliptic Curve Cryptography common functions
 *
 * @lld_start
 *
 * @lld_overview
 * This is the Elliptic Curve common functions.
 *
 * @lld_dependencies
 * - \ref LacMem "Mem" : For memory allocation and freeing, and translating
 * between scalar and pointer types
 * - OSAL : For atomics and logging
 *
 * @lld_initialisation
 * On initialisation this component clears the stats and allocates memory
 * pool.
 *
 * @lld_module_algorithms
 *
 * @lld_process_context
 *
 * @lld_end
 *
 ***************************************************************************/

/*
****************************************************************************
* Include public/global header files
****************************************************************************
*/

/* API Includes */
#include "cpa.h"
#include "cpa_cy_ec.h"
#include "cpa_cy_ecdh.h"
#include "cpa_cy_ecdsa.h"
#include "cpa_cy_ecsm2.h"
#include "cpa_cy_ctrl.h"

/* OSAL Includes */
#include "Osal.h"

/* adf includes */
#include "icp_adf_init.h"
#include "icp_adf_transport.h"
#include "icp_accel_devices.h"
#include "icp_adf_debug.h"

/* FW includes */
#include "icp_qat_fw_la.h"

/* Look Aside Includes */
#include "lac_common.h"
#include "lac_log.h"
#include "lac_pke_qat_comms.h"
#include "lac_mem.h"
#include "lac_mem_pools.h"
#include "lac_hooks.h"
#include "lac_pke_utils.h"
#include "lac_sync.h"
#include "lac_list.h"
#include "lac_sym_qat.h"
#include "lac_sal_types_crypto.h"
#include "lac_ec.h"
#include "lac_sal.h"
#include "lac_sal_ctrl.h"

#include "lac_ec_nist_curves.h"
#include "qae_flushCache.h"

#define LAC_EC_NUM_STATS (sizeof(CpaCyEcStats64) / sizeof(Cpa64U))
#define LAC_ECDH_NUM_STATS (sizeof(CpaCyEcdhStats64) / sizeof(Cpa64U))
#define LAC_ECDSA_NUM_STATS (sizeof(CpaCyEcdsaStats64) / sizeof(Cpa64U))
#define LAC_SM2_NUM_STATS (sizeof(CpaCyEcsm2Stats64) / sizeof(Cpa64U))
#define LAC_CTRL_NUM_STATS (sizeof(CpaCyCtrlStats64) / sizeof(Cpa64U))

#define LAC_EC_ALL_STATS_CLEAR(pCryptoService)                                 \
    do                                                                         \
    {                                                                          \
        Cpa32U i = 0;                                                          \
                                                                               \
        for (i = 0; i < LAC_EC_NUM_STATS; i++)                                 \
        {                                                                      \
            osalAtomicSet(0, &pCryptoService->pLacEcStatsArr[i]);              \
        }                                                                      \
        for (i = 0; i < LAC_ECDH_NUM_STATS; i++)                               \
        {                                                                      \
            osalAtomicSet(0, &pCryptoService->pLacEcdhStatsArr[i]);            \
        }                                                                      \
        for (i = 0; i < LAC_ECDSA_NUM_STATS; i++)                              \
        {                                                                      \
            osalAtomicSet(0, &pCryptoService->pLacEcdsaStatsArr[i]);           \
        }                                                                      \
        for (i = 0; i < LAC_SM2_NUM_STATS; i++)                                \
        {                                                                      \
            osalAtomicSet(0, &pCryptoService->pLacEcsm2StatsArr[i]);           \
        }                                                                      \
        for (i = 0; i < LAC_CTRL_NUM_STATS; i++)                               \
        {                                                                      \
            osalAtomicSet(0, &pCryptoService->pLacCtrlStatsArr[i]);            \
        }                                                                      \
    } while (0)
/**< @ingroup Lac_Ec
 * macro to initialize all EC stats (stored in internal array of atomics)
 * assumes pCryptoService has already been validated */

/*
********************************************************************************
* ECC Curve Parameters
* Standards for Efficient Cryptography (SEC)
* secp : Prime field curve
* secb : Binary field curve
********************************************************************************
*/
/*================================ secp192r1 ================================*/
const uint32_t Secp192r1[] =
{
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, /* p */
    0x00000002, 0x00000000, 0x00000003, 0x00000000, 0x00000002, 0x00000000, /* pHigh */
    0xFFFFFFFC, 0xFFFFFFFF, 0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, /* a */
    0xC146B9B1, 0xFEB8DEEC, 0x72243049, 0x0FA7E9AB, 0xE59C80E7, 0x64210519, /* b */
    0x82FF1012, 0xF4FF0AFD, 0x43A18800, 0x7CBF20EB, 0xB03090F6, 0x188DA80E, /* Gx */
    0x1E794811, 0x73F977A1, 0x6B24CDD5, 0x631011ED, 0xFFC8DA78, 0x07192B95, /* Gy */
    0xB4D22831, 0x146BC9B1, 0x99DEF836, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, /* n */
    0x83134C27, 0x01D1770A, 0xCAAF687F, 0xD69C6961, 0xCEF5D8C5, 0x126792C4, /* nHigh */
    0xC0A1E340, 0xB19963D8, 0x80D1090B, 0x4730D4F4, 0x184AC737, 0x51A581D9, /* 2_96_Gx */
    0xE69912A5, 0xECC56731, 0x2F683F16, 0x7CDFCEA0, 0xE0BB9F6E, 0x5BD81EE2, /* 2_96_Gy */
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000  /* n0 */
};

/*================================ secp224r1 ================================*/
const uint32_t Secp224r1[] =
{
    0x00000001, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, /* p */
    0x00000001, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFE,
    0xFFFFFFFF, /* pHigh */
    0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, /* a */
    0x2355FFB4, 0x270B3943, 0xD7BFD8BA, 0x5044B0B7, 0xF5413256, 0x0C04B3AB,
    0xB4050A85, /* b */
    0x115C1D21, 0x343280D6, 0x56C21122, 0x4A03C1D3, 0x321390B9, 0x6BB4BF7F,
    0xB70E0CBD, /* Gx */
    0x85007E34, 0x44D58199, 0x5A074764, 0xCD4375A0, 0x4C22DFE6, 0xB5F723FB,
    0xBD376388, /* Gy */
    0x5C5C2A3D, 0x13DD2945, 0xE0B8F03E, 0xFFFF16A2, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, /* n */
    0x5F517D15, 0x29947A69, 0x31D63F4B, 0xABC8FF59, 0xD9714856, 0x6AD15F7C,
    0xB1E97961, /* nHigh */
    0x6CAB26E3, 0xA0064196, 0x2991FAB0, 0x3A0B91FB, 0xEC27A4E1, 0x5F8EBEEF,
    0x0499AA8A, /* 2_112_Gx */
    0x7766AF5D, 0x50751040, 0x29610D54, 0xF70684D9, 0xD77AAE82, 0x338C5B81,
    0x6916F6D4, /* 2_112_Gy */
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000  /* n0 */
};

/*================================ secp256r1 ================================*/
const uint32_t Secp256r1[] =
{
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000,
    0x00000001, 0xFFFFFFFF, /* p */
    0x00000003, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFB, 0xFFFFFFFE, 0xFFFFFFFF,
    0xFFFFFFFD, 0x00000004, /* pHigh */
    0xFFFFFFFC, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000,
    0x00000001, 0xFFFFFFFF, /* a */
    0x27D2604B, 0x3BCE3C3E, 0xCC53B0F6, 0x651D06B0, 0x769886BC, 0xB3EBBD55,
    0xAA3A93E7, 0x5AC635D8, /* b */
    0xD898C296, 0xF4A13945, 0x2DEB33A0, 0x77037D81, 0x63A440F2, 0xF8BCE6E5,
    0xE12C4247, 0x6B17D1F2, /* Gx */
    0x37BF51F5, 0xCBB64068, 0x6B315ECE, 0x2BCE3357, 0x7C0F9E16, 0x8EE7EB4A,
    0xFE1A7F9B, 0x4FE342E2, /* Gy */
    0xFC632551, 0xF3B9CAC2, 0xA7179E84, 0xBCE6FAAD, 0xFFFFFFFF, 0xFFFFFFFF,
    0x00000000, 0xFFFFFFFF, /* n */
    0xBE79EEA2, 0x83244C95, 0x49BD6FA6, 0x4699799C, 0x2B6BEC59, 0x2845B239,
    0xF3D95620, 0x66E12D94, /* nHigh */
    0xD789BD85, 0x57C84FC9, 0xC297EAC3, 0xFC35FF7D, 0x88C6766E, 0xFB982FD5,
    0xEEDB5E67, 0x447D739B, /* [2^128]Gx */
    0x72E25B32, 0x0C7E33C9, 0xA7FAE500, 0x3D349B95, 0x3A4AAFF7, 0xE12E9D95,
    0x834131EE, 0x2D4825AB, /* [2^128]Gy */
    0xEE00BC4F, 0xCCD1C8AA, 0x7D74D2E4, 0x48C94408, 0xC588C6F6, 0x50FE77EC,
    0xA9D6281C, 0x60D06633  /* n0 */
};

/*================================ secp384r1 ================================*/
const uint32_t Secp384r1[] =
{
    0xFFFFFFFF, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFE, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, /* p */
    0x00000000, 0xFFFFFFFE, 0x00000002, 0x00000001, 0xFFFFFFFD, 0xFFFFFFFD,
    0x00000002, 0x00000003, 0x00000002, 0xFFFFFFFE, 0x00000000, 0x00000002, /* pHigh */
    0xFFFFFFFC, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFE, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, /* a */
    0xD3EC2AEF, 0x2A85C8ED, 0x8A2ED19D, 0xC656398D, 0x5013875A, 0x0314088F,
    0xFE814112, 0x181D9C6E, 0xE3F82D19, 0x988E056B, 0xE23EE7E4, 0xB3312FA7, /* b */
    0x72760AB7, 0x3A545E38, 0xBF55296C, 0x5502F25D, 0x82542A38, 0x59F741E0,
    0x8BA79B98, 0x6E1D3B62, 0xF320AD74, 0x8EB1C71E, 0xBE8B0537, 0xAA87CA22, /* Gx */
    0x90EA0E5F, 0x7A431D7C, 0x1D7E819D, 0x0A60B1CE, 0xB5F0B8C0, 0xE9DA3113,
    0x289A147C, 0xF8F41DBD, 0x9292DC29, 0x5D9E98BF, 0x96262C6F, 0x3617DE4A, /* Gy */
    0xCCC52973, 0xECEC196A, 0x48B0A77A, 0x581A0DB2, 0xF4372DDF, 0xC7634D81,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, /* n */
    0xAE87B9E7, 0x7FDB954A, 0xB6F62810, 0x8C23F7F3, 0xAAAE6873, 0xE99276EA,
    0xB33C33C5, 0xD558BFBC, 0xEED33213, 0x3B277D79, 0xF4A0E792, 0xCC9601F9, /* nHigh */
    0xAA03BD53, 0xA628B09A, 0xA4F52D78, 0xBA065458, 0x4D10DDEA, 0xDB298789,
    0x8A3E297D, 0xB42A31AF, 0x06421279, 0x40F7F9E7, 0x800119C4, 0xC19E0B4C, /* [2^128]Gx */
    0xE6C88C41, 0x822D0FC5, 0xE639D858, 0xAF68AA6D, 0x35F6EBF2, 0xC1C7CAD1,
    0xE3567AF9, 0x577A30EA, 0x1F5B77F6, 0xE5A0191D, 0x0356B301, 0x16F3FDBF, /* [2^128]Gy */
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000  /* n0 */
};

/*================================ secp521r1 ================================*/
const uint32_t Secp521r1[] =
{
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x000001FF, /* p */
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00004000, 0x00000000, /* pHigh */
    0xFFFFFFFC, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x000001FF, /* a */
    0x6B503F00, 0xEF451FD4, 0x3D2C34F1, 0x3573DF88, 0x3BB1BF07, 0x1652C0BD,
    0xEC7E937B, 0x56193951, 0x8EF109E1, 0xB8B48991, 0x99B315F3, 0xA2DA725B,
    0xB68540EE, 0x929A21A0, 0x8E1C9A1F, 0x953EB961, 0x00000051, /* b */
    0xC2E5BD66, 0xF97E7E31, 0x856A429B, 0x3348B3C1, 0xA2FFA8DE, 0xFE1DC127,
    0xEFE75928, 0xA14B5E77, 0x6B4D3DBA, 0xF828AF60, 0x053FB521, 0x9C648139,
    0x2395B442, 0x9E3ECB66, 0x0404E9CD, 0x858E06B7, 0x000000C6, /* Gx */
    0x9FD16650, 0x88BE9476, 0xA272C240, 0x353C7086, 0x3FAD0761, 0xC550B901,
    0x5EF42640, 0x97EE7299, 0x273E662C, 0x17AFBD17, 0x579B4468, 0x98F54449,
    0x2C7D1BD9, 0x5C8A5FB4, 0x9A3BC004, 0x39296A78, 0x00000118, /* Gy */
    0x91386409, 0xBB6FB71E, 0x899C47AE, 0x3BB5C9B8, 0xF709A5D0, 0x7FCC0148,
    0xBF2F966B, 0x51868783, 0xFFFFFFFA, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x000001FF, /* n */
    0x60EC0915, 0xABEE0E49, 0xA4DEB96A, 0x39B88F86, 0x6440173A, 0x36439BEA,
    0x53EBDD25, 0xABED7C82, 0x7A1814F8, 0xDB25B111, 0xB2DD5C97, 0xE290362F,
    0x158143E2, 0xA9EA49FF, 0x141CEAE0, 0x07E35810, 0x000001CD, /* nHigh */
    0x9185544D, 0x6D9B0C3C, 0x8DF2765F, 0xAD21890E, 0xCBE030A2, 0x47836EE3,
    0xF7651AED, 0x606B9133, 0x71C00932, 0xB1A31586, 0xCFE05F47, 0x9806A369,
    0xF57F3700, 0xC2EBC613, 0xF065F07C, 0x1022D6D2, 0x00000109, /* [2^260]Gx */
    0x514C45ED, 0xB292C583, 0x947E68A1, 0x89AC5BF2, 0xAF507C14, 0x633C4300,
    0x7DA4020A, 0x943D7BA5, 0xC0ED8274, 0xD1E90C7A, 0xE59426E6, 0x9634868C,
    0xC26BC9DE, 0x24A6FFF2, 0x152416CD, 0x1A012168, 0x0000000C, /* [2^260]Gy */
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000  /* n0 */
};

/*================================= secb163 =================================*/
const uint32_t Secb163[] =
{
    0x4a3205fd, 0x512f7874, 0x1481eb10, 0xb8c953ca, 0x0a601907, 0x00000002, // b
    0xe8343e36, 0xd4994637, 0xa0991168, 0x86a2d57e, 0xf0eba162, 0x00000003, // Gx
    0x797324f1, 0xb11c5c0c, 0xa2cdd545, 0x71a0094f, 0xd51fbc6c, 0x00000000, // Gy
    0xa4234c33, 0x77e70c12, 0x000292fe, 0x00000000, 0x00000000, 0x00000004, // n
    0xCB87C99F, 0x4E073392, 0x3B17A626, 0xC0AB1838, 0x74E4DA7A, 0x00000000, // hHigh
};

/*================================= secb233 =================================*/
const uint32_t Secb233[] =
{
    0x7d8f90ad, 0x81fe115f, 0x20e9ce42, 0x213b333b, 0x0923bb58, 0x332c7f8c,
    0x647ede6c, 0x00000066, // b
    0x71fd558b, 0xf8f8eb73, 0x391f8b36, 0x5fef65bc, 0x39f1bb75, 0x8313bb21,
    0xc9dfcbac, 0x000000fa, // Gx
    0x01f81052, 0x36716f7e, 0xf867a7ca, 0xbf8a0bef, 0xe58528be, 0x03350678,
    0x6a08a419, 0x00000100, // Gy
    0x03cfe0d7, 0x22031d26, 0xe72f8a69, 0x0013e974, 0x00000000, 0x00000000,
    0x00000000, 0x00000100, // n
    0xC26DD4D1, 0xCDAA1BA1, 0xE7E89545, 0x578CD5EF, 0x9138B004, 0xCDD6D0CC,
    0xB044AA57, 0x0000006A  // nHigh
};

/*================================= secb283 =================================*/
const uint32_t Secb283[] =
{
    0x3b79a2f5, 0xf6263e31, 0xa581485a, 0x45309fa2, 0xca97fd76, 0x19a0303f,
    0xa5a4af8a, 0xc8b8596d, 0x027b680a, // b
    0x86b12053, 0xf8cdbecd, 0x80e2e198, 0x557eac9c, 0x2eed25b8, 0x70b0dfec,
    0xe1934f8c, 0x8db7dd90, 0x05f93925, // Gx
    0xbe8112f4, 0x13f0df45, 0x826779c8, 0x350eddb0, 0x516ff702, 0xb20d02b4,
    0xb98fe6d4, 0xfe24141c, 0x03676854, // Gy
    0xefadb307, 0x5b042a7c, 0x938a9016, 0x399660fc, 0xffffef90, 0xffffffff,
    0xffffffff, 0xffffffff, 0x03ffffff, // n
    0xCBDC6381, 0x81A74E1B, 0xB0A0A776, 0xCF8D412E, 0xD73957BD, 0x572154E6,
    0x288B3017, 0x137D2D8C, 0x00D668C8  /* nHigh */ 
};

/*================================= secb409 =================================*/
const uint32_t Secb409[] =
{
    0x7b13545f, 0x4f50ae31, 0xd57a55aa, 0x72822f6c, 0xa9a197b2, 0xd6ac27c8,
    0x4761fa99, 0xf1f3dd67, 0x7fd6422e, 0x3b7b476b, 0x5c4b9a75, 0xc8ee9feb,
    0x0021a5c2, // b
    0xbb7996a7, 0x60794e54, 0x5603aeab, 0x8a118051, 0xdc255a86, 0x34e59703,
    0xb01ffe5b, 0xf1771d4d, 0x441cde4a, 0x64756260, 0x496b0c60, 0xd088ddb3,
    0x015d4860, // Gx
    0x0273c706, 0x81c364ba, 0xd2181b36, 0xdf4b4f40, 0x38514f1f, 0x5488d08f,
    0x0158aa4f, 0xa7bd198d, 0x7636b9c5, 0x24ed106a, 0x2bbfa783, 0xab6be5f3,
    0x0061b1cf, // Gy
    0xd9a21173, 0x8164cd37, 0x9e052f83, 0x5fa47c3c, 0xf33307be, 0xaad6a612,
    0x000001e2, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x01000000, // n
    0xE091D1C4, 0x10755B94, 0xDB7E134A, 0xC7F7547F, 0xF7AA85C4, 0x0603C932,
    0xFBD9AFFA, 0xD2367C03, 0xF64C76A9, 0x0C7628C0, 0x42C5B003, 0x6BE1B84F,
    0x00A152B0  // nHigh 
};

/*================================= secb571 =================================*/
const uint32_t Secb571[] =
{
    0x2955727a, 0x7ffeff7f, 0x39baca0c, 0x520e4de7, 0x78ff12aa, 0x4afd185a,
    0x56a66e29, 0x2be7ad67, 0x8efa5933, 0x84ffabbd, 0x4a9a18ad, 0xcd6ba8ce,
    0xcb8ceff1, 0x5c6a97ff, 0xb7f3d62f, 0xde297117, 0x2221f295, 0x02f40e7e, // b
    0x8eec2d19, 0xe1e7769c, 0xc850d927, 0x4abfa3b4, 0x8614f139, 0x99ae6003,
    0x5b67fb14, 0xcdd711a3, 0xf4c0d293, 0xbde53950, 0xdb7b2abd, 0xa5f40fc8,
    0x955fa80a, 0x0a93d1d2, 0x0d3cd775, 0x6c16c0d4, 0x34b85629, 0x0303001d, // Gx
    0x1b8ac15b, 0x1a4827af, 0x6e23dd3c, 0x16e2f151, 0x0485c19b, 0xb3531d2f,
    0x461bb2a8, 0x6291af8f, 0xbab08a57, 0x84423e43, 0x3921e8a6, 0x1980f853,
    0x009cbbca, 0x8c6c27a6, 0xb73d69d7, 0x6dccfffe, 0x42da639b, 0x037bf273, // Gy
    0x2fe84e47, 0x8382e9bb, 0x5174d66e, 0x161de93d, 0xc7dd9ca1, 0x6823851e,
    0x08059b18, 0xff559873, 0xe661ce18, 0xffffffff, 0xffffffff, 0xffffffff,
    0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0x03ffffff, // n
    0xD94E5A4A, 0x8C857823, 0x2B031B5F, 0x05CCD42E, 0xF9118F06, 0xA2D1B86C,
    0xD81D72B6, 0x1A889C93, 0xFF47D46A, 0xEE93AC2E, 0x72AFB86D, 0x9C9FFB59,
    0x4D1392FC, 0xBB04C6A4, 0x9582928F, 0xCAF5CAF6, 0x0C4EBF9F, 0x03E09D9D  // nHigh
};

/*================================= sm2 p256 =================================*/
const uint32_t Sm2p256r1[] =
{
    0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFE, /* p */
    0x00000003, 0x00000002, 0xFFFFFFFF, 0x00000002, 0x00000001, 0x00000001,
    0x00000002, 0x00000004, /* pHigh */
    0xFFFFFFFC, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFE, /* a */
    0x4D940E93, 0xDDBCBD41, 0x15AB8F92, 0xF39789F5, 0xCF6509A7, 0x4D5A9E4B,
    0x9D9F5E34, 0x28E9FA9E, /* b */
    0x334C74C7, 0x715A4589, 0xF2660BE1, 0x8FE30BBF, 0x6A39C994, 0x5F990446,
    0x1F198119, 0x32C4AE2C, /* Gx */
    0x2139F0A0, 0x02DF32E5, 0xC62A4740, 0xD0A9877C, 0x6B692153, 0x59BDCEE3,
    0xF4F6779C, 0xBC3736A2, /* Gy */
    0x39D54123, 0x53BBF409, 0x21C6052B, 0x7203DF6B, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFE, /* n */
    0x7C114F20, 0x901192AF, 0xDE6FA2FA, 0x3464504A, 0x3AFFE0D4, 0x620FC84C,
    0xA22B3D3B, 0x1EB5E412, /* nHigh */
    0xD13A42ED, 0xEAE3D9A9, 0x484E1B38, 0x2B2308F6, 0x88C21F3A, 0x3DB7B248,
    0x74D55DA9, 0xB692E5B5, /* [2^128]Gx */
    0xE295E5AB, 0xD186469D, 0x73438E6D, 0xDB61AC17, 0x544926F9, 0x5A924F85,
    0x0F3FB613, 0xA175051B, /* [2^128]Gy */
    0x72350975, 0x327F9E88, 0xFC8319A5, 0xDF1E8D34, 0xB08941D4, 0x2B0068D3,
    0x82E4C7BC, 0x6F39132F, /* n0 */
};

/**
 ***************************************************************************
 * @ingroup Lac_Ec
 *     Copies least significant len bytes from a flat buffer (if length of
 *     flat buffer is less than len padding will be added).
 *
 * @description
 *     This function copies data in a flat buffer to memory pointed to by
 *     ptr. This function performs no checks so it is assumed that there is
 *     enough memory allocated.
 *
 * @param[in/out]  ptr      Pointer to a pointer to an array of Cpa8U
 * @param[in]  pBuff        Pointer to a CpaFlatBuffer structure
 * @param[in]  len          Number of bytes to copy.
 *                          This is the amount by which *ptr is
 *                          incremented also.
 *
 ***************************************************************************/
STATIC void LacEc_FlatBuffToConcate(Cpa8U **ptr,
                                    const Cpa8U *pData,
                                    Cpa32U dataLenInBytes,
                                    Cpa32U dataOperationSizeBytes)
{
    Cpa8U *pMem = NULL;

    pMem = (Cpa8U *)*ptr;

    osalMemSet(pMem, 0, dataOperationSizeBytes);

    if(dataOperationSizeBytes >= dataLenInBytes)
    {
        memcpy(pMem, pData, dataLenInBytes);
    }
    else
    {
        memcpy(pMem, pData, dataOperationSizeBytes);
    }
    pMem = pMem + (((dataOperationSizeBytes + 7) >> 3) << 3);

    *ptr = pMem;
}

/**
 ***************************************************************************
 * @ingroup LacEc_GetSecbPara
 *      ECC curve paramter get
 *
 ***************************************************************************/
CpaStatus LacEc_GetSecbPara(const CpaInstanceHandle instanceHandle_in,
                            Cpa32U dataOperationSizeBytes,
                            Cpa8U **pCurveAddr)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
    CpaCyEccbCurveTypeDef curve;
    sal_crypto_service_t *pCryptoService = NULL;
    Cpa8U *pMemPoolConcate = NULL;
    Cpa8U *pConcateTemp = NULL;    

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    /* Need to concatenate user inputs - copy to ecc mempool memory */
    do
    {
        pMemPoolConcate =
            (Cpa8U *)Lac_MemPoolEntryAlloc(pCryptoService->lac_ec_pool);
        if (NULL == pMemPoolConcate)
        {
            LAC_LOG_ERROR("Cannot get mem pool entry");
            status = CPA_STATUS_RESOURCE;
        }
        else if ((void *)CPA_STATUS_RETRY == pMemPoolConcate)
        {
            osalYield();
        }
    } while ((void *)CPA_STATUS_RETRY == pMemPoolConcate);

    if (CPA_STATUS_SUCCESS != status)
    {
        return status;
    }

    *pCurveAddr = pMemPoolConcate;
    pConcateTemp = pMemPoolConcate;
    pConcateTemp += sizeof(CpaCyEccbCurveTypeDef);

    curve.b = (Cpa32U*)(LAC_OS_VIRT_TO_PHYS_INTERNAL(pConcateTemp));
    curve.Gx = curve.b + (dataOperationSizeBytes >> 2);
    curve.Gy = curve.Gx + (dataOperationSizeBytes >> 2);
    curve.n = curve.Gy + (dataOperationSizeBytes >> 2);
    curve.nHigh = curve.n + (dataOperationSizeBytes >> 2);

    switch(dataOperationSizeBytes)
    {
        case LC_EC_SIZE_W6_IN_BYTES:
            curve.bBitLen = 163;
            curve.nBitLen = 163;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secb163,
                    sizeof(Secb163), dataOperationSizeBytes * 5);
            break;
        case LC_EC_SIZE_W8_IN_BYTES:
            curve.bBitLen = 233;
            curve.nBitLen = 233;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secb233,
                    sizeof(Secb233), dataOperationSizeBytes * 5);          
            break;
        case LC_EC_SIZE_W9_IN_BYTES:
            curve.bBitLen = 283;
            curve.nBitLen = 283;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secb283,
                    sizeof(Secb283), dataOperationSizeBytes * 5);        
            break;
        case LC_EC_SIZE_W13_IN_BYTES:
            curve.bBitLen = 409;
            curve.nBitLen = 409;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secb409,
                    sizeof(Secb409), dataOperationSizeBytes * 5);           
            break;
        case LC_EC_SIZE_W18_IN_BYTES:
            curve.bBitLen = 571;
            curve.nBitLen = 571;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secb571,
                    sizeof(Secb571), dataOperationSizeBytes * 5);         
            break;
        default:
            status = CPA_STATUS_UNSUPPORTED;
            break;
    }

    memcpy(pMemPoolConcate, &curve, sizeof(CpaCyEccbCurveTypeDef));

    /* Flush cacheline */
    qaeFlushDCacheLines(
            (LAC_ARCH_UINT)pMemPoolConcate,
            sizeof(CpaCyEccbCurveTypeDef) + dataOperationSizeBytes * 5);

    return status;
}

/**
 ***************************************************************************
 * @ingroup LacEc_GetSecpPara
 *      ECC curve paramter get
 *
 ***************************************************************************/
CpaStatus LacEc_GetSecpPara(const CpaInstanceHandle instanceHandle_in,
                            Cpa32U dataOperationSizeBytes,
                            Cpa8U **pCurveAddr)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
    CpaCyEccpCurveTypeDef curve;
    sal_crypto_service_t *pCryptoService = NULL;
    Cpa8U *pMemPoolConcate = NULL;
    Cpa8U *pConcateTemp = NULL;

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    /* Need to concatenate user inputs - copy to ecc mempool memory */
    do
    {
        pMemPoolConcate =
            (Cpa8U *)Lac_MemPoolEntryAlloc(pCryptoService->lac_ec_pool);
        if (NULL == pMemPoolConcate)
        {
            LAC_LOG_ERROR("Cannot get mem pool entry");
            status = CPA_STATUS_RESOURCE;
        }
        else if ((void *)CPA_STATUS_RETRY == pMemPoolConcate)
        {
            osalYield();
        }
    } while ((void *)CPA_STATUS_RETRY == pMemPoolConcate);

    if (CPA_STATUS_SUCCESS != status)
    {
        return status;
    }

    *pCurveAddr = pMemPoolConcate;
    pConcateTemp = pMemPoolConcate;
    pConcateTemp += sizeof(CpaCyEccpCurveTypeDef);

    curve.p = (Cpa32U*)(LAC_OS_VIRT_TO_PHYS_INTERNAL(pConcateTemp));
    curve.pHigh = curve.p + (dataOperationSizeBytes >> 2);
    curve.a = curve.pHigh + (dataOperationSizeBytes >> 2);
    curve.b = curve.a + (dataOperationSizeBytes >> 2);
    curve.Gx = curve.b + (dataOperationSizeBytes >> 2);
    curve.Gy = curve.Gx + (dataOperationSizeBytes >> 2);
    curve.n = curve.Gy + (dataOperationSizeBytes >> 2);
    curve.nHigh = curve.n + (dataOperationSizeBytes >> 2);
    curve.HalfGx = curve.nHigh + (dataOperationSizeBytes >> 2);
    curve.HalfGy = curve.HalfGx + (dataOperationSizeBytes >> 2);
    curve.N0 = curve.HalfGy + (dataOperationSizeBytes >> 2);

    switch(dataOperationSizeBytes)
    {
        case LC_EC_SIZE_W6_IN_BYTES:
            curve.pBitLen = 192;
            curve.nBitLen = 192;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secp192r1,
                    sizeof(Secp192r1), dataOperationSizeBytes * 11);
            break;
        case LC_EC_SIZE_W7_IN_BYTES:
            curve.pBitLen = 224;
            curve.nBitLen = 224;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secp224r1,
                    sizeof(Secp224r1), dataOperationSizeBytes * 11);
            break;
        case LC_EC_SIZE_W8_IN_BYTES:
            curve.pBitLen = 256;
            curve.nBitLen = 256;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secp256r1,
                    sizeof(Secp256r1), dataOperationSizeBytes * 11);
            break;
        case LC_EC_SIZE_W12_IN_BYTES:
            curve.pBitLen = 384;
            curve.nBitLen = 384;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secp384r1,
                    sizeof(Secp384r1), dataOperationSizeBytes * 11);
            break;
        case LC_EC_SIZE_W17_IN_BYTES:
            curve.pBitLen = 521;
            curve.nBitLen = 521;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Secp521r1,
                    sizeof(Secp521r1), dataOperationSizeBytes * 11);
            break;
        default:
            status = CPA_STATUS_UNSUPPORTED;
            break;
    }

    memcpy(pMemPoolConcate, &curve, sizeof(CpaCyEccpCurveTypeDef));

    /* Flush cacheline */
    qaeFlushDCacheLines(
            (LAC_ARCH_UINT)pMemPoolConcate,
            sizeof(CpaCyEccpCurveTypeDef) + dataOperationSizeBytes * 11);

    return status;
}

/**
 ***************************************************************************
 * @ingroup LacEc_GetSm2PCurve
 *      Sm2 curve paramter get
 *
 ***************************************************************************/
CpaStatus LacEc_GetSm2PCurve(const CpaInstanceHandle instanceHandle_in,
                             Cpa32U dataOperationSizeBytes,
                             Cpa8U **pCurveAddr)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle instanceHandle = NULL;
    CpaCyEccpCurveTypeDef curve;
    sal_crypto_service_t *pCryptoService = NULL;
    Cpa8U *pMemPoolConcate = NULL;
    Cpa8U *pConcateTemp = NULL;

    if (CPA_INSTANCE_HANDLE_SINGLE == instanceHandle_in)
    {
        instanceHandle = Lac_GetFirstHandle(SAL_SERVICE_TYPE_CRYPTO_ASYM);
    }
    else
    {
        instanceHandle = instanceHandle_in;
    }

    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    /* Need to concatenate user inputs - copy to ecc mempool memory */
    do
    {
        pMemPoolConcate =
            (Cpa8U *)Lac_MemPoolEntryAlloc(pCryptoService->lac_ec_pool);
        if (NULL == pMemPoolConcate)
        {
            status = CPA_STATUS_RESOURCE;
        }
        else if ((void *)CPA_STATUS_RETRY == pMemPoolConcate)
        {
            osalYield();
        }
    } while ((void *)CPA_STATUS_RETRY == pMemPoolConcate);

    if (CPA_STATUS_SUCCESS != status)
    {
        return status;
    }

    *pCurveAddr = pMemPoolConcate;
    pConcateTemp = pMemPoolConcate;
    pConcateTemp += sizeof(CpaCyEccpCurveTypeDef);

    curve.p = (Cpa32U*)(LAC_OS_VIRT_TO_PHYS_INTERNAL(pConcateTemp));
    curve.pHigh = curve.p + (dataOperationSizeBytes >> 2);
    curve.a = curve.pHigh + (dataOperationSizeBytes >> 2);
    curve.b = curve.a + (dataOperationSizeBytes >> 2);
    curve.Gx = curve.b + (dataOperationSizeBytes >> 2);
    curve.Gy = curve.Gx + (dataOperationSizeBytes >> 2);
    curve.n = curve.Gy + (dataOperationSizeBytes >> 2);
    curve.nHigh = curve.n + (dataOperationSizeBytes >> 2);
    curve.HalfGx = curve.nHigh + (dataOperationSizeBytes >> 2);
    curve.HalfGy = curve.HalfGx + (dataOperationSizeBytes >> 2);
    curve.N0 = curve.HalfGy + (dataOperationSizeBytes >> 2);

    switch(dataOperationSizeBytes)
    {
        case LC_EC_SIZE_W8_IN_BYTES:
            curve.pBitLen = 256;
            curve.nBitLen = 256;
            LacEc_FlatBuffToConcate(&pConcateTemp, (Cpa8U *)Sm2p256r1,
                    sizeof(Sm2p256r1), dataOperationSizeBytes * 11);
            break;
        default:
            status = CPA_STATUS_UNSUPPORTED;
            break;
    }

    memcpy(pMemPoolConcate, &curve, sizeof(CpaCyEccpCurveTypeDef));

    /* Flush cacheline */
    qaeFlushDCacheLines(
            (LAC_ARCH_UINT)pMemPoolConcate,
            sizeof(CpaCyEccpCurveTypeDef) + dataOperationSizeBytes * 11);

    return status;
}

/**
 ***************************************************************************
 * @ingroup Lac_Ec
 *      ECC Initialisation function
 *
 ***************************************************************************/
CpaStatus LacEc_Init(CpaInstanceHandle instanceHandle)
{
    sal_crypto_service_t *pCryptoService = NULL;
    CpaStatus status = CPA_STATUS_SUCCESS;

    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    status = LAC_OS_MALLOC(&(pCryptoService->pLacEcStatsArr),
                           LAC_EC_NUM_STATS * sizeof(OsalAtomic));

    if (CPA_STATUS_SUCCESS == status)
    {
        status = LAC_OS_MALLOC(&(pCryptoService->pLacEcdhStatsArr),
                               LAC_ECDH_NUM_STATS * sizeof(OsalAtomic));
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        status = LAC_OS_MALLOC(&(pCryptoService->pLacEcdsaStatsArr),
                               LAC_ECDSA_NUM_STATS * sizeof(OsalAtomic));
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        status = LAC_OS_MALLOC(&(pCryptoService->pLacEcsm2StatsArr),
                               LAC_SM2_NUM_STATS * sizeof(OsalAtomic));
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        status = LAC_OS_MALLOC(&(pCryptoService->pLacCtrlStatsArr),
                               LAC_CTRL_NUM_STATS * sizeof(OsalAtomic));
    }

    /* initialize stats to zero */
    if (CPA_STATUS_SUCCESS == status)
    {
        LAC_EC_ALL_STATS_CLEAR(pCryptoService);
    }

    return status;
}

void LacEc_StatsFree(CpaInstanceHandle instanceHandle)
{
    sal_crypto_service_t *pCryptoService = NULL;
    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    if (NULL != pCryptoService->pLacEcStatsArr)
    {
        LAC_OS_FREE(pCryptoService->pLacEcStatsArr);
    }

    if (NULL != pCryptoService->pLacEcdhStatsArr)
    {
        LAC_OS_FREE(pCryptoService->pLacEcdhStatsArr);
    }

    if (NULL != pCryptoService->pLacEcdsaStatsArr)
    {
        LAC_OS_FREE(pCryptoService->pLacEcdsaStatsArr);
    }

    if (NULL != pCryptoService->pLacEcsm2StatsArr)
    {
        LAC_OS_FREE(pCryptoService->pLacEcsm2StatsArr);
    }

    if (NULL != pCryptoService->pLacCtrlStatsArr)
    {
        LAC_OS_FREE(pCryptoService->pLacCtrlStatsArr);
    }
}

void LacEc_StatsReset(CpaInstanceHandle instanceHandle)
{
    sal_crypto_service_t *pCryptoService = NULL;
    pCryptoService = (sal_crypto_service_t *)instanceHandle;

    LAC_EC_ALL_STATS_CLEAR(pCryptoService);
}

/**
 ***************************************************************************
 * @ingroup Lac_Ec
 *      Ecc Stats Show function
 ***************************************************************************/
void LacEc_StatsShow(CpaInstanceHandle instanceHandle)
{
    CpaCyEcStats64 ecStats = {0};
    CpaCyEcdhStats64 ecdhStats = {0};
    CpaCyEcdsaStats64 ecdsaStats = {0};

    /* retrieve the stats */
    (void)cpaCyEcQueryStats64(instanceHandle, &ecStats);
    (void)cpaCyEcdhQueryStats64(instanceHandle, &ecdhStats);
    (void)cpaCyEcdsaQueryStats64(instanceHandle, &ecdsaStats);

    /* log the stats to the standard output */

    /* engine info */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            SEPARATOR BORDER
            "  ECDSA Stats                               " BORDER
            "\n" SEPARATOR);

    /* sign r requests */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            BORDER " ECDSA Sign R Requests-Succ:     %16llu " BORDER "\n" BORDER
                   " ECDSA Sign R Request-Err:       %16llu " BORDER "\n" BORDER
                   " ECDSA Sign R Completed-Succ:    %16llu " BORDER "\n" BORDER
                   " ECDSA Sign R Completed-Err:     %16llu " BORDER "\n" BORDER
                   " ECDSA Sign R Output Invalid:    %16llu " BORDER
                   "\n" SEPARATOR,
            ecdsaStats.numEcdsaSignRRequests,
            ecdsaStats.numEcdsaSignRRequestErrors,
            ecdsaStats.numEcdsaSignRCompleted,
            ecdsaStats.numEcdsaSignRCompletedErrors,
            ecdsaStats.numEcdsaSignRCompletedOutputInvalid);

    /* s sign requests */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            BORDER " ECDSA Sign S Requests-Succ:     %16llu " BORDER "\n" BORDER
                   " ECDSA Sign S Request-Err:       %16llu " BORDER "\n" BORDER
                   " ECDSA Sign S Completed-Succ:    %16llu " BORDER "\n" BORDER
                   " ECDSA Sign S Completed-Err:     %16llu " BORDER "\n" BORDER
                   " ECDSA Sign S Output Invalid:    %16llu " BORDER
                   "\n" SEPARATOR,
            ecdsaStats.numEcdsaSignSRequests,
            ecdsaStats.numEcdsaSignSRequestErrors,
            ecdsaStats.numEcdsaSignSCompleted,
            ecdsaStats.numEcdsaSignSCompletedErrors,
            ecdsaStats.numEcdsaSignSCompletedOutputInvalid);

    /* rs sign requests */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            BORDER " ECDSA Sign RS Requests-Succ:    %16llu " BORDER "\n" BORDER
                   " ECDSA Sign RS Request-Err:      %16llu " BORDER "\n" BORDER
                   " ECDSA Sign RS Completed-Succ:   %16llu " BORDER "\n" BORDER
                   " ECDSA Sign RS Completed-Err:    %16llu " BORDER "\n" BORDER
                   " ECDSA Sign RS Output Invalid:   %16llu " BORDER
                   "\n" SEPARATOR,
            ecdsaStats.numEcdsaSignRSRequests,
            ecdsaStats.numEcdsaSignRSRequestErrors,
            ecdsaStats.numEcdsaSignRSCompleted,
            ecdsaStats.numEcdsaSignRSCompletedErrors,
            ecdsaStats.numEcdsaSignRSCompletedOutputInvalid);

    /* verify requests */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            BORDER " ECDSA Verify Requests-Succ:     %16llu " BORDER "\n" BORDER
                   " ECDSA Verify Request-Err:       %16llu " BORDER "\n" BORDER
                   " ECDSA Verify Completed-Succ:    %16llu " BORDER "\n" BORDER
                   " ECDSA Verify Completed-Err:     %16llu " BORDER "\n" BORDER
                   " ECDSA Verify Output Invalid:    %16llu " BORDER
                   "\n" SEPARATOR,
            ecdsaStats.numEcdsaVerifyRequests,
            ecdsaStats.numEcdsaVerifyRequestErrors,
            ecdsaStats.numEcdsaVerifyCompleted,
            ecdsaStats.numEcdsaVerifyCompletedErrors,
            ecdsaStats.numEcdsaVerifyCompletedOutputInvalid);

    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            SEPARATOR BORDER
            "  EC Stats                                  " BORDER
            "\n" SEPARATOR);

    /* ec point multiply requests */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            BORDER " EC Pt Multiply Requests-Succ:   %16llu " BORDER "\n" BORDER
                   " EC Pt Multiply Request-Err:     %16llu " BORDER "\n" BORDER
                   " EC Pt Multiply Completed-Succ:  %16llu " BORDER "\n" BORDER
                   " EC Pt Multiply Completed-Err:   %16llu " BORDER "\n" BORDER
                   " EC Pt Multiply Output Invalid:  %16llu " BORDER
                   "\n" SEPARATOR,
            ecStats.numEcPointMultiplyRequests,
            ecStats.numEcPointMultiplyRequestErrors,
            ecStats.numEcPointMultiplyCompleted,
            ecStats.numEcPointMultiplyCompletedError,
            ecStats.numEcPointMultiplyCompletedOutputInvalid);

    /* ec point verify requests */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            BORDER " EC Pt Verify Requests-Succ:     %16llu " BORDER "\n" BORDER
                   " EC Pt Verify Request-Err:       %16llu " BORDER "\n" BORDER
                   " EC Pt Verify Completed-Succ:    %16llu " BORDER "\n" BORDER
                   " EC Pt Verify Completed-Err:     %16llu " BORDER "\n" BORDER
                   " EC Pt Verify Output Invalid:    %16llu " BORDER
                   "\n" SEPARATOR,
            ecStats.numEcPointVerifyRequests,
            ecStats.numEcPointVerifyRequestErrors,
            ecStats.numEcPointVerifyCompleted,
            ecStats.numEcPointVerifyCompletedErrors,
            ecStats.numEcPointVerifyCompletedOutputInvalid);

    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            SEPARATOR BORDER
            "  ECDH Stats                                " BORDER
            "\n" SEPARATOR);

    /* ecdh point multiply requests */
    osalLog(OSAL_LOG_LVL_USER,
            OSAL_LOG_DEV_STDOUT,
            BORDER " ECDH Pt Multiply Requests-Succ: %16llu " BORDER "\n" BORDER
                   " ECDH Pt Multiply Request-Err:   %16llu " BORDER "\n" BORDER
                   " ECDH Pt Multiply Completed-Succ:%16llu " BORDER "\n" BORDER
                   " ECDH Pt Multiply Completed-Err: %16llu " BORDER "\n" BORDER
                   " ECDH Pt Multiply Output Invalid:%16llu " BORDER
                   "\n" SEPARATOR,
            ecdhStats.numEcdhPointMultiplyRequests,
            ecdhStats.numEcdhPointMultiplyRequestErrors,
            ecdhStats.numEcdhPointMultiplyCompleted,
            ecdhStats.numEcdhPointMultiplyCompletedError,
            ecdhStats.numEcdhRequestCompletedOutputInvalid);
}

void LacEc_CheckCurve4QWGF2(Cpa32U *pNumQWs,
                            const CpaFlatBuffer *pQ,
                            const CpaFlatBuffer *pA,
                            const CpaFlatBuffer *pB,
                            const CpaFlatBuffer *pR,
                            const CpaFlatBuffer *pH)
{
    Cpa32U bit_pos = 0;
    Cpa32U index = 0;
    size_t j = 0;
    CpaBoolean possible_curve = CPA_FALSE;
    CpaBoolean isZero = CPA_FALSE;

    /* Check modulus - Never NULL*/
    LacPke_GetBitPos(pQ, &bit_pos, &index, &isZero);
    if (NIST_GF2_Q_163_BIT_POS == bit_pos)
    {
        for (j = 0; j < (pQ->dataLenInBytes - index); j++)
        {
            if (nist_gf2_163_q[j] != pQ->pData[j + index])
            {
                break;
            }
        }
        if (j == (pQ->dataLenInBytes - index))
        {
            possible_curve = CPA_TRUE;
        }

    } /* if(NIST_GF2_Q_163_BIT_POS == bit_pos) */
    else
    {
        if (NIST_GF2_Q_233_BIT_POS == bit_pos)
        {
            for (j = 0; j < (pQ->dataLenInBytes - index); j++)
            {
                if (nist_gf2_233_q[j] != pQ->pData[j + index])
                {
                    break;
                }
            }
            if (j == (pQ->dataLenInBytes - index))
            {
                possible_curve = CPA_TRUE;
            }
        }

    } /* else(NIST_GF2_Q_163_BIT_POS != bit_pos) */

    if (CPA_TRUE != possible_curve)
    {
        /* Modulus not as required - cannot use L256 service */
        *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
        return;
    }
    if (NIST_GF2_Q_163_BIT_POS == bit_pos)
    {

        /* Possibly K163 or B163 Curves */

        /* Check coeff A - Never NULL */
        LacPke_GetBitPos(pA, &bit_pos, &index, &isZero);
        /* Coeff A is expected to be 1 -
           therefore no need to check the value of A
           just check the isZero flag and its bit_pos */
        if ((NIST_GF2_A_163_BIT_POS != bit_pos) || (CPA_TRUE == isZero))
        {
            /* Coeff A not as required for K163 or B163 curve */
            *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
            return;
        }

        /* Check cofactor - Can be NULL */
        if (NULL != pH)
        {
            if (NULL != pH->pData)
            {
                LacPke_GetBitPos(pH, &bit_pos, &index, &isZero);
                if ((NIST_GF2_H_163_BIT_POS != bit_pos) ||
                    (nist_gf2_163_h[0] != pH->pData[index]))
                {
                    /* Cofactor not as required for K163 or B163 curve */
                    *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                    return;
                }
            } /* if(NULL != pH->pData) */
        }     /* if(NULL != pH) */

        /* Check coeff B - Never NULL */
        LacPke_GetBitPos(pB, &bit_pos, &index, &isZero);
        /* Coeff B for K163 is expected to be 1 -
           therefore no need to check value just check isZero and bit_pos */
        if ((NIST_GF2_B_K163_BIT_POS == bit_pos) && (CPA_FALSE == isZero))
        {
            /* Check Order - Can be NULL */
            if (NULL != pR)
            {
                LacPke_GetBitPos(pR, &bit_pos, &index, &isZero);
                if (NIST_GF2_R_163_BIT_POS == bit_pos)
                {
                    for (j = 0; j < (pR->dataLenInBytes - index); j++)
                    {
                        if (nist_koblitz_gf2_163_r[j] != pR->pData[j + index])
                        {
                            break;
                        }
                    }
                    if (j != (pR->dataLenInBytes - index))
                    {
                        /* Order not as required for K163 curve */
                        *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                        return;
                    }

                } /* if(NIST_GF2_R_163_BIT_POS == bit_pos) */
                else
                {
                    /* Order not as required for K163 curve */
                    *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                    return;
                }

            } /* if(NULL != pR) */

            /* Found K163 curve - can use L256 service */
            *(pNumQWs) = LAC_EC_SIZE_QW4_IN_BYTES;
            return;

        } /* if((NIST_GF2_B_K163_BIT_POS == bit_pos) && (CPA_FALSE ==isZero)) */
        else
        {
            /* Check for B163 curve */
            if (NIST_GF2_B_B163_BIT_POS == bit_pos)
            {
                for (j = 0; j < (pB->dataLenInBytes - index); j++)
                {
                    if (nist_binary_gf2_163_b[j] != pB->pData[j + index])
                    {
                        break;
                    }
                }
                if (j != (pB->dataLenInBytes - index))
                {
                    /* Coeff B not as required for B163 curve */
                    *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                    return;
                }

                /* Check Order - Can be NULL */
                if (NULL != pR)
                {
                    LacPke_GetBitPos(pR, &bit_pos, &index, &isZero);
                    if (NIST_GF2_R_163_BIT_POS == bit_pos)
                    {
                        for (j = 0; j < (pR->dataLenInBytes - index); j++)
                        {
                            if (nist_binary_gf2_163_r[j] !=
                                pR->pData[j + index])
                            {
                                break;
                            }
                        }
                        if (j != (pR->dataLenInBytes - index))
                        {
                            /* Order not as required for B163 curve */
                            *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                            return;
                        }
                    }
                    else
                    {
                        /* Order not as required for B163 curve */
                        *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                        return;
                    }

                } /* if(NULL != pR) */

                /* Found B163 curve - can use L256 service */
                *(pNumQWs) = LAC_EC_SIZE_QW4_IN_BYTES;
                return;
            } /* if(NIST_GF2_B_B163_BIT_POS == bit_pos) */
            else
            {
                /* Coeff B not as required for K163 or B163 curve */
                *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                return;
            }
        } /*else((NIST_GF2_B_K163_BIT_POS != bit_pos)||(CPA_FALSE !=isZero))*/

    } /* if(NIST_GF2_Q_163_BIT_POS == bit_pos) */
    else
    {
        /* Possibly K233 or B233 Curves - pQ has been validated */

        /* Check coeff A - Never NULL*/
        LacPke_GetBitPos(pA, &bit_pos, &index, &isZero);
        if (CPA_TRUE == isZero)
        {
            /* coeff A = 0 - possibly K233 */

            /* Check Cofactor - Can be NULL */
            if (NULL != pH)
            {
                if (NULL != pH->pData)
                {
                    LacPke_GetBitPos(pH, &bit_pos, &index, &isZero);
                    if ((NIST_GF2_H_K233_BIT_POS != bit_pos) ||
                        (nist_koblitz_gf2_233_h[0] != pH->pData[index]))
                    {
                        /* Cofactor not as required for K233 curve */
                        *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                        return;
                    }
                }
            } /* if(NULL != pH) */

            /* Check Coeff B */
            LacPke_GetBitPos(pB, &bit_pos, &index, &isZero);
            /* Coeff B is expected to be 1 -
               therefore no need to check value just check isZero
               and bit_pos */
            if ((NIST_GF2_B_K233_BIT_POS != bit_pos) || (CPA_TRUE == isZero))
            {
                /* Coeff B not as required for K233 curve */
                *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                return;
            }

            /* Check R */
            if (NULL != pR)
            {
                LacPke_GetBitPos(pR, &bit_pos, &index, &isZero);
                if (NIST_GF2_R_K233_BIT_POS == bit_pos)
                {
                    for (j = 0; j < (pR->dataLenInBytes - index); j++)
                    {
                        if (nist_koblitz_gf2_233_r[j] != pR->pData[j + index])
                        {
                            break;
                        }
                    }
                    if (j != (pR->dataLenInBytes - index))
                    {
                        /* Order not as required for K233 curve */
                        *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                        return;
                    }
                }
                else
                {
                    /* Order not as required for K233 curve */
                    *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                    return;
                }

            } /* if(NULL != pR) */

            /* Found K233 curve - can use L256 service */
            *(pNumQWs) = LAC_EC_SIZE_QW4_IN_BYTES;
            return;

        } /* if(CPA_TRUE == isZero) */
        else
        {
            /* Check if coeff A = 1 */
            if (NIST_GF2_A_233_BIT_POS == bit_pos)
            {
                /* Possibly B233 */
                /* Check Cofactor - Can be NULL */
                if (NULL != pH)
                {
                    if (NULL != pH->pData)
                    {
                        LacPke_GetBitPos(pH, &bit_pos, &index, &isZero);
                        if ((NIST_GF2_H_B233_BIT_POS != bit_pos) ||
                            (nist_binary_gf2_233_h[0] != pH->pData[index]))
                        {
                            /* Cofactor not as required for B233 curve */
                            *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                            return;
                        }
                    }
                } /* if(NULL != pH)*/

                /* Check Coeff B */
                LacPke_GetBitPos(pB, &bit_pos, &index, &isZero);
                if (NIST_GF2_B_B233_BIT_POS == bit_pos)
                {
                    for (j = 0; j < (pB->dataLenInBytes - index); j++)
                    {
                        if (nist_binary_gf2_233_b[j] != pB->pData[j + index])
                        {
                            break;
                        }
                    }
                    if (j != (pB->dataLenInBytes - index))
                    {
                        /* Coeff B not as required for B233 curve */
                        *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                        return;
                    }
                } /* if(NIST_GF2_B_B233_BIT_POS == bit_pos) */
                else
                {
                    /* Coeff B not as required for B233 curve */
                    *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                    return;
                }

                /* Check R */
                if (NULL != pR)
                {
                    LacPke_GetBitPos(pR, &bit_pos, &index, &isZero);
                    if (NIST_GF2_R_B233_BIT_POS == bit_pos)
                    {
                        for (j = 0; j < (pR->dataLenInBytes - index); j++)
                        {
                            if (nist_binary_gf2_233_r[j] !=
                                pR->pData[j + index])
                            {
                                break;
                            }
                        }
                        if (j != (pR->dataLenInBytes - index))
                        {
                            /* Order not as required for B233 curve */
                            *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                            return;
                        }
                    }
                    else
                    {
                        /* Order not as required for B233 curve */
                        *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                        return;
                    }
                } /* if(NULL != pR) */

                /* Found B233 curve - can use L256 service */
                *(pNumQWs) = LAC_EC_SIZE_QW4_IN_BYTES;
                return;

            } /* if(NIST_GF2_A_233_BIT_POS == bit_pos) */
            else
            {
                /* Coeff A not as required for K233 or B233 curve */
                *(pNumQWs) = LAC_EC_SIZE_QW8_IN_BYTES;
                return;
            }

        } /* else(CPA_TRUE != isZero) */

    } /* else(NIST_GF2_Q_163_BIT_POS != bit_pos) */

    return;
}

CpaStatus LacEc_CheckCurve9QWGFP(const CpaFlatBuffer *pQ,
                                 const CpaFlatBuffer *pA,
                                 const CpaFlatBuffer *pB,
                                 const CpaFlatBuffer *pR,
                                 const CpaFlatBuffer *pH,
                                 const CpaFlatBuffer *pX,
                                 const CpaFlatBuffer *pY)
{
    /* status invalid unless curve is found */
    CpaStatus status = CPA_STATUS_INVALID_PARAM;
    Cpa32U bit_pos = 0;
    Cpa32U index = 0;
    size_t j = 0;
    CpaBoolean isZero = CPA_FALSE;

    /* Check modulus - Never NULL*/
    LacPke_GetBitPos(pQ, &bit_pos, &index, &isZero);
    if (NIST_GFP_Q_521_BIT_POS != bit_pos)
    {
        /* Length of modulus not 521 */
        LAC_INVALID_PARAM_LOG("Modulus not as expected for NIST P-521 curve");
        return status;
    }
    else
    {
        /* Modulus correct length - check value */
        for (j = 0; j < (pQ->dataLenInBytes - index); j++)
        {
            if (nist_p521_q[j] != pQ->pData[j + index])
            {
                break;
            }
        }
        if (j != (pQ->dataLenInBytes - index))
        {
            /* Modulus != 2^521-1 */
            LAC_INVALID_PARAM_LOG("Modulus not as expected for NIST "
                                  "P-521 curve");
            return status;
        }
    }
    /* Modulus as required for NIST GFP-521 curve - next check A coeff */
    LacPke_GetBitPos(pA, &bit_pos, &index, &isZero);
    if (NIST_GFP_A_521_BIT_POS != bit_pos)
    {
        /* Length not correct for P-521 curve */
        LAC_INVALID_PARAM_LOG("Coeff A not as expected for NIST P-521 curve");
        return status;
    }
    else
    {
        /* Valid length - check value */
        for (j = 0; j < (pA->dataLenInBytes - index); j++)
        {
            if (nist_p521_a[j] != pA->pData[j + index])
            {
                break;
            }
        }
        if (j != (pA->dataLenInBytes - index))
        {
            /* Value not as required for P_521 curve */
            LAC_INVALID_PARAM_LOG("Coeff A not as expected for NIST "
                                  "P-521 curve");
            return status;
        }
    }
    /* Modulus and A coeff as required - next check B coeff */
    LacPke_GetBitPos(pB, &bit_pos, &index, &isZero);
    if (NIST_GFP_B_521_BIT_POS != bit_pos)
    {
        /* Length not correct for P-521 curve */
        LAC_INVALID_PARAM_LOG("Coeff B not as expected for NIST P-521 curve");
        return status;
    }
    else
    {
        /* Valid length - check value */
        for (j = 0; j < (pB->dataLenInBytes - index); j++)
        {
            if (nist_p521_b[j] != pB->pData[j + index])
            {
                break;
            }
        }
        if (j != (pB->dataLenInBytes - index))
        {
            /* Value not as required for P-521 curve */
            LAC_INVALID_PARAM_LOG("Coeff B not as expected for NIST "
                                  "P-521 curve");
            return status;
        }
    }
    /* Modulus, A and B coeffs as required - next check Order */
    if (NULL != pR)
    {
        LacPke_GetBitPos(pR, &bit_pos, &index, &isZero);
        if (NIST_GFP_R_521_BIT_POS != bit_pos)
        {
            /* Length not correct for P-521 curve */
            LAC_INVALID_PARAM_LOG("Order not as expected for NIST "
                                  "P-521 curve");
            return status;
        }
        else
        {
            /* Valid length - check value */
            for (j = 0; j < (pR->dataLenInBytes - index); j++)
            {
                if (nist_p521_r[j] != pR->pData[j + index])
                {
                    break;
                }
            }
            if (j != (pR->dataLenInBytes - index))
            {
                /* Value not as required for P-521 curve */
                LAC_INVALID_PARAM_LOG("Order not as expected for NIST "
                                      "P-521 curve");
                return status;
            }
        }
    } /* if(NULL !== pR) */

    /* Modulus, A and B coeffs and Order as required - next check Cofactor */
    if (NULL != pH)
    {
        if (NULL != pH->pData)
        {
            LacPke_GetBitPos(pH, &bit_pos, &index, &isZero);
            if ((NIST_GFP_H_521_BIT_POS != bit_pos) || (CPA_TRUE == isZero))
            {
                /* Length not correct for P-521 curve */
                LAC_INVALID_PARAM_LOG("Cofactor not as expected for NIST "
                                      "P-521 curve");
                return status;
            }
        }
    }

    /* Curve as required - now check that x and y <= Modulus (2^521-1) */
    if (NULL != pX)
    {
        LacPke_GetBitPos(pX, &bit_pos, &index, &isZero);
        if (bit_pos > NIST_GFP_Q_521_BIT_POS)
        {
            /* x greater than Modulus */
            LAC_INVALID_PARAM_LOG("X coordinate not as expected for NIST "
                                  "P-521 curve");
            return status;
        }
    }
    if (NULL != pY)
    {
        LacPke_GetBitPos(pY, &bit_pos, &index, &isZero);
        if (bit_pos > NIST_GFP_Q_521_BIT_POS)
        {
            /* y coeff greater than Modulus */
            LAC_INVALID_PARAM_LOG("Y coordinate not as expected for NIST "
                                  "P-521 curve");
            return status;
        }
    }
    /* Curve as required - return success */
    return CPA_STATUS_SUCCESS;
}

CpaStatus LacEc_CheckCurve9QWGF2(const CpaFlatBuffer *pQ,
                                 const CpaFlatBuffer *pA,
                                 const CpaFlatBuffer *pB,
                                 const CpaFlatBuffer *pR,
                                 const CpaFlatBuffer *pH,
                                 const CpaFlatBuffer *pX,
                                 const CpaFlatBuffer *pY)
{
    /* status invalid unless curve is found */
    CpaStatus status = CPA_STATUS_INVALID_PARAM;
    Cpa32U bit_pos = 0;
    Cpa32U index = 0;
    size_t j = 0;
    CpaBoolean isZero = CPA_FALSE;

    /* Check modulus - Never NULL*/
    LacPke_GetBitPos(pQ, &bit_pos, &index, &isZero);
    if (NIST_GF2_Q_571_BIT_POS != bit_pos)
    {
        /* Length of modulus not as required */
        LAC_INVALID_PARAM_LOG("Modulus not as expected for NIST 571 curves");
        return status;
    }
    else
    {
        /* Modulus correct length - check value */
        for (j = 0; j < (pQ->dataLenInBytes - index); j++)
        {
            if (nist_gf2_571_q[j] != pQ->pData[j + index])
            {
                break;
            }
        }
        if (j != (pQ->dataLenInBytes - index))
        {
            /* Modulus value not as required */
            LAC_INVALID_PARAM_LOG("Modulus not as expected for NIST "
                                  "571 curves");
            return status;
        }
    }
    /* Modulus as required for NIST 571 curves - next check A coeff */
    LacPke_GetBitPos(pA, &bit_pos, &index, &isZero);
    if (NIST_GF2_A_571_BIT_POS != bit_pos)
    {
        /* For K-571 A coeff = 0 and for B-571 A coeff = 1, therefore
           bit_pos = 1 is only valid cases */
        /* Length of A coeff not as required for K-571 or B-571 */
        LAC_INVALID_PARAM_LOG("Coeff A not as expected for NIST 571 curves");
        return status;
    }
    if (CPA_TRUE == isZero)
    {
        /* Check for K-571 curve */
        /* Check B Coeff */
        LacPke_GetBitPos(pB, &bit_pos, &index, &isZero);
        if ((NIST_GF2_B_K571_BIT_POS != bit_pos) || (CPA_TRUE == isZero))
        {
            /* Length not correct for K-571 curve */
            LAC_INVALID_PARAM_LOG("Coeff B not as expected for NIST "
                                  "K571 curve");
            return status;
        }
        /* Modulus, Coeff A and B as required for K-571 */
        /* Check Order */
        if (NULL != pR)
        {
            LacPke_GetBitPos(pR, &bit_pos, &index, &isZero);
            if (NIST_GF2_R_K571_BIT_POS != bit_pos)
            {
                /* Length not correct for K-571 curve */
                LAC_INVALID_PARAM_LOG("Order not as expected for NIST "
                                      "K571 curve");
                return status;
            }
            else
            {
                /* Valid length - check value */
                for (j = 0; j < (pR->dataLenInBytes - index); j++)
                {
                    if (nist_koblitz_gf2_571_r[j] != pR->pData[j + index])
                    {
                        break;
                    }
                }
                if (j != (pR->dataLenInBytes - index))
                {
                    /* Value not as required for K-571 curve */
                    LAC_INVALID_PARAM_LOG("Order not as expected for NIST "
                                          "K571 curve");
                    return status;
                }
            }
        } /* if(NULL = pR) */

        /* Check Cofactor */
        if (NULL != pH)
        {
            if (NULL != pH->pData)
            {
                LacPke_GetBitPos(pH, &bit_pos, &index, &isZero);
                if (NIST_GF2_H_K571_BIT_POS != bit_pos)
                {
                    /* Length not correct for K-571 curve */
                    LAC_INVALID_PARAM_LOG("Cofactor not as expected for NIST "
                                          "K571 curve");
                    return status;
                }
                else
                {
                    /* Valid length - check value */
                    for (j = 0; j < (pH->dataLenInBytes - index); j++)
                    {
                        if (nist_koblitz_gf2_571_h[j] != pH->pData[j + index])
                        {
                            break;
                        }
                    }
                    if (j != (pH->dataLenInBytes - index))
                    {
                        /* Value not as required for K-571 curve */
                        LAC_INVALID_PARAM_LOG(
                            "Cofactor not as expected for NIST"
                            " K571 curve");
                        return status;
                    }
                }

            } /* if(NULL != pH->pData) */

        } /* if(NULL != pH) */

        /* Found K-571 curve - now check degree of X and Y is required */
        if (NULL != pX)
        {
            LacPke_GetBitPos(pX, &bit_pos, &index, &isZero);
            if (bit_pos >= NIST_GF2_Q_571_BIT_POS)
            {
                /* deg X should be less than deg modulus */
                LAC_INVALID_PARAM_LOG("X coordinate not as expected for NIST "
                                      "K571 curve");
                return status;
            }
        }
        if (NULL != pY)
        {

            LacPke_GetBitPos(pY, &bit_pos, &index, &isZero);
            if (bit_pos >= NIST_GF2_Q_571_BIT_POS)
            {
                /* deg Y should be less than deg modulus */
                LAC_INVALID_PARAM_LOG("Y coordinate not as expected for NIST "
                                      "K571 curve");
                return status;
            }
        }
        status = CPA_STATUS_SUCCESS;

    } /* if(CPA_TRUE==isZero) */
    else
    {
        /* Check for B-571 curve */
        /* Check B Coeff */
        LacPke_GetBitPos(pB, &bit_pos, &index, &isZero);
        if (NIST_GF2_B_B571_BIT_POS != bit_pos)
        {
            /* Length not correct for B-571 curve */
            LAC_INVALID_PARAM_LOG("Coeff B not as expected for NIST "
                                  "B571 curve");
            return status;
        }
        else
        {
            /* Valid length - check value */
            for (j = 0; j < (pB->dataLenInBytes - index); j++)
            {
                if (nist_binary_gf2_571_b[j] != pB->pData[j + index])
                {
                    break;
                }
            }
            if (j != (pB->dataLenInBytes - index))
            {
                /* Value not as required for B-571 curve */
                LAC_INVALID_PARAM_LOG("Coeff B not as expected for NIST "
                                      "B571 curve");
                return status;
            }
        }

        /* Modulus, Coeff A and B as required for B-571 */
        /* Check Order */
        if (NULL != pR)
        {
            LacPke_GetBitPos(pR, &bit_pos, &index, &isZero);
            if (NIST_GF2_R_B571_BIT_POS != bit_pos)
            {
                /* Length not correct for B-571 curve */
                LAC_INVALID_PARAM_LOG("Order not as expected for NIST "
                                      "B571 curve");
                return status;
            }
            else
            {
                /* Valid length - check value */
                for (j = 0; j < (pR->dataLenInBytes - index); j++)
                {
                    if (nist_binary_gf2_571_r[j] != pR->pData[j + index])
                    {
                        break;
                    }
                }
                if (j != (pR->dataLenInBytes - index))
                {
                    /* Value not as required for B-571 curve */
                    LAC_INVALID_PARAM_LOG("Order not as expected for NIST "
                                          "B571 curve");
                    return status;
                }
            }
        } /* if(NULL != pR) */

        /* Check Cofactor */
        if (NULL != pH)
        {
            if (NULL != pH->pData)
            {
                LacPke_GetBitPos(pH, &bit_pos, &index, &isZero);
                if (NIST_GF2_H_B571_BIT_POS != bit_pos)
                {
                    /* Length not correct for B-571 curve */
                    LAC_INVALID_PARAM_LOG("Cofactor not as expected for NIST "
                                          "B571 curve");
                    return status;
                }
                else
                {
                    /* Valid length - check value */
                    for (j = 0; j < (pH->dataLenInBytes - index); j++)
                    {
                        if (nist_binary_gf2_571_h[j] != pH->pData[j + index])
                        {
                            break;
                        }
                    }
                    if (j != (pH->dataLenInBytes - index))
                    {
                        /* Value not as required for B-571 curve */
                        LAC_INVALID_PARAM_LOG(
                            "Cofactor not as expected for NIST"
                            " B571 curve");
                        return status;
                    }
                }

            } /* if(NULL != pH->pData) */

        } /* if(NULL != pH) */

        /* Found B-571 curve */
        if (NULL != pX)
        {

            LacPke_GetBitPos(pX, &bit_pos, &index, &isZero);
            if (bit_pos >= NIST_GF2_Q_571_BIT_POS)
            {
                /* deg X should be less than deg modulus */
                LAC_INVALID_PARAM_LOG("X coordinate not as expected for NIST "
                                      "B571 curve");
                return status;
            }
        }
        if (NULL != pY)
        {

            LacPke_GetBitPos(pY, &bit_pos, &index, &isZero);
            if (bit_pos >= NIST_GF2_Q_571_BIT_POS)
            {
                /* deg Y should be less than deg modulus */
                LAC_INVALID_PARAM_LOG("Y coordinate not as expected for NIST "
                                      "B571 curve");
                return status;
            }
        }

        status = CPA_STATUS_SUCCESS;

    } /* else(CPA_TRUE != isZero) */

    return status;
}

CpaStatus LacEc_GetRange(Cpa32U size, Cpa32U *pMax)
{
    CpaStatus status = CPA_STATUS_SUCCESS;

    LAC_ASSERT_NOT_NULL(pMax);

    if(size <= LC_EC_SIZE_W6_IN_BYTES)
    {
        size = LC_EC_SIZE_W6_IN_BYTES;
    }
    else if(size <= LC_EC_SIZE_W7_IN_BYTES)
    {
        size = LC_EC_SIZE_W7_IN_BYTES;
    }
    else if(size <= LC_EC_SIZE_W8_IN_BYTES)
    {
        size = LC_EC_SIZE_W8_IN_BYTES;
    }
    else if(size <= LC_EC_SIZE_W9_IN_BYTES)
    {
        size = LC_EC_SIZE_W9_IN_BYTES;
    }
    else if(size <= LC_EC_SIZE_W12_IN_BYTES)
    {
        size = LC_EC_SIZE_W12_IN_BYTES;
    }
    else if(size <= LC_EC_SIZE_W13_IN_BYTES)
    {
        size = LC_EC_SIZE_W13_IN_BYTES;
    }
    else if(size <= LC_EC_SIZE_W17_IN_BYTES)
    {
        size = LC_EC_SIZE_W17_IN_BYTES;
    }
    else if(size <= LC_EC_SIZE_W18_IN_BYTES)
    {
        size = LC_EC_SIZE_W18_IN_BYTES;
    }
    else
    {
        status = CPA_STATUS_INVALID_PARAM;
    }

    *pMax = size;

    return status;

#if 0
    if (LAC_EC_SIZE_QW4_IN_BYTES >= size)
    {
        size = LAC_EC_SIZE_QW4_IN_BYTES;
    }
    else if (LAC_EC_SIZE_QW8_IN_BYTES >= size)
    {
        size = LAC_EC_SIZE_QW8_IN_BYTES;
    }
    else if (LAC_EC_SIZE_QW9_IN_BYTES >= size)
    {
        size = LAC_EC_SIZE_QW9_IN_BYTES;
    }
    else
    {
        status = CPA_STATUS_INVALID_PARAM;
    }
#endif
}
