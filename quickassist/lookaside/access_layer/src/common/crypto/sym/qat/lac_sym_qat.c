/***************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 *
 *   GPL LICENSE SUMMARY
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 *
 *   Contact Information:
 *   Intel Corporation
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/

/**
 *****************************************************************************
 * @file lac_sym_qat.c Interfaces for populating the symmetric qat structures
 *
 * @ingroup LacSymQat
 *
 *****************************************************************************/

/*
*******************************************************************************
* Include public/global header files
*******************************************************************************
*/
#include "cpa.h"

/*
*******************************************************************************
* Include private header files
*******************************************************************************
*/
#include "icp_accel_devices.h"
#include "icp_adf_cfg.h"
#include "lac_sym.h"
#include "lac_sym_qat.h"
#include "lac_sal_types_crypto.h"
#include "sal_string_parse.h"
#include "lac_sym_key.h"
#include "lac_sym_qat_hash_defs_lookup.h"
#include "lac_sym_qat_constants_table.h"
#include "lac_sym_qat_cipher.h"
#include "lac_sym_qat_hash.h"

#define EMBEDDED_CIPHER_KEY_MAX_SIZE 16
STATIC void LacSymQat_SymLogSliceHangError(icp_qat_fw_la_cmd_id_t symCmdId)
{
    Cpa8U cmdId = symCmdId;

    switch (cmdId)
    {
        case ICP_QAT_FW_LA_CMD_CIPHER:
        case ICP_QAT_FW_LA_CMD_CIPHER_PRE_COMP:
            LAC_LOG_ERROR("slice hang detected on CPM cipher slice. ");
            break;

        case ICP_QAT_FW_LA_CMD_AUTH:
        case ICP_QAT_FW_LA_CMD_AUTH_PRE_COMP:
            LAC_LOG_ERROR("slice hang detected on CPM auth slice. ");
            break;

        case ICP_QAT_FW_LA_CMD_CIPHER_HASH:
        case ICP_QAT_FW_LA_CMD_HASH_CIPHER:
        case ICP_QAT_FW_LA_CMD_SSL3_KEY_DERIVE:
        case ICP_QAT_FW_LA_CMD_TLS_V1_1_KEY_DERIVE:
        case ICP_QAT_FW_LA_CMD_TLS_V1_2_KEY_DERIVE:
        case ICP_QAT_FW_LA_CMD_MGF1:
        default:
            LAC_LOG_ERROR("slice hang detected on CPM cipher or auth slice. ");
    }
    return;
}

/* sym crypto response handlers */
STATIC
sal_qat_resp_handler_func_t respHandlerSymTbl[ICP_QAT_FW_LA_CMD_DELIMITER];

void LacSymQat_SymRespHandler(void *pRespMsg)
{
    Cpa8U lacCmdId = 0;
    void *pOpaqueData = NULL;
    icp_qat_fw_la_bulk_req_t *pRespMsgFn = NULL;
    Cpa8U opStatus = ICP_QAT_FW_COMN_STATUS_FLAG_OK;

    pRespMsgFn = (icp_qat_fw_la_bulk_req_t *)pRespMsg;
    LAC_ENSURE(pRespMsgFn != NULL,
               "LacSymQat_SymRespHandler - pRespMsgFn NULL\n");
    LAC_MEM_SHARED_READ_TO_PTR(pRespMsgFn->s.opaque_data, pOpaqueData);
    LAC_ENSURE(pOpaqueData != NULL,
               "LacSymQat_SymRespHandler - pOpaqueData NULL\n");

    lacCmdId = pRespMsgFn->s.service_cmd_id;

    /* call the response message handler registered for the command ID */
    respHandlerSymTbl[lacCmdId]((icp_qat_fw_la_cmd_id_t)lacCmdId,
                                pOpaqueData,
                                (icp_qat_fw_comn_flags)opStatus);
}

CpaStatus LacSymQat_Init(CpaInstanceHandle instanceHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;

    /* Initialize the SHRAM constants table */
    LacSymQat_ConstantsInitLookupTables(instanceHandle);

    /* Initialise the Hash lookup table */
    status = LacSymQat_HashLookupInit(instanceHandle);

    return status;
}

void LacSymQat_RespHandlerRegister(icp_qat_fw_la_cmd_id_t lacCmdId,
                                   sal_qat_resp_handler_func_t pCbHandler)
{
    LAC_ENSURE_RETURN_VOID((lacCmdId < ICP_QAT_FW_LA_CMD_DELIMITER),
                           "Invalid Command ID");

    /* set the response handler for the command ID */
    respHandlerSymTbl[lacCmdId] = pCbHandler;
}

void LacSymQat_LaPacketCommandFlagSet(icp_qat_fw_la_bulk_req_t *pReq,
                                      lac_session_desc_t *pSessionDesc,
                                      Cpa32U qatPacketType,
                                      Cpa64U srcLenInBytes)
{
    /* fill First Lsat list(bit 3~4) */
    ICP_CCAT_FW_COMN_PTR_LAST_SET(pReq->comn_hdr,
                                  CCAT_COMN_PTR_LAST_LAST);
    ICP_CCAT_FW_COMN_PTR_FIRST_SET(pReq->comn_hdr,
                                   CCAT_COMN_PTR_FIRST_FIRST);

    /* fill Init(bit21) \ Finish(bit22) */
    switch (qatPacketType)
    {
        case ICP_QAT_FW_LA_PARTIAL_START:
            ICP_CCAT_FW_COMN_PTR_INIT_SET(pReq->comn_hdr,
                                          CCAT_COMN_PTR_INIT_INIT);
            ICP_CCAT_FW_COMN_PTR_FINISH_SET(pReq->comn_hdr,
                                            CCAT_COMN_PTR_FINISH_NULL);
            pSessionDesc->srcTotalDataLenInBytes += (Cpa32U)srcLenInBytes;
            break;
        case ICP_QAT_FW_LA_PARTIAL_MID:
            ICP_CCAT_FW_COMN_PTR_INIT_SET(pReq->comn_hdr,
                                          CCAT_COMN_PTR_INIT_NULL);
            ICP_CCAT_FW_COMN_PTR_FINISH_SET(pReq->comn_hdr,
                                            CCAT_COMN_PTR_FINISH_NULL);
            pSessionDesc->srcTotalDataLenInBytes += (Cpa32U)srcLenInBytes;
            break;
        case ICP_QAT_FW_LA_PARTIAL_END:
            ICP_CCAT_FW_COMN_PTR_INIT_SET(pReq->comn_hdr,
                                          CCAT_COMN_PTR_INIT_NULL);
            ICP_CCAT_FW_COMN_PTR_FINISH_SET(pReq->comn_hdr,
                                            CCAT_COMN_PTR_FINISH_FINISH);
            pSessionDesc->srcTotalDataLenInBytes += (Cpa32U)srcLenInBytes;
            pReq->comn_hdr.header1 = pSessionDesc->srcTotalDataLenInBytes;
            break;
        case ICP_QAT_FW_LA_PARTIAL_NONE:
            ICP_CCAT_FW_COMN_PTR_INIT_SET(pReq->comn_hdr,
                                          CCAT_COMN_PTR_INIT_INIT);
            ICP_CCAT_FW_COMN_PTR_FINISH_SET(pReq->comn_hdr,
                                            CCAT_COMN_PTR_FINISH_FINISH);
            pReq->comn_hdr.header1 = srcLenInBytes;
            break;
        default:
            break;
    }

    return;
}

void LacSymQat_packetTypeGet(CpaCySymPacketType packetType,
                             CpaCySymPacketType packetState,
                             Cpa32U *pQatPacketType)
{
    /* partial */
    if (CPA_CY_SYM_PACKET_TYPE_PARTIAL == packetType)
    {
        /* if the previous state was full, then this is the first packet */
        if (CPA_CY_SYM_PACKET_TYPE_FULL == packetState)
        {
            *pQatPacketType = ICP_QAT_FW_LA_PARTIAL_START;
        }
        else
        {
            *pQatPacketType = ICP_QAT_FW_LA_PARTIAL_MID;
        }
    }
    /* final partial */
    else if (CPA_CY_SYM_PACKET_TYPE_LAST_PARTIAL == packetType)
    {
        /* if the previous state was full, then this is the first packet */
        if (CPA_CY_SYM_PACKET_TYPE_FULL == packetState)
        {
            *pQatPacketType = ICP_QAT_FW_LA_PARTIAL_NONE;
        }
        else
        {
            *pQatPacketType = ICP_QAT_FW_LA_PARTIAL_END;
        }
    }
    /* full packet - CPA_CY_SYM_PACKET_TYPE_FULL */
    else
    {
        *pQatPacketType = ICP_QAT_FW_LA_PARTIAL_NONE;
    }
}

void LacSymQat_LaSetDefaultFlags(icp_qat_fw_serv_specif_flags *laCmdFlags,
                                 CpaCySymOp symOp)
{

    ICP_QAT_FW_LA_PARTIAL_SET(*laCmdFlags, ICP_QAT_FW_LA_PARTIAL_NONE);

    ICP_QAT_FW_LA_UPDATE_STATE_SET(*laCmdFlags, ICP_QAT_FW_LA_NO_UPDATE_STATE);

    if (symOp != CPA_CY_SYM_OP_CIPHER)
    {
        ICP_QAT_FW_LA_RET_AUTH_SET(*laCmdFlags, ICP_QAT_FW_LA_RET_AUTH_RES);
    }
    else
    {
        ICP_QAT_FW_LA_RET_AUTH_SET(*laCmdFlags, ICP_QAT_FW_LA_NO_RET_AUTH_RES);
    }

    ICP_QAT_FW_LA_CMP_AUTH_SET(*laCmdFlags, ICP_QAT_FW_LA_NO_CMP_AUTH_RES);

    ICP_QAT_FW_LA_GCM_IV_LEN_FLAG_SET(*laCmdFlags,
                                      ICP_QAT_FW_LA_GCM_IV_LEN_NOT_12_OCTETS);
}

CpaBoolean LacSymQat_UseSymConstantsTable(lac_session_desc_t *pSession,
        Cpa8U *pCipherOffset,
        Cpa8U *pHashOffset)
{
    LAC_ENSURE_NOT_NULL(pSession);
    LAC_ENSURE_NOT_NULL(pCipherOffset);
    LAC_ENSURE_NOT_NULL(pHashOffset);

    if (pSession->laCmdId == ICP_QAT_FW_LA_CMD_CIPHER ||
            pSession->laCmdId == ICP_QAT_FW_LA_CMD_AUTH)
    {
        *pCipherOffset = 0;
        *pHashOffset = 0;
    }
    else
    {
        *pHashOffset = ICP_QAT_HW_CHAIN_CONTEXT_HASH_OFFSET;
    }

    return CPA_TRUE;
}

CpaBoolean LacSymQat_UseOptimisedContentDesc(lac_session_desc_t *pSession)
{
#if 0
    /* AES128-CBC-HMAC-SHA1 MODE 1*/
    if ((pSession->cipherAlgorithm == CPA_CY_SYM_CIPHER_AES_CBC &&
            pSession->cipherKeyLenInBytes <= EMBEDDED_CIPHER_KEY_MAX_SIZE) &&
            (pSession->hashAlgorithm == CPA_CY_SYM_HASH_SHA1 &&
             pSession->qatHashMode == ICP_QAT_HW_AUTH_MODE1))
    {
        return CPA_TRUE;
    }
    else
    {
        return CPA_FALSE;
    }
#endif
    return CPA_TRUE;
}
