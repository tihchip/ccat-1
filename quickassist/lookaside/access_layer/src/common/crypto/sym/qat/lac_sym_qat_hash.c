/***************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 *
 *   GPL LICENSE SUMMARY
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 *
 *   Contact Information:
 *   Intel Corporation
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/

/**
 ***************************************************************************
 * @file lac_sym_qat_hash.c
 *
 * @ingroup LacSymQatHash
 *
 * Implementation for populating QAT data structures for hash operation
 ***************************************************************************/

/*
*******************************************************************************
* Include public/global header files
*******************************************************************************
*/

#include "cpa.h"
#include "cpa_cy_sym.h"
#include "icp_accel_devices.h"
#include "icp_adf_debug.h"
#include "lac_mem.h"
#include "lac_sym.h"
#include "lac_common.h"
#include "lac_sym_qat.h"
#include "lac_list.h"
#include "lac_sal_types.h"
#include "lac_sym_qat_hash.h"
#include "lac_sym_qat_hash_defs_lookup.h"

/**
 * This structure contains pointers into the hash setup block of the
 * security descriptor. As the hash setup block contains fields that
 * are of variable length, pointers must be calculated to these fields
 * and the hash setup block is populated using these pointers. */
typedef struct lac_hash_blk_ptrs_s
{
    icp_qat_hw_auth_setup_t *pInHashSetup;
    /**< inner hash setup */
    Cpa8U *pInHashInitState1;
    /**< inner initial state 1 */
    Cpa8U *pInHashInitState2;
    /**< inner initial state 2 */
    icp_qat_hw_auth_setup_t *pOutHashSetup;
    /**< outer hash setup */
    Cpa8U *pOutHashInitState1;
    /**< outer hash initial state */
} lac_hash_blk_ptrs_t;

typedef struct lac_hash_blk_ptrs_optimised_s
{
    Cpa8U *pInHashInitState1;
    /**< inner initial state 1 */
    Cpa8U *pInHashInitState2;
    /**< inner initial state 2 */

} lac_hash_blk_ptrs_optimised_t;

/**
 * This function calculates the pointers into the hash setup block
 * based on the control block
 *
 * @param[in]  pHashControlBlock    Pointer to hash control block
 * @param[in]  pHwBlockBase         pointer to base of hardware block
 * @param[out] pHashBlkPtrs         structure containing pointers to
 *                                  various fields in the hash setup block
 *
 * @return void
 */
STATIC void LacSymQat_HashHwBlockPtrsInit(
    icp_qat_fw_auth_cd_ctrl_hdr_t *pHashControlBlock,
    void *pHwBlockBase,
    lac_hash_blk_ptrs_t *pHashBlkPtrs);

STATIC void LacSymQat_HashSetupBlockOptimisedFormatInit(
    const CpaCySymHashSetupData *pHashSetupData,
    icp_qat_fw_auth_cd_ctrl_hdr_t *pHashControlBlock,
    void *pHwBlockBase,
    icp_qat_hw_auth_mode_t qatHashMode,
    lac_sym_qat_hash_precompute_info_t *pPrecompute,
    lac_sym_qat_hash_defs_t *pHashDefs,
    lac_sym_qat_hash_defs_t *pOuterHashDefs);

/**
 * This function populates the hash setup block
 *
 * @param[in]  pHashSetupData             Pointer to the hash context
 * @param[in]  pHashControlBlock    Pointer to hash control block
 * @param[in]  pHwBlockBase         pointer to base of hardware block
 * @param[in]  qatHashMode          QAT hash mode
 * @param[in]  pPrecompute          For auth mode, this is the pointer
 *                                  to the precompute data. Otherwise this
 *                                  should be set to NULL
 * @param[in]  pHashDefs            Pointer to Hash definitions
 * @param[in]  pOuterHashDefs       Pointer to Outer Hash definitions.
 *                                  Required for nested hash mode only
 *
 * @return void
 */
#if 0
STATIC void LacSymQat_HashSetupBlockInit(
    const CpaCySymHashSetupData *pHashSetupData,
    icp_qat_fw_auth_cd_ctrl_hdr_t *pHashControlBlock,
    void *pHwBlockBase,
    icp_qat_hw_auth_mode_t qatHashMode,
    lac_sym_qat_hash_precompute_info_t *pPrecompute,
    lac_sym_qat_hash_defs_t *pHashDefs,
    lac_sym_qat_hash_defs_t *pOuterHashDefs);
#endif
STATIC void LacSymQat_HashSetupBlockInit(
    const CpaCySymHashSetupData *pHashSetupData,
    void *pHwBlockBase,
    icp_qat_hw_auth_mode_t qatHashMode,
    lac_sym_qat_hash_precompute_info_t *pPrecompute);

/** @ingroup LacSymQatHash */
void LacSymQat_HashGetCfgData(CpaInstanceHandle pInstance,
                              icp_qat_hw_auth_mode_t qatHashMode,
                              CpaCySymHashMode apiHashMode,
                              CpaCySymHashAlgorithm apiHashAlgorithm,
                              icp_qat_hw_auth_algo_t *pQatAlgorithm,
                              CpaBoolean *pQatNested)
{
    lac_sym_qat_hash_defs_t *pHashDefs = NULL;

    LAC_ENSURE_NOT_NULL(pInstance);
    LAC_ENSURE_NOT_NULL(pQatAlgorithm);
    LAC_ENSURE_NOT_NULL(pQatNested);

    LacSymQat_HashDefsLookupGet(pInstance, apiHashAlgorithm, &pHashDefs);

    LAC_ENSURE_NOT_NULL(pHashDefs);
    *pQatAlgorithm = pHashDefs->qatInfo->algoEnc;

    if (IS_HASH_MODE_2(qatHashMode))
    {
        /* set bit for nested hashing */
        *pQatNested = ICP_QAT_FW_AUTH_HDR_FLAG_DO_NESTED;
    }
    /* Nested hash in mode 0. */
    else if (CPA_CY_SYM_HASH_MODE_NESTED == apiHashMode)
    {
        /* set bit for nested hashing */
        *pQatNested = ICP_QAT_FW_AUTH_HDR_FLAG_DO_NESTED;
    }
    /* mode0 - plain or mode1 - auth */
    else
    {
        *pQatNested = ICP_QAT_FW_AUTH_HDR_FLAG_NO_NESTED;
    }
}

#if 0
void LacSymQat_HashContentDescInit(
    CpaInstanceHandle instanceHandle,
    const CpaCySymHashSetupData *pHashSetupData,
    void *pHwBlockBase,
    Cpa32U hwBlockOffsetInQuadWords,
    icp_qat_hw_auth_mode_t qatHashMode,
    lac_sym_qat_hash_precompute_info_t *pPrecompute,
    Cpa32U *pHashBlkSizeInBytes)
{
    Cpa32U hashSetupBlkSize = 0;

    LAC_ENSURE_NOT_NULL(pHashSetupData);
    LAC_ENSURE_NOT_NULL(pHwBlockBase);
    LAC_ENSURE_NOT_NULL(pHashBlkSizeInBytes);

    if (IS_HASH_MODE_0(qatHashMode))
    {
        hashSetupBlkSize = ICP_QAT_HW_CONTEXT_HASH_DIGEST_SIZEOF;
    }
    else if (IS_HASH_MODE_1(qatHashMode) || IS_HASH_MODE_2(qatHashMode))
    {
        hashSetupBlkSize = ICP_QAT_HW_CONTEXT_MAC_KEY_SIZEOF;
    }
    else
    {
        hashSetupBlkSize = 0;
    }

    *pHashBlkSizeInBytes = hashSetupBlkSize;

    /****************************************************************
     *                  Populate Hash Setup block                   *
     ****************************************************************/
    LacSymQat_HashSetupBlockInit(pHashSetupData,
                                 pHwBlockBase,
                                 qatHashMode,
                                 pPrecompute);
#ifdef ICP_DEBUG
    LAC_OSAL_LOG_PARAMS("[DEBUG][ccat] hash HwSetupContent in PSRAM, size:%u\n",
                        hashSetupBlkSize);
#endif
    return;
}
#endif

/* This fn populates fields in both the CD ctrl block and the ReqParams block
 * which describe the Hash ReqParams:
 * cd_ctrl.outer_prefix_offset
 * cd_ctrl.outer_prefix_sz
 * req_params.inner_prefix_sz/aad_sz
 * req_params.hash_state_sz
 * req_params.auth_res_sz
 *
 */
void LacSymQat_HashSetupReqParamsMetaData(
    icp_qat_la_bulk_req_ftr_t *pMsg,
    CpaInstanceHandle instanceHandle,
    const CpaCySymHashSetupData *pHashSetupData,
    CpaBoolean hashStateBuffer,
    icp_qat_hw_auth_mode_t qatHashMode,
    CpaBoolean digestVerify)
{
}

void LacSymQat_HashHwBlockPtrsInit(icp_qat_fw_auth_cd_ctrl_hdr_t *cd_ctrl,
                                   void *pHwBlockBase,
                                   lac_hash_blk_ptrs_t *pHashBlkPtrs)
{
    LAC_ENSURE_NOT_NULL(cd_ctrl);
    LAC_ENSURE_NOT_NULL(pHwBlockBase);
    LAC_ENSURE_NOT_NULL(pHashBlkPtrs);

    /* encoded offset for inner config is converted to a byte offset. */
    pHashBlkPtrs->pInHashSetup =
        (icp_qat_hw_auth_setup_t *)((Cpa8U *)pHwBlockBase +
                                    (cd_ctrl->hash_cfg_offset *
                                     LAC_QUAD_WORD_IN_BYTES));

    pHashBlkPtrs->pInHashInitState1 =
        (Cpa8U *)pHashBlkPtrs->pInHashSetup + sizeof(icp_qat_hw_auth_setup_t);

    pHashBlkPtrs->pInHashInitState2 =
        (Cpa8U *)(pHashBlkPtrs->pInHashInitState1) + cd_ctrl->inner_state1_sz;

    pHashBlkPtrs->pOutHashSetup =
        (icp_qat_hw_auth_setup_t *)((Cpa8U *)(pHashBlkPtrs->pInHashInitState2) +
                                    cd_ctrl->inner_state2_sz);

    pHashBlkPtrs->pOutHashInitState1 = (Cpa8U *)(pHashBlkPtrs->pOutHashSetup) +
                                       sizeof(icp_qat_hw_auth_setup_t);
}

STATIC void LacSymQat_HashSetupBlockInit(
    const CpaCySymHashSetupData *pHashSetupData,
    void *pHwBlockBase,
    icp_qat_hw_auth_mode_t qatHashMode,
    lac_sym_qat_hash_precompute_info_t *pPrecompute)
{
    if (IS_HASH_MODE_1(qatHashMode) || IS_HASH_MODE_2(qatHashMode))
    {
        memcpy((Cpa8U *)pHwBlockBase,
               pHashSetupData->authModeSetupData.authKey,
               pHashSetupData->authModeSetupData.authKeyLenInBytes);
        pPrecompute->pState1 = pHwBlockBase;
    }

    return;
}

void LacSymQat_HashOpHwBlockPtrsInit(
    icp_qat_fw_auth_cd_ctrl_hdr_t *cd_ctrl,
    void *pHwBlockBase,
    lac_hash_blk_ptrs_optimised_t *pHashBlkPtrs)
{
    LAC_ENSURE_NOT_NULL(cd_ctrl);
    LAC_ENSURE_NOT_NULL(pHwBlockBase);
    LAC_ENSURE_NOT_NULL(pHashBlkPtrs);
    pHashBlkPtrs->pInHashInitState1 = (((Cpa8U *)pHwBlockBase) + 16);
    pHashBlkPtrs->pInHashInitState2 =
        (Cpa8U *)(pHashBlkPtrs->pInHashInitState1) + cd_ctrl->inner_state1_sz;

    return;
}

STATIC void LacSymQat_HashSetupBlockOptimisedFormatInit(
    const CpaCySymHashSetupData *pHashSetupData,
    icp_qat_fw_auth_cd_ctrl_hdr_t *pHashControlBlock,
    void *pHwBlockBase,
    icp_qat_hw_auth_mode_t qatHashMode,
    lac_sym_qat_hash_precompute_info_t *pPrecompute,
    lac_sym_qat_hash_defs_t *pHashDefs,
    lac_sym_qat_hash_defs_t *pOuterHashDefs)
{

    Cpa32U state1PadLen = 0;
    Cpa32U state2PadLen = 0;

    lac_hash_blk_ptrs_optimised_t pHashBlkPtrs = {0};

    LacSymQat_HashOpHwBlockPtrsInit(
        pHashControlBlock, pHwBlockBase, &pHashBlkPtrs);

    if (pHashControlBlock->inner_state1_sz > pHashDefs->algInfo->stateSize)
    {
        state1PadLen =
            pHashControlBlock->inner_state1_sz - pHashDefs->algInfo->stateSize;
    }

    if (pHashControlBlock->inner_state2_sz > pHashDefs->algInfo->stateSize)
    {
        state2PadLen =
            pHashControlBlock->inner_state2_sz - pHashDefs->algInfo->stateSize;
    }

    if (state1PadLen > 0)
    {

        LAC_OS_BZERO(pHashBlkPtrs.pInHashInitState1 +
                     pHashDefs->algInfo->stateSize,
                     state1PadLen);
    }

    if (state2PadLen > 0)
    {

        LAC_OS_BZERO(pHashBlkPtrs.pInHashInitState2 +
                     pHashDefs->algInfo->stateSize,
                     state2PadLen);
    }
    pPrecompute->state1Size = pHashDefs->qatInfo->state1Length;
    pPrecompute->state2Size = pHashDefs->qatInfo->state2Length;

    /* Set the destination for pre-compute state1 data to be written */
    pPrecompute->pState1 = pHashBlkPtrs.pInHashInitState1;

    /* Set the destination for pre-compute state1 data to be written */
    pPrecompute->pState2 = pHashBlkPtrs.pInHashInitState2;
}

void LacSymQat_HashStatePrefixAadBufferSizeGet(
    icp_qat_la_bulk_req_ftr_t *pMsg,
    lac_sym_qat_hash_state_buffer_info_t *pHashStateBuf)
{
#if 0
    const icp_qat_fw_auth_cd_ctrl_hdr_t *cd_ctrl;
    icp_qat_la_auth_req_params_t *pHashReqParams;

    LAC_ENSURE_NOT_NULL(pMsg);
    LAC_ENSURE_NOT_NULL(pHashStateBuf);

    cd_ctrl = (icp_qat_fw_auth_cd_ctrl_hdr_t *) & (pMsg->cd_ctrl);
    pHashReqParams =
        (icp_qat_la_auth_req_params_t *)(&(pMsg->serv_specif_rqpars));

    LAC_ENSURE_NOT_NULL(cd_ctrl);
    LAC_ENSURE_NOT_NULL(pHashReqParams);

    /* hash state storage needed to support partial packets. Space reserved
     * for this in all cases */
    pHashStateBuf->stateStorageSzQuadWords = LAC_BYTES_TO_QUADWORDS(
                sizeof(icp_qat_hw_auth_counter_t) + cd_ctrl->inner_state1_sz);

    pHashStateBuf->prefixAadSzQuadWords = pHashReqParams->hash_state_sz;
#endif
}

void LacSymQat_HashStatePrefixAadBufferPopulate(
    lac_sym_qat_hash_state_buffer_info_t *pHashStateBuf,
    icp_qat_la_bulk_req_ftr_t *pMsg,
    Cpa8U *pInnerPrefixAad,
    Cpa8U innerPrefixSize,
    Cpa8U *pOuterPrefix,
    Cpa8U outerPrefixSize)
{
#if 0
    const icp_qat_fw_auth_cd_ctrl_hdr_t *cd_ctrl =
        (icp_qat_fw_auth_cd_ctrl_hdr_t *) & (pMsg->cd_ctrl);

    icp_qat_la_auth_req_params_t *pHashReqParams =
        (icp_qat_la_auth_req_params_t *)(&(pMsg->serv_specif_rqpars));

    LAC_ENSURE_NOT_NULL(pHashStateBuf);
    LAC_ENSURE_NOT_NULL(cd_ctrl);

    /*
     * Let S be the supplied secret
     * S1 = S/2 if S is even and (S/2 + 1) if S is odd.
     * Set length S2 (inner prefix) = S1 and the start address
     * of S2 is S[S1/2] i.e. if S is odd then S2 starts at the last byte of S1
     * _____________________________________________________________
     * |  outer prefix  |                padding                    |
     * |________________|                                           |
     * |                                                            |
     * |____________________________________________________________|
     * |  inner prefix  |                padding                    |
     * |________________|                                           |
     * |                                                            |
     * |____________________________________________________________|
     *
     */
    if (NULL != pInnerPrefixAad)
    {
        Cpa8U *pLocalInnerPrefix =
            (Cpa8U *)(pHashStateBuf->pData) +
            LAC_QUADWORDS_TO_BYTES(pHashStateBuf->stateStorageSzQuadWords);
        Cpa8U padding = pHashReqParams->u2.inner_prefix_sz - innerPrefixSize;
        /* copy the inner prefix or aad data */
        memcpy(pLocalInnerPrefix, pInnerPrefixAad, innerPrefixSize);

        /* Reset with zeroes any area reserved for padding in this block */
        if (0 < padding)
        {
            LAC_OS_BZERO(pLocalInnerPrefix + innerPrefixSize, padding);
        }
    }

    if (NULL != pOuterPrefix)
    {
        Cpa8U *pLocalOuterPrefix =
            (Cpa8U *)pHashStateBuf->pData +
            LAC_QUADWORDS_TO_BYTES(pHashStateBuf->stateStorageSzQuadWords +
                                   cd_ctrl->outer_prefix_offset);
        Cpa8U padding =
            LAC_QUADWORDS_TO_BYTES(pHashStateBuf->prefixAadSzQuadWords) -
            pHashReqParams->u2.inner_prefix_sz - outerPrefixSize;

        /* copy the outer prefix */
        memcpy(pLocalOuterPrefix, pOuterPrefix, outerPrefixSize);

        /* Reset with zeroes any area reserved for padding in this block */
        if (0 < padding)
        {
            LAC_OS_BZERO(pLocalOuterPrefix + outerPrefixSize, padding);
        }
    }
#endif
}

inline CpaStatus LacSymQat_HashRequestParamsPopulate(
    icp_qat_fw_la_bulk_req_t *pReq,
    Cpa32U authOffsetInBytes,
    Cpa32U authLenInBytes,
    sal_service_t *pService,
    lac_sym_qat_hash_state_buffer_info_t *pHashStateBuf,
    Cpa32U packetType,
    Cpa32U hashResultSize,
    CpaBoolean digestVerify,
    Cpa8U *pAuthResult,
    CpaCySymHashAlgorithm alg,
    void *pHKDFSecret)
{
#if 0
    /* FIXME */
    Cpa64U authResultPhys = 0;
    icp_qat_fw_la_auth_req_params_t *pHashReqParams;
#ifdef ICP_PARAM_CHECK
    LAC_ENSURE_NOT_NULL(pReq);
#endif

    pHashReqParams = (icp_qat_fw_la_auth_req_params_t
                      *)((Cpa8U *) & (pReq->serv_specif_rqpars) +
                         ICP_QAT_FW_HASH_REQUEST_PARAMETERS_OFFSET);

    pHashReqParams->auth_off = authOffsetInBytes;
    pHashReqParams->auth_len = authLenInBytes;

    /* Set the physical location of secret for HKDF */
    if (NULL != pHKDFSecret)
    {
        LAC_MEM_SHARED_WRITE_VIRT_TO_PHYS_PTR_EXTERNAL(
            (*pService), pHashReqParams->u1.aad_adr, pHKDFSecret);

        if (0 == pHashReqParams->u1.aad_adr)
        {
            LAC_LOG_ERROR("Unable to get the physical address of the"
                          " HKDF secret\n");
            return CPA_STATUS_FAIL;
        }
    }

    /* For a Full packet or last partial need to set the digest result pointer
     * and the auth result field */
    if (NULL != pAuthResult)
    {
        authResultPhys =
            LAC_OS_VIRT_TO_PHYS_EXTERNAL((*pService), (void *)pAuthResult);

        if (authResultPhys == 0)
        {
            LAC_LOG_ERROR("Unable to get the physical address of the"
                          " auth result\n");
            return CPA_STATUS_FAIL;
        }

        pHashReqParams->auth_res_addr = authResultPhys;
    }
    else
    {
        pHashReqParams->auth_res_addr = 0;
    }

    if (CPA_TRUE == digestVerify)
    {
        /* auth result size in bytes to be read in for a verify
         *  operation */
        pHashReqParams->auth_res_sz = (Cpa8U)hashResultSize;
    }
    else
    {
        pHashReqParams->auth_res_sz = 0;
    }

    /* If there is a hash state prefix buffer */
    if (NULL != pHashStateBuf)
    {
        /* Only write the pointer to the buffer if the size is greater than 0
         * this will be the case for plain and auth mode due to the
         * state storage required for partial packets and for nested mode (when
         * the prefix data is > 0) */
        if ((pHashStateBuf->stateStorageSzQuadWords +
                pHashStateBuf->prefixAadSzQuadWords) > 0)
        {
            /* For the first partial packet, the QAT expects the pointer to the
             * inner prefix even if there is no memory allocated for this. The
             * QAT will internally calculate where to write the state back. */
            if ((ICP_QAT_FW_LA_PARTIAL_START == packetType) ||
                    (ICP_QAT_FW_LA_PARTIAL_NONE == packetType))
            {
                // prefix_addr changed to auth_partial_st_prefix
                pHashReqParams->u1.auth_partial_st_prefix =
                    ((pHashStateBuf->pDataPhys) +
                     LAC_QUADWORDS_TO_BYTES(
                         pHashStateBuf->stateStorageSzQuadWords));
            }
            else
            {
                pHashReqParams->u1.auth_partial_st_prefix =
                    pHashStateBuf->pDataPhys;
            }
        }
        /* nested mode when the prefix data is 0 */
        else
        {
            pHashReqParams->u1.auth_partial_st_prefix = 0;
        }

        /* For middle & last partial, state size is the hash state storage
         * if hash mode 2 this will include the prefix data */
        if ((ICP_QAT_FW_LA_PARTIAL_MID == packetType) ||
                (ICP_QAT_FW_LA_PARTIAL_END == packetType))
        {
            pHashReqParams->hash_state_sz =
                (pHashStateBuf->stateStorageSzQuadWords +
                 pHashStateBuf->prefixAadSzQuadWords);
        }
        /* For full packets and first partials set the state size to that of
         * the prefix/aad. prefix includes both the inner and  outer prefix */
        else
        {
            pHashReqParams->hash_state_sz = pHashStateBuf->prefixAadSzQuadWords;
        }
    }
    else
    {
        pHashReqParams->u1.auth_partial_st_prefix = 0;
        pHashReqParams->hash_state_sz = 0;
    }

    /*  GMAC only */
    if (CPA_CY_SYM_HASH_AES_GMAC == alg)
    {
        pHashReqParams->hash_state_sz = 0;
        pHashReqParams->u1.aad_adr = 0;
    }

    /* This field is only used by TLS requests */
    /* In TLS case this is set after this function is called */
    pHashReqParams->resrvd1 = 0;
#endif
    return CPA_STATUS_SUCCESS;
}

