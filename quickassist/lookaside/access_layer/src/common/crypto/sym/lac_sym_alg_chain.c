/***************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 *
 *   GPL LICENSE SUMMARY
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 *
 *   Contact Information:
 *   Intel Corporation
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/

/**
 ***************************************************************************
 * @file lac_sym_alg_chain.c      Algorithm Chaining Perform
 *
 * @ingroup LacAlgChain
 ***************************************************************************/

/*
*******************************************************************************
* Include public/global header files
*******************************************************************************
*/
#include "cpa.h"
#include "cpa_cy_sym.h"

#include "icp_accel_devices.h"
#include "icp_adf_init.h"
#include "icp_adf_transport.h"
#include "icp_adf_debug.h"

/*
*******************************************************************************
* Include private header files
*******************************************************************************
*/

#include "lac_mem.h"
#include "lac_log.h"
#include "lac_sym.h"
#include "lac_list.h"
#include "icp_qat_fw_la.h"
#include "lac_sal_types_crypto.h"
#include "lac_sal.h"
#include "lac_sal_ctrl.h"
#include "lac_sym_alg_chain.h"
#include "lac_sym_cipher.h"
#include "lac_sym_cipher_defs.h"
#include "lac_sym_hash.h"
#include "lac_sym_hash_defs.h"
#include "lac_sym_qat_cipher.h"
#include "lac_sym_qat_hash.h"
#include "lac_sym_stats.h"
#include "lac_sym_queue.h"
#include "lac_sym_cb.h"
#include "sal_string_parse.h"
#include "lac_sym_auth_enc.h"
#include "lac_sym_qat.h"
#ifdef ICP_TRACE
#include "lac_dissect_desc.h"
#endif
#include "qae_flushCache.h"

/* Cipher block maximal size for session update purpose */
#define CCM_GCM_CIPHER_MAX_BLOCK_SIZE                                          \
    (ICP_QAT_HW_AES_256_KEY_SZ + sizeof(icp_qat_hw_cipher_config_t))

/* Session writer timeout */
#define LOCK_SESSION_WRITER_TIMEOUT 1000

static inline void LacAlgChain_LockSessionReader(
    lac_session_desc_t *pSessionDesc)
{
    /* Session lock in TRAD API */
    if (!pSessionDesc->isDPSession)
    {
        LAC_LOCK_MUTEX(&pSessionDesc->accessLock, OSAL_WAIT_FOREVER);

        pSessionDesc->accessReaders++;

        LAC_UNLOCK_MUTEX(&pSessionDesc->accessLock);
    }
}

static inline void LacAlgChain_UnlockSessionReader(
    lac_session_desc_t *pSessionDesc)
{
    /* Session lock in TRAD API */
    if (!pSessionDesc->isDPSession)
    {
        LAC_LOCK_MUTEX(&pSessionDesc->accessLock, OSAL_WAIT_FOREVER);

        pSessionDesc->accessReaders--;

        LAC_UNLOCK_MUTEX(&pSessionDesc->accessLock);
    }
}

static inline CpaStatus LacAlgChain_LockSessionWriter(
    lac_session_desc_t *pSessionDesc)
{
    CpaStatus status = CPA_STATUS_SUCCESS;

    if (!pSessionDesc->isDPSession)
    {
        /* Session lock in TRAD API */
        if (CPA_STATUS_SUCCESS != LAC_LOCK_MUTEX(&pSessionDesc->accessLock,
                LOCK_SESSION_WRITER_TIMEOUT))
        {
            status = CPA_STATUS_RETRY;
        }

        if (CPA_STATUS_SUCCESS == status)
        {
            if (pSessionDesc->accessReaders ||
                    osalAtomicGet(&(pSessionDesc->u.pendingCbCount)) > 0)
            {
                status = CPA_STATUS_RETRY;
                LAC_UNLOCK_MUTEX(&pSessionDesc->accessLock);
            }
        }
    }
    else
    {
        /* DP API */
        if (pSessionDesc->u.pendingDpCbCount > 0)
        {
            status = CPA_STATUS_RETRY;
        }
    }

    return status;
}

static inline void LacAlgChain_UnlockSessionWriter(
    lac_session_desc_t *pSessionDesc)
{
    /* Session lock in TRAD API */
    if (!pSessionDesc->isDPSession)
    {
        LAC_UNLOCK_MUTEX(&pSessionDesc->accessLock);
    }
}

/**
 * @ingroup LacAlgChain
 * This callback function will be invoked whenever a hash precompute
 * operation completes.  It will dequeue and send any QAT requests
 * which were queued up while the precompute was in progress.
 *
 * @param[in] callbackTag  Opaque value provided by user. This will
 *                         be a pointer to the session descriptor.
 *
 * @retval
 *     None
 *
 */
STATIC void LacSymAlgChain_HashPrecomputeDoneCb(void *callbackTag)
{
    LacSymCb_PendingReqsDequeue((lac_session_desc_t *)callbackTag);
}

/**
 * @ingroup LacAlgChain
 * Walk the buffer list and find the address for the given offset within
 * a buffer.
 *
 * @param[in] pBufferList   Buffer List
 * @param[in] packetOffset  Offset in the buffer list for which address
 *                          is to be found.
 * @param[out] ppDataPtr    This is where the sought pointer will be put
 * @param[out] pSpaceLeft   Pointer to a variable in which information about
 *                          available space from the given offset to the end
 *                          of the flat buffer it is located in will be returned
 *
 * @retval CPA_STATUS_SUCCESS Address with a given offset is found in the list
 * @retval CPA_STATUS_FAIL    Address with a given offset not found in the list.
 *
 */
STATIC CpaStatus
LacSymAlgChain_PtrFromOffsetGet(const CpaBufferList *pBufferList,
                                const Cpa32U packetOffset,
                                Cpa8U **ppDataPtr)
{
    Cpa32U currentOffset = 0;
    Cpa32U i = 0;

    for (i = 0; i < pBufferList->numBuffers; i++)
    {
        Cpa8U *pCurrData = pBufferList->pBuffers[i].pData;
        Cpa32U currDataSize = pBufferList->pBuffers[i].dataLenInBytes;

        /* If the offset is within the address space of the current buffer */
        if ((packetOffset >= currentOffset) &&
                (packetOffset < (currentOffset + currDataSize)))
        {
            /* increment by offset of the address in the current buffer */
            *ppDataPtr = pCurrData + (packetOffset - currentOffset);
            return CPA_STATUS_SUCCESS;
        }

        /* Increment by the size of the buffer */
        currentOffset += currDataSize;
    }

    return CPA_STATUS_FAIL;
}

#if 0
static void LacAlgChain_CipherCDBuild_ForOptimisedCD(
    const CpaCySymCipherSetupData *pCipherData,
    lac_session_desc_t *pSessionDesc,
    icp_qat_fw_slice_t nextSlice,
    Cpa8U cipherOffsetInConstantsTable,
    Cpa8U *pOptimisedHwBlockBaseInDRAM,
    Cpa32U *pOptimisedHwBlockOffsetInDRAM)
{
    Cpa8U *pCipherKeyField = NULL;
    Cpa32U sizeInBytes = 0;
    pCipherKeyField = pOptimisedHwBlockBaseInDRAM;

    /* Need to build up the alternative CD for SHRAM Constants Table use with
     * an optimised content desc of 64 bytes for this case.
     * Cipher key will be in the Content desc in DRAM, The cipher config data
     * is now in the SHRAM constants table. */

    LacSymQat_CipherHwBlockPopulateKeySetup(pCipherData,
                                            pCipherData->cipherKeyLenInBytes,
                                            pCipherKeyField,
                                            &sizeInBytes);

    LacSymQat_CipherCtrlBlockWrite(&(pSessionDesc->shramReqCacheFtr),
                                   pSessionDesc->cipherAlgorithm,
                                   pSessionDesc->cipherKeyLenInBytes,
                                   nextSlice,
                                   cipherOffsetInConstantsTable);

    *pOptimisedHwBlockOffsetInDRAM += sizeInBytes;
}

static void LacAlgChain_CipherCDBuild_ForSHRAM(
    const CpaCySymCipherSetupData *pCipherData,
    lac_session_desc_t *pSessionDesc,
    icp_qat_fw_slice_t nextSlice,
    Cpa8U cipherOffsetInConstantsTable)
{
    Cpa32U sizeInBytes = 0;
    Cpa8U *pCipherKeyField = NULL;
    /* Need to build up the alternative CD for SHRAM Constants Table use
     * Cipher key will be in the Request, The cipher config data is now in the
     * SHRAM constants table. And nothing is now stored in the content desc */
    pCipherKeyField = (Cpa8U *) & (
                          pSessionDesc->shramReqCacheHdr.cd_pars.s1.serv_specif_fields);

    LacSymQat_CipherHwBlockPopulateKeySetup(pCipherData,
                                            pCipherData->cipherKeyLenInBytes,
                                            pCipherKeyField,
                                            &sizeInBytes);

    LacSymQat_CipherCtrlBlockWrite(&(pSessionDesc->shramReqCacheFtr),
                                   pSessionDesc->cipherAlgorithm,
                                   pSessionDesc->cipherKeyLenInBytes,
                                   nextSlice,
                                   cipherOffsetInConstantsTable);
}
#endif

static void LacAlgChain_CipherCDBuild(
    const CpaCySymCipherSetupData *pCipherData,
    lac_session_desc_t *pSessionDesc,
    Cpa8U *pHwBlockBaseInDRAM)
{
    Cpa8U *pCipherKeyField = NULL;
    Cpa32U keyLen = 0;
    Cpa32U offSet = 0;

    LAC_ENSURE_NOT_NULL(pHwBlockBaseInDRAM);

    /* if xts mode, need process key len */
    if (LAC_CIPHER_IS_XTS_MODE(pCipherData->cipherAlgorithm))
    {
        keyLen = pCipherData->cipherKeyLenInBytes / 2;
    }
    else
    {
        keyLen = pCipherData->cipherKeyLenInBytes;
    }

    /* part1: sym key */
    if (LAC_CIPHER_IS_ECB_MODE(pCipherData->cipherAlgorithm) ||
            LAC_CIPHER_IS_CBC_MODE(pCipherData->cipherAlgorithm) ||
            LAC_CIPHER_IS_CFB_MODE(pCipherData->cipherAlgorithm) ||
            LAC_CIPHER_IS_OFB_MODE(pCipherData->cipherAlgorithm) ||
            LAC_CIPHER_IS_CTR_MODE(pCipherData->cipherAlgorithm) ||
            LAC_CIPHER_IS_XTS_MODE(pCipherData->cipherAlgorithm))
    {
        pCipherKeyField = pHwBlockBaseInDRAM + offSet;
        memcpy(pCipherKeyField, pCipherData->pCipherKey, keyLen);
    }
    offSet += ICP_QAT_HW_CIPHER_CONTEXT_KEY_SIZE;

    /* part2: Step_size / XTS_key */
    if (LAC_CIPHER_IS_XTS_MODE(pCipherData->cipherAlgorithm))
    {
        pCipherKeyField = pHwBlockBaseInDRAM + offSet;
        memcpy(pCipherKeyField, pCipherData->pCipherKey + keyLen, keyLen);
    }
    else if (LAC_CIPHER_IS_CTR_MODE(pCipherData->cipherAlgorithm))
    {
        pCipherKeyField = pHwBlockBaseInDRAM + offSet;
        pCipherKeyField[0] = 0x01; /* step size */
    }
    offSet += ICP_QAT_HW_CIPHER_CONTEXT_KEY_SIZE;

    return;
}

void LacAlgChain_HashCDBuild(
    const CpaCySymHashSetupData *pHashData,
    CpaInstanceHandle instanceHandle,
    lac_session_desc_t *pSessionDesc,
    Cpa8U hashOffsetInConstantsTable,
    lac_sym_qat_hash_precompute_info_t *pPrecomputeData,
    Cpa8U *pHwBlockBaseInDRAM)
{
    Cpa8U *pMacKeyField = NULL;
    icp_qat_hw_auth_mode_t qatHashMode;

    LAC_ENSURE_NOT_NULL(pHwBlockBaseInDRAM);

    qatHashMode = pSessionDesc->qatHashMode;
    pMacKeyField = pHwBlockBaseInDRAM + hashOffsetInConstantsTable;

    if (IS_HASH_MODE_1(qatHashMode) || IS_HASH_MODE_2(qatHashMode))
    {
        memcpy((Cpa8U *)pMacKeyField,
               pHashData->authModeSetupData.authKey,
               pHashData->authModeSetupData.authKeyLenInBytes);
        pPrecomputeData->pState1 = pMacKeyField;
    }

    return;
}

static void buildCmdData(lac_session_desc_t *pSessionDesc,
                         CpaCySymAlgChainOrder *chainOrder)
{
    switch (pSessionDesc->symOperation)
    {
        case CPA_CY_SYM_OP_CIPHER:
            pSessionDesc->laCmdId = ICP_QAT_FW_LA_CMD_CIPHER;
            break;

        case CPA_CY_SYM_OP_HASH:
            pSessionDesc->laCmdId = ICP_QAT_FW_LA_CMD_AUTH;
            break;

        case CPA_CY_SYM_OP_ALGORITHM_CHAINING:
            if (LAC_CIPHER_IS_CCM(pSessionDesc->cipherAlgorithm))
            {
                /* Derive chainOrder from direction for isAuthEncryptOp
                 * cases */
                /* For CCM & GCM modes: force digest verify flag _TRUE
                   for decrypt and _FALSE for encrypt. For all other cases
                   use user defined value */

                if (CPA_CY_SYM_CIPHER_DIRECTION_ENCRYPT ==
                        pSessionDesc->cipherDirection)
                {
                    pSessionDesc->digestVerify = CPA_FALSE;
                }
                else
                {
                    pSessionDesc->digestVerify = CPA_TRUE;
                }
                *chainOrder = CPA_CY_SYM_ALG_CHAIN_ORDER_HASH_THEN_CIPHER;
            }
            else if (LAC_CIPHER_IS_GCM(pSessionDesc->cipherAlgorithm))
            {
                /* Derive chainOrder from direction for isAuthEncryptOp
                 * cases */
                /* For CCM & GCM modes: force digest verify flag _TRUE
                   for decrypt and _FALSE for encrypt. For all other cases
                   use user defined value */

                if (CPA_CY_SYM_CIPHER_DIRECTION_ENCRYPT ==
                        pSessionDesc->cipherDirection)
                {
                    pSessionDesc->digestVerify = CPA_FALSE;
                }
                else
                {
                    pSessionDesc->digestVerify = CPA_TRUE;
                }
                *chainOrder = CPA_CY_SYM_ALG_CHAIN_ORDER_CIPHER_THEN_HASH;
            }
            else
            {
                pSessionDesc->isAuthEncryptOp = CPA_FALSE;
            }

            if (CPA_CY_SYM_ALG_CHAIN_ORDER_CIPHER_THEN_HASH == *chainOrder)
            {
                pSessionDesc->laCmdId = ICP_QAT_FW_LA_CMD_CIPHER_HASH;
            }
            else if (CPA_CY_SYM_ALG_CHAIN_ORDER_HASH_THEN_CIPHER == *chainOrder)
            {
                pSessionDesc->laCmdId = ICP_QAT_FW_LA_CMD_HASH_CIPHER;
            }
            break;

        default:
            break;
    }

    return;
}

/** @ingroup LacAlgChain */
CpaStatus LacAlgChain_SessionUpdate(
    lac_session_desc_t *pSessionDesc,
    const CpaCySymSessionUpdateData *pSessionUpdateData)
{
    /* FIXME */
    CpaStatus status = CPA_STATUS_SUCCESS;
    return status;
}

/** @ingroup LacAlgChain */
CpaStatus LacAlgChain_SessionInit(
    const CpaInstanceHandle instanceHandle,
    const CpaCySymSessionSetupData *pSessionSetupData,
    lac_session_desc_t *pSessionDesc)
{
    CpaStatus stat, status = CPA_STATUS_SUCCESS;
    sal_qat_content_desc_info_t *pCdInfo = NULL;
    Cpa8U *pHwBlockBaseInDRAM = NULL;
    //Cpa32U hwBlockOffsetInDRAM = 0;
    Cpa8U cipherOffsetInConstantsTable = 0;
    Cpa8U hashOffsetInConstantsTable = 0;
    icp_qat_fw_la_bulk_req_t *pMsg = NULL;
    const CpaCySymCipherSetupData *pCipherData;
    const CpaCySymHashSetupData *pHashData;
    CpaCySymAlgChainOrder chainOrder = 0;
    lac_sym_qat_hash_precompute_info_t precomputeData = {0};

    LAC_ENSURE_NOT_NULL(instanceHandle);
    LAC_ENSURE_NOT_NULL(pSessionSetupData);
    LAC_ENSURE_NOT_NULL(pSessionDesc);

    pCipherData = &(pSessionSetupData->cipherSetupData);
    pHashData = &(pSessionSetupData->hashSetupData);

    /*-------------------------------------------------------------------------
     * Populate session data
     *-----------------------------------------------------------------------*/

    /* Initialise Request Queue */
    stat = LAC_SPINLOCK_INIT(&pSessionDesc->requestQueueLock);
    if (CPA_STATUS_SUCCESS != stat)
    {
        LAC_LOG_ERROR("Spinlock init failed for sessionLock");
        return CPA_STATUS_RESOURCE;
    }

    /* Initialise session readers writers */
    stat = LAC_INIT_MUTEX(&pSessionDesc->accessLock);
    if (CPA_STATUS_SUCCESS != stat)
    {
        LAC_LOG_ERROR("Mutex init failed for accessLock");
        return CPA_STATUS_RESOURCE;
    }

    pSessionDesc->pRequestQueueHead = NULL;
    pSessionDesc->pRequestQueueTail = NULL;
    pSessionDesc->nonBlockingOpsInProgress = CPA_TRUE;
    pSessionDesc->pInstance = instanceHandle;
    pSessionDesc->digestIsAppended = pSessionSetupData->digestIsAppended;
    pSessionDesc->digestVerify = pSessionSetupData->verifyDigest;

    /* Reset the pending callback counter */
    osalAtomicSet(0, &pSessionDesc->u.pendingCbCount);
    pSessionDesc->u.pendingDpCbCount = 0;
    pSessionDesc->accessReaders = 0;

    /* Partial state must be set to full, to indicate that next packet
    * expected on the session is a full packet or the start of a
    * partial packet. */
    pSessionDesc->partialState = CPA_CY_SYM_PACKET_TYPE_FULL;

    pSessionDesc->symOperation = pSessionSetupData->symOperation;

    switch (pSessionDesc->symOperation)
    {
        case CPA_CY_SYM_OP_CIPHER:
            pSessionDesc->isCipher = CPA_TRUE;
            pSessionDesc->isAuth = CPA_FALSE;
            pSessionDesc->isAuthEncryptOp = CPA_FALSE;
            break;

        case CPA_CY_SYM_OP_HASH:
            pSessionDesc->isCipher = CPA_FALSE;
            pSessionDesc->isAuth = CPA_TRUE;
            pSessionDesc->isAuthEncryptOp = CPA_FALSE;
            break;

        case CPA_CY_SYM_OP_ALGORITHM_CHAINING:
            pSessionDesc->isCipher = CPA_TRUE;
            pSessionDesc->isAuth = CPA_TRUE;

            {
                /* set up some useful shortcuts */
                CpaCySymCipherAlgorithm cipherAlgorithm =
                    pSessionSetupData->cipherSetupData.cipherAlgorithm;

                if (LAC_CIPHER_IS_CCM(cipherAlgorithm))
                {
                    pSessionDesc->isAuthEncryptOp = CPA_TRUE;
                    //pSessionDesc->digestIsAppended = CPA_TRUE;
                }
                else if (LAC_CIPHER_IS_GCM(cipherAlgorithm))
                {
                    pSessionDesc->isAuthEncryptOp = CPA_TRUE;
                }
                else
                {
                    pSessionDesc->isAuthEncryptOp = CPA_TRUE;
                    /* Use the chainOrder passed in */
                    chainOrder = pSessionSetupData->algChainOrder;
#ifdef ICP_PARAM_CHECK
                    if ((chainOrder != CPA_CY_SYM_ALG_CHAIN_ORDER_HASH_THEN_CIPHER)
                            && (chainOrder != CPA_CY_SYM_ALG_CHAIN_ORDER_CIPHER_THEN_HASH))
                    {
                        LAC_INVALID_PARAM_LOG("algChainOrder");
                        return CPA_STATUS_INVALID_PARAM;
                    }
#endif
                }
            }
            break;

        default:
            break;
    }

    if (pSessionDesc->isCipher)
    {
        /* Populate cipher specific session data */
#ifdef ICP_PARAM_CHECK
        status = LacCipher_SessionSetupDataCheck(pCipherData);
#endif
        if (CPA_STATUS_SUCCESS == status)
        {
            pSessionDesc->cipherAlgorithm = pCipherData->cipherAlgorithm;
            pSessionDesc->cipherKeyLenInBytes = pCipherData->cipherKeyLenInBytes;
            pSessionDesc->cipherDirection = pCipherData->cipherDirection;
        }
    }

    if ((CPA_STATUS_SUCCESS == status) && pSessionDesc->isAuth)
    {
        /* Populate auth-specific session data */
        const CpaCySymHashSetupData *pHashData =
            &pSessionSetupData->hashSetupData;

//#ifdef ICP_PARAM_CHECK
//        status = LacHash_HashContextCheck(instanceHandle, pHashData);
//#endif
        if (CPA_STATUS_SUCCESS == status)
        {
            pSessionDesc->hashResultSize = pHashData->digestResultLenInBytes;
            pSessionDesc->hashMode = pHashData->hashMode;
            pSessionDesc->hashAlgorithm = pHashData->hashAlgorithm;

            /* Save the authentication key length for further update */
            if (CPA_CY_SYM_HASH_MODE_AUTH == pHashData->hashMode)
            {
                pSessionDesc->authKeyLenInBytes =
                    pHashData->authModeSetupData.authKeyLenInBytes;
            }

            if (CPA_TRUE == pSessionDesc->isAuthEncryptOp)
            {
                pSessionDesc->aadLenInBytes =
                    pHashData->authModeSetupData.aadLenInBytes;
            }

            /* Set the QAT hash mode */
            if ((pHashData->hashMode == CPA_CY_SYM_HASH_MODE_NESTED) ||
                    (pHashData->hashMode == CPA_CY_SYM_HASH_MODE_PLAIN) ||
                    (pHashData->hashMode == CPA_CY_SYM_HASH_MODE_AUTH &&
                     pHashData->hashAlgorithm == CPA_CY_SYM_HASH_ZUC_EIA3))
            {
                pSessionDesc->qatHashMode = ICP_QAT_HW_AUTH_MODE0;
            }
            else /* CPA_CY_SYM_HASH_MODE_AUTH
                    && anything except CPA_CY_SYM_HASH_ZUC_EIA3  */
            {
                if (CPA_CY_SYM_HASH_AES_CBC_MAC == pHashData->hashAlgorithm ||
                        CPA_CY_SYM_HASH_SM4_CMAC == pHashData->hashAlgorithm)
                {
                    pSessionDesc->qatHashMode = ICP_QAT_HW_AUTH_MODE2;
                }
                else if (IS_HMAC_ALG(pHashData->hashAlgorithm))
                {
                    pSessionDesc->qatHashMode = ICP_QAT_HW_AUTH_MODE1;
                }
                else
                {
                    pSessionDesc->qatHashMode = ICP_QAT_HW_AUTH_MODE_DELIMITER;
                }
            }
#ifdef ICP_TRACE
            char *str_mode = NULL;
            switch (pSessionDesc->qatHashMode)
            {
                case ICP_QAT_HW_AUTH_MODE0:
                    str_mode = "mode0, NESTED | PLAIN | ZUC_EIA3";
                    break;
                case ICP_QAT_HW_AUTH_MODE1:
                    str_mode = "mode1, AUTH && HMAC";
                    break;
                case ICP_QAT_HW_AUTH_MODE2:
                    str_mode = "mode2, AUTH && CMAC";
                    break;
                default:
                    str_mode = "DELIMITER";
                    break;
            }
            LAC_OSAL_LOG_PARAMS("[DEBUG][ccat] "
                                "set inter hash mode: %s\n", str_mode);
#endif
        }
    }

    /*-------------------------------------------------------------------------
     * build the message templates
     * create two content descriptors in the case we can support using SHRAM
     * constants and an optimised content descriptor. we have to do this in case
     * of partials.
     * 64 byte content desciptor is used in the SHRAM case for AES-128-HMAC-SHA1
     *------------------------------------------------------------------------*/
    if (CPA_STATUS_SUCCESS == status)
    {
        /* Build configuration data */
        buildCmdData(pSessionDesc, &chainOrder);

        LacSymQat_UseSymConstantsTable(pSessionDesc,
                                       &cipherOffsetInConstantsTable,
                                       &hashOffsetInConstantsTable);

        /* setup some convenience pointers */
        pCdInfo = &(pSessionDesc->contentDescInfo);
        pHwBlockBaseInDRAM = (Cpa8U *)pCdInfo->pData;
        //hwBlockOffsetInDRAM = 0;

        switch (pSessionDesc->symOperation)
        {
            case CPA_CY_SYM_OP_CIPHER:
                LacAlgChain_CipherCDBuild(pCipherData,
                                          pSessionDesc,
                                          pHwBlockBaseInDRAM);
                break;

            case CPA_CY_SYM_OP_HASH:
                LacAlgChain_HashCDBuild(pHashData,
                                        instanceHandle,
                                        pSessionDesc,
                                        hashOffsetInConstantsTable,
                                        &precomputeData,
                                        pHwBlockBaseInDRAM);
                break;

            case CPA_CY_SYM_OP_ALGORITHM_CHAINING:
                LacAlgChain_CipherCDBuild(pCipherData,
                                          pSessionDesc,
                                          pHwBlockBaseInDRAM);

                LacAlgChain_HashCDBuild(pHashData,
                                        instanceHandle,
                                        pSessionDesc,
                                        hashOffsetInConstantsTable,
                                        &precomputeData,
                                        pHwBlockBaseInDRAM);
                break;

            default:
                LAC_LOG_ERROR("Invalid sym operation\n");
                status = CPA_STATUS_INVALID_PARAM;
                break;
        }
    }

    if ((CPA_STATUS_SUCCESS == status) && pSessionDesc->isAuth)
    {
        if (IS_HASH_MODE_1(pSessionDesc->qatHashMode))
        {
#ifdef ICP_TRACE
            LAC_OSAL_LOG_PARAMS("[DEBUG][ccat] block messages "
                                "until precompute is completed\n");
#endif
            /* Block messages until precompute is completed */
            //pSessionDesc->nonBlockingOpsInProgress = CPA_FALSE;
            status = LacHash_PrecomputeDataCreate(
                         instanceHandle,
                         (CpaCySymSessionSetupData *)pSessionSetupData,
                         LacSymAlgChain_HashPrecomputeDoneCb,
                         pSessionDesc,
                         precomputeData.pState1,
                         precomputeData.pState2);
        }
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /* Configure the Hdr and ContentDescriptor field
         * in the request if not done already */
        //pCdInfo->hwBlkSzQuadWords = LAC_BYTES_TO_QUADWORDS(hwBlockOffsetInDRAM);
        pMsg = (icp_qat_fw_la_bulk_req_t *) & (pSessionDesc->reqCache);
        SalQatMsg_CmnHdrWrite(pMsg, pSessionSetupData);
        SalQatMsg_CmnContentWrite(pMsg, pCdInfo);
    }

    return status;
}

/** @ingroup LacAlgChain */
CpaStatus LacAlgChain_Perform(const CpaInstanceHandle instanceHandle,
                              lac_session_desc_t *pSessionDesc,
                              void *pCallbackTag,
                              const CpaCySymOpData *pOpData,
                              const CpaBufferList *pSrcBuffer,
                              CpaBufferList *pDstBuffer,
                              CpaBoolean *pVerifyResult)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    sal_crypto_service_t *pService = (sal_crypto_service_t *)instanceHandle;
    lac_sym_bulk_cookie_t *pCookie = NULL;
    lac_sym_cookie_t *pSymCookie = NULL;
    icp_qat_fw_la_bulk_req_t *pMsg = NULL;
    Cpa8U *pMsgDummy = NULL;
    Cpa8U *pMsgCtxDummy = NULL;
    CpaPhysicalAddr physAddress = 0;
    CpaPhysicalAddr physAddressAligned = 0;
    Cpa8U *pCacheDummy = NULL;
    Cpa32U qatPacketType = 0;
    Cpa64U srcAddrPhys = 0;
    Cpa64U dstAddrPhys = 0;
    icp_qat_fw_la_cmd_id_t laCmdId;
#ifdef ICP_PARAM_CHECK
    Cpa64U srcPktSize = 0;
#endif
    Cpa64U srcTotalDataLenInBytes = 0;

    LAC_ENSURE_NOT_NULL(pSessionDesc);
    LAC_ENSURE_NOT_NULL(pOpData);

    LacAlgChain_LockSessionReader(pSessionDesc);

    /* Set the command id */
    laCmdId = pSessionDesc->laCmdId;

    if (CPA_TRUE == pSessionDesc->isAuthEncryptOp)
    {
        if (CPA_CY_SYM_HASH_AES_CCM == pSessionDesc->hashAlgorithm)
        {
#ifdef ICP_PARAM_CHECK
            status =
                LacSymAlgChain_CheckCCMData(pOpData->pAdditionalAuthData,
                                            pOpData->pIv,
                                            pOpData->messageLenToCipherInBytes,
                                            pOpData->ivLenInBytes);
#endif
//            if (CPA_STATUS_SUCCESS == status)
//            {
//                LacSymAlgChain_PrepareCCMData(
//                    pSessionDesc,
//                    pOpData->pAdditionalAuthData,
//                    pOpData->pIv,
//                    pOpData->messageLenToCipherInBytes,
//                    pOpData->ivLenInBytes);
//            }
        }
        else if (CPA_CY_SYM_HASH_AES_GCM == pSessionDesc->hashAlgorithm)
        {
#ifdef ICP_PARAM_CHECK
            if (pSessionDesc->aadLenInBytes != 0 &&
                    pOpData->pAdditionalAuthData == NULL)
            {
                LAC_INVALID_PARAM_LOG("pAdditionalAuthData");
                status = CPA_STATUS_INVALID_PARAM;
            }
#endif
            if (CPA_STATUS_SUCCESS == status)
            {
                LacSymAlgChain_PrepareGCMData(pSessionDesc,
                                              pOpData->pAdditionalAuthData);
            }
        }
    }

    /* allocate cookie (used by callback function) */
    if (CPA_STATUS_SUCCESS == status)
    {
        do
        {
            pSymCookie = (lac_sym_cookie_t *)Lac_MemPoolEntryAlloc(
                             pService->lac_sym_cookie_pool);
            if (pSymCookie == NULL)
            {
                LAC_LOG_ERROR("Cannot allocate cookie - NULL");
                status = CPA_STATUS_RESOURCE;
            }
            else if ((void *)CPA_STATUS_RETRY == pSymCookie)
            {
#ifdef ICP_NONBLOCKING_PARTIALS_PERFORM
                status = CPA_STATUS_RETRY;
                break;
#else
                osalYield();
#endif
            }
            else
            {
                pCookie = &(pSymCookie->u.bulkCookie);
            }
        }
        while ((void *)CPA_STATUS_RETRY == pSymCookie);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /* write the buffer descriptors */
        status = LacBuffDesc_BufferListDescWriteAndGetSize(
                     (CpaBufferList *)pSrcBuffer,
                     &srcAddrPhys,
                     CPA_FALSE,
                     &srcTotalDataLenInBytes,
                     &(pService->generic_service_info));
        if (CPA_STATUS_SUCCESS != status)
        {
            LAC_LOG_ERROR("Unable to write src buffer descriptors");
        }

        /* For out of place operations */
        if (CPA_STATUS_SUCCESS == status)
        {
            status = LacBuffDesc_BufferListDescWrite(
                         pDstBuffer,
                         &dstAddrPhys,
                         CPA_FALSE,
                         &(pService->generic_service_info));
            if (CPA_STATUS_SUCCESS != status)
            {
                LAC_LOG_ERROR("Unable to write dest buffer descriptors");
            }
        }
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /* populate the cookie */
        pCookie->pCallbackTag = pCallbackTag;
        pCookie->sessionCtx = pOpData->sessionCtx;
        pCookie->pOpData = (const CpaCySymOpData *)pOpData;
        pCookie->pDstBuffer = pDstBuffer;
        pCookie->updateSessionIvOnSend = CPA_FALSE;
        pCookie->updateUserIvOnRecieve = CPA_FALSE;
        pCookie->updateKeySizeOnRecieve = CPA_FALSE;
        pCookie->pNext = NULL;
        pCookie->instanceHandle = pService;

        /* get the qat packet type for LAC packet type */
        LacSymQat_packetTypeGet(
            pOpData->packetType, pSessionDesc->partialState, &qatPacketType);
        /*
         * For XTS mode, the key size must be updated after
         * the first partial has been sent. Set a flag here so the
         * response knows to do this.
         */
        if ((laCmdId != ICP_QAT_FW_LA_CMD_AUTH) &&
                (CPA_CY_SYM_PACKET_TYPE_PARTIAL == pOpData->packetType) &&
                (LAC_CIPHER_IS_XTS_MODE(pSessionDesc->cipherAlgorithm)) &&
                (qatPacketType == ICP_QAT_FW_LA_PARTIAL_START))
        {
            pCookie->updateKeySizeOnRecieve = CPA_TRUE;
        }

        /*
         * Now create the Request.
         * Start by populating it from the cache in the session descriptor.
         */
        pMsg = &(pCookie->qatMsg);
        pMsgDummy = (Cpa8U *)pMsg;

        /* Normally, we want to use the SHRAM Constants Table if possible
         * for best performance (less DRAM accesses incurred by CPM).  But
         * we can't use it for partial-packet hash operations.  This is why
         * we build 2 versions of the message template at sessionInit,
         * one for SHRAM Constants Table usage and the other (default) for
         * Content Descriptor h/w setup data in DRAM.  And we chose between
         * them here on a per-request basis, when we know the packetType
         */
        pCacheDummy = (Cpa8U *) & (pSessionDesc->reqCache);

        osalMemCopy(pMsgDummy,
                    pCacheDummy,
                    (LAC_LONG_WORD_IN_BYTES * LAC_SIZE_OF_CACHE_REQ_IN_LW));

        /* FIXME, Populate the comn_mid section */
        SalQatMsg_CmnMidWrite(pMsg,
                              pCookie,
                              CCAT_COMN_PTR_ADDR_TYPE_SGL,
                              srcAddrPhys,
                              dstAddrPhys,
                              0,
                              0);
        /* Populate the software reserved section */
        SalQatMsg_CmnSoftwareReserved(pMsg, pCookie, laCmdId);

        /* FIXME, Populate the serv_specif_flags field of the Request header
         * Some of the flags are set up here.
         * Others are set up later when the RequestParams are set up. */
        LacSymQat_LaPacketCommandFlagSet(pMsg,
                                         pSessionDesc,
                                         qatPacketType,
                                         srcTotalDataLenInBytes);
        /* FIXME */
#ifdef ICP_PARAM_CHECK
        LacBuffDesc_BufferListTotalSizeGet(pSrcBuffer, &srcPktSize);
#endif

        /*
         * Populate the CipherRequestParams section of the Request
         */
        if (laCmdId != ICP_QAT_FW_LA_CMD_AUTH)
        {
            Cpa8U *pIvBuffer = NULL;

#ifdef ICP_PARAM_CHECK
            status = LacCipher_PerformParamCheck(
                         pSessionDesc->cipherAlgorithm, pOpData, srcPktSize);
            if (CPA_STATUS_SUCCESS != status)
            {
                /* free the cookie */
                if ((NULL != pCookie) &&
                        (((void *)CPA_STATUS_RETRY) != pCookie))
                {
                    Lac_MemPoolEntryFree(pCookie);
                }

                /* Unlock session on error */
                LacAlgChain_UnlockSessionReader(pSessionDesc);

                return status;
            }
#endif

	        if(ICP_CCAT_FW_COMN_PTR_LAST_GET(pMsg->comn_hdr) &&
	            ICP_CCAT_FW_COMN_PTR_FIRST_GET(pMsg->comn_hdr))
	        {
	            /* Setup content descriptor info structure
	            * assumption that content descriptor is the first field in
	            * in the session descriptor */
	            pMsgCtxDummy = (Cpa8U *) pCookie->qatContext;
	            physAddress = LAC_OS_VIRT_TO_PHYS_EXTERNAL(
	                            (*pService), (Cpa8U *)pMsgCtxDummy);
	            if (0 == physAddress)
	            {
	                LAC_LOG_ERROR("Unable to get the physical address of the pool context\n");
	                return CPA_STATUS_FAIL;
	            }
	            physAddressAligned =
	                LAC_ALIGN_POW2_ROUNDUP(physAddress, LAC_64BYTE_ALIGNMENT);

	            pCacheDummy = (Cpa8U *) pSessionDesc->contentDescInfo.pData;
	            pMsgCtxDummy += physAddressAligned - physAddress;
                osalMemCopy(pMsgCtxDummy, pCacheDummy, 512);

	            pSessionDesc->contentDescInfo.pData = pMsgCtxDummy;
	            pSessionDesc->contentDescInfo.hardwareSetupBlockPhys = physAddressAligned;

	            pMsg->comn_context.context_addr = pSessionDesc->contentDescInfo.hardwareSetupBlockPhys;
	            pMsg->comn_context.context_addr &= 0x0000FFFFFFFFFFFFULL;
	        }

            if (CPA_STATUS_SUCCESS == status)
            {
                /* align cipher IV */
                status =
                    LacCipher_PerformIvCheck(&(pService->generic_service_info),
                                             pCookie,
                                             qatPacketType,
                                             &pIvBuffer);
            }
            /* populate the cipher request parameters */
            if (CPA_STATUS_SUCCESS == status)
            {
                status = LacSymQat_CipherRequestParamsPopulate(pSessionDesc,
                         qatPacketType,
                         pIvBuffer,
                         pOpData->ivLenInBytes,
                         pOpData->pAdditionalAuthData,
                         pOpData->padLen,
                         &(pService->generic_service_info));
            }
        }

        /*
         * Set up HashRequestParams part of Request
         */
        if ((status == CPA_STATUS_SUCCESS) &&
                (laCmdId != ICP_QAT_FW_LA_CMD_CIPHER))
        {
#ifdef ICP_PARAM_CHECK
            status = LacHash_PerformParamCheck(instanceHandle,
                                               pSessionDesc,
                                               pOpData,
                                               srcPktSize,
                                               pVerifyResult);
            if (CPA_STATUS_SUCCESS != status)
            {
                /* free the cookie */
                if ((NULL != pCookie) &&
                        (((void *)CPA_STATUS_RETRY) != pCookie))
                {
                    Lac_MemPoolEntryFree(pCookie);
                }

                /* Unlock session on error */
                LacAlgChain_UnlockSessionReader(pSessionDesc);
                return status;
            }
#endif
            if (CPA_STATUS_SUCCESS == status)
            {
#if 0
                status = LacSymQat_HashRequestParamsPopulate(
                             pSessionDesc,
                             qatPacketType,
                             0,
                             0,
                             pSessionDesc->aadLenInBytes * 8,
                             pOpData->pAdditionalAuthData)
#endif
            }

        }
    }

    /* Increase pending callbacks before unlocking session */
    if (CPA_STATUS_SUCCESS == status)
    {
        osalAtomicInc(&(pSessionDesc->u.pendingCbCount));
    }
    LacAlgChain_UnlockSessionReader(pSessionDesc);

#ifdef ICP_TRACE
    LacDissectDescriptors(pMsg, pSessionDesc,
                          (CpaBufferList *)pSrcBuffer,
                          (CpaBufferList *)pDstBuffer);
#endif

    /* Flush desc context(256bytes, 16 Cachelines) */
    qaeFlushDCacheLines((LAC_ARCH_UINT)(pSessionDesc->contentDescInfo.pData), 512);

    /*
     * send the message to the QAT
     */
    if (CPA_STATUS_SUCCESS == status)
    {
        status = LacSymQueue_RequestSend(instanceHandle, pCookie, pSessionDesc);
        if (CPA_STATUS_SUCCESS != status)
        {
            /* Decrease pending callback counter on send fail. */
            osalAtomicDec(&(pSessionDesc->u.pendingCbCount));
        }
#ifdef ICP_TRACE
        else
        {
            LAC_OSAL_LOG_PARAMS("[DEBUG][ccat] session:%lu perform - "
                                "send the message to the ccat:%s\n",
                                pSessionDesc->timestamp,
                                (status == CPA_STATUS_SUCCESS) ?
                                "success" : "fail");
        }
#endif
    }
    /* Case that will catch all error status's for this function */
    if (CPA_STATUS_SUCCESS != status)
    {
        /* free the cookie */
        if (NULL != pSymCookie)
        {
            Lac_MemPoolEntryFree(pSymCookie);
        }
    }

    return status;
}

