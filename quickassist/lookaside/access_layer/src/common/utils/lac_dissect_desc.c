/***************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 *
 *   GPL LICENSE SUMMARY
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 *
 *   Contact Information:
 *   Intel Corporation
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/

/**
 *****************************************************************************
 * @file lac_dissect_desc.c  Utility functions for dissect descriptors
 *
 * @ingroup LacDissectDesc
 *
 *****************************************************************************/

// #include <stdio.h>

#include "cpa.h"
#include "Osal.h"
#include "lac_session.h"
#include "icp_qat_fw_la.h"
#ifndef ICP_DC_DYN_NOT_SUPPORTED
#include "icp_buffer_desc.h"
#endif

typedef enum
{
    S_TYPE_UNKNOWN = 0,
    S_TYPE_SYM,
    S_TYPE_PK,
    S_TYPE_CTRL,
} service_type;

#define GET_ADDR_LIST_ADDRESSES(data)                                          \
    (((icp_buffer_list_desc_t)(data)) & 0x0000FFFFFFFFFFFFULL)
#define GET_ADDR_LIST_COUNT(data)                                              \
    (((icp_buffer_list_desc_t)(data) & 0xFFFF000000000000ULL) >> 48)

static const char *ServiceType2str[] =
{
    "cipher mode",
    "hash mode",
    "auth mode",
    "aead mode",
    "ZUC mode",
    "reserve",
    "IPsec mode",
    "Bypass mode"
};

static const char *CipherAlg2str[] =
{
    "NULL",
    "SM4",
    "SM7",
    "AES128",
    "AES192",
    "AES256",
    "DES",
    "3DES",
    "BASE64",
    "ZUC_EEA3"
};

static const char *CipherMode2str[] =
{
    "NULL",
    "ECB",
    "CBC",
    "CFB",
    "OFB",
    "CTR",
    "XTS",
    "GCM",
    "CCM"
};

static const char *HashAlg2str[] =
{
    "NULL",
    "SM3",
    "MD5",
    "SHA1",
    "SHA224",
    "SHA256",
    "SHA384",
    "SHA512",
    "GHASH"
};

static const char *AuthMode2str[] =
{
    "NULL",
    "HMAC",
    "CMAC",
    "XCBC_MAC",
    "GMAC",
    "GCM",
    "CCM",
    "ZUC_EIA"
};

static const char *AeadMode2str[] =
{
    "NULL",
    "ETM",
    "MTE"
};

static const char *AeadTag2str[] =
{
    "16",
    "12",
    "8",
    "4"
};

static const char *Operation2str[] =
{
    "Encrypt",
    "Decrypt"
};

static const char *CtxCtrl2str[] =
{
    "External CTX, Not Save, Writeback",
    "External CTX, Save, Not Writeback",
    "Internal CTX, Save, Not Writeback",
    "Internal CTX, Not Save, Writeback"
};

static const char *KeyMode2str[] =
{
    "External plaintext key, saved in context",
    "External ciphertext key, saved in context",
    "Use internal key, key ID saved in the context"
};

static const char *LaCmd2str[] =
{
    "Cipher Request",
    "Auth Request",
    "Cipher-Hash Request",
    "Hash-Cipher Request",
    "TRNG Get Random Request",
    "TRNG Test Request",
    "SSL3 Key Derivation Request",
    "TLS Key Derivation Request",
    "TLS Key Derivation Request",
    "MGF1 Request",
    "Auth Pre-Compute Request",
    "Auth Pre-Compute Request",
    "HKDF Extract Request",
    "HKDF Expand Request",
    "HKDF Extract and Expand Request",
    "HKDF Expand Label Request",
    "HKDF Extract and Expand Label Request",
    "Delimiter type"
};

static void LacDissectDescriptors_Header(icp_qat_fw_la_bulk_req_t *pMsg)
{
#define HEADER_FMT "    field: %-16s    filled: %s\n"

    LAC_OSAL_LOG_PARAMS("Common Request Header\n");

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "ServiceType",
           ServiceType2str[
               ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_GET(pMsg->comn_hdr)]);

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "Last",
           ICP_CCAT_FW_COMN_PTR_LAST_GET(pMsg->comn_hdr) ? "true" : "false");
    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "First",
           ICP_CCAT_FW_COMN_PTR_FIRST_GET(pMsg->comn_hdr) ? "true" : "false");

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "AddrList",
           ICP_CCAT_FW_COMN_PTR_SGL_TYPE_GET(pMsg->comn_hdr) ? "list" : "addr");

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "CipherAlg",
           CipherAlg2str[
               ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_GET(pMsg->comn_hdr)]);
    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "CipherMode",
           CipherMode2str[
               ICP_CCAT_FW_COMN_PTR_CIPHER_MODE_GET(pMsg->comn_hdr)]);

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "HashAlg",
           HashAlg2str[
               ICP_CCAT_FW_COMN_PTR_HASH_ALG_GET(pMsg->comn_hdr)]);
    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "AuthMode",
           AuthMode2str[
               ICP_CCAT_FW_COMN_PTR_AUTH_MODE_GET(pMsg->comn_hdr)]);

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "Init",
           ICP_CCAT_FW_COMN_PTR_INIT_GET(pMsg->comn_hdr) ? "true" : "false");
    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "Finish",
           ICP_CCAT_FW_COMN_PTR_FINISH_GET(pMsg->comn_hdr) ? "true" : "false");

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "AeadMode",
           AeadMode2str[
               ICP_CCAT_FW_COMN_PTR_AEAD_MODE_GET(pMsg->comn_hdr)]);
    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "AeadTag",
           AeadTag2str[
               ICP_CCAT_FW_COMN_PTR_AEAD_TAG_GET(pMsg->comn_hdr)]);
    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "Operation",
           Operation2str[
               ICP_CCAT_FW_COMN_PTR_OPERATION_GET(pMsg->comn_hdr)]);

    LAC_OSAL_LOG_PARAMS(HEADER_FMT, "CtxCtrl",
           CtxCtrl2str[
               ICP_CCAT_FW_COMN_PTR_CTX_CTRL_GET(pMsg->comn_hdr)]);

    LAC_OSAL_LOG_PARAMS(HEADER_FMT,  "KeyMode",
           KeyMode2str[
               ICP_CCAT_FW_COMN_PTR_KEY_MODE_GET(pMsg->comn_hdr)]);

    return;

#undef HEADER_FMT
}

static void LacDissectDescriptors_Mid(icp_qat_fw_la_bulk_req_t *pMsg,
                                      const CpaBufferList *pSrcBuffer,
                                      const CpaBufferList *pDstBuffer)
{
    int i;
    int addr;

    LAC_OSAL_LOG_PARAMS("\nCommon Request Plaintext\n");

    addr = ICP_CCAT_FW_COMN_PTR_SGL_TYPE_GET(pMsg->comn_hdr);
    switch (addr)
    {
        case CCAT_COMN_PTR_ADDR_TYPE_FLAT:
            break;
        case CCAT_COMN_PTR_ADDR_TYPE_SGL:
            for (i = 0; i < pSrcBuffer->numBuffers; i++)
            {
                LAC_OSAL_LOG_PARAMS("    src[%d]:                    %u Bytes\n",
                       i, pSrcBuffer[i].pBuffers->dataLenInBytes);
                LAC_OSAL_LOG_PARAMS("    dst[%d]:                    %u Bytes\n",
                       i, pDstBuffer[i].pBuffers->dataLenInBytes);
            }

            if (pSrcBuffer->numBuffers == 0)
            {
                LAC_OSAL_LOG_PARAMS("    src_data = %lx\n", pMsg->comn_mid.src_data);
                LAC_OSAL_LOG_PARAMS("    dst_data = %lx\n", pMsg->comn_mid.dst_data);
            }
            break;
        default:
            osalLog(OSAL_LOG_LVL_ERROR,
                    OSAL_LOG_DEV_STDERR,
                    "does not support this addr type\n");
            break;
    }

    return;
}

static void LacDissectDescriptors_Context(icp_qat_fw_la_bulk_req_t *pMsg,
        lac_session_desc_t *pSessionDesc)
{
    LAC_OSAL_LOG_PARAMS("\nCommon Request Context Section\n");

    if (NULL != pSessionDesc->contentDescInfo.pData &&
            LAC_SYM_QAT_CONTENT_DESC_MAX_SIZE > 0)
    {
//        osalHexdump("    ",
//                    pSessionDesc->contentDescInfo.pData,
//                    LAC_SYM_QAT_CONTENT_DESC_MAX_SIZE);
    }

    return;
}

static void LacDissectDescriptors_SoftwareResrvd(icp_qat_fw_la_bulk_req_t *pMsg)
{
    LAC_OSAL_LOG_PARAMS("\nCommon Request Software Resrvd\n");
    LAC_OSAL_LOG_PARAMS("    field: %-16s    filled: 0x%lx\n", "OpaqueData",
           pMsg->s.opaque_data);
    LAC_OSAL_LOG_PARAMS("    field: %-16s    filled: %s\n", "CmdID",
           LaCmd2str[pMsg->s.service_cmd_id]);
    return;
}

void LacDissectDescriptors(icp_qat_fw_la_bulk_req_t *pMsg,
                           lac_session_desc_t *pSessionDesc,
                           const CpaBufferList *pSrcBuffer,
                           const CpaBufferList *pDstBuffer)
{
    LAC_OSAL_LOG_PARAMS("[DEBUG][ccat] "
           "session:%lu print request descriptors\n", pSessionDesc->timestamp);
//    osalHexdump("", pMsg, sizeof(icp_qat_fw_la_bulk_req_t));
    LAC_OSAL_LOG_PARAMS("\n");

    LacDissectDescriptors_Header(pMsg);
    LacDissectDescriptors_Mid(pMsg, pSrcBuffer, pDstBuffer);
    LacDissectDescriptors_Context(pMsg, pSessionDesc);
    LacDissectDescriptors_SoftwareResrvd(pMsg);

    return;
}

