/******************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 *
 *   GPL LICENSE SUMMARY
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 *
 *   Contact Information:
 *   Intel Corporation
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 *****************************************************************************/

/**
 *****************************************************************************
 * @file sal_qat_cmn_msg.h
 *
 * @defgroup SalQatCmnMessage
 *
 * @ingroup SalQatCmnMessage
 *
 * Interfaces for populating the common QAT structures for a lookaside
 * operation.
 *
 *****************************************************************************/

/*
******************************************************************************
* Include public/global header files
******************************************************************************
*/
#include "cpa.h"

/*
*******************************************************************************
* Include private header files
*******************************************************************************
*/
#include "icp_accel_devices.h"
#include "icp_qat_fw_la.h"
#include "icp_qat_hw.h"
#include "lac_common.h"
#include "lac_mem.h"
#include "lac_log.h"
#include "sal_qat_cmn_msg.h"
#include "lac_sym_hash_defs.h"
#include "lac_sym_cipher_defs.h"

#define CMN_HASH_PLAIN(a)                                                      \
    do {                                                                       \
        ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(pMsg->comn_hdr,                  \
                                              CCAT_COMN_PTR_SERVICE_TYPE_HASH);\
        ICP_CCAT_FW_COMN_PTR_HASH_ALG_SET(pMsg->comn_hdr,(a));                 \
    } while (0)

#define CMN_CIPHER_AES(a,m)                                                    \
    do {                                                                       \
        ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(pMsg->comn_hdr,(a));               \
        ICP_CCAT_FW_COMN_PTR_CIPHER_MODE_SET(pMsg->comn_hdr,(m));              \
    } while (0)

#define CMN_CIPHER_DES(m)                                                      \
    do {                                                                       \
        ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(pMsg->comn_hdr,                    \
                                            CCAT_COMN_PTR_CIPHER_ALG_DES);     \
        ICP_CCAT_FW_COMN_PTR_CIPHER_MODE_SET(pMsg->comn_hdr,(m));              \
    } while (0)

#define CMN_CIPHER_3DES(m)                                                     \
    do {                                                                       \
        ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(pMsg->comn_hdr,                    \
                                            CCAT_COMN_PTR_CIPHER_ALG_3DES);    \
        ICP_CCAT_FW_COMN_PTR_CIPHER_MODE_SET(pMsg->comn_hdr,(m));              \
    } while (0)

#define CMN_CIPHER_SM4(m)                                                      \
    do {                                                                       \
        ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(pMsg->comn_hdr,                    \
                                            CCAT_COMN_PTR_CIPHER_ALG_SM4);     \
        ICP_CCAT_FW_COMN_PTR_CIPHER_MODE_SET(pMsg->comn_hdr,(m));              \
    } while (0)

static inline int SalQatMsg_convertAesAlg(
    const CpaCySymCipherSetupData *pCipherData)
{
    int aesAlgorithm = CCAT_COMN_PTR_CIPHER_ALG_NULL;

    if (LAC_CIPHER_IS_XTS_MODE(pCipherData->cipherAlgorithm))
    {
        switch (pCipherData->cipherKeyLenInBytes)
        {
            case ICP_QAT_HW_AES_128_XTS_KEY_SZ:
                aesAlgorithm = CCAT_COMN_PTR_CIPHER_ALG_AES128;
                break;
            case ICP_QAT_HW_AES_256_XTS_KEY_SZ:
                aesAlgorithm = CCAT_COMN_PTR_CIPHER_ALG_AES256;
                break;
        }
    }
    else
    {
        switch (pCipherData->cipherKeyLenInBytes)
        {
            case ICP_QAT_HW_AES_128_KEY_SZ:
                aesAlgorithm = CCAT_COMN_PTR_CIPHER_ALG_AES128;
                break;
            case ICP_QAT_HW_AES_192_KEY_SZ:
                aesAlgorithm = CCAT_COMN_PTR_CIPHER_ALG_AES192;
                break;
            case ICP_QAT_HW_AES_256_KEY_SZ:
                aesAlgorithm = CCAT_COMN_PTR_CIPHER_ALG_AES256;
                break;
        }
    }

    return aesAlgorithm;
}

void SalQatMsg_CmnHdrWriteCipherAlg(
    icp_qat_fw_la_bulk_req_t *pMsg,
    const CpaCySymSessionSetupData *pSessionSetupData)
{
    const CpaCySymCipherSetupData *pCipherData =
        &pSessionSetupData->cipherSetupData;

    ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(
        pMsg->comn_hdr, CCAT_COMN_PTR_SERVICE_TYPE_CIPHER);

    /* fill Cipher algorithm(bit 6~9), fill Cipher mode(bit 10~12) */
    switch (pCipherData->cipherAlgorithm)
    {
        case CPA_CY_SYM_CIPHER_AES_ECB:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_ECB);
            break;
        case CPA_CY_SYM_CIPHER_AES_CBC:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_CBC);
            break;
        case CPA_CY_SYM_CIPHER_AES_CTR:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_CTR);
            break;
        case CPA_CY_SYM_CIPHER_AES_XTS:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_XTS);
            break;
        case CPA_CY_SYM_CIPHER_AES_OFB:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_OFB);
            break;
        case CPA_CY_SYM_CIPHER_AES_CFB1:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_AES_CFB8:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_AES_CFB128:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_AES_CCM:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_CCM);
            break;
        case CPA_CY_SYM_CIPHER_AES_GCM:
            CMN_CIPHER_AES(SalQatMsg_convertAesAlg(pCipherData),
                           CCAT_COMN_PTR_CIPHER_MODE_GCM);
            break;
        case CPA_CY_SYM_CIPHER_DES_ECB:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_ECB);
            break;
        case CPA_CY_SYM_CIPHER_DES_CBC:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_CBC);
            break;
        case CPA_CY_SYM_CIPHER_DES_OFB:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_OFB);
            break;
        case CPA_CY_SYM_CIPHER_DES_CFB1:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_DES_CFB8:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_DES_CFB64:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_DES_EDE_ECB:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_ECB);
            break;
        case CPA_CY_SYM_CIPHER_DES_EDE_CBC:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_CBC);
            break;
        case CPA_CY_SYM_CIPHER_DES_EDE_OFB:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_OFB);
            break;
        case CPA_CY_SYM_CIPHER_DES_EDE_CFB64:
            CMN_CIPHER_DES(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_3DES_ECB:
            CMN_CIPHER_3DES(CCAT_COMN_PTR_CIPHER_MODE_ECB);
            break;
        case CPA_CY_SYM_CIPHER_3DES_CBC:
            CMN_CIPHER_3DES(CCAT_COMN_PTR_CIPHER_MODE_CBC);
            break;
        case CPA_CY_SYM_CIPHER_3DES_CTR:
            CMN_CIPHER_3DES(CCAT_COMN_PTR_CIPHER_MODE_CTR);
            break;
        case CPA_CY_SYM_CIPHER_3DES_OFB:
            CMN_CIPHER_3DES(CCAT_COMN_PTR_CIPHER_MODE_OFB);
            break;
        case CPA_CY_SYM_CIPHER_3DES_CFB1:
            CMN_CIPHER_3DES(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_3DES_CFB8:
            CMN_CIPHER_3DES(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_3DES_CFB64:
            CMN_CIPHER_3DES(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_SM4_ECB:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_ECB);
            break;
        case CPA_CY_SYM_CIPHER_SM4_CBC:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_CBC);
            break;
        case CPA_CY_SYM_CIPHER_SM4_CTR:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_CTR);
            break;
        case CPA_CY_SYM_CIPHER_SM4_OFB:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_OFB);
            break;
        case CPA_CY_SYM_CIPHER_SM4_CFB:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_CFB);
            break;
        case CPA_CY_SYM_CIPHER_SM4_XTS:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_XTS);
            break;
        case CPA_CY_SYM_CIPHER_SM4_CCM:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_CCM);
            break;
        case CPA_CY_SYM_CIPHER_SM4_GCM:
            CMN_CIPHER_SM4(CCAT_COMN_PTR_CIPHER_MODE_GCM);
            break;
        case CPA_CY_SYM_CIPHER_ZUC_EEA3:
            ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_CIPHER_ALG_ZUC_EEA3);
            break;
        default:
            ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_CIPHER_ALG_NULL);
            break;
    }

    /* fill operation(bit27) (1 - encrypt \0 - decrypt) */
    switch (pCipherData->cipherDirection)
    {
        case CPA_CY_SYM_CIPHER_DIRECTION_ENCRYPT:
            ICP_CCAT_FW_COMN_PTR_OPERATION_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_OPERATION_ENCRYPT);
            break;
        case CPA_CY_SYM_CIPHER_DIRECTION_DECRYPT:
            ICP_CCAT_FW_COMN_PTR_OPERATION_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_OPERATION_DECRYPT);
    }

    return;
}

void SalQatMsg_CmnHdrWriteHashPlainAlg(
    icp_qat_fw_la_bulk_req_t *pMsg,
    const CpaCySymSessionSetupData *pSessionSetupData)
{
    const CpaCySymHashSetupData *pHashData =
        &pSessionSetupData->hashSetupData;

    /* fill Hash algorithm(bit 13~16) */
    switch (pHashData->hashAlgorithm)
    {
        case CPA_CY_SYM_HASH_SM3:
            CMN_HASH_PLAIN(CCAT_COMN_PTR_HASH_ALG_SM3);
            break;
        case CPA_CY_SYM_HASH_MD5:
            CMN_HASH_PLAIN(CCAT_COMN_PTR_HASH_ALG_MD5);
            break;
        case CPA_CY_SYM_HASH_SHA1:
            CMN_HASH_PLAIN(CCAT_COMN_PTR_HASH_ALG_SHA1);
            break;
        case CPA_CY_SYM_HASH_SHA224:
            CMN_HASH_PLAIN(CCAT_COMN_PTR_HASH_ALG_SHA224);
            break;
        case CPA_CY_SYM_HASH_SHA256:
            CMN_HASH_PLAIN(CCAT_COMN_PTR_HASH_ALG_SHA256);
            break;
        case CPA_CY_SYM_HASH_SHA384:
            CMN_HASH_PLAIN(CCAT_COMN_PTR_HASH_ALG_SHA384);
            break;
        case CPA_CY_SYM_HASH_SHA512:
            CMN_HASH_PLAIN(CCAT_COMN_PTR_HASH_ALG_SHA512);
            break;
        default:
            ICP_CCAT_FW_COMN_PTR_HASH_ALG_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_HASH_ALG_NULL);
            break;
    }

    return;
}

void SalQatMsg_CmnHdrWriteHashAlg(
    icp_qat_fw_la_bulk_req_t *pMsg,
    const CpaCySymSessionSetupData *pSessionSetupData)
{
    const CpaCySymHashSetupData *pHashData =
        &pSessionSetupData->hashSetupData;

    switch (pHashData->hashMode)
    {
        case CPA_CY_SYM_HASH_MODE_NESTED:
        case CPA_CY_SYM_HASH_MODE_PLAIN:
            SalQatMsg_CmnHdrWriteHashPlainAlg(pMsg, pSessionSetupData);
            break;

        case CPA_CY_SYM_HASH_MODE_AUTH:
            if (IS_HMAC_ALG(pHashData->hashAlgorithm))
            {
                SalQatMsg_CmnHdrWriteHashPlainAlg(pMsg, pSessionSetupData);
                ICP_CCAT_FW_COMN_PTR_AUTH_MODE_SET(
                    pMsg->comn_hdr,
                    CCAT_COMN_PTR_AUTH_MODE_HMAC);
                ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(
                    pMsg->comn_hdr,
                    CCAT_COMN_PTR_SERVICE_TYPE_AUTH);
            }
            else if (/*pHashData->hashMode == CPA_CY_SYM_HASH_MODE_AUTH && */
                pHashData->hashAlgorithm == CPA_CY_SYM_HASH_AES_CBC_MAC)
            {
                //SalQatMsg_CmnHdrWriteCipherAlg(pMsg, pSessionSetupData);
                if (pHashData->authModeSetupData.authKeyLenInBytes == 32)
                {
                    ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(
                        pMsg->comn_hdr,
                        CCAT_COMN_PTR_CIPHER_ALG_AES256);
                }
                else
                {
                    ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(
                        pMsg->comn_hdr,
                        CCAT_COMN_PTR_CIPHER_ALG_AES128);
                }
                ICP_CCAT_FW_COMN_PTR_AUTH_MODE_SET(
                    pMsg->comn_hdr,
                    CCAT_COMN_PTR_AUTH_MODE_CBC_MAC);
                ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(
                    pMsg->comn_hdr,
                    CCAT_COMN_PTR_SERVICE_TYPE_AUTH);
            }
            else if (pHashData->hashAlgorithm == CPA_CY_SYM_HASH_SM4_CMAC)
            {
                ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(
                    pMsg->comn_hdr,
                    CCAT_COMN_PTR_CIPHER_ALG_SM4);
                ICP_CCAT_FW_COMN_PTR_AUTH_MODE_SET(
                    pMsg->comn_hdr,
                    CCAT_COMN_PTR_AUTH_MODE_CBC_MAC);
                ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(
                    pMsg->comn_hdr,
                    CCAT_COMN_PTR_SERVICE_TYPE_AUTH);
            }
            break;

        default:
            break;
    }

    return;
}

/*******************************************************************************
 * @ingroup SalQatMsg_CmnHdrWrite
 *
 * @description
 *      This function fills in all fields in the icp_qat_fw_comn_req_hdr_t
 *      section of the Request Msg. Build LW0 + LW1 -
 *      service part of the request
 *
 * @param[in]   pMsg                    Pointer to 32Bytes Request Msg buffer
 * @param[in]   pSessionSetupData       Type of CpaCySymSessionSetupData
 *
 * @return
 *      None
 *
 * @Note
 *      The SessionSetupData and descriptor do not match exactly,
 *      so new logic needs to be established
 ******************************************************************************/
void SalQatMsg_CmnHdrWrite(icp_qat_fw_la_bulk_req_t *pMsg,
                           const CpaCySymSessionSetupData *pSessionSetupData)
{
    /* fill ServiceType(bit 0~2)
     * fill Cipher algorithm(bit 6~9)
     * fill Cipher mode(bit 10~13)
     * fill Hash algorithm(bit 14~17)
     * fill Auth mode(bit 18~20) */
    switch (pSessionSetupData->symOperation)
    {
        case CPA_CY_SYM_OP_CIPHER:
            SalQatMsg_CmnHdrWriteCipherAlg(pMsg, pSessionSetupData);
            break;
        case CPA_CY_SYM_OP_HASH:
            /* Populate auth-specific session data */
            SalQatMsg_CmnHdrWriteHashAlg(pMsg, pSessionSetupData);
            break;
        case CPA_CY_SYM_OP_ALGORITHM_CHAINING:
            SalQatMsg_CmnHdrWriteCipherAlg(pMsg, pSessionSetupData);
            SalQatMsg_CmnHdrWriteHashAlg(pMsg, pSessionSetupData);
            ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_SERVICE_TYPE_AEAD);
            break;
        default:
            ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_SERVICE_TYPE_BYPASS);
            break;
    }

    /* fill aead mode (bit 23~24) */
    switch (pSessionSetupData->algChainOrder)
    {
        case CPA_CY_SYM_ALG_CHAIN_ORDER_HASH_THEN_CIPHER:
            ICP_CCAT_FW_COMN_PTR_AEAD_MODE_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_AEAD_MODE_MTE);
            break;
        case CPA_CY_SYM_ALG_CHAIN_ORDER_CIPHER_THEN_HASH:
            ICP_CCAT_FW_COMN_PTR_AEAD_MODE_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_AEAD_MODE_ETM);
            break;
        default:
            ICP_CCAT_FW_COMN_PTR_AEAD_MODE_SET(
                pMsg->comn_hdr,
                CCAT_COMN_PTR_AEAD_MODE_NULL);
            break;
    }

    /* fill tag_len (bit 25~26) */
    if (LAC_CIPHER_IS_CCM(pSessionSetupData->cipherSetupData.cipherAlgorithm))
    {
        switch (pSessionSetupData->hashSetupData.digestResultLenInBytes)
        {
            case 4:  /* 4bytes */
                ICP_CCAT_FW_COMN_PTR_AEAD_TAG_SET(pMsg->comn_hdr,
                                                  CCAT_COMN_PTR_AEAD_TAG_4);
                break;
            case 8:  /* 8bytes */
                ICP_CCAT_FW_COMN_PTR_AEAD_TAG_SET(pMsg->comn_hdr,
                                                  CCAT_COMN_PTR_AEAD_TAG_8);
                break;
            case 12: /* 12bytes */
                ICP_CCAT_FW_COMN_PTR_AEAD_TAG_SET(pMsg->comn_hdr,
                                                  CCAT_COMN_PTR_AEAD_TAG_12);
                break;
            case 16: /* 16bytes */
            default:
                ICP_CCAT_FW_COMN_PTR_AEAD_TAG_SET(pMsg->comn_hdr,
                                                  CCAT_COMN_PTR_AEAD_TAG_16);
                break;
        }
    }

    /* fill ctx_ctrl (bit 28~29) */
    ICP_CCAT_FW_COMN_PTR_CTX_CTRL_SET(
        pMsg->comn_hdr,
        CCAT_COMN_PTR_EX_CTX_NO_SAVE_WB);

    /* fill key mode(bit 30~31) */
    ICP_CCAT_FW_COMN_PTR_KEY_MODE_SET(
        pMsg->comn_hdr,
        CCAT_COMN_PTR_KEY_EXT_DECRYPT_KEY);

    return;
}

/********************************************************************
 * @ingroup SalQatCmnMessage
 *
 * @description
 *      This function fills in all fields in the icp_qat_fw_comn_req_mid_t
 *      section of the Request Msg and the corresponding SGL/Flat flag
 *      in the Hdr.
 *
 * @param[in]   pReq            Pointer to 128B Request Msg buffer
 * @param[in]   pOpaqueData     Pointer to opaque data used by callback
 * @param[in]   bufferFormat    src and dst Buffers are either SGL or Flat
 *                              format
 * @param[in]   pSrcBuffer      Address of source buffer
 * @param[in]   pDstBuffer      Address of destination buffer
 * @param[in]   pSrcLength      Length of source buffer
 * @param[in]   pDstLength      Length of destination buffer
 *

 * @assumptions
 *      All fields in mid section are zero before fn is called

 * @return
 *      None
 *
 *****************************************/
void inline SalQatMsg_CmnMidWrite(icp_qat_fw_la_bulk_req_t *pReq,
                                  const void *pOpaqueData,
                                  Cpa8U bufferFormat,
                                  Cpa64U srcBuffer,
                                  Cpa64U dstBuffer,
                                  Cpa32U srcLength,
                                  Cpa32U dstLength)
{
    icp_qat_fw_comn_req_mid_t *pMid = &(pReq->comn_mid);
    pMid->src_data = srcBuffer;

//    /* In place */
//    if (0 == dstBuffer)
//    {
//        pMid->dst_data = srcBuffer;
//    }
//    /* Out of place */
//    else
//    {
    pMid->dst_data = dstBuffer;
//    }

    /* fill AddrList(bit 5) */
    switch (bufferFormat)
    {
        case CCAT_COMN_PTR_ADDR_TYPE_SGL:
            /* Using ScatterGatherLists so set flag in header */
            ICP_CCAT_FW_COMN_PTR_SGL_TYPE_SET(
                pReq->comn_hdr, CCAT_COMN_PTR_ADDR_TYPE_SGL);

            /* Assumption: No need to set src and dest length in this case as
             * not used */
            break;
        default:
            /* Using Flat buffers so set flag in header */
            ICP_CCAT_FW_COMN_PTR_SGL_TYPE_SET(
                pReq->comn_hdr, CCAT_COMN_PTR_ADDR_TYPE_FLAT);
            break;
    }
    return;
}

/********************************************************************
 * @ingroup SalQatMsg_ContentDescHdrWrite
 *
 * @description
 *      This function fills in all fields in the
 *      icp_qat_fw_comn_req_hdr_cd_pars_t section of the Request Msg.
 *
 * @param[in]   pMsg             Pointer to 128B Request Msg buffer.
 * @param[in]   pContentDescInfo content descripter info.
 *
 * @return
 *      none
 *void SalQatMsg_ContentDescHdrWrite(
 *   icp_qat_fw_comn_req_t *pMsg,
 *   const sal_qat_content_desc_info_t *pContentDescInfo)
 *****************************************/
void SalQatMsg_CmnContentWrite(
    icp_qat_fw_la_bulk_req_t *pMsg,
    const sal_qat_content_desc_info_t *pContentDescInfo)
{
    pMsg->comn_context.context_addr = pContentDescInfo->hardwareSetupBlockPhys;
    pMsg->comn_context.context_addr &= 0x0000FFFFFFFFFFFFULL;
    return;
}

/*******************************************************************************
 * @ingroup SalQatMsg_CmnSoftwareReserved
 *
 * @description
 *      This function fills in all fields in the
 *      icp_qat_fw_la_bulk_req_s section of the software reserved Msg.
 *
 * @param[in]   pMsg            Pointer to 64B Request Msg buffer.
 * @param[in]   pOpaqueData
 * @param[in]   ServiceCmdId
 *
 * @return
 *      none
 *****************************************/
void SalQatMsg_CmnSoftwareReserved(icp_qat_fw_la_bulk_req_t *pMsg,
                                   const void *pOpaqueData,
                                   Cpa64U ServiceCmdId)
{
    LAC_MEM_SHARED_WRITE_FROM_PTR(pMsg->s.opaque_data, pOpaqueData);
    pMsg->s.service_cmd_id = ServiceCmdId;
    return;
}

/********************************************************************
 * @ingroup SalQatMsg_CtrlBlkSetToReserved
 *
 * @description
 *      This function sets the whole control block to a reserved state.
 *
 * @param[in]   _pMsg            Pointer to 128B Request Msg buffer.
 *
 * @return
 *      none
 *
 *****************************************/
void SalQatMsg_CtrlBlkSetToReserved(icp_qat_fw_comn_req_t *pMsg)
{

    icp_qat_fw_comn_req_cd_ctrl_t *pCd_ctrl = &(pMsg->cd_ctrl);

    osalMemSet(pCd_ctrl, 0, sizeof(icp_qat_fw_comn_req_cd_ctrl_t));
}

/********************************************************************
 * @ingroup SalQatMsg_transPutMsg
 *
 * @description
 *
 *
 * @param[in]   trans_handle
 * @param[in]   pqat_msg
 * @param[in]   size_in_lws
 * @param[in]   service
 *
 * @return
 *      CpaStatus
 *
 *****************************************/
CpaStatus SalQatMsg_transPutMsg(icp_comms_trans_handle trans_handle,
                                void *pqat_msg,
                                Cpa32U size_in_lws,
                                Cpa8U service,
                                Cpa64U *seq_num)
{
    return icp_adf_transPutMsg(trans_handle, pqat_msg, size_in_lws, seq_num);
}

void SalQatMsg_updateQueueTail(icp_comms_trans_handle trans_handle)
{
    icp_adf_updateQueueTail(trans_handle);
}
