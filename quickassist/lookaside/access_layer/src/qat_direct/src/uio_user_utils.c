/***************************************************************************
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <libudev.h>

#include "cpa.h"
#include "uio_user_utils.h"
#include "icp_platform.h"
#include "uio_user_cfg.h"

//static const char *BDF_STRING = "%04x:%02x:%02x.%1x";

CpaStatus uio_udev_get_device_from_devid(int devid, struct udev_device **dev)
{
    struct udev *udev;
    char sysname[32];

    if (CPA_STATUS_SUCCESS !=
        icp_adf_cfgGetDeviceName(devid, sysname, sizeof(sysname))
       ) {
        ADF_DEBUG("error reading the device name of devid %d\n", devid);
        return CPA_STATUS_FAIL;
    }

    udev = udev_new();
    if (!udev) {
        ADF_ERROR("udev_new failed\n");
        return CPA_STATUS_FAIL;
    }

    *dev = udev_device_new_from_subsystem_sysname(udev, "platform", sysname);
    if (*dev) {
        const char *accelId =
            udev_device_get_sysattr_value(*dev, UDEV_ATTRIBUTE_ACCELID);
#if 0
        ADF_DEBUG("new udev device from devid\n"
                  "    devid: %d\n"
                  "    subsystem: %s\n"
                  "    sysname: %s\n"
                  "    syspath: %s\n",
                  devid,
                  subsystem,
                  sysname,
                  udev_device_get_syspath(*dev));
#endif
        if (accelId) {
            return CPA_STATUS_SUCCESS;
        } else {
            ADF_DEBUG(
                "uio_udev_get_device_from_devid: no %s sysattr for devid %d\n",
                UDEV_ATTRIBUTE_ACCELID,
                devid);
            udev_device_unref(*dev);
            *dev = NULL;
        }
    } else {
        ADF_ERROR(
            "udev_device_new_from_subsystem_sysname failed for "
            "sysname %s, error %d:%s\n",
            sysname, errno, strerror(errno));
    }

    udev_unref(udev);
    return CPA_STATUS_FAIL;
}

CpaStatus uio_udev_free_device(struct udev_device *dev)
{
    struct udev *udev = NULL;

    if (!dev) {
        ADF_ERROR("uio_udev_free_device fail, dev == NULL\n");
        return CPA_STATUS_FAIL;
    }
#if 0
    ADF_DEBUG("free udev device\n"
              "    subsystem: %s\n"
              "    sysname: %s\n"
              "    syspath: %s\n",
              udev_device_get_subsystem(dev),
              udev_device_get_sysname(dev),
              udev_device_get_syspath(dev));
#endif
    udev = udev_device_get_udev(dev);
    udev_device_unref(dev);
    udev_unref(udev);
    return CPA_STATUS_SUCCESS;
}

static CpaStatus uio_udev_read_str_ap(struct udev_device *dev,
                                      char *value,
                                      unsigned size,
                                      const char *attribute,
                                      va_list ap)
{
    char attribute_name[128];
    const char *attribute_value;

    vsnprintf(attribute_name, sizeof(attribute_name), attribute, ap);

    attribute_value = udev_device_get_sysattr_value(dev, attribute_name);

    if (!attribute_value)
        return CPA_STATUS_FAIL;

    snprintf(value, size, "%s", attribute_value);

#if 0
    ADF_DEBUG("read attribute from udev device\n"
              "    syspath: %s\n"
              "    attribute: %s\n"
              "    value: %s\n",
              udev_device_get_syspath(dev),
              attribute_name,
              attribute_value);
#endif
    return CPA_STATUS_SUCCESS;
}

CpaStatus uio_udev_read_str(struct udev_device *dev,
                            char *value,
                            unsigned size,
                            const char *attribute,
                            ...)
{
    CpaStatus status;
    va_list ap;

    va_start(ap, attribute);
    status = uio_udev_read_str_ap(dev, value, size, attribute, ap);
    va_end(ap);

    return status;
}

CpaStatus uio_udev_read_long(struct udev_device *dev,
                             unsigned long *value,
                             const char *attribute,
                             ...)
{
    CpaStatus status;
    char attribute_value[32];
    va_list ap;

    va_start(ap, attribute);
    status = uio_udev_read_str_ap(
                 dev, attribute_value, sizeof(attribute_value), attribute, ap);
    va_end(ap);

    if (status == CPA_STATUS_SUCCESS)
        *value = strtol(attribute_value, NULL, 0);

    return status;
}

CpaStatus uio_udev_read_uint(struct udev_device *dev,
                             unsigned int *value,
                             const char *attribute,
                             ...)
{
    CpaStatus status;
    long long_value;
    char attribute_string[32];
    va_list ap;

    va_start(ap, attribute);
    status = uio_udev_read_str_ap(
                 dev, attribute_string, sizeof(attribute_string), attribute, ap);
    va_end(ap);

    if (status == CPA_STATUS_SUCCESS) {
        long_value = strtol(attribute_string, NULL, 0);
        *value = (unsigned int)long_value;
    }

    return status;
}
