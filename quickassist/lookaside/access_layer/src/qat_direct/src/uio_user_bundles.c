/***************************************************************************
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>
#include <pthread.h>

#include "cpa.h"
#include "icp_platform.h"

#include "uio_user.h"
#include "adf_kernel_types.h"
#include "adf_cfg_user.h"
#include "uio_user_cfg.h"

#ifdef ICP_DEBUG
#include "lac_common.h"
#include "adf_platform_common.h"
#include "adf_platform_acceldev_common.h"

static inline __attribute__((always_inline))
int uio_ring_show(uint32_t ring_num,
                  uint32_t *csr_base_addr,
                  char *p, int room)
{
    return snprintf(p, room, "    ring%d:\n"
                    "        Bus width:             32\n"
                    "        Big/Little endian:     little endian\n"
                    "        Base virtaddr:         %p\n"
                    "        reg name               offset    value\n"
                    "            queue ctrl         0x00      0x%08x\n"
                    "            queue IE           0x04      0x%08x\n"
                    "            queue ISR          0x08      0x%08x\n"
                    "            queue LSR          0x0c      0x%08x\n"
                    "            interrupt ctrl     0x10      0x%08x\n"
                    "            descriptor ctrl    0x14      0x%08x\n"
                    "            ring base low      0x20      0x%08x\n"
                    "            ring base high     0x24      0x%08x\n"
                    "            ring header        0x28      0x%08x\n"
                    "            ring tail          0x2c      0x%08x\n"
                    "            buffer ctrl        0x30      0x%08x\n"
                    "            tph ctrl           0x34      0x%08x\n"
                    "            func id            0x38      0x%08x\n",
                    ring_num,
                    (void *)csr_base_addr,
                    READ_CSR_RING_QUEUE_CTRL,
                    READ_CSR_RING_QUEUE_IE,
                    READ_CSR_RING_QUEUE_ISR,
                    READ_CSR_RING_QUEUE_LSR,
                    READ_CSR_RING_INTC,
                    READ_CSR_RING_DESC_CTRL,
                    READ_CSR_RING_BASE_L,
                    READ_CSR_RING_BASE_H,
                    READ_CSR_RING_HEADER_INDEX,
                    READ_CSR_RING_TAIL_INDEX,
                    READ_CSR_RING_BUFFER_CTRL,
                    READ_CSR_RING_TPH_CTRL,
                    READ_CSR_RING_FUNC_ID);
}

static inline __attribute__((always_inline))
void uio_bundle_show(const char *dname,
                     struct adf_uio_user_bundle *bundle,
                     int bundle_nr,
                     Cpa32U maxNumRingsPerBank)
{
    int i;
    int lx = 0;
    char show[4096];

    lx += snprintf(show + lx, sizeof(show) - lx,
                   "mapping %s to hw bundle #bank%d\n",
                   dname, bundle_nr);

    for (i = 0; i < maxNumRingsPerBank; i++)
    {
        lx += uio_ring_show(i, (bundle->ptr)[i],
                            show + lx, sizeof(show) - lx);
    }

    ADF_DEBUG("%s", show);
    return;
}
#endif

static inline void *uio_map_bundle_ptr(struct adf_uio_user_bundle *bundle,
                                       int map_index)
{
    unsigned long size;
    void *bundle_ptr;

    if (CPA_STATUS_SUCCESS != uio_udev_read_long(bundle->udev_dev,
            &size,
            UDEV_ATTRIBUTE_MAP_SIZE,
            bundle->device_minor,
            map_index))
        return NULL;

    bundle_ptr = ICP_MMAP(NULL,
                          size,
                          PROT_READ | PROT_WRITE,
                          MAP_SHARED,
                          bundle->fd,
                          map_index * getpagesize());

    madvise(bundle_ptr, size, MADV_DONTFORK);
    if (bundle_ptr == MAP_FAILED)
    {
        ADF_ERROR("Mapping[%d] error - %s\n", map_index, strerror(errno));
        return NULL;
    }

    return (void *)((unsigned char *)bundle_ptr);
}

static void uio_free_bundle_ptr(void *bundle_ptr, unsigned long size)
{
    ICP_MUNMAP(bundle_ptr, size);
}

/**
 * This function is using 'udev_device_get_sysattr_value()' function.
 *
 * From man pages: The retrieved value is cached in the device.
 * Repeated calls will return the same value and not open the attribute again.
 *
 * More calls of 'get_free_bundle_from_dev_cached()' without enumerating
 * of the device via 'uio_udev_get_device_from_devid()' gives incorrect values
 * and is major issue for 'used' flag! (e.g. 'uio_ctrl/bundle_0/used').
 */
static inline struct adf_uio_user_bundle *get_bundle_from_dev_cached(
    struct udev_device *dev,
    int bundle_nr)
{
    unsigned long attribute_value;
    struct adf_uio_user_bundle *bundle = NULL;
    char uio_dev_name[32];
    Cpa32U i = 0;
    Cpa32S x = 0;
    Cpa32U maxNumRingsPerBank = 3;
    unsigned long size = -1;

    if (uio_udev_read_str(dev,
                          uio_dev_name,
                          sizeof(uio_dev_name),
                          UDEV_ATTRIBUTE_DEV_NAME,
                          bundle_nr) != CPA_STATUS_SUCCESS)
        return NULL;

    bundle = ICP_MALLOC_GEN(sizeof(*bundle));
    if (!bundle)
    {
        perror("failed to allocate bundle structure\n");
        return NULL;
    }

    bundle->ptr = ICP_MALLOC_GEN(sizeof(uint64_t *) * ADF_MAX_UIO_MAPS);
    if (NULL == bundle->ptr)
    {
        ICP_FREE(bundle);
        perror("failed to allocate bundle ptr\n");
        return NULL;
    }

    bundle->fd = open(uio_dev_name, O_RDWR);
    if (bundle->fd < 0)
    {
        ADF_ERROR("failed to open uio dev %s\n", uio_dev_name);
        ICP_FREE(bundle->ptr);
        ICP_FREE(bundle);
        return NULL;
    }
    bundle->udev_dev = dev;
    uio_udev_read_long(dev, &attribute_value,
                       UDEV_ATTRIBUTE_DEV_MINOR,
                       bundle_nr);
    bundle->device_minor = attribute_value;

    /* FIXME */
    for (i = 0; i < maxNumRingsPerBank; i++)
    {
        (bundle->ptr)[i] = (uint32_t *)uio_map_bundle_ptr(bundle, i);
        if (NULL == (bundle->ptr)[i])
        {
            ADF_ERROR("failed to map[%d] uio device %s bundle ptr\n",
                      i, uio_dev_name);
            for (x = i - 1; x >= 0; x--)
            {
                if (CPA_STATUS_SUCCESS != uio_udev_read_long(bundle->udev_dev,
                        &size,
                        UDEV_ATTRIBUTE_MAP_SIZE,
                        bundle->device_minor,
                        x))
                {
                    ADF_ERROR("Failed to read size from sysfs "
                              "attribute map[%d]\n", x);
                }
                else
                {
                    uio_free_bundle_ptr((bundle->ptr)[x], size);
                }
            }

            close(bundle->fd);
            ICP_FREE(bundle->ptr);
            ICP_FREE(bundle);
            return NULL;
        }
    }

#ifdef ICP_DEBUG
    uio_bundle_show(uio_dev_name, bundle, bundle_nr, maxNumRingsPerBank);
#endif
    return bundle;
}

struct adf_uio_user_bundle *uio_get_bundle_from_accelid(int accelid,
        int bundle_nr)
{
    struct udev_device *dev;
    struct adf_uio_user_bundle *bundle;

    if (CPA_STATUS_SUCCESS != uio_udev_get_device_from_devid(accelid, &dev))
        return NULL;

    bundle = get_bundle_from_dev_cached(dev, bundle_nr);
    return bundle;
}

void uio_free_bundle(struct adf_uio_user_bundle *bundle)
{
    Cpa32U i = 0;
    Cpa32U maxNumRingsPerBank = 3;
    unsigned long size = -1;

    for (i = 0; i < maxNumRingsPerBank; i++)
    {
        if (CPA_STATUS_SUCCESS != uio_udev_read_long(bundle->udev_dev,
                &size,
                UDEV_ATTRIBUTE_MAP_SIZE,
                bundle->device_minor,
                i))
        {
            ADF_ERROR("Failed to read size from sysfs attribute map[%d]\n", i);
        }
        else
        {
            uio_free_bundle_ptr((bundle->ptr)[i], size);
        }
    }

    close(bundle->fd);

    uio_udev_free_device(bundle->udev_dev);
    ICP_FREE(bundle->ptr);
    ICP_FREE(bundle);
    /* dont do that, but... */
#if 0
    unsigned long size = -1;

    if (CPA_STATUS_SUCCESS != uio_udev_read_long(bundle->udev_dev,
            &size,
            UDEV_ATTRIBUTE_MAP0_SIZE,
            bundle->device_minor))
    {
        printf("Failed to read size from sysfs attribute %s\n",
               UDEV_ATTRIBUTE_MAP0_SIZE);
    }
    else
    {
        uio_free_bundle_ptr(bundle->ptr, size);
    }
#endif
}

static int uio_populate_accel_dev_internal(struct udev_device *udev_dev,
        icp_accel_dev_t *accel_dev)
{
    CpaStatus status;
    char config_value[ADF_CFG_MAX_VAL_LEN_IN_BYTES];
    unsigned long attribute_value;
    char attr_str[ADF_DEVICE_TYPE_LENGTH] = {0};

    uio_udev_read_long(udev_dev, &attribute_value, UDEV_ATTRIBUTE_ACCELID);
    accel_dev->accelId = attribute_value;
    accel_dev->maxNumRingsPerBank = 3;

    /* read Maximal Number of Banks */
    if (CPA_STATUS_SUCCESS !=
            icp_adf_cfgGetParamValue(
                accel_dev, ADF_GENERAL_SEC, ADF_DEV_MAX_BANKS, config_value))
    {
        ADF_ERROR("accel devid %d error get MAX_BANKS from GENERAL_SEC\n",
                  accel_dev->accelId);
        return -EINVAL;
    }
    accel_dev->maxNumBanks =
        (Cpa32U)strtoul(config_value, NULL, ADF_CFG_BASE_DEC);

    /* Get capabilities mask */
    if (CPA_STATUS_SUCCESS !=
            icp_adf_cfgGetParamValue(accel_dev,
                                     ADF_GENERAL_SEC,
                                     ADF_DEV_CAPABILITIES_MASK,
                                     config_value))
    {
        ADF_ERROR("accel devid %d error get CAPABILITIES_MASK "
                  "from GENERAL_SEC\n",
                  accel_dev->accelId);
        return -EINVAL;
    }
    accel_dev->accelCapabilitiesMask =
        (Cpa32U)strtoul(config_value, NULL, ADF_CFG_BASE_HEX);

    /* Get dc extended capabilities */
    if (CPA_STATUS_SUCCESS != icp_adf_cfgGetParamValue(
                accel_dev,
                ADF_GENERAL_SEC,
                ADF_DC_EXTENDED_FEATURES,
                config_value))
    {
        ADF_ERROR("accel devid %d error get DC_EXTENDED_FEATURES "
                  "from GENERAL_SEC\n",
                  accel_dev->accelId);
        return -EINVAL;
    }
    accel_dev->dcExtendedFeatures =
        (Cpa32U)strtoul(config_value, NULL, ADF_CFG_BASE_HEX);

    /* Get device type */
    uio_udev_read_long(udev_dev, &attribute_value, UDEV_ATTRIBUTE_ACCEL_TYPE);
    accel_dev->deviceType = attribute_value;

    /* Get device name */
    uio_udev_read_str(
        udev_dev, attr_str, sizeof(attr_str), UDEV_ATTRIBUTE_ACCEL_NAME);
    ICP_STRNCPY(accel_dev->deviceName, attr_str, sizeof(accel_dev->deviceName));

    /* Get device revision id */
    uio_udev_read_long(udev_dev, &attribute_value, UDEV_ATTRIBUTE_ACCEL_REVID);
    accel_dev->revisionId = attribute_value;

    /* Get mmp addr */
    uio_udev_read_long(udev_dev, &attribute_value, UDEV_ATTRIBUTE_MMP_ADDR);

    /* Get numa node */
    status =
        uio_udev_read_long(udev_dev, &attribute_value, UDEV_ATTRIBUTE_NODEID);
    accel_dev->numa_node = attribute_value;
    if (status != CPA_STATUS_SUCCESS)
    {
        accel_dev->numa_node = -1;
    }

    return 0;
}

static int uio_populate_accel_dev(struct udev_device *udev_dev,
                                  icp_accel_dev_t *accel_dev)
{
    ICP_MEMSET(accel_dev, '\0', sizeof(icp_accel_dev_t));
    return uio_populate_accel_dev_internal(udev_dev, accel_dev);
}

static int uio_repopulate_accel_dev(struct udev_device *udev_dev,
                                    icp_accel_dev_t *accel_dev)
{
    void *pSalHandle = NULL;
    void *pQatStats = NULL;
    void *banks = NULL;

    pSalHandle = accel_dev->pSalHandle;
    pQatStats = accel_dev->pQatStats;
    banks = accel_dev->banks;

    ICP_MEMSET(accel_dev, '\0', sizeof(*accel_dev));
    accel_dev->pSalHandle = pSalHandle;
    accel_dev->pQatStats = pQatStats;
    accel_dev->banks = banks;

    return uio_populate_accel_dev_internal(udev_dev, accel_dev);
}

int uio_acces_dev_exist(int dev_id, struct udev_device **udev_dev)
{
    struct udev_device *dev;

    if (CPA_STATUS_SUCCESS != uio_udev_get_device_from_devid(dev_id, &dev))
        return 0;
    if (udev_dev)
        *udev_dev = dev;
    else
        uio_udev_free_device(dev);

    return 1;
}

int uio_create_accel_dev(icp_accel_dev_t **accel_dev, int dev_id)
{
    CpaStatus status;
    struct udev_device *dev;

    *accel_dev = ICP_MALLOC_GEN(sizeof(**accel_dev));
    if (!*accel_dev)
        return -ENOMEM;

    if (!uio_acces_dev_exist(dev_id, &dev))
    {
        status = -EINVAL;
        ADF_DEBUG("uio acces devid %d exist\n", dev_id);
        goto accel_fail;
    }

    if (uio_populate_accel_dev(dev, *accel_dev))
    {
        status = -EINVAL;
        ADF_ERROR("uio accel devid %d error populate\n", dev_id);
        uio_udev_free_device(dev);
        goto accel_fail;
    }

    ADF_DEBUG(
        "determine uio device and populate accel device\n"
        "    devid:                     %d\n"
        "    accelID:                   %d\n"
        "    number banks:              %u\n"
        "    number rings per bank:     %u\n"
        "    capabilities mask:         0x%x\n"
        "    device type:               %d\n"
        "    device name:               %s\n",
        dev_id,
        (*accel_dev)->accelId,
        (*accel_dev)->maxNumBanks,
        (*accel_dev)->maxNumRingsPerBank,
        (*accel_dev)->accelCapabilitiesMask,
        (*accel_dev)->deviceType,
        (*accel_dev)->deviceName);

    uio_udev_free_device(dev);
    return 0;

accel_fail:
    ICP_FREE(*accel_dev);
    *accel_dev = NULL;
    return status;
}

int uio_reinit_accel_dev(icp_accel_dev_t **accel_dev, int dev_id)
{
    CpaStatus status;
    struct udev_device *dev;

    if (!*accel_dev)
        return -ENOMEM;

    if (!uio_acces_dev_exist(dev_id, &dev))
    {
        status = -EINVAL;
        goto accel_fail;
    }

    if (uio_repopulate_accel_dev(dev, *accel_dev))
    {
        status = -EINVAL;
        uio_udev_free_device(dev);
        goto accel_fail;
    }
    uio_udev_free_device(dev);

    return 0;

accel_fail:
    ICP_FREE(*accel_dev);
    *accel_dev = NULL;
    return status;
}

void uio_destroy_accel_dev(icp_accel_dev_t *accel_dev)
{
    ICP_FREE(accel_dev);
}
