/***************************************************************************
 *
 *   BSD LICENSE
 * 
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/

/*
 * This is sample code that uses the primality testing APIs. A hard-coded
 * prime number is tested with four different algorithms:
 *
 * - GCD primality test
 * - Fermat primality test
 * - Miller-Rabin primality test. This test requires random numbers that are
 *   also hardcoded here (see unsigned char MR[])
 * - Lucas primality test
 */
#include "cpa.h"
#include "cpa_cy_im.h"
#include "cpa_cy_ecsm2.h"

#include "cpa_sample_utils.h"

#define TIMEOUT_MS                  5000 /* 5 seconds*/
#define SM2_DATA_LEN                32

typedef enum
{
    CPA_SM2_SIGN                    = 0,
    CPA_SM2_VERIFY                  = 1,
    CPA_SM2_ENCRYPT                 = 2,
    CPA_SM2_DECRYPT                 = 3,
    CPA_SM2_GET_Z                   = 4,
    CPA_SM2_GET_E                   = 5,
    CPA_SM2_KEY_GEN                 = 6,
    CPA_SM2_KEY_EXCHANGE            = 7,
    CPA_SM2_POINT_VERIFY            = 8,
    CPA_SM2_POINT_MULTIPLY          = 9,
} CpaSm2AlgTypedef;

typedef struct
{
    Cpa8U index;
    char *name;
    /* SM2 perform operation function pointer */
    CpaStatus (*funcOp)(CpaInstanceHandle);
} PerformOpTypedef;

static CpaStatus sm2SignPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2VerifyPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2EncryptPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2DecryptPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2GetZPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2GetEPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2KeyGenPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2KeyExchangePerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2PointVerifyPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus sm2PointMultiplyPerformOp(CpaInstanceHandle cyInstHandle);

static PerformOpTypedef op[] = 
{
    {
        .index = CPA_SM2_SIGN,
        .name = "sm2 sign",
        .funcOp = sm2SignPerformOp,
    },
    {
        .index = CPA_SM2_VERIFY,
        .name = "sm2 verify",
        .funcOp = sm2VerifyPerformOp,
    },
    {
        .index = CPA_SM2_GET_Z,
        .name = "sm2 get Z",
        .funcOp = sm2GetZPerformOp,
    },
    {
        .index = CPA_SM2_GET_E,
        .name = "sm2 get E",
        .funcOp = sm2GetEPerformOp,
    },
    {
        .index = CPA_SM2_ENCRYPT,
        .name = "sm2 encrypt",
        .funcOp = sm2EncryptPerformOp,
    },
    {
        .index = CPA_SM2_DECRYPT,
        .name = "sm2 decrypt",
        .funcOp = sm2DecryptPerformOp,
    },
    {
        .index = CPA_SM2_KEY_GEN,
        .name = "sm2 key generate",
        .funcOp = sm2KeyGenPerformOp,
    },
    {
        .index = CPA_SM2_KEY_EXCHANGE,
        .name = "sm2 key exchange",
        .funcOp = sm2KeyExchangePerformOp,
    },
    {
        .index = CPA_SM2_POINT_VERIFY,
        .name = "sm2 point verify",
        .funcOp = sm2PointVerifyPerformOp,
    },
    {
        .index = CPA_SM2_POINT_MULTIPLY,
        .name = "sm2 point multiply",
        .funcOp = sm2PointMultiplyPerformOp,
    },
};

static Cpa8U Sm2Rand[32] =
{
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02,
};

static Cpa8U Sm2PriKey[32] =
{
    0xec, 0x0c, 0xa0, 0x92, 0x36, 0x2c, 0x1e, 0x18,
    0xcf, 0xeb, 0xfe, 0x83, 0x9c, 0x75, 0x60, 0x0a,
    0xa5, 0x52, 0xe6, 0x68, 0x7c, 0x0c, 0x93, 0xfc,
    0x22, 0xfb, 0x67, 0x4b, 0xb4, 0xe0, 0x56, 0x80,
};

static Cpa8U Sm2PubKey[64] =
{
    0xe1, 0x1d, 0x15, 0xa0, 0xc3, 0x1c, 0x0b, 0xca,
    0xbc, 0x03, 0x29, 0x76, 0x06, 0x03, 0x98, 0x2f,
    0x2f, 0x2d, 0xcd, 0x34, 0xd1, 0xe4, 0xe0, 0x3b,
    0x8d, 0x6b, 0x6a, 0xcb, 0x5e, 0x1b, 0x28, 0xa2,
    0x7d, 0x2f, 0x54, 0xe7, 0x12, 0xd5, 0xc0, 0x83,
    0x33, 0xd1, 0xa8, 0x4a, 0xab, 0xb7, 0xf4, 0x78,
    0xbe, 0x1e, 0xba, 0x8e, 0xa0, 0x34, 0xe7, 0xea,
    0xe5, 0xaa, 0x83, 0xea, 0xb8, 0x22, 0x98, 0xf2,
};

static Cpa8U Sm2Msg[16] =
{
    0xad, 0xbe, 0x13, 0xee, 0xad, 0xe1, 0xf1, 0xc8,
    0xbb, 0xf1, 0x64, 0xf2, 0x1d, 0x82, 0x1e, 0x74,
};

static Cpa8U Sm2UserId[16] =
{
    0x09, 0x1f, 0x87, 0x07, 0xc8, 0x14, 0xce, 0xf1,
    0x64, 0xbd, 0x52, 0x68, 0x63, 0x50, 0xd0, 0x9b,
};

static Cpa8U Sm2Z[32] =
{
    0x71, 0xc9, 0xbc, 0xc1, 0xd8, 0xdc, 0xe8, 0x5d,
    0x5e, 0xde, 0xb5, 0x20, 0x95, 0x82, 0xc7, 0xbe,
    0xb7, 0x60, 0x8d, 0xe8, 0x76, 0xd2, 0x0f, 0x24,
    0xe4, 0xa7, 0x26, 0xbe, 0xb6, 0xcc, 0x10, 0xd2,
};

static Cpa8U Sm2E[32] =
{
    0xe0, 0xf3, 0x67, 0x4d, 0xaa, 0xda, 0x06, 0x9a,
    0xed, 0x5f, 0xbc, 0xe3, 0x4c, 0x65, 0x1d, 0xae,
    0x28, 0xf4, 0xef, 0x1b, 0x71, 0x3a, 0xbd, 0xd9,
    0x91, 0xe9, 0x6e, 0x3d, 0xf4, 0x4b, 0x83, 0x18,
};

// static Cpa8U Sm2Sig[64] =
// {
//     0x1a, 0x44, 0x43, 0xaa, 0xda, 0x50, 0xfa, 0x88,
//     0x88, 0x4a, 0x2d, 0x85, 0x55, 0x96, 0x94, 0xa5,
//     0x44, 0x89, 0x68, 0xcb, 0xa7, 0xfe, 0xb8, 0xfc,
//     0x76, 0x3d, 0xe4, 0x99, 0xc1, 0x99, 0x00, 0xbd,
//     0x6a, 0xbb, 0x7c, 0x33, 0xa9, 0xb5, 0xfb, 0x32,
//     0xe6, 0x97, 0x98, 0x62, 0x38, 0x0e, 0x9a, 0xdd,
//     0xea, 0x4e, 0xaf, 0xeb, 0xb6, 0x2c, 0x91, 0x48,
//     0xb2, 0x22, 0x9a, 0x5b, 0xda, 0x5b, 0x77, 0x70,
// };

static Cpa8U Sm2Plain[16] =
{
    0xad, 0xbe, 0x13, 0xee, 0xad, 0xe1, 0xf1, 0xc8,
    0xbb, 0xf1, 0x64, 0xf2, 0x1d, 0x82, 0x1e, 0x74,
};

static Cpa8U Sm2Cipher[112] =
{
    0x56, 0xce, 0xfd, 0x60, 0xd7, 0xc8, 0x7c, 0x00,
    0x0d, 0x58, 0xef, 0x57, 0xfa, 0x73, 0xba, 0x4d,
    0x9c, 0x0d, 0xfa, 0x08, 0xc0, 0x8a, 0x73, 0x31,
    0x49, 0x5c, 0x2e, 0x1d, 0xa3, 0xf2, 0xbd, 0x52,
    0x31, 0xb7, 0xe7, 0xe6, 0xcc, 0x81, 0x89, 0xf6,
    0x68, 0x53, 0x5c, 0xe0, 0xf8, 0xea, 0xf1, 0xbd,
    0x6d, 0xe8, 0x4c, 0x18, 0x2f, 0x6c, 0x8e, 0x71,
    0x6f, 0x78, 0x0d, 0x3a, 0x97, 0x0a, 0x23, 0xc3,
    0x1b, 0x3f, 0x1b, 0x9c, 0xa6, 0xde, 0xf7, 0xd5,
    0x03, 0xde, 0x13, 0xb5, 0x7f, 0x54, 0x0d, 0xa4,
    0x5a, 0x6d, 0x84, 0x06, 0x4c, 0x36, 0xf5, 0xd5,
    0x23, 0xa5, 0xfb, 0xd3, 0xde, 0xd0, 0xa2, 0x8b,
    0x6c, 0x76, 0x8f, 0x1d, 0x51, 0xa6, 0x0e, 0x21,
    0x6a, 0x8c, 0xcd, 0x57, 0x84, 0x95, 0x60, 0xc7,
};

// static Cpa8U Sm2IdA[16] =
// {
//     0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
//     0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
// };

static Cpa8U Sm2dA[32] =
{
    0x81, 0xeb, 0x26, 0xe9, 0x41, 0xbb, 0x5a, 0xf1,
    0x6d, 0xf1, 0x16, 0x49, 0x5f, 0x90, 0x69, 0x52,
    0x72, 0xae, 0x2c, 0xd6, 0x3d, 0x6c, 0x4a, 0xe1,
    0x67, 0x84, 0x18, 0xbe, 0x48, 0x23, 0x00, 0x29,
};

// static Cpa8U Sm2PA[64] =
// {
//     0x16, 0x0e, 0x12, 0x89, 0x7d, 0xf4, 0xed, 0xb6,
//     0x1d, 0xd8, 0x12, 0xfe, 0xb9, 0x67, 0x48, 0xfb,
//     0xd3, 0xcc, 0xf4, 0xff, 0xe2, 0x6a, 0xa6, 0xf6,
//     0xdb, 0x95, 0x40, 0xaf, 0x49, 0xc9, 0x42, 0x32,
//     0x4a, 0x7d, 0xad, 0x08, 0xbb, 0x9a, 0x45, 0x95,
//     0x31, 0x69, 0x4b, 0xeb, 0x20, 0xaa, 0x48, 0x9d,
//     0x66, 0x49, 0x97, 0x5e, 0x1b, 0xfc, 0xf8, 0xc4,
//     0x74, 0x1b, 0x78, 0xb4, 0xb2, 0x23, 0x00, 0x7f,
// };

static Cpa8U Sm2rA[32] =
{
    0xd4, 0xde, 0x15, 0x47, 0x4d, 0xb7, 0x4d, 0x06,
    0x49, 0x1c, 0x44, 0x0d, 0x30, 0x5e, 0x01, 0x24,
    0x00, 0x99, 0x0f, 0x3e, 0x39, 0x0c, 0x7e, 0x87,
    0x15, 0x3c, 0x12, 0xdb, 0x2e, 0xa6, 0x0b, 0xb3,
};

static Cpa8U Sm2RA[64] =
{
    0x64, 0xce, 0xd1, 0xbd, 0xbc, 0x99, 0xd5, 0x90,
    0x04, 0x9b, 0x43, 0x4d, 0x0f, 0xd7, 0x34, 0x28,
    0xcf, 0x60, 0x8a, 0x5d, 0xb8, 0xfe, 0x5c, 0xe0,
    0x7f, 0x15, 0x02, 0x69, 0x40, 0xba, 0xe4, 0x0e,
    0x37, 0x66, 0x29, 0xc7, 0xab, 0x21, 0xe7, 0xdb,
    0x26, 0x09, 0x22, 0x49, 0x9d, 0xdb, 0x11, 0x8f,
    0x07, 0xce, 0x8e, 0xaa, 0xe3, 0xe7, 0x72, 0x0a,
    0xfe, 0xf6, 0xa5, 0xcc, 0x06, 0x20, 0x70, 0xc0,
};

// static Cpa8U Sm2IdB[16] =
// {
//     0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
//     0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
// };

// static Cpa8U Sm2dB[32] =
// {
//     0x78, 0x51, 0x29, 0x91, 0x7d, 0x45, 0xa9, 0xea,
//     0x54, 0x37, 0xa5, 0x93, 0x56, 0xb8, 0x23, 0x38,
//     0xea, 0xad, 0xda, 0x6c, 0xeb, 0x19, 0x90, 0x88,
//     0xf1, 0x4a, 0xe1, 0x0d, 0xef, 0xa2, 0x29, 0xb5,
// };

static Cpa8U Sm2PB[64] =
{
    0x6a, 0xe8, 0x48, 0xc5, 0x7c, 0x53, 0xc7, 0xb1,
    0xb5, 0xfa, 0x99, 0xeb, 0x22, 0x86, 0xaf, 0x07,
    0x8b, 0xa6, 0x4c, 0x64, 0x59, 0x1b, 0x8b, 0x56,
    0x6f, 0x73, 0x57, 0xd5, 0x76, 0xf1, 0x6d, 0xfb,
    0xee, 0x48, 0x9d, 0x77, 0x16, 0x21, 0xa2, 0x7b,
    0x36, 0xc5, 0xc7, 0x99, 0x20, 0x62, 0xe9, 0xcd,
    0x09, 0xa9, 0x26, 0x43, 0x86, 0xf3, 0xfb, 0xea,
    0x54, 0xdf, 0xf6, 0x93, 0x05, 0x62, 0x1c, 0x4d,
};

// static Cpa8U Sm2rB[32] =
// {
//     0x7e, 0x07, 0x12, 0x48, 0x14, 0xb3, 0x09, 0x48,
//     0x91, 0x25, 0xea, 0xed, 0x10, 0x11, 0x13, 0x16,
//     0x4e, 0xbf, 0x0f, 0x34, 0x58, 0xc5, 0xbd, 0x88,
//     0x33, 0x5c, 0x1f, 0x9d, 0x59, 0x62, 0x43, 0xd6,
// };

static Cpa8U Sm2RB[64] =
{
    0xac, 0xc2, 0x76, 0x88, 0xa6, 0xf7, 0xb7, 0x06,
    0x09, 0x8b, 0xc9, 0x1f, 0xf3, 0xad, 0x1b, 0xff,
    0x7d, 0xc2, 0x80, 0x2c, 0xdb, 0x14, 0xcc, 0xcc,
    0xdb, 0x0a, 0x90, 0x47, 0x1f, 0x9b, 0xd7, 0x07,
    0x2f, 0xed, 0xac, 0x04, 0x94, 0xb2, 0xff, 0xc4,
    0xd6, 0x85, 0x38, 0x76, 0xc7, 0x9b, 0x8f, 0x30,
    0x1c, 0x65, 0x73, 0xad, 0x0a, 0xa5, 0x0f, 0x39,
    0xfc, 0x87, 0x18, 0x1e, 0x1a, 0x1b, 0x46, 0xfe,
};

static Cpa8U Sm2S1SB[32] =
{
    0xd3, 0xa0, 0xfe, 0x15, 0xde, 0xe1, 0x85, 0xce,
    0xae, 0x90, 0x7a, 0x6b, 0x59, 0x5c, 0xc3, 0x2a,
    0x26, 0x6e, 0xd7, 0xb3, 0x36, 0x7e, 0x99, 0x83,
    0xa8, 0x96, 0xdc, 0x32, 0xfa, 0x20, 0xf8, 0xeb,
};

static Cpa8U Sm2S2SA[32] =
{
    0x18, 0xc7, 0x89, 0x4b, 0x38, 0x16, 0xdf, 0x16,
    0xcf, 0x07, 0xb0, 0x5c, 0x5e, 0xc0, 0xbe, 0xf5,
    0xd6, 0x55, 0xd5, 0x8f, 0x77, 0x9c, 0xc1, 0xb4,
    0x00, 0xa4, 0xf3, 0x88, 0x46, 0x44, 0xdb, 0x88,
};

static Cpa8U Sm2ExcKey[] =
{
    0x6c, 0x89, 0x34, 0x73, 0x54, 0xde, 0x24, 0x84,
    0xc6, 0x0b, 0x4a, 0xb1, 0xfd, 0xe4, 0xc6, 0xe5,
};

static Cpa8U Sm2SignR[32] =
{
    0x37, 0xc2, 0x64, 0xaf, 0x82, 0xa2, 0x82, 0x9a,
    0xfa, 0xb8, 0xac, 0x3b, 0x46, 0xd8, 0xd7, 0xfc,
    0x52, 0xff, 0x09, 0xb9, 0x0f, 0xff, 0x2b, 0xdf,
    0x87, 0x89, 0xa8, 0x52, 0x5e, 0x68, 0xff, 0x47,
};

static Cpa8U Sm2SignS[32] =
{
    0x81, 0x69, 0x0e, 0x96, 0xcc, 0xa6, 0xa5, 0x2c,
    0x8f, 0xd4, 0xda, 0xc2, 0x89, 0x13, 0x95, 0x21,
    0xc6, 0x4c, 0x70, 0x92, 0x66, 0x3e, 0xfd, 0x8b,
    0xf1, 0x5c, 0x5f, 0xf8, 0xb5, 0x8a, 0xb8, 0x1c,
};

static Cpa8U Sm2ZA[32] =
{
    0x3b, 0x85, 0xa5, 0x71, 0x79, 0xe1, 0x1e, 0x7e,
    0x51, 0x3a, 0xa6, 0x22, 0x99, 0x1f, 0x2c, 0xa7,
    0x4d, 0x18, 0x07, 0xa0, 0xbd, 0x4d, 0x4b, 0x38,
    0xf9, 0x09, 0x87, 0xa1, 0x7a, 0xc2, 0x45, 0xb1,
};

static Cpa8U Sm2ZB[32] =
{
    0x79, 0xc9, 0x88, 0xd6, 0x32, 0x29, 0xd9, 0x7e,
    0xf1, 0x9f, 0xe0, 0x2c, 0xa1, 0x05, 0x6e, 0x01,
    0xe6, 0xa7, 0x41, 0x1e, 0xd2, 0x46, 0x94, 0xaa,
    0x8f, 0x83, 0x4f, 0x4a, 0x4a, 0xb0, 0x22, 0xf7,
};

extern int gDebugParam;

/* Forward declaration */
CpaStatus sm2Sample(void);


/**
 * @brief 
 * 
 * @param str 
 * @param buf 
 * @param len 
 * @param align 
 */
static void sm2PrintHex(char *str, Cpa8U *buf, Cpa32U len, Cpa32U align)
{
    Cpa32U i;

    printf("%s \n", str);
    for(i = 0; i < len; i++)
    {
        if((i != 0)  && ((i % align) == 0))
            printf("\n");

        printf("%02x ", buf[i]);
    }
    printf("\n");
}

/**
 * @brief 
 * 
 * @param data1 
 * @param data2 
 * @param len 
 * @return CpaBoolean 
 */
static CpaBoolean sm2ValueCompare(Cpa8U *data1, Cpa8U *data2, Cpa32U len)
{
    for(Cpa32U i = 0; i < len; i++)
    {
        if(data1[i] != data2[i])
        {
            return CPA_FALSE;
        }
    }
    return CPA_TRUE;
}

/**
 * @brief SM2 get z callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pZ 
 */
static void sm2GetZCallback(void *pCallbackTag,
                            CpaStatus status,
                            void *pOpData,
                            CpaFlatBuffer *pZ)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Sm2 get z callback failed, status = %d\n", status);
        goto end;
    }

    if(sm2ValueCompare(Sm2Z, pZ->pData, pZ->dataLenInBytes))
    {
        PRINT_DBG("Sm2 get z value test success\n");
    }
    else
    {
        PRINT_DBG("Sm2 get z value test failure\n");
        sm2PrintHex("Expect value...", Sm2Z, sizeof(Sm2Z), 16);
        sm2PrintHex("Actual value...", pZ->pData, pZ->dataLenInBytes, 16);
    }

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief SM2 get e callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pE 
 */
static void sm2GetECallback(void *pCallbackTag,
                            CpaStatus status,
                            void *pOpData,
                            CpaFlatBuffer *pE)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Sm2 get e callback failed, status = %d\n", status);
        goto end;
    }

    if(sm2ValueCompare(Sm2E, pE->pData, pE->dataLenInBytes))
    {
        PRINT_DBG("Sm2 get e value test success\n");
    }
    else
    {
        PRINT_DBG("Sm2 get e value test failure\n");
        sm2PrintHex("Expect value...", Sm2E, sizeof(Sm2E), 16);
        sm2PrintHex("Actual value...", pE->pData, pE->dataLenInBytes, 16);
    }

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief SM2 sign callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param testPassed 
 */
static void sm2SignCallback(void *pCallbackTag,
                            CpaStatus status,
                            void *pOpData,
                            CpaBoolean pass,
                            CpaFlatBuffer *pR,
                            CpaFlatBuffer *pS)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Sm2 sign callback failed, status = %d\n", status);
        goto end;
    }

    if((sm2ValueCompare(Sm2SignR, pR->pData, pR->dataLenInBytes)) &&
       (sm2ValueCompare(Sm2SignS, pS->pData, pS->dataLenInBytes)))
    {
        PRINT_DBG("Sm2 sign test success\n");
    }
    else
    {
        PRINT_DBG("Sm2 Sign Test Failure\n");
        sm2PrintHex("Expect(R) value...", Sm2SignR, sizeof(Sm2SignR), 16);
        sm2PrintHex("Actual(R) value...", pR->pData, pR->dataLenInBytes, 16);
        sm2PrintHex("Expect(S) value...", Sm2SignS, sizeof(Sm2SignS), 16);
        sm2PrintHex("Actual(S) value...", pS->pData, pS->dataLenInBytes, 16);
    }

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief SM2 verify callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param verifyStatus 
 */
static void sm2VerifyCallback(void *pCallbackTag,
                              CpaStatus status,
                              void *pOpData,
                              CpaBoolean verifyStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Sm2 verify callback error status %d\n", status);
    }
    if (CPA_TRUE != verifyStatus)
    {
        PRINT_ERR("Sm2 verify failed!\n");
    }
    else
    {
        PRINT_ERR("Sm2 verify success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief SM2 encrypt callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pCipher 
 */
static void sm2EncryptCallback(void *pCallbackTag,
                               CpaStatus status,
                               void *pOpData,
                               CpaFlatBuffer *pCipher)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Sm2 encrypt callback failed, status = %d\n", status);
        goto end;
    }

    if(sm2ValueCompare(Sm2Cipher, pCipher->pData, pCipher->dataLenInBytes))
    {
        PRINT_ERR("Sm2 encrypt test success!\n");
    }
    else
    {
        PRINT_DBG("Sm2 encrypt failure!\n");
        sm2PrintHex("Expect value...", Sm2Cipher, sizeof(Sm2Cipher), 16);
        sm2PrintHex("Actual value...", 
                            pCipher->pData, pCipher->dataLenInBytes, 16);
    }

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief Sm2 decrypt callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pPlain 
 */
static void sm2DecryptCallback(void *pCallbackTag,
                               CpaStatus status,
                               void *pOpData,
                               CpaFlatBuffer *pPlain)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Sm2 decrypt callback failed, status = %d\n", status);
        goto end;
    }

    if(sm2ValueCompare(Sm2Plain, pPlain->pData, pPlain->dataLenInBytes))
    {
        PRINT_ERR("Sm2 decrypt success!\n");
    }
    else
    {
        PRINT_DBG("Sm2 decrypt failure!\n");
        sm2PrintHex("Expect value...", Sm2Plain, sizeof(Sm2Plain), 16);
        sm2PrintHex("Actual value...", 
                            pPlain->pData, pPlain->dataLenInBytes, 16);
    }

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief Sm2 key generate callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pOut 
 */
static void sm2KeyGenCallback(void *pCallbackTag,
                              CpaStatus status,
                              void *pOpData,
                              CpaCyEcsm2KeyGenOutputData *pOut)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Sm2 generate key callback failed, status = %d\n", status);
        goto end;
    }

    PRINT_DBG("Sm2 generate key success!\n");
    sm2PrintHex("Prikey value...",
                pOut->PriKey.pData, pOut->PriKey.dataLenInBytes, 16);
    sm2PrintHex("Pubkey(X) value...",
                pOut->PubKeyX.pData, pOut->PubKeyX.dataLenInBytes, 16);
    sm2PrintHex("Pubkey(Y) value...",
                pOut->PubKeyY.pData, pOut->PubKeyY.dataLenInBytes, 16);

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief Sm2 key exchange callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pOut 
 */
static void sm2KeyExchangeCallback(void *pCallbackTag,
                              CpaStatus status,
                              void *pOpData,
                              CpaCyEcsm2KeyExOutputData *pOut)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("callback failed, status = %d\n", status);
        goto end;
    }

    if((sm2ValueCompare(Sm2ExcKey, pOut->Key.pData, pOut->Key.dataLenInBytes))
        && (sm2ValueCompare(Sm2S1SB, pOut->S1.pData, pOut->S1.dataLenInBytes))
        && (sm2ValueCompare(Sm2S2SA, pOut->SA.pData, pOut->SA.dataLenInBytes)))
    {
        PRINT_DBG("Sm2 exchange key success!\n");
    }
    else
    {
        PRINT_DBG("Sm2 exchange key failure!\n");
    }

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief Sm2 point verify callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param verifyStatus 
 */
static void sm2PointVerifyCallback(void *pCallbackTag,
                              CpaStatus status,
                              void *pOpData,
                              CpaBoolean verifyStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Sm2 point verify callback error status %d\n", status);
    }
    if (CPA_TRUE != verifyStatus)
    {
        PRINT_ERR("Sm2 point verify failed!\n");
    }
    else
    {
        PRINT_ERR("Sm2 point verify success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief Sm2 point multiply callback function process
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param multiplyStatus 
 * @param pXk 
 * @param pYk 
 */
static void sm2PointMultiplyCallback(void *pCallbackTag,
                                    CpaStatus status,
                                    void *pOpData,
                                    CpaBoolean multiplyStatus,
                                    CpaFlatBuffer *pXk,
                                    CpaFlatBuffer *pYk)
{
    Cpa8U Px[] =
    {
        0x56, 0xce, 0xfd, 0x60, 0xd7, 0xc8, 0x7c, 0x00,
        0x0d, 0x58, 0xef, 0x57, 0xfa, 0x73, 0xba, 0x4d,
        0x9c, 0x0d, 0xfa, 0x08, 0xc0, 0x8a, 0x73, 0x31,
        0x49, 0x5c, 0x2e, 0x1d, 0xa3, 0xf2, 0xbd, 0x52,
    };
    Cpa8U Py[] =
    {
        0x31, 0xb7, 0xe7, 0xe6, 0xcc, 0x81, 0x89, 0xf6,
        0x68, 0x53, 0x5c, 0xe0, 0xf8, 0xea, 0xf1, 0xbd,
        0x6d, 0xe8, 0x4c, 0x18, 0x2f, 0x6c, 0x8e, 0x71,
        0x6f, 0x78, 0x0d, 0x3a, 0x97, 0x0a, 0x23, 0xc3,
    };

    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Sm2 point multiply callback error status %d\n", status);
    }
    if ((CPA_TRUE == multiplyStatus) &&
        (CPA_TRUE == sm2ValueCompare(Px, pXk->pData, pXk->dataLenInBytes)) &&
        (CPA_TRUE == sm2ValueCompare(Py, pYk->pData, pYk->dataLenInBytes)))
    {
        PRINT_ERR("Sm2 point multiply success!\n");
    }
    else
    {
        PRINT_ERR("Sm2 point multiply failed!\n");
        sm2PrintHex("Expect pXk", Px, sizeof(Px), 16);
        sm2PrintHex("Expect pYk", Py, sizeof(Py), 16);
        sm2PrintHex("Actual pXk", pXk->pData, pXk->dataLenInBytes, 16);
        sm2PrintHex("Actual pYk", pYk->pData, pXk->dataLenInBytes, 16);
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief Sm2 get z perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2GetZPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaCyEcsm2GetZOpData *pOpData = NULL;
    Cpa8U *pId = NULL, *pPubKey = NULL;
    CpaFlatBuffer *pZ = NULL;

    PRINT_DBG("Sm2 get z perform operation\n");

    COMPLETION_INIT(&complete);

    pId = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2UserId), node, BYTE_ALIGNMENT_64);
    pPubKey = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2PubKey), node, BYTE_ALIGNMENT_64);
    pZ = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    pOpData = (CpaCyEcsm2GetZOpData *)qaeMemAllocNUMA(
                        sizeof(CpaCyEcsm2GetZOpData), node, BYTE_ALIGNMENT_64);
    if(!pId || !pPubKey || !pZ || !pOpData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    pZ->pData = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Z), node, BYTE_ALIGNMENT_64);
    if(NULL == pZ->pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    memcpy(pId, Sm2UserId, sizeof(Sm2UserId));
    memcpy(pPubKey, Sm2PubKey, sizeof(Sm2PubKey));
    memset(pZ->pData, 0x00, sizeof(Sm2Z));
    pZ->dataLenInBytes = sizeof(Sm2Z);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->id.pData = pId;
        pOpData->id.dataLenInBytes = sizeof(Sm2UserId);
        pOpData->xP.pData = pPubKey;
        pOpData->xP.dataLenInBytes = 32;
        pOpData->yP.pData = pPubKey + 32;
        pOpData->yP.dataLenInBytes = 32;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2GetZ(cyInstHandle,
                                (const CpaCyEcsm2GetZCbFunc)sm2GetZCallback,
                                (void *)&complete,
                                pOpData,
                                pZ);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

    /** Free all allocated structures before exit*/
cleanup:
    if(pId)
        qaeMemFreeNUMA((void**)&(pId));
    if(pPubKey)
        qaeMemFreeNUMA((void**)&(pPubKey));
    if(pZ->pData)
        qaeMemFreeNUMA((void**)&pZ->pData);
    if(pZ)
        qaeMemFreeNUMA((void**)&pZ);
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);

    COMPLETION_DESTROY(&complete);

    return status;
}

/**
 * @brief Sm2 get e perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2GetEPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaCyEcsm2GetEOpData *pOpData = NULL;
    Cpa8U *pMsg = NULL, *pZ = NULL;
    CpaFlatBuffer *pE = NULL;

    PRINT_DBG("Sm2 get e perform operation\n");

    COMPLETION_INIT(&complete);

    pMsg = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Msg), node, BYTE_ALIGNMENT_64);
    pZ = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Z), node, BYTE_ALIGNMENT_64);
    pE = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    pOpData = (CpaCyEcsm2GetEOpData *)qaeMemAllocNUMA(
                        sizeof(CpaCyEcsm2GetEOpData), node, BYTE_ALIGNMENT_64);
    if(!pMsg || !pE || !pZ || !pOpData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pE->pData = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2E), node, BYTE_ALIGNMENT_64);
    if(NULL == pE->pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    memcpy(pMsg, Sm2Msg, sizeof(Sm2Msg));
    memcpy(pZ, Sm2Z, sizeof(Sm2Z));
    memset(pE->pData, 0x00, sizeof(Sm2E));
    pE->dataLenInBytes = sizeof(Sm2E);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->m.dataLenInBytes = sizeof(Sm2Msg);
        pOpData->m.pData = pMsg;
        pOpData->z.dataLenInBytes = sizeof(Sm2Z);
        pOpData->z.pData = pZ;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2GetE(cyInstHandle,
                                (const CpaCyEcsm2GetECbFunc)sm2GetECallback,
                                (void *)&complete,
                                pOpData,
                                pE);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pMsg)
        qaeMemFreeNUMA((void**)&(pMsg));
    if(pZ)
        qaeMemFreeNUMA((void**)&(pZ));
    if(pOpData)
        qaeMemFreeNUMA((void**)&(pOpData));
    if(pE->pData)
        qaeMemFreeNUMA((void**)&pE->pData);
    if(pE)
        qaeMemFreeNUMA((void**)&pE);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief Sm2 sign perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2SignPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaBoolean signStatus;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    Cpa8U *e = NULL, *priKey = NULL, *rand = NULL;
    CpaFlatBuffer *pR = NULL, *pS = NULL;
    CpaCyEcsm2SignOpData *pOpData = NULL;

    PRINT_DBG("Sm2 sign perform operation\n");

    COMPLETION_INIT(&complete);

    rand = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Rand), node, BYTE_ALIGNMENT_64);
    e = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2PriKey), node, BYTE_ALIGNMENT_64);
    priKey = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2E), node, BYTE_ALIGNMENT_64);
    pR = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    pS = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    pOpData = (CpaCyEcsm2SignOpData *)qaeMemAllocNUMA(sizeof(CpaCyEcsm2SignOpData), node, BYTE_ALIGNMENT_64);
    if(!rand || !e || !priKey || !pR || !pS || !pOpData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memcpy(rand, Sm2Rand, sizeof(Sm2Rand));
    memcpy(e, Sm2E, sizeof(Sm2E));
    memcpy(priKey, Sm2PriKey, sizeof(Sm2PriKey));

    pR->dataLenInBytes = SM2_DATA_LEN;
    pS->dataLenInBytes = SM2_DATA_LEN;
    pR->pData = (Cpa8U *)qaeMemAllocNUMA(pR->dataLenInBytes, node, BYTE_ALIGNMENT_64);
    pS->pData = (Cpa8U *)qaeMemAllocNUMA(pS->dataLenInBytes, node, BYTE_ALIGNMENT_64);
    if ((NULL == pR->pData) || (NULL == pS->pData))
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memset(pR->pData, 0x00, pR->dataLenInBytes);
    memset(pS->pData, 0x00, pR->dataLenInBytes);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->e.pData = e;
        pOpData->e.dataLenInBytes = sizeof(Sm2E);
        pOpData->k.pData = rand;
        pOpData->k.dataLenInBytes = sizeof(Sm2Rand);
        pOpData->d.pData = priKey;
        pOpData->d.dataLenInBytes = sizeof(Sm2PriKey);
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2Sign(cyInstHandle,
                                (const CpaCyEcsm2SignCbFunc)sm2SignCallback,
                                (void *)&complete,
                                pOpData,
                                &signStatus,
                                pR,
                                pS);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

    /** Free all allocated structures before exit*/
cleanup:
    if(pR->pData)
        qaeMemFreeNUMA((void**)&(pR->pData));
    if(pS->pData)
        qaeMemFreeNUMA((void**)&(pS->pData));
    if(pR)
        qaeMemFreeNUMA((void**)&pR);
    if(pS)
        qaeMemFreeNUMA((void**)&pS);
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(e)
        qaeMemFreeNUMA((void**)&e);
    if(rand)
        qaeMemFreeNUMA((void**)&rand);
    if(priKey)
        qaeMemFreeNUMA((void**)&priKey);

    COMPLETION_DESTROY(&complete);

    return status;
}

/**
 * @brief Sm2 verify perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2VerifyPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaBoolean verifyStatus = CPA_FALSE;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pE = NULL, *pPubKey = NULL;
    CpaFlatBuffer *pR = NULL, *pS = NULL;
    CpaCyEcsm2VerifyOpData *pOpData = NULL;

    PRINT_DBG("sm2 verify perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyEcsm2VerifyOpData *)qaeMemAllocNUMA(sizeof(CpaCyEcsm2VerifyOpData), node, BYTE_ALIGNMENT_64);
    pPubKey = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2PubKey), node, BYTE_ALIGNMENT_64);
    pE = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2E), node, BYTE_ALIGNMENT_64);
    pR = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    pS = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pPubKey || !pE || !pR || !pS)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pR->pData = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2SignR), node, BYTE_ALIGNMENT_64);
    pS->pData = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2SignS), node, BYTE_ALIGNMENT_64);
    if ((NULL == pR->pData) || (NULL == pS->pData))
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    memcpy(pPubKey, Sm2PubKey, sizeof(Sm2PubKey));
    memcpy(pE, Sm2E, sizeof(Sm2E));
    pR->dataLenInBytes = sizeof(Sm2SignR);
    memcpy(pR->pData, Sm2SignR, sizeof(Sm2SignR));
    pS->dataLenInBytes = sizeof(Sm2SignS);
    memcpy(pS->pData, Sm2SignS, sizeof(Sm2SignS));

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->e.dataLenInBytes = sizeof(Sm2E);
        pOpData->e.pData = pE;
        pOpData->xP.dataLenInBytes = SM2_DATA_LEN;
        pOpData->xP.pData = pPubKey;
        pOpData->yP.dataLenInBytes = SM2_DATA_LEN;
        pOpData->yP.pData = pPubKey + SM2_DATA_LEN;
        pOpData->r.dataLenInBytes = sizeof(Sm2SignR);
        pOpData->r.pData = pR->pData;
        pOpData->s.dataLenInBytes = sizeof(Sm2SignS);
        pOpData->s.pData = pS->pData;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2Verify(cyInstHandle,
                                (const CpaCyEcsm2VerifyCbFunc)sm2VerifyCallback,
                                (void *)&complete,
                                pOpData,
                                &verifyStatus);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pPubKey)
        qaeMemFreeNUMA((void**)&pPubKey);
    if(pE)
        qaeMemFreeNUMA((void**)&pE);
    if(pR->pData)
        qaeMemFreeNUMA((void**)&pR->pData);
    if(pS->pData)
        qaeMemFreeNUMA((void**)&pS->pData);
    if(pR)
        qaeMemFreeNUMA((void**)&pR);
    if(pS)
        qaeMemFreeNUMA((void**)&pS);
    return status;
}

/**
 * @brief Sm2 encrypt perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2EncryptPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pRandK = NULL, *pPubKey = NULL, *pPlain = NULL;
    CpaFlatBuffer *pCipher = NULL;
    CpaCyEcsm2EncryptOpData *pOpData = NULL;

    PRINT_DBG("Sm2 encrypt perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyEcsm2EncryptOpData *)qaeMemAllocNUMA(
                    sizeof(CpaCyEcsm2EncryptOpData), node, BYTE_ALIGNMENT_64);
    pPubKey = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2PubKey), node, BYTE_ALIGNMENT_64);
    pRandK = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Rand), node, BYTE_ALIGNMENT_64);
    pPlain = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Plain), node, BYTE_ALIGNMENT_64);
    pCipher = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pPubKey || !pRandK || !pPlain || !pCipher)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pCipher->pData = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Cipher), node, BYTE_ALIGNMENT_64);
    if (NULL == pCipher->pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memcpy(pPubKey, Sm2PubKey, sizeof(Sm2PubKey));
    memcpy(pRandK, Sm2Rand, sizeof(Sm2Rand));
    memcpy(pPlain, Sm2Plain, sizeof(Sm2Plain));
    pCipher->dataLenInBytes = sizeof(Sm2Cipher);
    memset(pCipher->pData, 0x00, pCipher->dataLenInBytes);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->k.dataLenInBytes = sizeof(Sm2Rand);
        pOpData->k.pData = pRandK;
        pOpData->m.dataLenInBytes = sizeof(Sm2Plain);
        pOpData->m.pData = pPlain;
        pOpData->xP.dataLenInBytes = SM2_DATA_LEN;
        pOpData->xP.pData = pPubKey;
        pOpData->yP.dataLenInBytes = SM2_DATA_LEN;
        pOpData->yP.pData = pPubKey + SM2_DATA_LEN;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2Encrypt(cyInstHandle,
                                (const CpaCyEcsm2EncryptCbFunc)sm2EncryptCallback,
                                (void *)&complete,
                                pOpData,
                                pCipher);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pPubKey)
        qaeMemFreeNUMA((void**)&pPubKey);
    if(pRandK)
        qaeMemFreeNUMA((void**)&pRandK);
    if(pPlain)
        qaeMemFreeNUMA((void**)&pPlain);
    if(pCipher->pData)
        qaeMemFreeNUMA((void**)&pCipher->pData);
    if(pCipher)
        qaeMemFreeNUMA((void**)&pCipher);
    return status;
}

/**
 * @brief Sm2 decrypt perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2DecryptPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pPriKey = NULL, *pCipher = NULL;
    CpaFlatBuffer *pPlain = NULL;
    CpaCyEcsm2DecryptOpData *pOpData = NULL;

    PRINT_DBG("Sm2 dncrypt perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyEcsm2DecryptOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyEcsm2DecryptOpData), node, BYTE_ALIGNMENT_64);
    pCipher = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Cipher), node, BYTE_ALIGNMENT_64);
    pPriKey = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2PriKey), node, BYTE_ALIGNMENT_64);
    pPlain = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pCipher || !pPriKey || !pPlain)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pPlain->pData = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2Plain), node, BYTE_ALIGNMENT_64);
    if (NULL == pPlain->pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    memcpy(pCipher, Sm2Cipher, sizeof(Sm2Cipher));
    memcpy(pPriKey, Sm2PriKey, sizeof(Sm2PriKey));
    pPlain->dataLenInBytes = sizeof(Sm2Plain);
    memset(pPlain->pData, 0x00, pPlain->dataLenInBytes);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->d.dataLenInBytes = sizeof(Sm2PriKey);
        pOpData->d.pData = pPriKey;
        pOpData->c.dataLenInBytes = sizeof(Sm2Cipher);
        pOpData->c.pData = pCipher;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2Decrypt(cyInstHandle,
                                (const CpaCyEcsm2DecryptCbFunc)sm2DecryptCallback,
                                (void *)&complete,
                                pOpData,
                                pPlain);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pCipher)
        qaeMemFreeNUMA((void**)&pCipher);
    if(pPriKey)
        qaeMemFreeNUMA((void**)&pPriKey);
    if(pPlain->pData)
        qaeMemFreeNUMA((void**)&pPlain->pData);
    if(pPlain)
        qaeMemFreeNUMA((void**)&pPlain);

    return status;
}

/**
 * @brief Sm2 key generate perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2KeyGenPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaCyEcsm2KeyGenOpData *pOpData = NULL;
    CpaCyEcsm2KeyGenOutputData  *pOutput = NULL;

    PRINT_DBG("Sm2 generate key perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyEcsm2KeyGenOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyEcsm2KeyGenOpData), node, BYTE_ALIGNMENT_64);
    pOutput = (CpaCyEcsm2KeyGenOutputData *)qaeMemAllocNUMA(
                sizeof(CpaCyEcsm2KeyGenOutputData), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pOutput)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pOutput->PriKey.pData = (Cpa8U *)qaeMemAllocNUMA(SM2_DATA_LEN , node, BYTE_ALIGNMENT_64);
    pOutput->PubKeyX.pData = (Cpa8U *)qaeMemAllocNUMA(SM2_DATA_LEN , node, BYTE_ALIGNMENT_64);
    pOutput->PubKeyY.pData = (Cpa8U *)qaeMemAllocNUMA(SM2_DATA_LEN , node, BYTE_ALIGNMENT_64);
    if(!pOutput->PriKey.pData || !pOutput->PubKeyX.pData || !pOutput->PubKeyY.pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pOutput->PriKey.dataLenInBytes = SM2_DATA_LEN;
    pOutput->PubKeyX.dataLenInBytes = SM2_DATA_LEN;
    pOutput->PubKeyY.dataLenInBytes = SM2_DATA_LEN;
    memset(pOutput->PriKey.pData, 0x00, SM2_DATA_LEN);
    memset(pOutput->PubKeyX.pData, 0x00, SM2_DATA_LEN);
    memset(pOutput->PubKeyY.pData, 0x00, SM2_DATA_LEN);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2KeyGen(cyInstHandle,
                                (const CpaCyEcsm2KeyGenCbFunc)sm2KeyGenCallback,
                                (void *)&complete,
                                pOpData,
                                pOutput);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pOutput->PriKey.pData)
        qaeMemFreeNUMA((void**)&pOutput->PriKey.pData);
    if(pOutput->PubKeyX.pData)
        qaeMemFreeNUMA((void**)&pOutput->PubKeyX.pData);
    if(pOutput->PubKeyY.pData)
        qaeMemFreeNUMA((void**)&pOutput->PubKeyY.pData);
    if(pOutput)
        qaeMemFreeNUMA((void**)&pOutput);

    return status;
}

/**
 * @brief Sm2 key exchange perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2KeyExchangePerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaCyEcsm2KeyExOpData *pOpData = NULL;
    CpaCyEcsm2KeyExOutputData  *pOutput = NULL;
    Cpa8U *dA, *PB, *rA, *RA, *RB, *ZA, *ZB;

    PRINT_DBG("Sm2 key exchange perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyEcsm2KeyExOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyEcsm2KeyExOpData), node, BYTE_ALIGNMENT_64);
    pOutput = (CpaCyEcsm2KeyExOutputData *)qaeMemAllocNUMA(
                sizeof(CpaCyEcsm2KeyExOutputData), node, BYTE_ALIGNMENT_64);
    dA = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2dA), node, BYTE_ALIGNMENT_64);
    PB = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2PB), node, BYTE_ALIGNMENT_64);
    rA = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2rA), node, BYTE_ALIGNMENT_64);
    RA = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2RA), node, BYTE_ALIGNMENT_64);
    RB = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2RB), node, BYTE_ALIGNMENT_64);
    ZA = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2ZA), node, BYTE_ALIGNMENT_64);
    ZB = (Cpa8U *)qaeMemAllocNUMA(sizeof(Sm2ZB), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pOutput || !dA || !PB || !rA || !RA || !RB || !ZA || !ZB)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pOutput->Key.pData = (Cpa8U *)qaeMemAllocNUMA(16, node, BYTE_ALIGNMENT_64);
    pOutput->S1.pData = (Cpa8U *)qaeMemAllocNUMA(SM2_DATA_LEN, node, BYTE_ALIGNMENT_64);
    pOutput->SA.pData = (Cpa8U *)qaeMemAllocNUMA(SM2_DATA_LEN, node, BYTE_ALIGNMENT_64);
    if(!pOutput->Key.pData || !pOutput->S1.pData || !pOutput->SA.pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pOutput->Key.dataLenInBytes = 16;
    pOutput->S1.dataLenInBytes = SM2_DATA_LEN;
    pOutput->SA.dataLenInBytes = SM2_DATA_LEN;
    memset(pOutput->Key.pData, 0x00, pOutput->Key.dataLenInBytes);
    memset(pOutput->S1.pData, 0x00, pOutput->S1.dataLenInBytes);
    memset(pOutput->SA.pData, 0x00, pOutput->SA.dataLenInBytes);

    memcpy(dA, Sm2dA, sizeof(Sm2dA));
    memcpy(PB, Sm2PB, sizeof(Sm2PB));
    memcpy(rA, Sm2rA, sizeof(Sm2rA));
    memcpy(RA, Sm2RA, sizeof(Sm2RA));
    memcpy(RB, Sm2RB, sizeof(Sm2RB));
    memcpy(ZA, Sm2ZA, sizeof(Sm2ZA));
    memcpy(ZB, Sm2ZB, sizeof(Sm2ZB));

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->dA.dataLenInBytes = sizeof(Sm2dA);
        pOpData->dA.pData = dA;

        pOpData->PB_X.dataLenInBytes = sizeof(Sm2PB) >> 1;
        pOpData->PB_X.pData = PB;
        pOpData->PB_Y.dataLenInBytes = sizeof(Sm2PB) >> 1;
        pOpData->PB_Y.pData = PB + (sizeof(Sm2PB) >> 1);

        pOpData->rA.dataLenInBytes = sizeof(Sm2rA);
        pOpData->rA.pData = rA;

        pOpData->RA_X.dataLenInBytes = sizeof(Sm2RA) >> 1;
        pOpData->RA_X.pData = RA;
        pOpData->RA_Y.dataLenInBytes = sizeof(Sm2RA) >> 1;
        pOpData->RA_Y.pData = RA + ((sizeof(Sm2RA)) >> 1);

        pOpData->RB_X.dataLenInBytes = sizeof(Sm2RB) >> 1;
        pOpData->RB_X.pData = RB;
        pOpData->RB_Y.dataLenInBytes = sizeof(Sm2RB) >> 1;
        pOpData->RB_Y.pData = RB + (sizeof(Sm2RB) >> 1);

        pOpData->ZA.dataLenInBytes = sizeof(Sm2ZA);
        pOpData->ZA.pData = ZA;

        pOpData->ZB.dataLenInBytes = sizeof(Sm2ZB);
        pOpData->ZB.pData = ZB;

        pOpData->keyLenInBytes = 16;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;

        status = cpaCyEcsm2KeyExchange(cyInstHandle,
                        (const CpaCyEcsm2KeyExchangeCbFunc)sm2KeyExchangeCallback,
                        (void *)&complete,
                        pOpData,
                        pOutput);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(dA)
        qaeMemFreeNUMA((void**)&dA);
    if(PB)
        qaeMemFreeNUMA((void**)&PB);
    if(rA)
        qaeMemFreeNUMA((void**)&rA);
    if(RA)
        qaeMemFreeNUMA((void**)&RA);
    if(RB)
        qaeMemFreeNUMA((void**)&RB);
    if(ZA)
        qaeMemFreeNUMA((void**)&ZA);
    if(ZB)
        qaeMemFreeNUMA((void**)&ZB);
    if(pOutput->Key.pData)
        qaeMemFreeNUMA((void**)&pOutput->Key.pData);
    if(pOutput->S1.pData)
        qaeMemFreeNUMA((void**)&pOutput->S1.pData);
    if(pOutput->SA.pData)
        qaeMemFreeNUMA((void**)&pOutput->SA.pData);
    if(pOutput)
        qaeMemFreeNUMA((void**)&pOutput);

    return status;
}

/**
 * @brief Sm2 point verify perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2PointVerifyPerformOp(CpaInstanceHandle cyInstHandle)
{
    static Cpa8U Px[] =
    {
        0xd6, 0x7a, 0x83, 0xad, 0xe1, 0x2e, 0x5d, 0x53,
        0xab, 0x2e, 0x93, 0x19, 0xde, 0xc1, 0xba, 0x24,
        0x61, 0x95, 0x09, 0x7d, 0x21, 0xf2, 0x37, 0x93,
        0x8e, 0xbd, 0x37, 0x5a, 0xbd, 0x6d, 0x30, 0x36,
    };
    static Cpa8U Py[] =
    {
        0x1b, 0x10, 0xac, 0xf8, 0xde, 0x83, 0xc3, 0x01,
        0xdb, 0x09, 0xb6, 0x19, 0xfa, 0x19, 0x3e, 0x05,
        0x19, 0x20, 0xae, 0x19, 0x47, 0x5a, 0xb7, 0xcc,
        0xc3, 0x39, 0xe7, 0xa7, 0x84, 0x70, 0xc8, 0x1d,
    };
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaCyEcsm2PointVerifyOpData *pOpData = NULL;
    Cpa8U *pPx = NULL, *pPy = NULL;
    CpaBoolean verifyStatus = CPA_FALSE;

    PRINT_DBG("Sm2 pointer verify perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyEcsm2PointVerifyOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyEcsm2PointVerifyOpData), node, BYTE_ALIGNMENT_64);
    if(!pOpData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pPx = (Cpa8U *)qaeMemAllocNUMA(sizeof(Px), node, BYTE_ALIGNMENT_64);
    pPy = (Cpa8U *)qaeMemAllocNUMA(sizeof(Py), node, BYTE_ALIGNMENT_64);
    if(!pPx || !pPy)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memcpy(pPx, Px, sizeof(Px));
    memcpy(pPy, Py, sizeof(Py));

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->x.dataLenInBytes = sizeof(Px);
        pOpData->x.pData = pPx;
        pOpData->y.dataLenInBytes = sizeof(Py);
        pOpData->y.pData = pPy;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;
        status = cpaCyEcsm2PointVerify(cyInstHandle,
                        (const CpaCyEcPointVerifyCbFunc)sm2PointVerifyCallback,
                        (void *)&complete,
                        pOpData,
                        &verifyStatus);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pPx)
        qaeMemFreeNUMA((void**)&pPx);
    if(pPy)
        qaeMemFreeNUMA((void**)&pPy);

    return status;
}

/**
 * @brief Sm2 point multiply perform
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus sm2PointMultiplyPerformOp(CpaInstanceHandle cyInstHandle)
{
    static Cpa8U K[] =
    {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02,
    };
    static Cpa8U Px[] =
    {
        0x32, 0xC4, 0xAE, 0x2C, 0x1F, 0x19, 0x81, 0x19,
        0x5F, 0x99, 0x04, 0x46, 0x6A, 0x39, 0xC9, 0x94,
        0x8F, 0xE3, 0x0B, 0xBF, 0xF2, 0x66, 0x0B, 0xE1,
        0x71, 0x5A, 0x45, 0x89, 0x33, 0x4C, 0x74, 0xC7
    };
    static Cpa8U Py[] =
    {
        0xBC, 0x37, 0x36, 0xA2, 0xF4, 0xF6, 0x77, 0x9C,
        0x59, 0xBD, 0xCE, 0xE3, 0x6B, 0x69, 0x21, 0x53,
        0xD0, 0xA9, 0x87, 0x7C, 0xC6, 0x2A, 0x47, 0x40,
        0x02, 0xDF, 0x32, 0xE5, 0x21, 0x39, 0xF0, 0xA0
    };

    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaCyEcsm2PointMultiplyOpData *pOpData = NULL;
    Cpa8U *pPx = NULL, *pPy = NULL, *pK = NULL;
    CpaBoolean multiplyStatus = CPA_FALSE;
    CpaFlatBuffer *pXk = NULL, *pYk = NULL;

    PRINT_DBG("Sm2 pointer multiply perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyEcsm2PointMultiplyOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyEcsm2PointMultiplyOpData), node, BYTE_ALIGNMENT_64);
    pXk = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    pYk = (CpaFlatBuffer *)qaeMemAllocNUMA(sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pXk || !pYk)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    pXk->pData = (Cpa8U *)qaeMemAllocNUMA(SM2_DATA_LEN, node, BYTE_ALIGNMENT_64);
    pYk->pData = (Cpa8U *)qaeMemAllocNUMA(SM2_DATA_LEN, node, BYTE_ALIGNMENT_64);
    pPx = (Cpa8U *)qaeMemAllocNUMA(sizeof(Px), node, BYTE_ALIGNMENT_64);
    pPy = (Cpa8U *)qaeMemAllocNUMA(sizeof(Py), node, BYTE_ALIGNMENT_64);
    pK = (Cpa8U *)qaeMemAllocNUMA(sizeof(K), node, BYTE_ALIGNMENT_64);
    if(!pPx || !pPy || !pK || !pXk->pData || !pYk->pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memcpy(pPx, Px, sizeof(Px));
    memcpy(pPy, Py, sizeof(Py));
    memcpy(pK, K, sizeof(K));
    pXk->dataLenInBytes = SM2_DATA_LEN;
    memset(pXk->pData, 0x00, pXk->dataLenInBytes);
    pYk->dataLenInBytes = SM2_DATA_LEN;
    memset(pYk->pData, 0x00, pYk->dataLenInBytes);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->k.dataLenInBytes = sizeof(K);
        pOpData->k.pData = pK;
        pOpData->x.dataLenInBytes = sizeof(Px);
        pOpData->x.pData = pPx;
        pOpData->y.dataLenInBytes = sizeof(Py);
        pOpData->y.pData = pPy;
        pOpData->fieldType = CPA_CY_EC_FIELD_TYPE_PRIME;
        status = cpaCyEcsm2PointMultiply(cyInstHandle,
                        (const CpaCyEcPointMultiplyCbFunc)sm2PointMultiplyCallback,
                        (void *)&complete,
                        pOpData,
                        &multiplyStatus,
                        pXk,
                        pYk);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in cpaCySymPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pPx)
        qaeMemFreeNUMA((void**)&pPx);
    if(pPy)
        qaeMemFreeNUMA((void**)&pPy);
    if(pK)
        qaeMemFreeNUMA((void**)&pK);
    if(pXk->pData)
        qaeMemFreeNUMA((void**)&pXk->pData);
    if(pXk)
        qaeMemFreeNUMA((void**)&pXk);
    if(pYk->pData)
        qaeMemFreeNUMA((void**)&pYk->pData);
    if(pYk)
        qaeMemFreeNUMA((void**)&pYk);

    return status;
}

/**
 * @brief Sm2 sample test
 * 
 * @return CpaStatus 
 */
CpaStatus sm2Sample(void)
{
    Cpa8U i;
    Cpa64U numEcsm2Requests;
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle cyInstHandle = NULL;
    CpaCyEcsm2Stats64 sm2Stats = {0};
    PerformOpTypedef *performOp = NULL;

    /* get alg name */
    for (i = 0; i < sizeof(op) / sizeof(PerformOpTypedef); i++)
    {
        performOp = &op[i];

        printf("\n===============================================\n");
        PRINT_DBG("Start of %s sample code\n", performOp->name);

        /* In this simplified version of instance discovery, we discover
        exactly one instance of a crypto service. */
        sampleCyGetInstance(&cyInstHandle);
        if (cyInstHandle == NULL)
        {
            return CPA_STATUS_FAIL;
        };

        /* Start Cryptographic component */
        status = cpaCyStartInstance(cyInstHandle);

        if (CPA_STATUS_SUCCESS == status)
        {
            /* Set the address translation function for the instance */
            status = cpaCySetAddressTranslation(cyInstHandle, sampleVirtToPhys);
        }

        if (CPA_STATUS_SUCCESS == status)
        {
            /* If the instance is polled start the polling thread. Note that
            how the polling is done is implementation-dependent. */
            sampleCyStartPolling(cyInstHandle);

            /** Perform Primality test operations */
            status = performOp->funcOp(cyInstHandle);
        }

        if (CPA_STATUS_SUCCESS == status)
        {
            PRINT_DBG("cpaCySm2QueryStats\n");
            status = cpaCyEcsm2QueryStats64(cyInstHandle, &sm2Stats);
            if (status != CPA_STATUS_SUCCESS)
            {
                PRINT_ERR("cpaCySm2QueryStats() failed. (status = %d)\n",
                            status);
            }
            switch(performOp->index)
            {
                case CPA_SM2_SIGN:
                    numEcsm2Requests = sm2Stats.numEcsm2SignRequests;
                    break;
                case CPA_SM2_VERIFY:
                    numEcsm2Requests = sm2Stats.numEcsm2VerifyRequests;
                    break;
                case CPA_SM2_ENCRYPT:
                    numEcsm2Requests = sm2Stats.numEcsm2EncryptRequests;
                    break;
                case CPA_SM2_DECRYPT:
                    numEcsm2Requests = sm2Stats.numEcsm2DecryptRequests;
                    break;
                case CPA_SM2_GET_Z:
                    numEcsm2Requests = sm2Stats.numEcsm2GetZRequests;
                    break;
                case CPA_SM2_GET_E:
                    numEcsm2Requests = sm2Stats.numEcsm2GetERequests;
                    break;
                case CPA_SM2_KEY_GEN:
                    numEcsm2Requests = sm2Stats.numEcsm2KeyGenRequests;
                    break;
                case CPA_SM2_KEY_EXCHANGE:
                    numEcsm2Requests = sm2Stats.numEcsm2KeyExRequests;
                    break;
                case CPA_SM2_POINT_VERIFY:
                    numEcsm2Requests = sm2Stats.numEcsm2PointVerifyRequests;
                    break;
                case CPA_SM2_POINT_MULTIPLY:
                    numEcsm2Requests = sm2Stats.numEcsm2PointMultiplyRequests;
                    break;
                default:
                    break;
            }
            PRINT_DBG("Number of %s requests: %lu\n",
                            performOp->name, numEcsm2Requests);
        }

        /* Stop the polling thread */
        sampleCyStopPolling();

        /** Stop Cryptographic component */
        status = cpaCyStopInstance(cyInstHandle);

        if (CPA_STATUS_SUCCESS == status)
        {
            PRINT_DBG("Sample code ran successfully\n");
        }
        else
        {
            PRINT_DBG("Sample code failed with status of %d\n", status);
        }
    }

    return status;
}

