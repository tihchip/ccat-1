/***************************************************************************
 *
 *   BSD LICENSE
 * 
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/

/*
 * This is sample code that uses the primality testing APIs. A hard-coded
 * prime number is tested with four different algorithms:
 *
 * - GCD primality test
 * - Fermat primality test
 * - Miller-Rabin primality test. This test requires random numbers that are
 *   also hardcoded here (see unsigned char MR[])
 * - Lucas primality test
 */
#include "cpa.h"
#include "cpa_cy_im.h"
#include "cpa_cy_ctrl.h"

#include "cpa_sample_utils.h"

#define TIMEOUT_MS                  5000 /* 5 seconds*/
#define CTRL_OTP_SIZE               512  /* bytes */

/* HSM-CTRL Descriptor Header Key type Bits enumeration */
#define CTRL_KEY_TYPE_CIPHER        0
#define CTRL_KEY_TYPE_AUTH          1
#define CTRL_KEY_TYPE_SM2           2
#define CTRL_KEY_TYPE_ECC           3
#define CTRL_KEY_TYPE_RSA           4
#define CTRL_KEY_TYPE_RSA_CRT       5

extern int gDebugParam;

typedef enum
{
    CPA_CTRL_CMD_RAND_GET,
    CPA_CTRL_CMD_KEY_GEN,
    CPA_CTRL_CMD_KEY_INPUT,
    CPA_CTRL_CMD_KEY_OUTPUT,
    CPA_CTRL_CMD_KEY_UPDATE,
    CPA_CTRL_CMD_KEY_CLEAR,
    CPA_CTRL_CMD_KEY_BASE_ADDR_SET,
    CPA_CTRL_CMD_OTP_PROG,
    CPA_CTRL_CMD_OTP_READ,
    CPA_CTRL_CMD_SYM_RESET,
    CPA_CTRL_CMD_SLEEP,
    CPA_CTRL_CMD_WAKE,
    CPA_CTRL_CMD_TDC_TEMP_GET,
} CpaCtrlAlgTypedef;

typedef struct
{
    Cpa8U index;
    char *name;
    /* SM2 perform operation function pointer */
    CpaStatus (*funcOp)(CpaInstanceHandle);
} PerformOpTypedef;

static CpaStatus CtrlRandGetPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlKeyGenPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlKeyInputPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlKeyOutputPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlKeyUpdatePerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlKeyClearPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlKeyBaseAddrSetPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlOtpProgPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlOtpReadPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlSymResetPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlSleepPerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlWakePerformOp(CpaInstanceHandle cyInstHandle);
static CpaStatus CtrlGetTdcTempPerformOp(CpaInstanceHandle cyInstHandle);

static PerformOpTypedef op[] = 
{
    {
        .index = CPA_CTRL_CMD_RAND_GET,
        .name = "hsm ctrl(get random)",
        .funcOp = CtrlRandGetPerformOp,
    },
    // {
    //     .index = CPA_CTRL_CMD_KEY_GEN,
    //     .name = "hsm ctrl(generate key)",
    //     .funcOp = CtrlKeyGenPerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_KEY_INPUT,
    //     .name = "hsm ctrl(key input)",
    //     .funcOp = CtrlKeyInputPerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_KEY_OUTPUT,
    //     .name = "hsm ctrl(key output)",
    //     .funcOp = CtrlKeyOutputPerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_KEY_UPDATE,
    //     .name = "hsm ctrl(key update)",
    //     .funcOp = CtrlKeyUpdatePerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_KEY_CLEAR,
    //     .name = "hsm ctrl(key clear)",
    //     .funcOp = CtrlKeyClearPerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_KEY_BASE_ADDR_SET,
    //     .name = "hsm ctrl(set key base address)",
    //     .funcOp = CtrlKeyBaseAddrSetPerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_OTP_PROG,
    //     .name = "hsm ctrl(otp program)",
    //     .funcOp = CtrlOtpProgPerformOp,
    // },
    {
        .index = CPA_CTRL_CMD_OTP_READ,
        .name = "hsm ctrl(otp read)",
        .funcOp = CtrlOtpReadPerformOp,
    },
    // {
    //     .index = CPA_CTRL_CMD_SYM_RESET,
    //     .name = "hsm ctrl(sym reset)",
    //     .funcOp = CtrlSymResetPerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_SLEEP,
    //     .name = "hsm ctrl(sleep)",
    //     .funcOp = CtrlSleepPerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_WAKE,
    //     .name = "hsm ctrl(wake)",
    //     .funcOp = CtrlWakePerformOp,
    // },
    // {
    //     .index = CPA_CTRL_CMD_TDC_TEMP_GET,
    //     .name = "hsm ctrl(get tdc temperature)",
    //     .funcOp = CtrlGetTdcTempPerformOp,
    // },
};

static Cpa8U Key[] = 
{
    0x00,
};

/**
 * @brief 
 * 
 * @param str 
 * @param buf 
 * @param len 
 * @param align 
 */
static void ctrlPrintHex(char *str, Cpa8U *buf, Cpa32U len, Cpa32U align)
{
    Cpa32U i;

    printf("%s \n", str);
    for(i = 0; i < len; i++)
    {
        if((i != 0)  && ((i % align) == 0))
            printf("\n");

        printf("%02x ", buf[i]);
    }
    printf("\n");
}

/**
 * @brief 
 * 
 * @param data1 
 * @param data2 
 * @param len 
 * @return CpaBoolean 
 */
static CpaBoolean ctrlValueCompare(Cpa8U *data1, Cpa8U *data2, Cpa32U len)
{
    for(Cpa32U i = 0; i < len; i++)
    {
        if(data1[i] != data2[i])
        {
            return CPA_FALSE;
        }
    }
    return CPA_TRUE;
}

/**
 * @brief hsm ctrl callback (get random callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pRand 
 */
static void CtrlRandGetCallback(void *pCallbackTag,
                                CpaStatus status,
                                void *pOpData,
                                CpaFlatBuffer *pRand)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Hsm ctrl (get random) callback failed, status = %d\n", status);
        goto end;
    }

    PRINT_DBG("Hsm ctrl (get random) test success\n");
    ctrlPrintHex("Rand value...", pRand->pData, pRand->dataLenInBytes, 16);

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl callback (key generate callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param keyGenStatus 
 */
static void CtrlKeyGenCallback(void *pCallbackTag,
                               CpaStatus status,
                               void *pOpData,
                               CpaBoolean keyGenStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (key generate) callback error status %d\n", status);
    }
    if (CPA_TRUE != keyGenStatus)
    {
        PRINT_ERR("Hsm ctrl (key generate) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (key generate) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl callback (key input callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param keyInputStatus 
 */
static void CtrlKeyInputCallback(void *pCallbackTag,
                                 CpaStatus status,
                                 void *pOpData,
                                 CpaBoolean keyInputStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (key input) callback error status %d\n", status);
    }
    if (CPA_TRUE != keyInputStatus)
    {
        PRINT_ERR("Hsm ctrl (key input) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (key input) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl callback (key output callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pKey 
 */
static void CtrlKeyOutputCallback(void *pCallbackTag,
                                  CpaStatus status,
                                  void *pOpData,
                                  CpaFlatBuffer *pKey)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Hsm ctrl (key output) callback failed, status = %d\n", status);
        goto end;
    }

    if(ctrlValueCompare(Key, pKey->pData, pKey->dataLenInBytes))
    {
        PRINT_DBG("Hsm ctrl (key output) test success\n");
    }
    else
    {
        PRINT_DBG("Hsm ctrl (key output) test failure\n");
        ctrlPrintHex("Expect value...", Key, sizeof(Key), 16);
        ctrlPrintHex("Actual value...", pKey->pData, pKey->dataLenInBytes, 16);
    }

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl callback (key update callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param updateStatus 
 */
static void CtrlKeyUpdateCallback(void *pCallbackTag,
                                  CpaStatus status,
                                  void *pOpData,
                                  CpaBoolean updateStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (key update) callback error status %d\n", status);
    }
    if (CPA_TRUE != updateStatus)
    {
        PRINT_ERR("Hsm ctrl (key update) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (key update) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl callback (key clear callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param clearStatus 
 */
static void CtrlKeyClearCallback(void *pCallbackTag,
                                 CpaStatus status,
                                 void *pOpData,
                                 CpaBoolean clearStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (key clear) callback error status %d\n", status);
    }
    if (CPA_TRUE != clearStatus)
    {
        PRINT_ERR("Hsm ctrl (key clear) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (key clear) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}


/**
 * @brief hsm ctrl callback (set key base address callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param setStatus 
 */
static void CtrlKeyBaseAddrSetCallback(void *pCallbackTag,
                                       CpaStatus status,
                                       void *pOpData,
                                       CpaBoolean setStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (set key base address) "
                  "callback error status %d\n", status);
    }
    if (CPA_TRUE != setStatus)
    {
        PRINT_ERR("Hsm ctrl (set key base address) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (set key base address) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl callback (otp program callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param otpProgStatus 
 */
static void CtrlOtpProgCallback(void *pCallbackTag,
                                CpaStatus status,
                                void *pOpData,
                                CpaBoolean otpProgStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (otp program) callback error status %d\n", status);
    }
    if (CPA_TRUE != otpProgStatus)
    {
        PRINT_ERR("Hsm ctrl (otp program) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (otp program) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl callback (otp read callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pOtp 
 */
static void CtrlOtpReadCallback(void *pCallbackTag,
                                CpaStatus status,
                                void *pOpData,
                                CpaFlatBuffer *pOtp)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Hsm ctrl (otp read) callback failed, status = %d\n", status);
        goto end;
    }

    PRINT_DBG("Hsm ctrl (otp read) test success\n");
    ctrlPrintHex("Otp value...", pOtp->pData, pOtp->dataLenInBytes, 16);

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}
/**
 * @brief hsm ctrl (sym reset callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param symResetStatus 
 */
static void CtrlSymResetCallback(void *pCallbackTag,
                                 CpaStatus status,
                                 void *pOpData,
                                 CpaBoolean symResetStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (sym reset) callback error status %d\n", status);
    }
    if (CPA_TRUE != symResetStatus)
    {
        PRINT_ERR("Hsm ctrl (sym reset) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (sym reset) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl (sleep callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param sleepStatus 
 */
static void CtrlSleepCallback(void *pCallbackTag,
                              CpaStatus status,
                              void *pOpData,
                              CpaBoolean sleepStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (sleep) callback error status %d\n", status);
    }
    if (CPA_TRUE != sleepStatus)
    {
        PRINT_ERR("Hsm ctrl (sleep) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (sleep) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl (wake callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param wakeStatus 
 */
static void CtrlWakeCallback(void *pCallbackTag,
                             CpaStatus status,
                             void *pOpData,
                             CpaBoolean wakeStatus)
{
    if (status != CPA_STATUS_SUCCESS)
    {
        PRINT_ERR("Hsm ctrl (wake) callback error status %d\n", status);
    }
    if (CPA_TRUE != wakeStatus)
    {
        PRINT_ERR("Hsm ctrl (wake) failed!\n");
    }
    else
    {
        PRINT_ERR("Hsm ctrl (wake) success!\n");
    }

    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl (get tdc temperature callback)
 * 
 * @param pCallbackTag 
 * @param status 
 * @param pOpData 
 * @param pTdcTemp 
 */
static void CtrlGetTdcTempCallback(void *pCallbackTag,
                                   CpaStatus status,
                                   void *pOpData,
                                   CpaFlatBuffer *pTdcTemp)
{
    if (NULL == pOpData)
    {
        PRINT_ERR("pOpData is null, status = %d\n", status);
        goto end;
    }

    if (CPA_STATUS_SUCCESS != status)
    {
        PRINT_ERR("Hsm ctrl (otp read) callback failed, status = %d\n", status);
        goto end;
    }

    PRINT_DBG("Hsm ctrl (get tdc temperature) test success\n");
    ctrlPrintHex("Current temperature...",
                    pTdcTemp->pData, pTdcTemp->dataLenInBytes, 16);

end:
    COMPLETE((struct COMPLETION_STRUCT *)pCallbackTag);
}

/**
 * @brief hsm ctrl (get random)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlRandGetPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0 ,randSize = 128;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pData = NULL;
    CpaFlatBuffer *pOutput = NULL;
    CpaCyCtrlRandGetOpData *pOpData = NULL;

    PRINT_DBG("Ctrl get random perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlRandGetOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlRandGetOpData), node, BYTE_ALIGNMENT_64);
    pOutput = (CpaFlatBuffer *)qaeMemAllocNUMA(
                sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pOutput)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pData = (Cpa8U *)qaeMemAllocNUMA(randSize, node, BYTE_ALIGNMENT_64);
    if(!pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memset(pData, 0x00, randSize);
    pOutput->dataLenInBytes = randSize;
    pOutput->pData = pData;

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->randLength = randSize;

        status = cpaCyCtrlRandGet(
                    cyInstHandle,
                    (const CpaCyCtrlRandGetCbFunc)CtrlRandGetCallback,
                    (void *)&complete,
                    pOpData,
                    pOutput);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlRandGetPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pData)
        qaeMemFreeNUMA((void**)&pData);
    if(pOutput)
        qaeMemFreeNUMA((void**)&pOutput);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (key generate)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlKeyGenPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    CpaBoolean keyGenStatus = CPA_FALSE;
    struct COMPLETION_STRUCT complete;
    CpaCyCtrlKeyGenOpData *pOpData = NULL;

    PRINT_DBG("Ctrl key generate perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlKeyGenOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlKeyGenOpData), node, BYTE_ALIGNMENT_64);
    if(!pOpData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->keyType = CTRL_KEY_TYPE_CIPHER;
        pOpData->keyId = 1;

        status = cpaCyCtrlKeyGen(
                    cyInstHandle,
                    (const CpaCyCtrlKeyGenCbFunc)CtrlKeyGenCallback,
                    (void *)&complete,
                    pOpData,
                    &keyGenStatus);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlKeyGenPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (key input)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlKeyInputPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    CpaBoolean inputStatus = CPA_FALSE;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pData = NULL;
    CpaFlatBuffer *pKey = NULL;
    CpaCyCtrlKeyInputOpData *pOpData = NULL;
    Cpa32U keyLen = 16;

    PRINT_DBG("Ctrl key generate perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlKeyInputOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlKeyInputOpData), node, BYTE_ALIGNMENT_64);
    pKey = (CpaFlatBuffer *)qaeMemAllocNUMA(
                sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pKey)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pData = (Cpa8U *)qaeMemAllocNUMA(keyLen, node, BYTE_ALIGNMENT_64);
    if(!pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memset(pData, 0x11, keyLen);

    if (CPA_STATUS_SUCCESS == status)
    {
        /* TODO */
        pOpData->kekId = 0;
        pOpData->keyId = 0;
        pOpData->keyType = CTRL_KEY_TYPE_CIPHER;
        pOpData->key.dataLenInBytes = keyLen;
        pOpData->key.pData = pData;

        status = cpaCyCtrlKeyInput(
                    cyInstHandle,
                    (const CpaCyCtrlKeyInputCbFunc)CtrlKeyInputCallback,
                    (void *)&complete,
                    pOpData,
                    &inputStatus);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlKeyInputPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pKey)
        qaeMemFreeNUMA((void**)&pKey);
    if(pData)
        qaeMemFreeNUMA((void**)&pData);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (key output)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlKeyOutputPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pData = NULL;
    CpaFlatBuffer *pKeyOutput = NULL;
    CpaCyCtrlKeyOutputOpData *pOpData = NULL;
    Cpa32U keyLen = 16;

    PRINT_DBG("Ctrl key generate perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlKeyOutputOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlKeyOutputOpData), node, BYTE_ALIGNMENT_64);
    pKeyOutput = (CpaFlatBuffer *)qaeMemAllocNUMA(
                sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pKeyOutput)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pData = (Cpa8U *)qaeMemAllocNUMA(keyLen, node, BYTE_ALIGNMENT_64);
    if(!pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memset(pData, 0x00, keyLen);
    pKeyOutput->dataLenInBytes = keyLen;
    pKeyOutput->pData = pData;

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->kekId = 0;
        pOpData->keyId = 0;
        pOpData->keyType = CTRL_KEY_TYPE_CIPHER;

        status = cpaCyCtrlKeyOutput(
                    cyInstHandle,
                    (const CpaCyCtrlKeyOutputCbFunc)CtrlKeyOutputCallback,
                    (void *)&complete,
                    pOpData,
                    pKeyOutput);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlKeyOutputPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pKeyOutput)
        qaeMemFreeNUMA((void**)&pKeyOutput);
    if(pData)
        qaeMemFreeNUMA((void**)&pData);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (key update)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlKeyUpdatePerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    CpaBoolean updateStatus = CPA_FALSE;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pData = NULL;
    CpaFlatBuffer *pKey = NULL;
    CpaCyCtrlKeyUpdateOpData *pOpData = NULL;
    Cpa32U keyLen = 16;

    PRINT_DBG("Ctrl key generate perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlKeyUpdateOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlKeyUpdateOpData), node, BYTE_ALIGNMENT_64);
    pKey = (CpaFlatBuffer *)qaeMemAllocNUMA(
                sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pKey)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pData = (Cpa8U *)qaeMemAllocNUMA(keyLen, node, BYTE_ALIGNMENT_64);
    if(!pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memset(pData, 0x00, keyLen);

    if (CPA_STATUS_SUCCESS == status)
    {
        /* TODO */
        pOpData->kekId = 0;
        pOpData->keyId = 0;
        pOpData->keyType = CTRL_KEY_TYPE_CIPHER;
        pOpData->key.dataLenInBytes = keyLen;
        pOpData->key.pData = pData;

        status = cpaCyCtrlKeyUpdate(
                    cyInstHandle,
                    (const CpaCyCtrlKeyUpdateCbFunc)CtrlKeyUpdateCallback,
                    (void *)&complete,
                    pOpData,
                    &updateStatus);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlKeyUpdatePerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pKey)
        qaeMemFreeNUMA((void**)&pKey);
    if(pData)
        qaeMemFreeNUMA((void**)&pData);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (key clear)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlKeyClearPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    CpaBoolean clearStatus = CPA_FALSE;
    struct COMPLETION_STRUCT complete;
    CpaCyCtrlKeyClearOpData *pOpData = NULL;

    PRINT_DBG("Ctrl key generate perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlKeyClearOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlKeyClearOpData), node, BYTE_ALIGNMENT_64);
    if(!pOpData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /* TODO */
        pOpData->keyId = 0;
        pOpData->keyType = CTRL_KEY_TYPE_CIPHER;

        status = cpaCyCtrlKeyClear(
                    cyInstHandle,
                    (const CpaCyCtrlKeyClearCbFunc)CtrlKeyClearCallback,
                    (void *)&complete,
                    pOpData,
                    &clearStatus);
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlKeyClearPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (set key base address)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlKeyBaseAddrSetPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaBoolean keyAddrStatus = CPA_FALSE;
    Cpa8U *pData = NULL;
    CpaCyCtrlKeyBaseAddrSetOpData *pOpData = NULL;

    PRINT_DBG("Ctrl otp prog perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlKeyBaseAddrSetOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlKeyBaseAddrSetOpData), node, BYTE_ALIGNMENT_64);
    if(!pOpData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /* TODO */
        pOpData->keyType = CTRL_KEY_TYPE_CIPHER;
        pOpData->keyBaseAddr = 0x1000;
        pOpData->keyLen = 16;

        status = cpaCyCtrlKeyBaseAddrSet(
                    cyInstHandle,
                    (const CpaCyCtrlKeyBaseAddrSetCbFunc)CtrlKeyBaseAddrSetCallback,
                    (void *)&complete,
                    pOpData,
                    &keyAddrStatus);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlOtpProgPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pData)
        qaeMemFreeNUMA((void**)&pData);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (otp prog)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlOtpProgPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    CpaBoolean otpProgStatus = CPA_FALSE;
    Cpa8U *pData = NULL;
    CpaCyCtrlOtpProgOpData *pOpData = NULL;

    PRINT_DBG("Ctrl otp prog perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlOtpProgOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlOtpProgOpData), node, BYTE_ALIGNMENT_64);
    pData = (Cpa8U *)qaeMemAllocNUMA(CTRL_OTP_SIZE, node, BYTE_ALIGNMENT_64);
    if(!pOpData || !pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memset(pData, 0x00, CTRL_OTP_SIZE);

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->offset = 0;
        pOpData->otpData.dataLenInBytes = CTRL_OTP_SIZE;
        pOpData->otpData.pData = pData;
        status = cpaCyCtrlOtpProg(
                    cyInstHandle,
                    (const CpaCyCtrlOtpProgCbFunc)CtrlOtpProgCallback,
                    (void *)&complete,
                    pOpData,
                    &otpProgStatus);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlOtpProgPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pData)
        qaeMemFreeNUMA((void**)&pData);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (otp read)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlOtpReadPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pOtp = NULL;
    CpaCyCtrlOtpReadOpData *pOpData = NULL;
    CpaFlatBuffer *pOtpOutput = NULL;

    PRINT_DBG("Ctrl otp read perform operation\n");

    COMPLETION_INIT(&complete);

    pOpData = (CpaCyCtrlOtpReadOpData *)qaeMemAllocNUMA(
                sizeof(CpaCyCtrlOtpReadOpData), node, BYTE_ALIGNMENT_64);
    pOtpOutput = (CpaFlatBuffer *)qaeMemAllocNUMA(
                sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);

    if(!pOpData || !pOtpOutput)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    pOtp = (Cpa8U *)qaeMemAllocNUMA(CTRL_OTP_SIZE, node, BYTE_ALIGNMENT_64);
    if(!pOtp)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }
    memset(pOtp, 0x00, CTRL_OTP_SIZE);
    pOtpOutput->dataLenInBytes = CTRL_OTP_SIZE;
    pOtpOutput->pData = pOtp;

    if (CPA_STATUS_SUCCESS == status)
    {
        pOpData->offset = 0;
        status = cpaCyCtrlOtpRead(
                    cyInstHandle,
                    (const CpaCyCtrlOtpReadCbFunc)CtrlOtpReadCallback,
                    (void *)&complete,
                    pOpData,
                    pOtpOutput);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlOtpReadPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pOpData)
        qaeMemFreeNUMA((void**)&pOpData);
    if(pOtp)
        qaeMemFreeNUMA((void**)&pOtp);
    if(pOtpOutput)
        qaeMemFreeNUMA((void**)&pOtpOutput);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (sym reset)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlSymResetPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    struct COMPLETION_STRUCT complete;
    CpaBoolean symResetStatus = CPA_FALSE;

    PRINT_DBG("Ctrl otp read perform operation\n");

    COMPLETION_INIT(&complete);

    if (CPA_STATUS_SUCCESS == status)
    {
        status = cpaCyCtrlSymReset(
                    cyInstHandle,
                    (const CpaCyCtrlSymResetCbFunc)CtrlSymResetCallback,
                    (void *)&complete,
                    &symResetStatus);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlSymResetPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

    return status;
}

/**
 * @brief hsm ctrl (sleep)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlSleepPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    struct COMPLETION_STRUCT complete;
    CpaBoolean sleepStatus = CPA_FALSE;

    PRINT_DBG("Ctrl sleep perform operation\n");

    COMPLETION_INIT(&complete);

    if (CPA_STATUS_SUCCESS == status)
    {
        status = cpaCyCtrlSleep(cyInstHandle,
                                (const CpaCyCtrlSleepCbFunc)CtrlSleepCallback,
                                (void *)&complete,
                                &sleepStatus);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlSleepPerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (wake)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlWakePerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    struct COMPLETION_STRUCT complete;
    CpaBoolean wakeStatus = CPA_FALSE;

    PRINT_DBG("Ctrl wake perform operation\n");

    COMPLETION_INIT(&complete);

    if (CPA_STATUS_SUCCESS == status)
    {
        status = cpaCyCtrlWake(cyInstHandle,
                               (const CpaCyCtrlWakeCbFunc)CtrlWakeCallback,
                               (void *)&complete,
                               &wakeStatus);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlWakePerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief hsm ctrl (get tdc temperature)
 * 
 * @param cyInstHandle 
 * @return CpaStatus 
 */
static CpaStatus CtrlGetTdcTempPerformOp(CpaInstanceHandle cyInstHandle)
{
    CpaStatus status = CPA_STATUS_SUCCESS;
    Cpa32U node = 0;
    struct COMPLETION_STRUCT complete;
    Cpa8U *pData = NULL;
    CpaFlatBuffer *pTempOutput = NULL;

    PRINT_DBG("Ctrl get tdc temperature perform operation\n");

    COMPLETION_INIT(&complete);

    pTempOutput = (CpaFlatBuffer *)qaeMemAllocNUMA(
                sizeof(CpaFlatBuffer), node, BYTE_ALIGNMENT_64);
    if(!pTempOutput)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    pData = (Cpa8U *)qaeMemAllocNUMA(4, node, BYTE_ALIGNMENT_64);
    if(!pData)
    {
        PRINT_ERR("memory allocation error\n");
        goto cleanup;
    }

    memset(pData, 0x00, 4);
    pTempOutput->dataLenInBytes = 4;
    pTempOutput->pData = pData;

    if (CPA_STATUS_SUCCESS == status)
    {
        status = cpaCyCtrlGetTdcTemperature(
                        cyInstHandle,
                        (const CpaCyCtrlTdcTempCbFunc)CtrlGetTdcTempCallback,
                        (void *)&complete,
                        pTempOutput);

    }

    if (CPA_STATUS_SUCCESS == status)
    {
        /** Wait until the callback function has been called*/
        if (!COMPLETION_WAIT(&complete, TIMEOUT_MS))
        {
            PRINT_ERR("timeout or interruption in CtrlWakePerformOp\n");
            status = CPA_STATUS_FAIL;
        }
    }

cleanup:
    if(pTempOutput->pData)
        qaeMemFreeNUMA((void**)&pTempOutput->pData);
    if(pTempOutput)
        qaeMemFreeNUMA((void**)&pTempOutput);

    COMPLETION_DESTROY(&complete);
    return status;
}

/**
 * @brief Ctrl sample test
 * 
 * @return CpaStatus 
 */
CpaStatus ctrlSample(void)
{
    Cpa8U i;
    Cpa64U numCtrlRequests;
    CpaStatus status = CPA_STATUS_SUCCESS;
    CpaInstanceHandle cyInstHandle = NULL;
    CpaCyCtrlStats64 ctrlStats = {0};
    PerformOpTypedef *performOp = NULL;

    /* get alg name */
    for (i = 0; i < sizeof(op) / sizeof(PerformOpTypedef); i++)
    {
        performOp = &op[i];

        printf("\n===============================================\n");
        PRINT_DBG("Start of %s sample code\n", performOp->name);

        /* In this simplified version of instance discovery, we discover
        exactly one instance of a crypto service. */
        sampleCyGetInstance(&cyInstHandle);
        if (cyInstHandle == NULL)
        {
            return CPA_STATUS_FAIL;
        };

        /* Start Cryptographic component */
        status = cpaCyStartInstance(cyInstHandle);

        if (CPA_STATUS_SUCCESS == status)
        {
            /* Set the address translation function for the instance */
            status = cpaCySetAddressTranslation(cyInstHandle, sampleVirtToPhys);
        }

        if (CPA_STATUS_SUCCESS == status)
        {
            /* If the instance is polled start the polling thread. Note that
            how the polling is done is implementation-dependent. */
            sampleCyStartPolling(cyInstHandle);

            /** Perform Primality test operations */
            status = performOp->funcOp(cyInstHandle);
        }

        if (CPA_STATUS_SUCCESS == status)
        {
            PRINT_DBG("cpaCyCtrlQueryStats\n");
            status = cpaCyCtrlQueryStats64(cyInstHandle, &ctrlStats);
            if (status != CPA_STATUS_SUCCESS)
            {
                PRINT_ERR("cpaCyCtrlQueryStats() failed. (status = %d)\n", status);
            }
            switch(performOp->index)
            {
                case CPA_CTRL_CMD_RAND_GET:
                    numCtrlRequests = ctrlStats.numCtrlRandGetRequests;
                    break;
                case CPA_CTRL_CMD_KEY_GEN:
                    numCtrlRequests = ctrlStats.numCtrlKeyGenRequests;
                    break;
                case CPA_CTRL_CMD_KEY_INPUT:
                    numCtrlRequests = ctrlStats.numCtrlKeyInputRequests;
                    break;
                case CPA_CTRL_CMD_KEY_OUTPUT:
                    numCtrlRequests = ctrlStats.numCtrlKeyOutputRequests;
                    break;
                case CPA_CTRL_CMD_KEY_UPDATE:
                    numCtrlRequests = ctrlStats.numCtrlKeyUpdateRequests;
                    break;
                case CPA_CTRL_CMD_KEY_CLEAR:
                    numCtrlRequests = ctrlStats.numCtrlKeyClearRequests;
                    break;
                case CPA_CTRL_CMD_KEY_BASE_ADDR_SET:
                    numCtrlRequests = ctrlStats.numCtrlKeyBaseAddrSetRequests;
                    break;
                case CPA_CTRL_CMD_OTP_PROG:
                    numCtrlRequests = ctrlStats.numCtrlOtpProgRequests;
                    break;
                case CPA_CTRL_CMD_OTP_READ:
                    numCtrlRequests = ctrlStats.numCtrlOtpReadRequests;
                    break;
                case CPA_CTRL_CMD_SYM_RESET:
                    numCtrlRequests = ctrlStats.numCtrlSymResetRequests;
                    break;
                case CPA_CTRL_CMD_SLEEP:
                    numCtrlRequests = ctrlStats.numCtrlSleepRequests;
                    break;
                case CPA_CTRL_CMD_WAKE:
                    numCtrlRequests = ctrlStats.numCtrlWakeRequests;
                    break;
                case CPA_CTRL_CMD_TDC_TEMP_GET:
                    numCtrlRequests = ctrlStats.numCtrlTdcTempRequests;
                    break;
                default:
                    break;
            }
            PRINT_DBG("Number of %s requests: %lu\n",
                            performOp->name, numCtrlRequests);
        }

        /* Stop the polling thread */
        sampleCyStopPolling();

        /** Stop Cryptographic component */
        status = cpaCyStopInstance(cyInstHandle);

        if (CPA_STATUS_SUCCESS == status)
        {
            PRINT_DBG("Sample code ran successfully\n");
        }
        else
        {
            PRINT_DBG("Sample code failed with status of %d\n", status);
        }
    }
}

