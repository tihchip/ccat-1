
#include <stdlib.h>     /* exit, abort */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <malloc.h>

#include "cpa.h"
#include "OsalOsTypes.h"

#include "include/coherent.h"
#include "coherent_stat.h"
#include "coherent_timer.h"
#include "coherent_ck_pr.h"
#include "coherent_options.h"
#include "coherent_mem_utils.h"

/* Memory request type definition */
typedef enum
{
    MEM_OP_NONE,
    MEM_OP_READ,
    MEM_OP_WRITE
} mem_op_t;

/* Memory scope type definition */
typedef enum
{
    MEM_SCOPE_LOCAL,
    MEM_SCOPE_NUMA,
    MEM_SCOPE_DMABUF,
} mem_scope_t;

#define SIZEOF_SIZE_T sizeof(size_t)

typedef void co_execute_event(void);
co_execute_event *coherentExecuteEvent;     /* event execution function */

typedef void *co_malloc(size_t, size_t);
co_malloc *coherentMalloc;                  /* malloc execution function */

typedef void co_free(void *);
co_free *coherentFree;                      /* free execution function */

static ssize_t       memory_block_size;
static long long int memory_total_size;
static unsigned int  memory_scope;
static unsigned int  memory_oper;
static unsigned int  memory_access_rnd;
static ssize_t       max_offset;

static size_t   *block_buffer = NULL;
static uint64_t run_counters;

/* running pid */
static pid_t coherent_pid;

/* global execution timer */
coherent_timer_t coherent_exec_timer;

/* global variables */
coherent_globals_t coherent_globals;
coherent_stat_t coherent_stat;

/* Get OS page size */
static size_t coherentGetPageSize(void)
{
#ifdef _SC_PAGESIZE
    return sysconf(_SC_PAGESIZE);
#else
    return getpagesize();
#endif
}

#define SIZE_T_LOAD(ptr)       ck_pr_load_64((uint64_t *)(ptr))
#define SIZE_T_STORE(ptr,val)  ck_pr_store_64((uint64_t *)(ptr),(uint64_t)(val))

static inline __attribute__((always_inline))
void coherentSeqNone(void)
{
    for (size_t *buf = block_buffer, *end = buf + max_offset; buf <= end; buf++)
    {
        ck_pr_barrier(); /* nop */
    }
    return;
}

static inline __attribute__((always_inline))
void coherentSeqRead(void)
{
    for (size_t *buf = block_buffer, *end = buf + max_offset; buf < end; buf++)
    {
        size_t val = SIZE_T_LOAD(buf);
        (void) val; /* unused */
    }
    return;
}

static inline __attribute__((always_inline))
void coherentSeqReadWithDirect(void)
{
    ck_pr_l1_inval_Dcachelines((size_t)block_buffer,
                               (size_t)block_buffer + memory_block_size);
    coherentSeqRead();
    return;
}

static inline __attribute__((always_inline))
void coherentSeqWrite(void)
{
    for (size_t *buf = block_buffer, *end = buf + max_offset; buf < end; buf++)
    {
        SIZE_T_STORE(buf, (size_t)coherent_pid);
    }
    return;
}

static inline __attribute__((always_inline))
void coherentSeqWriteWithDirect(void)
{
    coherentSeqWrite();
    ck_pr_l1_wbinval_Dcachelines((size_t)block_buffer,
                                 (size_t)block_buffer + memory_block_size);
    return;
}

static inline __attribute__((always_inline))
void coherentRndNone(void)
{
    printf("%s:%d, FIXME\n", __func__, __LINE__);
    return;
}

static inline __attribute__((always_inline))
void coherentRndRead(void)
{
    printf("%s:%d, FIXME\n", __func__, __LINE__);
    return;
}

static inline __attribute__((always_inline))
void coherentRndReadWithDirect(void)
{
    printf("%s:%d, FIXME\n", __func__, __LINE__);
    return;
}

static inline __attribute__((always_inline))
void coherentRndWrite(void)
{
    printf("%s:%d, FIXME\n", __func__, __LINE__);
    return;
}

static inline __attribute__((always_inline))
void coherentRndWriteWithDirect(void)
{
    printf("%s:%d, FIXME\n", __func__, __LINE__);
    return;
}

int coherentInit(void)
{
    const char *s;

    s = coherentGetValueString("scope");
    if (!strcmp(s, "local"))
        memory_scope = MEM_SCOPE_LOCAL;
    else if (!strcmp(s, "numa"))
        memory_scope = MEM_SCOPE_NUMA;
    else if (!strcmp(s, "dmabuf"))
        memory_scope = MEM_SCOPE_DMABUF;
    else
    {
        printf("Invalid value for memory scope: %s", s);
        return -1;
    }

    memory_block_size = coherentGetValueSize("block-size");
    if (memory_block_size < SIZEOF_SIZE_T ||
            /* Must be a power of 2 */
            (memory_block_size & (memory_block_size - 1)) != 0)
    {
        printf("Invalid value for memory block-size: %s",
               coherentGetValueString("block-size"));
        return -1;
    }

    max_offset = memory_block_size / SIZEOF_SIZE_T - 1;
    memory_total_size = coherentGetValueSize("total-size");

    s = coherentGetValueString("oper");
    if (!strcmp(s, "write"))
        memory_oper = MEM_OP_WRITE;
    else if (!strcmp(s, "read"))
        memory_oper = MEM_OP_READ;
    else if (!strcmp(s, "none"))
        memory_oper = MEM_OP_NONE;
    else
    {
        printf("Invalid value for memory oper: %s", s);
        return -1;
    }

    s = coherentGetValueString("access-mode");
    if (!strcmp(s, "seq"))
        memory_access_rnd = 0;
    else if (!strcmp(s, "rnd"))
        memory_access_rnd = 1;
    else
    {
        printf("Invalid value for memory-access-mode: %s", s);
        return -1;
    }

    /* malloc block buffer */
    switch (memory_scope)
    {
        case MEM_SCOPE_LOCAL:
            coherentMalloc = coherentMemAlignFromLocal;
            coherentFree = coherentFreeToLocal;
            break;

        case MEM_SCOPE_NUMA:
            coherentMalloc = coherentMemAlignFromNUMA;
            coherentFree = coherentFreeToNUMA;
            break;

        case MEM_SCOPE_DMABUF:
            coherentMalloc = coherentMemAlignFromDMABuf;
            coherentFree = coherentFreeToDMABuf;
            break;
    }

    block_buffer = coherentMalloc(memory_block_size, coherentGetPageSize());
    if (NULL == block_buffer)
    {
        printf("Failed to allocate buffer!\n");
        return -1;
    }
    memset(block_buffer, 0, memory_block_size);

    run_counters = memory_total_size / memory_block_size;

    switch (memory_oper)
    {
        case MEM_OP_NONE:
            coherentExecuteEvent =
                memory_access_rnd ? coherentRndNone : coherentSeqNone;
            break;

        case MEM_OP_READ:
            coherentExecuteEvent =
                memory_access_rnd ? coherentRndRead : coherentSeqRead;
            break;

        case MEM_OP_WRITE:
            coherentExecuteEvent =
                memory_access_rnd ? coherentRndWrite : coherentSeqWrite;
            break;

        default:
            printf("Unknown memory request type\n");
            return -1;
    }
    /* direct? */
    if (coherentGetValueFlag("direct"))
    {
        switch (memory_oper)
        {
            case MEM_OP_READ:
                coherentExecuteEvent =
                    memory_access_rnd ? coherentRndReadWithDirect :
                    coherentSeqReadWithDirect;
                break;

            case MEM_OP_WRITE:
                coherentExecuteEvent =
                    memory_access_rnd ? coherentRndWriteWithDirect :
                    coherentSeqWriteWithDirect;
                break;
        }
    }

    /* get running pid */
    coherent_pid = getpid();

    /* init timer */
    coherentInitTimer(&coherent_exec_timer);

    /* limit for total execution time in seconds, default: 10s */
    coherent_globals.max_time_ns = SEC2NS(10);

    memset(&coherent_stat, 0, sizeof(coherent_stat_t));
    return 0;
}

/* print mode */
void coherentPrintRunMode(void)
{
    char *str;

    printf("Running the test with following options:\n");
    printf("Number of threads: %d\n", 1);

    printf("Initializing random number generator from current time\n");
    srandom(time(NULL));

    printf("Running memory speed test with the following options:\n");
    printf("  block size: %ldKiB\n", (long)(memory_block_size / 1024));
    printf("  total size: %ldMiB\n", (long)(memory_total_size / 1024 / 1024));

    switch (memory_access_rnd)
    {
        case 0:
            str = "seq";
            break;
        case 1:
            str = "rnd";
            break;
        default:
            str = "(unknown)";
            break;
    }
    printf("  mode: %s\n", str);

    switch (memory_oper)
    {
        case MEM_OP_READ:
            str = "read";
            break;
        case MEM_OP_WRITE:
            str = "write";
            break;
        case MEM_OP_NONE:
            str = "none";
            break;
        default:
            str = "(unknown)";
            break;
    }
    printf("  operation: %s\n", str);

    switch (memory_scope)
    {
        case MEM_SCOPE_LOCAL:
            str = "local";
            break;
        case MEM_SCOPE_NUMA:
            str = "numa";
            break;
        case MEM_SCOPE_DMABUF:
            str = "dmabuf";
            break;
        default:
            str = "(unknown)";
            break;
    }
    printf("  scope: %s\n\n", str);
    return;
}

static CpaBoolean coherentMoreEvents(void)
{
    /* Check if we have a time limit */
    if (coherent_globals.max_time_ns > 0 &&
            unlikely(coherentTimerSum(&coherent_exec_timer) >= coherent_globals.max_time_ns))
    {
        printf("Time limit exceeded, exiting...\n");
        return CPA_FALSE;
    }
    return CPA_TRUE;
}

static CpaBoolean coherentNextEvents(void)
{
    if (memory_total_size > 0 && run_counters-- == 0)
    {
        return CPA_FALSE;
    }
    return CPA_TRUE;
}

/* run test */
void coherentRun(void)
{
    printf("Initializing worker threads...\n");
    printf("Worker thread (#%d) started\n\n", coherent_pid);

    while (coherentMoreEvents())
    {
        if (!coherentNextEvents())
            break;

        coherentTimerStart(&coherent_exec_timer);
        coherentExecuteEvent();
        coherentTimerStop(&coherent_exec_timer);
    }
    return;
}

/* cumulative */
void coherentReportCumulative(void)
{
    const double megabyte = 1024.0 * 1024.0;

    /* aggregate temporary timers copy populated */
    coherent_stat.latency_min = coherentTimerMin(&coherent_exec_timer);
    coherent_stat.latency_max = coherentTimerMax(&coherent_exec_timer);
    coherent_stat.latency_avg = coherentTimerAvg(&coherent_exec_timer);

    coherent_stat.time_total = coherentTimerSum(&coherent_exec_timer);
    coherent_stat.events = coherent_exec_timer.events;

    /* cumulative */
    printf("Latency (ms):\n");
    printf("        min: %41.2f\n", NS2MS(coherent_stat.latency_min));
    printf("        avg: %41.2f\n", NS2MS(coherent_stat.latency_avg));
    printf("        max: %41.2f\n", NS2MS(coherent_stat.latency_max));

    printf("\nThroughput:\n");
    printf("        execution time:                        %.4fs\n",
           NS2SEC(coherent_stat.time_total));
    printf("        total number of events:              %" PRIu64 "\n",
           coherent_exec_timer.events);
    if (memory_oper != MEM_OP_NONE)
    {
        const double mb = coherent_stat.events * memory_block_size / megabyte;
        printf("        transferred:                      %4.2f MiB\n\n", mb);
    }

    return;
}

void coherentDestroy(void)
{
    if (block_buffer)
    {
        coherentFree(block_buffer);
        block_buffer = NULL;
    }
    coherentOptionsDone();
    return;
}
