
#include <time.h>       /* gettimeofday */
#include <sys/time.h>   /* setitimer, gettimeofday */
#include <signal.h>     /* SIGALRM */
#include <errno.h>
#include <unistd.h>     /* pause() */
#include <stdlib.h>     /* random, debugging only */
#include <string.h>

#include "coherent_timer.h"

void coherentTimerReset(coherent_timer_t *t)
{
    t->min_time = UINT64_MAX;
    t->max_time = 0;
    t->sum_time = 0;
    t->events = 0;
    t->queue_time = 0;
}

/* ret 0 on success, <0 on error*/
int coherentInitTimer(coherent_timer_t *t)
{
    memset(&t->time_start, 0, sizeof(struct timespec));
    memset(&t->time_end, 0, sizeof(struct timespec));

    coherentTimerReset(t);
    return 0;
}

/* get total time for all events */
uint64_t coherentTimerSum(coherent_timer_t *t)
{
    return t->sum_time;
}

/* get minimum time */
uint64_t coherentTimerMin(coherent_timer_t *t)
{
    if (t->events == 0)
        return 0;
    return t->min_time;
}

/* get maximum time */
uint64_t coherentTimerMax(coherent_timer_t *t)
{
    return t->max_time;
}

/* get average time per event */
uint64_t coherentTimerAvg(coherent_timer_t *t)
{
    if (t->events == 0)
        return 0; /* return zero if there were no events */
    return (t->sum_time / t->events);
}

