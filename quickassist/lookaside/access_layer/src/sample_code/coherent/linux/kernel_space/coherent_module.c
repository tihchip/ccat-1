
#include <asm/io.h>
#include <linux/cdev.h>
#include <linux/debugfs.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/nodemask.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/version.h>
#include <linux/hugetlb.h>
#include <linux/dma-mapping.h>

#include "coherent_module.h"
#include "../../include/coherent.h"

#define DEV_MEM_NAME            "coherent"
#define DEV_MEM_NAME_DMABUF     "coherent_dmabuf"

#define MODULE_NAME             "coherent_module"
#define DEV_MEM_MAJOR           0
#define DEV_MEM_MAX_MINOR       1
#define DEV_MEM_BASE_MINOR      0

static DEFINE_MUTEX(dev_mem_lock);
static DEFINE_MUTEX(dev_mem_lock_dmabuf);
static user_mem_dev_t *mem_dev_numa   = NULL;
static user_mem_dev_t *mem_dev_dmabuf = NULL;

typedef struct chr_drv_info_s
{
    struct module           *owner;
    unsigned                major;
    unsigned                min_minor;
    unsigned                max_minor;
    char                    *name;
    struct file_operations  *file_ops;
    struct cdev             drv_cdev;
    struct class            *drv_class;
    struct device           *drv_class_dev;
    struct device           *dma_dev;
    unsigned                num_devices;
    unsigned                unregistered;
} chr_drv_info_t;

static chr_drv_info_t mem_drv_info;
static chr_drv_info_t mem_drv_info_dmabuf;

/* Find memory information */
kdev_mem_info_t *user_mem_get_info(user_mem_dev_t *dev,
                                   uint32_t id,
                                   uint32_t pid)
{
    user_proc_mem_list_t *list = NULL;
    kdev_mem_info_t *kmem = NULL;

    list = dev->head;
    while (list)
    {
        if (pid == list->pid)
        {
            kmem = list->head;
            while (kmem)
            {
                if (kmem->id == id)
                {
                    return kmem;
                }
                kmem = kmem->pNext;
            }
        }
        list = list->pNext;
    }
    return NULL;
}

/* Free numa memory */
void user_mem_free(user_mem_dev_t *dev, uint32_t id, uint32_t pid)
{
    kdev_mem_info_t *kmem = NULL;
    user_proc_mem_list_t *list = NULL;

    list = dev->head;
    while (list)
    {
        if (pid == list->pid)
        {
            kmem = list->head;
            while (kmem)
            {
                if (kmem->id == id)
                {
                    REMOVE_ELEMENT_FROM_LIST(kmem, list->tail, list->head);
                    list->allocs_nr--;

                    kfree(kmem->mem_info);
                    kfree(kmem->kmalloc_ptr);
                    kfree(kmem);
                    return;
                }
                kmem = kmem->pNext;
            }
        }
        list = list->pNext;
    }
    mm_info("Could not find id: %d pid: %d\n", id, pid);
    return;
}

/* Free all numa memory */
void user_mem_free_all(user_mem_dev_t *dev)
{
    kdev_mem_info_t *kmem = NULL, *next = NULL;
    user_proc_mem_list_t *list = NULL, *l_next = NULL;

    list = dev->head;
    while (list)
    {
        l_next = list->pNext;
        kmem = list->head;
        while (kmem)
        {
            next = kmem->pNext;
            kfree(kmem->mem_info);
            kfree(kmem->kmalloc_ptr);
            REMOVE_ELEMENT_FROM_LIST(kmem, list->tail, list->head);
            kfree(kmem);
            kmem = next;
        }

        REMOVE_ELEMENT_FROM_LIST(list, dev->tail, dev->head);
        kfree(list);
        list = l_next;
    }
    return;
}

/* Clean all memory for a process */
void user_mem_free_all_pid(user_mem_dev_t *dev, uint32_t pid)
{
    kdev_mem_info_t *kmem = NULL, *next = NULL;
    user_proc_mem_list_t *list = NULL, *l_next = NULL;

    list = dev->head;
    while (list)
    {
        l_next = list->pNext;
        if (list->pid == pid)
        {
            kmem = list->head;
            while (kmem)
            {
                next = kmem->pNext;
                kfree(kmem->mem_info);
                kfree(kmem->kmalloc_ptr);
                REMOVE_ELEMENT_FROM_LIST(kmem, list->tail, list->head);
                kfree(kmem);
                kmem = next;
            }
            REMOVE_ELEMENT_FROM_LIST(list, dev->tail, dev->head);
            kfree(list);
        }
        list = l_next;
    }
    return;
}

/* Free dmabuf memory */
void user_mem_free_dmabuf(user_mem_dev_t *dev, uint32_t id, uint32_t pid)
{
    kdev_mem_info_t *kmem = NULL;
    user_proc_mem_list_t *list = NULL;

    list = dev->head;
    while (list)
    {
        if (pid == list->pid)
        {
            kmem = list->head;
            while (kmem)
            {
                if (kmem->id == id)
                {
                    REMOVE_ELEMENT_FROM_LIST(kmem, list->tail, list->head);
                    list->allocs_nr--;
                    kfree(kmem->mem_info);
                    dma_free_coherent(mem_drv_info_dmabuf.dma_dev,
                                      kmem->alloc_size,
                                      kmem->cpu_addr,
                                      kmem->dma_handle);
                    kfree(kmem);
                    return;
                }
                kmem = kmem->pNext;
            }
        }
        list = list->pNext;
    }
    mm_info("Could not find id: %d pid: %d\n", id, pid);
    return;
}

/* Free all dmabuf memory */
void user_mem_free_all_dmabuf(user_mem_dev_t *dev)
{
    kdev_mem_info_t *kmem = NULL, *next = NULL;
    user_proc_mem_list_t *list = NULL, *l_next = NULL;

    list = dev->head;
    while (list)
    {
        l_next = list->pNext;
        kmem = list->head;
        while (kmem)
        {
            next = kmem->pNext;
            kfree(kmem->mem_info);
            dma_free_coherent(mem_drv_info_dmabuf.dma_dev,
                              kmem->alloc_size,
                              kmem->cpu_addr,
                              kmem->dma_handle);
            REMOVE_ELEMENT_FROM_LIST(kmem, list->tail, list->head);
            kfree(kmem);
            kmem = next;
        }

        REMOVE_ELEMENT_FROM_LIST(list, dev->tail, dev->head);
        kfree(list);
        list = l_next;
    }
    return;
}

/* Clean all dmabuf memory for a process */
void user_mem_free_all_pid_dmabuf(user_mem_dev_t *dev, uint32_t pid)
{
    kdev_mem_info_t *kmem = NULL, *next = NULL;
    user_proc_mem_list_t *list = NULL, *l_next = NULL;

    list = dev->head;
    while (list)
    {
        l_next = list->pNext;
        if (list->pid == pid)
        {
            kmem = list->head;
            while (kmem)
            {
                next = kmem->pNext;
                kfree(kmem->mem_info);
                dma_free_coherent(mem_drv_info_dmabuf.dma_dev,
                                  kmem->alloc_size,
                                  kmem->cpu_addr,
                                  kmem->dma_handle);
                REMOVE_ELEMENT_FROM_LIST(kmem, list->tail, list->head);
                kfree(kmem);
                kmem = next;
            }
            REMOVE_ELEMENT_FROM_LIST(list, dev->tail, dev->head);
            kfree(list);
        }
        list = l_next;
    }
    return;
}

static int dev_mem_free(struct file *fp, unsigned int cmd, unsigned long arg)
{
    int ret = 0;
    dev_mem_info_t user_mem_info = {0};

    ret = copy_from_user(&user_mem_info,
                         (dev_mem_info_t *)arg,
                         sizeof(dev_mem_info_t));
    if (ret != 0)
    {
        mm_err("%s:%d copy_from_user failed, ret=%d\n",
               __func__, __LINE__, ret);
        return -EIO;
    }
    user_mem_free(mem_dev_numa, user_mem_info.id, current->tgid);
    return 0;
}

static int dev_mem_free_dmabuf(struct file *fp, unsigned int cmd,
                               unsigned long arg)
{
    int ret = 0;
    dev_mem_info_t user_mem_info = {0};

    ret = copy_from_user(&user_mem_info,
                         (dev_mem_info_t *)arg,
                         sizeof(dev_mem_info_t));
    if (ret != 0)
    {
        mm_err("%s:%d copy_from_user failed, ret=%d\n",
               __func__, __LINE__, ret);
        return -EIO;
    }
    user_mem_free(mem_dev_dmabuf, user_mem_info.id, current->tgid);
    return 0;
}

/* Allocate numa memory */
dev_mem_info_t *user_mem_alloc(user_mem_dev_t *dev,
                               size_t size,
                               int64_t node,
                               uint32_t pid)
{
    dev_mem_info_t *mem_info = NULL;
    kdev_mem_info_t *kmem = NULL;
    user_proc_mem_list_t *list = NULL;

    if (0 == size)
    {
        mm_err("Invalid parameter size value\n");
        return NULL;
    }

    /* Find the process allocation list */
    list = dev->head;
    while (list)
    {
        if (pid == list->pid)
            break;
        list = list->pNext;
    }
    if (NULL == list)
    {
        mm_err("userMemAlloc(): find pid in the list failed\n");
        return NULL;
    }

    mem_info = kmalloc_node(sizeof(dev_mem_info_t), GFP_KERNEL, node);
    if (NULL == mem_info)
    {
        mm_err("userMemAlloc(): allocation failed\n");
        return NULL;
    }
    memset(mem_info, '\0', sizeof(dev_mem_info_t));

    kmem = kmalloc_node(sizeof(kdev_mem_info_t), GFP_KERNEL, node);
    if (!kmem)
    {
        mm_err("%s:%d Unable to allocate Kernel control block\n",
               __func__, __LINE__);
        kfree(mem_info);
        return NULL;
    }
    memset(kmem, '\0', sizeof(kdev_mem_info_t));

    kmem->mem_info = mem_info;
    kmem->id = list->max_id++;
    kmem->size = size;

    kmem->kmalloc_ptr = kmalloc_node(kmem->size, GFP_KERNEL, node);
    if (NULL == kmem->kmalloc_ptr)
    {
        mm_err("Unable to allocate memory kmem->kmalloc_ptr\n");
        kfree(mem_info);
        kfree(kmem);
        return NULL;
    }
    kmem->kmalloc_area = ((int *)
                          ((((unsigned long)kmem->kmalloc_ptr) + PAGE_SIZE - 1)
                           & PAGE_MASK));
    kmem->phy_addr = virt_to_phys(kmem->kmalloc_ptr);
    list->allocs_nr++;
    ADD_ELEMENT_TO_END_OF_LIST(kmem, list->tail, list->head);

    /* copy */
    mem_info->id = kmem->id;
    mem_info->nodeId = node;
    mem_info->size = size;
    return mem_info;
}

/* Allocate dmabuf memory */
dev_mem_info_t *user_mem_alloc_dmabuf(size_t size, int64_t node, uint32_t pid)
{
    dev_mem_info_t *mem_info = NULL;
    kdev_mem_info_t *kmem = NULL;
    user_proc_mem_list_t *list = NULL;

    if (0 == size)
    {
        mm_err("Invalid parameter size value\n");
        return NULL;
    }

    /* Find the process allocation list */
    list = mem_dev_dmabuf->head;
    while (list)
    {
        if (pid == list->pid)
            break;
        list = list->pNext;
    }
    if (NULL == list)
    {
        mm_err("userMemAlloc(): find pid in the list failed\n");
        return NULL;
    }

    mem_info = kmalloc_node(sizeof(dev_mem_info_t), GFP_KERNEL, node);
    if (NULL == mem_info)
    {
        mm_err("userMemAlloc(): allocation failed\n");
        return NULL;
    }
    memset(mem_info, '\0', sizeof(dev_mem_info_t));

    kmem = kmalloc_node(sizeof(kdev_mem_info_t), GFP_KERNEL, node);
    if (!kmem)
    {
        mm_err("%s:%d Unable to allocate Kernel control block\n",
               __func__, __LINE__);
        kfree(mem_info);
        return NULL;
    }
    memset(kmem, '\0', sizeof(kdev_mem_info_t));

    kmem->mem_info = mem_info;
    kmem->id = list->max_id++;
    kmem->size = size;

    /* setup buffer size and allocation size */
    kmem->alloc_size =
        ((kmem->size + ((1 << PAGE_SHIFT) - 1)) >> PAGE_SHIFT) << PAGE_SHIFT;
    mm_info("alloc dmabuf, buffer_size(%llu) allocation_size(%llu)\n",
            kmem->size,
            kmem->alloc_size);

    kmem->cpu_addr = dma_alloc_coherent(
                         mem_drv_info_dmabuf.dma_dev,
                         kmem->alloc_size,
                         &kmem->dma_handle,
                         GFP_KERNEL);
    if (IS_ERR_OR_NULL(kmem->cpu_addr))
    {
        int ret = PTR_ERR(kmem->cpu_addr);
        mm_err("Unable to dma_alloc_coherent dmabuf memory(size=%llu), "
               "return(%d)\n", kmem->size, ret);
        kfree(mem_info);
        kfree(kmem);
        return NULL;
    }
    list->allocs_nr++;
    ADD_ELEMENT_TO_END_OF_LIST(kmem, list->tail, list->head);

    /* copy */
    mem_info->id = kmem->id;
    mem_info->nodeId = node;
    mem_info->size = size;
    return mem_info;
}

static int dev_mem_alloc(struct file *fp, unsigned int cmd, unsigned long arg)
{
    int ret = 0;
    dev_mem_info_t *mem_info = NULL;
    dev_mem_info_t user_mem_info = {0};

    ret = copy_from_user(&user_mem_info,
                         (dev_mem_info_t *)arg,
                         sizeof(dev_mem_info_t));
    if (ret != 0)
    {
        mm_err("dev_mem_alloc: copy_from_user failed, ret=%d\n", ret);
        return -EIO;
    }

    mem_info = user_mem_alloc(mem_dev_numa,
                              user_mem_info.size,
                              user_mem_info.nodeId,
                              current->tgid);
    if (NULL == mem_info)
    {
        mm_err("dev_mem_alloc(): userMemAlloc failed\n");
        return -ENOMEM;
    }

    ret = copy_to_user((dev_mem_info_t *)arg,
                       mem_info,
                       sizeof(dev_mem_info_t));
    if (ret != 0)
    {
        user_mem_free(mem_dev_numa, mem_info->id, current->tgid);
        mm_err("dev_mem_alloc: copy_to_user failed, ret=%d\n", ret);
        return -EIO;
    }
    return 0;
}

static int dev_mem_alloc_dmabuf(struct file *fp,
                                unsigned int cmd,
                                unsigned long arg)
{
    int ret = 0;
    dev_mem_info_t *mem_info = NULL;
    dev_mem_info_t user_mem_info = {0};

    ret = copy_from_user(&user_mem_info,
                         (dev_mem_info_t *)arg,
                         sizeof(dev_mem_info_t));
    if (ret != 0)
    {
        mm_err("user_mem_alloc_dmabuf: copy_from_user failed, ret=%d\n", ret);
        return -EIO;
    }

    mem_info = user_mem_alloc_dmabuf(user_mem_info.size,
                                     user_mem_info.nodeId,
                                     current->tgid);
    if (NULL == mem_info)
    {
        mm_err("dev_mem_alloc_dmabuf(): alloc coherent memory failed\n");
        return -ENOMEM;
    }

    ret = copy_to_user((dev_mem_info_t *)arg,
                       mem_info,
                       sizeof(dev_mem_info_t));
    if (ret != 0)
    {
        user_mem_free_dmabuf(mem_dev_dmabuf, mem_info->id, current->tgid);
        mm_err("user_mem_alloc_dmabuf: copy_to_user failed, ret=%d\n", ret);
        return -EIO;
    }
    return 0;
}

static int mem_mmap(struct file *fp, struct vm_area_struct *vma)
{
    int ret = 0;
    uint32_t id = 0;
    unsigned long phys_kmalloc_area = 0;
    kdev_mem_info_t *kmem = NULL;
    uint64_t size = vma->vm_end - vma->vm_start;

    id = vma->vm_pgoff << PAGE_SHIFT;
    ret = mutex_lock_interruptible(&dev_mem_lock);
    if (ret)
        return ret;
    kmem = user_mem_get_info(mem_dev_numa, id, current->tgid);
    mutex_unlock(&dev_mem_lock);

    if (!kmem)
    {
        mm_err("mem_mmap(): cannot find numa meminfo\n");
        return -ENOMEM;
    }

    /* There is an agreement that mmap(PAGE_SIZE) means memory block, */
    /* not means control block */
    phys_kmalloc_area = virt_to_phys((void*)kmem->kmalloc_area);

    /* Ensure memory mapping does not exceed the allocated memory region */
    mm_info("%s:%d map numa allocated memory region, "
            "mmap size=%lld, memory region=%lld\n",
            __func__, __LINE__, size, kmem->size);
    ret = remap_pfn_range(vma,
                          vma->vm_start,
                          phys_kmalloc_area >> PAGE_SHIFT,
                          size,
                          vma->vm_page_prot);
    if (unlikely(ret))
    {
        mm_err("%s:%d remap_pfn_range failed, ret = %d\n",
               __func__, __LINE__, ret);
    }
    return ret;
}

static int mem_mmap_dmabuf(struct file *fp, struct vm_area_struct *vma)
{
    int ret = 0;
    uint32_t id = 0;
    kdev_mem_info_t *kmem = NULL;

    id = vma->vm_pgoff << PAGE_SHIFT;
    ret = mutex_lock_interruptible(&dev_mem_lock_dmabuf);
    if (ret)
        return ret;
    kmem = user_mem_get_info(mem_dev_dmabuf, id, current->tgid);
    mutex_unlock(&dev_mem_lock_dmabuf);

    if (!kmem)
    {
        mm_err("%s:%d cannot find dmabuf meminfo\n", __func__, __LINE__);
        return -ENOMEM;
    }

    if (vma->vm_pgoff + vma_pages(vma) > (kmem->alloc_size >> PAGE_SHIFT))
    {
        mm_err("%s:%d not PAGE_SHIFT\n", __func__, __LINE__);
        return -EINVAL;
    }

    /* If necessary, set the vma flag here */
    return dma_mmap_coherent(
               mem_drv_info_dmabuf.dma_dev,
               vma,
               kmem->cpu_addr,
               kmem->dma_handle,
               kmem->alloc_size);
}

static long mem_ioctl(struct file *fp, uint32_t cmd, unsigned long arg)
{
    int ret = 0;
    /* FIXME */
    switch (cmd)
    {
        case DEV_MEM_IOC_MEMALLOC:
            ret = mutex_lock_interruptible(&dev_mem_lock);
            if (ret)
                return ret;

            ret = dev_mem_alloc(fp, cmd, arg);
            if (0 != ret)
            {
                mutex_unlock(&dev_mem_lock);
                return -ENOMEM;
            }
            mutex_unlock(&dev_mem_lock);
            break;
        case DEV_MEM_IOC_MEMFREE:
            ret = mutex_lock_interruptible(&dev_mem_lock);
            if (ret)
                return ret;

            ret = dev_mem_free(fp, cmd, arg);
            if (0 != ret)
            {
                mutex_unlock(&dev_mem_lock);
                return -EIO;
            }
            mutex_unlock(&dev_mem_lock);
            break;
        default:
            mm_err("Invalid IOCTL command specified(0x%x)\n", cmd);
            return -ENOTTY;
    }
    return 0;
}

static long mem_ioctl_dmabuf(struct file *fp,
                             unsigned int cmd,
                             unsigned long arg)
{
    int ret = 0;
    switch (cmd)
    {
        case DEV_MEM_IOC_MEMALLOCDMABUF:
            ret = mutex_lock_interruptible(&dev_mem_lock_dmabuf);
            if (ret)
                return ret;

            ret = dev_mem_alloc_dmabuf(fp, cmd, arg);
            if (0 != ret)
            {
                mutex_unlock(&dev_mem_lock_dmabuf);
                return -ENOMEM;
            }
            mutex_unlock(&dev_mem_lock_dmabuf);
            break;
        case DEV_MEM_IOC_MEMFREEDMABUF:
            ret = mutex_lock_interruptible(&dev_mem_lock_dmabuf);
            if (ret)
                return ret;

            ret = dev_mem_free_dmabuf(fp, cmd, arg);
            if (0 != ret)
            {
                mutex_unlock(&dev_mem_lock_dmabuf);
                return -EIO;
            }
            mutex_unlock(&dev_mem_lock_dmabuf);
            break;
        default:
            mm_err("Invalid IOCTL command specified(0x%x)\n", cmd);
            return -ENOTTY;
    }
    return 0;
}

static int mem_open(struct inode *inp, struct file *fp)
{
    user_proc_mem_list_t *list = NULL;
    int ret = 0;

    /* Alloc process allocation list */
    list = kzalloc(sizeof(user_proc_mem_list_t), GFP_KERNEL);
    if (!list)
    {
        mm_err("memory allocation failed\n");
        return -ENOMEM;
    }
    list->pid = current->tgid;
    ret = mutex_lock_interruptible(&dev_mem_lock);
    if (ret)
    {
        kfree(list);
        return ret;
    }
    ADD_ELEMENT_TO_END_OF_LIST(list,
                               mem_dev_numa->tail,
                               mem_dev_numa->head);
    mutex_unlock(&dev_mem_lock);
    return 0;
}

static int mem_open_dmabuf(struct inode *inp, struct file *fp)
{
    user_proc_mem_list_t *list = NULL;
    int ret = 0;

    /* Alloc process allocation list */
    list = kzalloc(sizeof(user_proc_mem_list_t), GFP_KERNEL);
    if (!list)
    {
        mm_err("memory allocation failed\n");
        return -ENOMEM;
    }
    list->pid = current->tgid;
    ret = mutex_lock_interruptible(&dev_mem_lock_dmabuf);
    if (ret)
    {
        kfree(list);
        return ret;
    }
    ADD_ELEMENT_TO_END_OF_LIST(list,
                               mem_dev_dmabuf->tail,
                               mem_dev_dmabuf->head);
    mutex_unlock(&dev_mem_lock_dmabuf);
    return 0;
}

static int mem_release(struct inode *inp, struct file *fp)
{
    mutex_lock(&dev_mem_lock);
    user_mem_free_all_pid(mem_dev_numa, current->tgid);
    mutex_unlock(&dev_mem_lock);
    return 0;
}

static int mem_release_dmabuf(struct inode *inp, struct file *fp)
{
    mutex_lock(&dev_mem_lock_dmabuf);
    user_mem_free_all_pid_dmabuf(mem_dev_dmabuf, current->tgid);
    mutex_unlock(&dev_mem_lock_dmabuf);
    return 0;
}

static struct file_operations mem_ops =
{
owner:
    THIS_MODULE,
mmap:
    mem_mmap,
unlocked_ioctl:
    mem_ioctl,
compat_ioctl:
    mem_ioctl,
open:
    mem_open,
release:
    mem_release,
};

static struct file_operations mem_ops_dmabuf =
{
owner:
    THIS_MODULE,
mmap:
    mem_mmap_dmabuf,
unlocked_ioctl:
    mem_ioctl_dmabuf,
compat_ioctl:
    mem_ioctl_dmabuf,
open:
    mem_open_dmabuf,
release:
    mem_release_dmabuf,
};

static chr_drv_info_t mem_drv_info =
{
owner:
    THIS_MODULE,
    major: 0,
min_minor:
    DEV_MEM_BASE_MINOR,
max_minor:
    DEV_MEM_MAX_MINOR,
name:
    DEV_MEM_NAME,
file_ops:
    &mem_ops,
};

static chr_drv_info_t mem_drv_info_dmabuf =
{
owner:
    THIS_MODULE,
    major: 0,
min_minor:
    DEV_MEM_BASE_MINOR,
max_minor:
    DEV_MEM_MAX_MINOR,
name:
    DEV_MEM_NAME_DMABUF,
file_ops:
    &mem_ops_dmabuf,
};

static void chr_drv_destroy_class(chr_drv_info_t *drv_info)
{
    if (NULL == drv_info)
    {
        mm_err("%s:%d Invalid parameter value\n", __func__, __LINE__);
        return;
    };
    class_destroy(drv_info->drv_class);
    drv_info->drv_class = NULL;
    return;
}

static inline void chr_drv_destroy_device(chr_drv_info_t *drv_info)
{
    if (NULL == drv_info)
    {
        mm_err("%s:%d Invalid parameter value\n", __func__, __LINE__);
        return;
    }
    if (NULL != drv_info->drv_class_dev)
    {
        device_destroy(drv_info->drv_class,
                       MKDEV(drv_info->major, DEV_MEM_BASE_MINOR));
    }
    cdev_del(&(drv_info->drv_cdev));
    unregister_chrdev_region(MKDEV(drv_info->major, DEV_MEM_BASE_MINOR),
                             drv_info->max_minor);
    return;
}

static int32_t chr_drv_create_class(chr_drv_info_t *drv_info)
{
    int ret = 0;

    drv_info->drv_class = class_create(THIS_MODULE, drv_info->name);
    if (IS_ERR_OR_NULL(drv_info->drv_class))
    {
        ret = PTR_ERR(drv_info->drv_class);
        mm_err("%s:%d class_create failed, return=%d\n\n",
               __func__, __LINE__, ret);
        return -ENODEV;
    }
    return 0;
}

static int chr_drv_create_device(chr_drv_info_t *drv_info)
{
    int ret = 0;
    dev_t devid = 0;

    ret = alloc_chrdev_region(&devid,
                              drv_info->min_minor,
                              drv_info->max_minor,
                              drv_info->name);
    if (unlikely(ret))
    {
        mm_err("%s:%d Unable to allocate chrdev region\n",
               __func__, __LINE__);
        return -ENOMEM;
    }
    drv_info->major = MAJOR(devid);
    drv_info->drv_cdev.owner = THIS_MODULE;
    cdev_init(&(drv_info->drv_cdev), drv_info->file_ops);
    ret = cdev_add(&(drv_info->drv_cdev), devid, drv_info->max_minor);
    if (unlikely(ret))
    {
        mm_err("%s:%d cdev add failed\n", __func__, __LINE__);
        chr_drv_destroy_device(drv_info);
        return -ENOENT;
    }
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27))
    drv_info->drv_class_dev = device_create(
                                  drv_info->drv_class,
                                  NULL,
                                  MKDEV(drv_info->major, DEV_MEM_BASE_MINOR),
                                  NULL,
                                  "%s",
                                  drv_info->name);
#else
    drv_info->drv_class_dev = device_create(
                                  drv_info->drv_class,
                                  NULL,
                                  MKDEV(drv_info->major, DEV_MEM_BASE_MINOR),
                                  drv_info->name);
#endif
    if (NULL == drv_info->drv_class_dev)
    {
        mm_err("%s:%d device_create failed\n", __func__, __LINE__);
        chr_drv_destroy_device(drv_info);
        return -ENOMEM;
    }
    return 0;
}

static int32_t register_device_driver(void)
{
    int ret = 0;

    mutex_init(&dev_mem_lock);

    mem_dev_numa = kzalloc(sizeof(user_mem_dev_t), GFP_KERNEL);
    if (!mem_dev_numa)
    {
        mm_err("failed to allocate memory for numa mem device\n");
        return -ENOMEM;
    }

    ret = chr_drv_create_class(&mem_drv_info);
    if (unlikely(ret))
    {
        mm_err("failed to create device driver class\n");
        kfree(mem_dev_numa);
        return -ENODEV;
    }
    ret = chr_drv_create_device(&mem_drv_info);
    if (unlikely(ret))
    {
        mm_err("failed to create mem numa device driver\n");
        chr_drv_destroy_class(&mem_drv_info);
        kfree(mem_dev_numa);
        return -ENODEV;
    }
    return 0;
}

static int32_t register_device_driver_dmabuf(void)
{
    int ret = 0;
    /* dma_mask_bit module parameter */
    int dma_mask_bit = 32;

    mutex_init(&dev_mem_lock_dmabuf);

    mem_dev_dmabuf = kzalloc(sizeof(user_mem_dev_t), GFP_KERNEL);
    if (!mem_dev_dmabuf)
    {
        mm_err("failed to allocate memory for dmabuf mem device\n");
        return -ENOMEM;
    }

    ret = chr_drv_create_class(&mem_drv_info_dmabuf);
    if (unlikely(ret))
    {
        mm_err("failed to create device driver class\n");
        kfree(mem_dev_dmabuf);
        return -ENODEV;
    }

    ret = chr_drv_create_device(&mem_drv_info_dmabuf);
    if (unlikely(ret))
    {
        mm_err("failed to create mem dmabuf device driver\n");
        chr_drv_destroy_class(&mem_drv_info_dmabuf);
        kfree(mem_dev_dmabuf);
        return -ENODEV;
    }

    mem_drv_info_dmabuf.dma_dev = get_device(mem_drv_info_dmabuf.drv_class_dev);

    /* set this->dma_dev->dma_mask */
    if (mem_drv_info_dmabuf.dma_dev->dma_mask == NULL)
    {
        mem_drv_info_dmabuf.dma_dev->dma_mask =
            &mem_drv_info_dmabuf.dma_dev->coherent_dma_mask;

        mm_info("%s:%d dma_mask = coherent_dma_mask\n", __func__, __LINE__);
    }
    /** set *this->dma_dev->dma_mask and this->dma_dev->coherent_dma_mask
    * Executing dma_set_mask_and_coherent() before of_dma_configure() may fail.
    * Because dma_set_mask_and_coherent() will fail unless dev->dma_ops is set.
    * When dma_set_mask_and_coherent() fails, it is forcefuly setting
    * the dma-mask value. */
    if (*mem_drv_info_dmabuf.dma_dev->dma_mask == 0)
    {
        int ret = dma_set_mask_and_coherent(mem_drv_info_dmabuf.dma_dev,
                                            DMA_BIT_MASK(dma_mask_bit));
        if (ret != 0)
        {
            mm_err("dma_set_mask_and_coherent(DMA_BIT_MASK(%d)) failed. "
                   "return=(%d)\n", dma_mask_bit, ret);
            *mem_drv_info_dmabuf.dma_dev->dma_mask         =
                DMA_BIT_MASK(dma_mask_bit);
            mem_drv_info_dmabuf.dma_dev->coherent_dma_mask =
                DMA_BIT_MASK(dma_mask_bit);
        }

        mm_info("%s:%d dma_set_mask_and_coherent\n", __func__, __LINE__);
    }
    return 0;
}

/* unregister the numa device driver */
static void unregister_device_driver(void)
{
    if (!mem_drv_info.unregistered)
    {
        chr_drv_destroy_device(&mem_drv_info);
        chr_drv_destroy_class(&mem_drv_info);

        user_mem_free_all(mem_dev_numa);
        kfree(mem_dev_numa);
        mem_dev_numa = NULL;

        mem_drv_info.unregistered = 1;
    }
    return;
}

/* unregister the dmabuf device driver */
static void unregister_device_driver_dmabuf(void)
{
    if (!mem_drv_info_dmabuf.unregistered)
    {
        put_device(mem_drv_info_dmabuf.dma_dev);
        chr_drv_destroy_device(&mem_drv_info_dmabuf);
        chr_drv_destroy_class(&mem_drv_info_dmabuf);

        user_mem_free_all_dmabuf(mem_dev_dmabuf);
        kfree(mem_dev_dmabuf);
        mem_dev_dmabuf = NULL;

        mem_drv_info_dmabuf.unregistered = 1;
    }
    return;
}

static int __init coherent_module_init(void)
{
    mm_info("Loading coherent Module...\n");
    if (register_device_driver())
    {
        pr_err("Error register NUMA device\n");
        return -1;
    }
    if (register_device_driver_dmabuf())
    {
        pr_err("Error register DMABUF device\n");
        goto unregister_numa_device;
    }
    return 0;

unregister_numa_device:
    unregister_device_driver();
    return -1;
}

static void coherent_module_exit(void)
{
    mm_info("Unloading coherent Module...\n");
    unregister_device_driver();
    unregister_device_driver_dmabuf();
}

module_init(coherent_module_init);
module_exit(coherent_module_exit);
MODULE_LICENSE("GPL");
