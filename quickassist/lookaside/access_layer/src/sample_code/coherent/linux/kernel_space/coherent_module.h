
#ifndef COHERENT_MODULE_H_
#define COHERENT_MODULE_H_

/* Kernel space memory information structure. */
typedef struct kdev_mem_info_s
{
    /* Id of this block */
    uint32_t id;
    /* Node id for NUMA, shared b/w user/kernel */
    int64_t nodeId;
    /* Size of this block (bytes), shared b/w user/kernel */
    uint64_t size;
    /* Size of allocation size */
    uint64_t alloc_size;
    /* Size used to call mmap (bytes) */
    uint32_t mmap_size;
    /* Physical address of the area, shared b/w user/kernel */
    uint64_t phy_addr;

    /*----------------------------------NUMA----------------------------------*/

    /* kernel space only */
    union
    {
        void *kmalloc_ptr;
        uint64_t padding_kmalloc_ptr;
    };
    /* Pointer to mem originally returned by kmalloc, kernel space only  */
    union
    {
        int32_t *kmalloc_area;
        uint64_t padding_kmalloc_area;
    };
    /* virtual address return from mmap, and it is saved for munmap*/
    /* Please be noted that padding should be used to make the same
       structure size for both 32 bit and 64 bit */
    union
    {
        void *fvirt_addr;
        uint64_t padding_fvirt_addr;
    };

    /*----------------------------------DMA-----------------------------------*/

    /* kernel space only */
    dma_addr_t dma_handle;
    /* kernel space only */
    union
    {
        void *cpu_addr;
        uint64_t padding_cpu_addr;
    };
    /* Pointer to mem originally returned by dma_alloc_coherent, */
    /* kernel space only */
    union
    {
        int32_t *cpu_addr_area;
        uint64_t padding_cpu_addr_area;
    };

    /* kernel space only */
    void *mem_info;

    union
    {
        struct kdev_mem_info_s *pPrev;
        uint64_t padding_pPrev;
    };
    union
    {
        struct kdev_mem_info_s *pNext;
        uint64_t padding_pNext;
    };
} kdev_mem_info_t;

typedef struct user_proc_mem_list_s
{
    int pid;
    uint64_t allocs_nr;
    uint32_t max_id;
    kdev_mem_info_t *head;
    kdev_mem_info_t *tail;
    struct user_proc_mem_list_s *pPrev;
    struct user_proc_mem_list_s *pNext;
} user_proc_mem_list_t;

typedef struct user_mem_dev_s
{
    user_proc_mem_list_t *head;
    user_proc_mem_list_t *tail;
} user_mem_dev_t;

#define COHERENT_MOD    "coherent_module: "
#define mm_err(...)     pr_err(COHERENT_MOD __VA_ARGS__)
#define mm_info(...)    pr_info(COHERENT_MOD __VA_ARGS__)
#define mm_warning(...) pr_warn(COHERENT_MOD __VA_ARGS__)

#endif
