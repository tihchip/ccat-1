
#ifndef COHERENT_OPTIONS_H
#define COHERENT_OPTIONS_H

#include "cpa.h"
#include "lac_list.h"

/* Helper option declaration macros */
#define COHERENT_OPT(n, d, v, t)                                               \
    { .name = (n),                                                             \
      .desc = (d),                                                             \
      .type = COHERENT_ARG_TYPE_##t,                                           \
      .value = (v) }

#define COHERENT_OPT_END { .type = COHERENT_TYPE_NULL }

/* Option types definition */
typedef enum
{
    COHERENT_ARG_TYPE_NULL,
    COHERENT_ARG_TYPE_BOOL,
    COHERENT_ARG_TYPE_INT,
    COHERENT_ARG_TYPE_SIZE,
    COHERENT_ARG_TYPE_DOUBLE,
    COHERENT_ARG_TYPE_STRING,
    COHERENT_ARG_TYPE_LIST,
    COHERENT_ARG_TYPE_FILE,
    COHERENT_ARG_TYPE_MAX
} coherent_arg_type_t;

/* Test option definition */
typedef struct
{
    char                *name;
    char                *desc;
    char                *value;
    coherent_arg_type_t  type;
} coherent_option_t;

/* Initilize options library */
int coherentOptionsInit(void);

/* Release resource allocated by the options library */
int coherentOptionsDone(void);

/* Set value 'value' of type 'type' for option 'name' */
coherent_option_t *coherentSetOption(const char *name, const char *value,
                                     coherent_arg_type_t type);

/* Find option specified by 'name' */
coherent_option_t *coherentFindOption(const char *name);

coherent_option_t *coherentAddOption(const char *name);

const char *coherentGetValueString(const char *name);

unsigned long long coherentGetValueSize(const char *name);

int coherentGetValueFlag(const char *name);

char *coherentNewValue(const char *data);

void coherentFreeValue(char *data);

#endif

