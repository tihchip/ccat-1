
#ifndef COHERENT_MEM_UTILS_H
#define COHERENT_MEM_UTILS_H

void *coherentMemAlignFromLocal(size_t size, size_t alignment);
void *coherentMemAlignFromNUMA(size_t size, size_t alignment);
void *coherentMemAlignFromDMABuf(size_t size, size_t alignment);

void coherentFreeToLocal(void *pVirtAddress);
void coherentFreeToNUMA(void *pVirtAddress);
void coherentFreeToDMABuf(void *pVirtAddress);

#endif

