
#ifndef COHERENT_CK_PR_H
#define COHERENT_CK_PR_H

#if defined(__x86_64__)
#include <ck_pr.h>
#include <ck_cc.h>
#include <ck_md.h>

/*
 * Don't have to use these function by default.
 * pgprot_noncached()/pgprot_writecombine()/pgprot_dmacoherent() is
 * smart enough to control and mantainence the internal L1 D-Cache of the core.
 */
static inline __attribute__((always_inline))
void ck_pr_l1_inval_Dcachelines(size_t start, size_t end)
{
    return;
}

static inline __attribute__((always_inline))
void ck_pr_l1_wbinval_Dcachelines(size_t start, size_t end)
{
    return;
}

#elif defined(__riscv)
#include "include/arch/riscv/page.h"
#include "include/arch/riscv/cache.h"
#include "include/arch/riscv/nmsis_core.h"

#define ck_pr_load_64(SRC)       __LD((SRC))
#define ck_pr_store_64(DST, VAL) __SD((DST), (VAL))

static inline __attribute__((always_inline))
void ck_pr_barrier(void)
{
    __asm__ __volatile__("" ::: "memory");
    return;
}

static inline __attribute__((always_inline))
void ck_pr_l1_inval_Dcachelines(size_t start, size_t end)
{
    start = _ALIGN_DOWN(start, CACHE_LINE_SIZE);
    end = _ALIGN_UP(end, CACHE_LINE_SIZE);

    while (start < end)
    {
        UInvalDCacheLine(start);
        start += CACHE_LINE_SIZE;
    }
    return;
}

static inline __attribute__((always_inline))
void ck_pr_l1_wbinval_Dcachelines(size_t start, size_t end)
{
    start = _ALIGN_DOWN(start, CACHE_LINE_SIZE);
    end = _ALIGN_UP(end, CACHE_LINE_SIZE);

    while (start < end)
    {
        UFlushInvalDCacheLine(start);
        start += CACHE_LINE_SIZE;
    }
    return;
}

#elif !defined(__GNUC__)
#error Your platform is unsupported
#endif

#endif

