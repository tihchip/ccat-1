
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <malloc.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "include/coherent.h"

static int numa_fd   = 0;
static int dmabuf_fd = 0;

static dev_mem_info_t *pUserMemList = NULL;
static dev_mem_info_t *pUserMemListHead = NULL;

static dev_mem_info_t *pUserMemListDmabuf = NULL;
static dev_mem_info_t *pUserMemListHeadDmabuf = NULL;

int userMemListAdd(dev_mem_info_t *pMemInfo)
{
    ADD_ELEMENT_TO_HEAD_OF_LIST(pMemInfo, pUserMemList, pUserMemListHead);
    return 0;
}

int userMemListAddDMABuf(dev_mem_info_t *pMemInfo)
{
    ADD_ELEMENT_TO_HEAD_OF_LIST(pMemInfo,
                                pUserMemListDmabuf,
                                pUserMemListHeadDmabuf);
    return 0;
}

dev_mem_info_t *userMemLookupByVirtAddr(void *virt_addr)
{
    dev_mem_info_t *pCurr = NULL;

    for (pCurr = pUserMemListHead; pCurr != NULL; pCurr = pCurr->pNext)
    {
        if ((uint64_t)pCurr->virt_addr <= (uint64_t)virt_addr &&
                ((uint64_t)pCurr->virt_addr + pCurr->size) > (uint64_t)virt_addr)
        {
            return pCurr;
        }
    }
    return NULL;
}

dev_mem_info_t *userMemLookupByVirtAddrDMABuf(void *virt_addr)
{
    dev_mem_info_t *pCurr = NULL;

    for (pCurr = pUserMemListHeadDmabuf; pCurr != NULL; pCurr = pCurr->pNext)
    {
        if ((uint64_t)pCurr->virt_addr <= (uint64_t)virt_addr &&
                ((uint64_t)pCurr->virt_addr + pCurr->size) > (uint64_t)virt_addr)
        {
            return pCurr;
        }
    }
    return NULL;
}

void userMemListFree(dev_mem_info_t *pMemInfo)
{
    dev_mem_info_t *pCurr = NULL;

    for (pCurr = pUserMemListHead; pCurr != NULL; pCurr = pCurr->pNext)
    {
        if (pCurr == pMemInfo)
        {
            REMOVE_ELEMENT_FROM_LIST(pCurr, pUserMemList, pUserMemListHead);
            break;
        }
    }
    return;
}

void userMemListFreeDMABuf(dev_mem_info_t *pMemInfo)
{
    dev_mem_info_t *pCurr = NULL;

    for (pCurr = pUserMemListHeadDmabuf; pCurr != NULL; pCurr = pCurr->pNext)
    {
        if (pCurr == pMemInfo)
        {
            REMOVE_ELEMENT_FROM_LIST(pCurr,
                                     pUserMemListDmabuf,
                                     pUserMemListHeadDmabuf);
            break;
        }
    }
    return;
}

void *coherentMemAlignFromLocal(size_t size, size_t alignment)
{
    return memalign(alignment, size);
}

void *coherentMemAlignFromNUMA(size_t size, size_t alignment)
{
    int ret = 0;
    dev_mem_info_t *pMemInfo = NULL;
    void *pVirtAddress = NULL;

    if (size == 0 || alignment == 0)
    {
        printf("Invalid size or alignment parameter \n");
        return NULL;
    }

    numa_fd = open(COHERENT_SAMPLE_MEM, O_RDWR);
    if (numa_fd < 0)
    {
        printf("unable to open %s %d\n", COHERENT_SAMPLE_MEM, numa_fd);
        return NULL;
    }

    pMemInfo = malloc(sizeof(dev_mem_info_t));
    if (NULL == pMemInfo)
    {
        printf("unable to allocate pMemInfo buffer\n");
        return NULL;
    }
    pMemInfo->size = size;
    pMemInfo->nodeId = -1;

    /* Try to allocate memory as size */
    ret = ioctl(numa_fd, DEV_MEM_IOC_MEMALLOC, pMemInfo);
    if (ret != 0)
    {
        printf("ioctl memory alloc failed, ret = %d\n", ret);
        free(pMemInfo);
        return NULL;
    }

    pMemInfo->virt_addr = mmap((caddr_t)0,
                               pMemInfo->size,
                               PROT_READ | PROT_WRITE,
                               MAP_SHARED,
                               numa_fd,
                               (pMemInfo->id * getpagesize()));
    if (pMemInfo->virt_addr == (caddr_t)MAP_FAILED)
    {
        printf("mmap failed\n");
        ret = ioctl(numa_fd, DEV_MEM_IOC_MEMFREE, pMemInfo);
        if (ret != 0)
        {
            printf("ioctl DEV_MEM_IOC_MEMFREE call failed, ret = %d\n", ret);
        }
        free(pMemInfo);
        return NULL;
    }

    pMemInfo->allocations = 1;
    pVirtAddress = pMemInfo->virt_addr;

    if (0 != userMemListAdd(pMemInfo))
    {
        printf("Error on mem list add\n");
        return NULL;
    }
    return pVirtAddress;
}

void *coherentMemAlignFromDMABuf(size_t size, size_t alignment)
{
    int ret = 0;
    dev_mem_info_t *pMemInfo = NULL;
    void *pVirtAddress = NULL;

    if (size == 0 || alignment == 0)
    {
        printf("Invalid size or alignment parameter \n");
        return NULL;
    }

    dmabuf_fd = open(COHERENT_SAMPLE_MEM_DMABUF, O_RDWR);
    if (dmabuf_fd < 0)
    {
        printf("unable to open %s %d\n", COHERENT_SAMPLE_MEM_DMABUF, dmabuf_fd);
        return NULL;
    }

    pMemInfo = malloc(sizeof(dev_mem_info_t));
    if (NULL == pMemInfo)
    {
        printf("unable to allocate pMemInfo buffer\n");
        return NULL;
    }
    pMemInfo->size = size;
    pMemInfo->nodeId = -1;

    /* Try to allocate memory as size */
    ret = ioctl(dmabuf_fd, DEV_MEM_IOC_MEMALLOCDMABUF, pMemInfo);
    if (ret != 0)
    {
        printf("ioctl memory alloc failed, ret = %d\n", ret);
        free(pMemInfo);
        return NULL;
    }

    pMemInfo->virt_addr = mmap((caddr_t)0,
                               pMemInfo->size,
                               PROT_READ | PROT_WRITE,
                               MAP_SHARED,
                               dmabuf_fd,
                               (pMemInfo->id * getpagesize()));
    if (pMemInfo->virt_addr == (caddr_t)MAP_FAILED)
    {
        printf("mmap failed\n");
        ret = ioctl(dmabuf_fd, DEV_MEM_IOC_MEMFREEDMABUF, pMemInfo);
        if (ret != 0)
        {
            printf("ioctl DEV_MEM_IOC_MEMFREE call failed, ret = %d\n", ret);
        }
        free(pMemInfo);
        return NULL;
    }

    pMemInfo->allocations = 1;
    pVirtAddress = pMemInfo->virt_addr;

    if (0 != userMemListAddDMABuf(pMemInfo))
    {
        printf("Error on mem list add\n");
        return NULL;
    }
    return pVirtAddress;
}

void coherentFreeToLocal(void *pVirtAddress)
{
    if (pVirtAddress)
        free(pVirtAddress);
}

void coherentFreeToNUMA(void *pVirtAddress)
{
    int ret = 0;
    dev_mem_info_t *pMemInfo = NULL;

    if (NULL == pVirtAddress)
    {
        printf("Invalid numa virtual address\n");
        return;
    }

    if ((pMemInfo = userMemLookupByVirtAddr(pVirtAddress)) != NULL)
    {
        pMemInfo->allocations -= 1;
        if (pMemInfo->allocations != 0)
        {
            return;
        }
    }
    else
    {
        printf("userMemLookupByVirtAddr failed\n");
        return;
    }

    ret = munmap(pMemInfo->virt_addr, pMemInfo->size);
    if (ret != 0)
    {
        printf("numa munmap failed, ret = %d\n", ret);
    }

    ret = ioctl(numa_fd, DEV_MEM_IOC_MEMFREE, pMemInfo);
    if (ret != 0)
    {
        printf("numa ioctl call failed, ret = %d\n", ret);
    }
    close(numa_fd);

    userMemListFree(pMemInfo);
    free(pMemInfo);
    return;
}

void coherentFreeToDMABuf(void *pVirtAddress)
{
    int ret = 0;
    dev_mem_info_t *pMemInfo = NULL;

    if (NULL == pVirtAddress)
    {
        printf("Invalid dmabuf virtual address\n");
        return;
    }

    if ((pMemInfo = userMemLookupByVirtAddrDMABuf(pVirtAddress)) != NULL)
    {
        pMemInfo->allocations -= 1;
        if (pMemInfo->allocations != 0)
        {
            return;
        }
    }
    else
    {
        printf("%s:%d failed\n", __func__, __LINE__);
        return;
    }

    ret = munmap(pMemInfo->virt_addr, pMemInfo->size);
    if (ret != 0)
    {
        printf("dmabuf munmap failed, ret = %d\n", ret);
    }

    ret = ioctl(dmabuf_fd, DEV_MEM_IOC_MEMFREEDMABUF, pMemInfo);
    if (ret != 0)
    {
        printf("dmabuf ioctl call failed, ret = %d\n", ret);
    }
    close(dmabuf_fd);

    userMemListFreeDMABuf(pMemInfo);
    free(pMemInfo);
    return;
}

int coherentNUMAVerify(void *pVirtAddress, size_t size)
{
    /* FIXME */
    //uint32_t csum = coherent_csum(pVirtAddress, size);
    return 0;
}

int coherentDMABufVerify(void *pVirtAddress, size_t size)
{
    /* FIXME */
    return 0;
}

