
#ifndef COHERENT_STAT_H
#define COHERENT_STAT_H

#include <ctype.h>
#include <unistd.h>
#include <inttypes.h>

/* global variables */
typedef struct
{
    uint64_t max_events;   /* maximum number of events to execute */
    uint64_t max_time_ns;  /* total execution time limit */
} coherent_globals_t;

/* Statistics */
typedef struct
{
    uint32_t threads_running;   /* Number of active threads */

    double   time_interval;     /* Time elapsed since the last report */
    double   time_total;        /* Time elapsed since the benchmark start */

    double   latency_pct;       /* Latency percentile */

    double   latency_min;       /* Minimum latency (cumulative reports only) */
    double   latency_max;       /* Maximum latency (cumulative reports only) */
    double   latency_avg;       /* Average latency (cumulative reports only) */
    double   latency_sum;       /* Sum latency (cumulative reports only) */

    uint64_t events;            /* Number of executed events */
    uint64_t reads;             /* Number of read operations */
    uint64_t writes;            /* Number of write operations */
    uint64_t other;             /* Number of other operations */
    uint64_t errors;            /* Number of ignored errors */
    uint64_t reconnects;        /* Number of reconnects to server */

    uint64_t bytes_read;        /* Bytes read */
    uint64_t bytes_written;     /* Bytes written */

    uint64_t queue_length;      /* Event queue length (tx_rate-only) */
    uint64_t concurrency;       /* Number of in-flight events (tx_rate-only) */

    uint32_t st_l1hit;          /* count of stores that hit L1D */
    uint32_t st_l1miss;         /* count of stores that miss L1D */

    uint32_t ld_miss;           /* count of loads that miss */
    uint32_t ld_l1hit;          /* count of loads that hit L1D */
    uint32_t ld_l2hit;          /* count of loads that hit L2D */
    uint32_t ld_llchit;         /* count of loads that hit LLC */
} coherent_stat_t;

#endif

