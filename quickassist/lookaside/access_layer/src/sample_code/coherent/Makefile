#########################################################################
#
# @par
# This file is provided under a dual BSD/GPLv2 license.  When using or
#   redistributing this file, you may do so under either license.
# 
#   GPL LICENSE SUMMARY
# 
#   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
# 
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of version 2 of the GNU General Public License as
#   published by the Free Software Foundation.
# 
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
#   The full GNU General Public License is included in this distribution
#   in the file called LICENSE.GPL.
# 
#   Contact Information:
#   Intel Corporation
# 
#   BSD LICENSE
# 
#   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
#   All rights reserved.
# 
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions
#   are met:
# 
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in
#       the documentation and/or other materials provided with the
#       distribution.
#     * Neither the name of Intel Corporation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
# 
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
#  version: QAT1.7.L.4.13.0-00009
############################################################################


####################Common variables and definitions########################

CC?=gcc

COHERENTBUILD_BINS = -1

COHERENTBUILD_BINS = $(shell echo -e "\#include <qat/cpa.h>\n void main () {}" \
		| $(CC) -lqat -lusdm  -xc - -o /dev/null 2> /dev/null; \
		echo $$?)

ifeq ($(COHERENTBUILD_BINS),0)
     SC_SYM_SESSION_INUSE_DISABLED=0
     ifeq ($(ICP_BUILDSYSTEM_PATH),)
         -include build.mk
     endif
endif

ifneq ($(COHERENTBUILD_BINS),0)
    ICP_ENV_DIR?=$(ICP_ROOT)/quickassist/build_system/build_files/env_files
    ICP_BUILDSYSTEM_PATH?=$(ICP_ROOT)/quickassist/build_system

    #Add your project environment Makefile
    include $(ICP_ENV_DIR)/environment.mk
    #Lac include paths
    include $(LAC_DIR)/common.mk
    ICP_OS_LEVEL?=kernel_space

else
    ICP_OS_LEVEL=user_space
    ICP_OS?=linux_2.6
endif

ICP_OS?=linux
ICP_BUILD_OUTPUT?=$(ICP_ROOT)/build

ifeq ($(ICP_OS),linux_2.6)
OS=linux
else
OS=freebsd
endif

ifneq ($(COHERENTBUILD_BINS),0)
    USDM_SAMPLE_SRC_ROOT ?= $(ICP_ROOT)/quickassist/lookaside/access_layer/src/sample_code/usdm/usdm_sample_code
    SAMPLE_BUILD_OUTPUT?=$(USDM_SAMPLE_SRC_ROOT)/../build
    #include the makefile with all the default and common Make variable definitions
    include $(ICP_BUILDSYSTEM_PATH)/build_files/common.mk
else
    USDM_SAMPLE_SRC_ROOT=$(shell pwd)
    SAMPLE_BUILD_OUTPUT=$(USDM_SAMPLE_SRC_ROOT)/../build
endif

ifeq ($(COHERENTBUILD_BINS),0)
    -include $(ICP_ENV_DIR)/environment.mk
    -include $(LAC_DIR)/common.mk
    -include $(ICP_BUILDSYSTEM_PATH)/build_files/common.mk
    -include $(ICP_ENV_DIR)/$(ICP_OS)_$(ICP_OS_LEVEL).mk
endif

#Add the name for the executable, Library or Module output definitions
OUTPUT_NAME=coherent

ifeq ($(ICP_OS_LEVEL),user_space)
SOURCES:= coherent_main.c       \
          coherent_api.c        \
          coherent_mem_utils.c  \
          coherent_options.c    \
          coherent_timer.c

EXTRA_CFLAGS += -g -DUSER_SPACE -D_GNU_SOURCE
EXTRA_LDFLAGS += -L$(ICP_BUILD_OUTPUT)

ifneq ($(COHERENTBUILD_BINS),0)
    ADDITIONAL_OBJECTS += $(ICP_BUILD_OUTPUT)/libusdm_drv_s.so
    ADDITIONAL_OBJECTS += $(ICP_BUILD_OUTPUT)/libqat_s.so
else
    ADDITIONAL_OBJECTS += -lusdm -lqat
endif

ADDITIONAL_OBJECTS += -lreadline

ifneq ($(COHERENTBUILD_BINS),0)
#include your $(ICP_OS)_$(ICP_OS_LEVEL).mk file
include $(ICP_ENV_DIR)/$(ICP_OS)_$(ICP_OS_LEVEL).mk
endif
endif

ifeq ($(ICP_OS_LEVEL),kernel_space)
MODULE_SOURCES:= $(OS)/$(ICP_OS_LEVEL)/coherent_module.c
endif

# On the line directly below list the outputs you wish to build for
ifeq ($(ICP_OS_LEVEL),user_space)
install: exe
all: exe
else
install: module
endif
.PHONY : clean
clean: usdm_clean
usdm_clean:
	@echo ; echo "Cleaning usdm generated files. $(USDM_SAMPLE_SRC_ROOT) ";

###################Include rules makefiles########################
-include $(ICP_BUILDSYSTEM_PATH)/build_files/rules.mk
###################End of Rules inclusion#########################

