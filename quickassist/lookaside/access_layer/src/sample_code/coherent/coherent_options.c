
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "coherent_options.h"

/* Global options list */
static sal_list_t *options = NULL;
sal_list_t *options_tail = NULL;

/* List of size modifiers (kilo, mega, giga, tera) */
static const char sizemods[] = "KMGT";

char *coherentNewValue(const char *data)
{
    char *newval;

    if (data == NULL)
        return NULL;

    if ((newval = strdup(data)) == NULL)
    {
        return NULL;
    }

    return newval;
}

void coherentFreeValue(char *data)
{
    if (data == NULL)
        return;

    free(data);
    return;
}

static coherent_option_t *coherentNewOption(void)
{
    coherent_option_t *newopt;

    newopt = (coherent_option_t *)malloc(sizeof(coherent_option_t));
    if (newopt != NULL)
    {
        memset(newopt, 0, sizeof(coherent_option_t));
    }

    return newopt;
}

void coherentFreeOption(coherent_option_t *option)
{
    if (option == NULL)
        return ;

    coherentFreeValue(option->name);
    option->name = NULL;

    coherentFreeValue(option->value);
    option->value = NULL;

    free(option);
    return;
}

/* Initialize options library */
int coherentOptionsInit(void)
{
    return 0;
}

static void coherentFreeOptions(void)
{
    sal_list_t *next_element = options;

    while (next_element)
    {
        coherent_option_t *opt = NULL;

        opt = (coherent_option_t *)SalList_getObject(next_element);
        coherentFreeOption(opt);
        SalList_delObject(&next_element);
        next_element = SalList_next(next_element);
    }

    SalList_free(&options);

    options = NULL;
    options_tail = NULL;
    return;
}

/* Release resource allocated by the options library */
int coherentOptionsDone(void)
{
    coherentFreeOptions();
    return 0;
}

static coherent_option_t *findOption(const char *name)
{
    sal_list_t *curr_element = options;
    coherent_option_t *opt = NULL;

    if (name == NULL)
        return NULL;

    while (NULL != curr_element)
    {
        opt = (coherent_option_t *)SalList_getObject(curr_element);
        if (!strcmp(opt->name, name))
            return opt;

        curr_element = SalList_next(curr_element);
    }
    return NULL;
}

coherent_option_t *coherentFindOption(const char *name)
{
    return findOption(name);
}

coherent_option_t *coherentAddOption(const char *name)
{
    coherent_option_t *option;

    if (name == NULL)
        return NULL;

    if ((option = findOption(name)) != NULL)
        return option;

    if ((option = coherentNewOption()) == NULL)
        return NULL;

    option->name = strdup(name);

    if (CPA_STATUS_SUCCESS != SalList_add(&options, &options_tail, option))
        return NULL;

    return option;
}

coherent_option_t *coherentSetOption(const char *name,
                                     const char *value,
                                     coherent_arg_type_t type)
{
    coherent_option_t *option;

    option = coherentAddOption(name);
    if (option == NULL)
    {
        printf("usdmAddOption == NULL\n");
        return NULL;
    }
    option->type = type;

    if (type != COHERENT_ARG_TYPE_BOOL && (value == NULL || value[0] == '\0'))
        return option;

    switch (type)
    {
        case COHERENT_ARG_TYPE_BOOL:
            if (value == NULL || !strcmp(value, "on") ||
                    !strcmp(value, "true") || !strcmp(value, "1"))
            {
                option->value = coherentNewValue(value);
            }
            else if (strcmp(value, "off") && strcmp(value, "false") &&
                     strcmp(value, "0"))
            {
                return NULL;
            }
            break;

        case COHERENT_ARG_TYPE_INT:
        case COHERENT_ARG_TYPE_SIZE:
        case COHERENT_ARG_TYPE_DOUBLE:
        case COHERENT_ARG_TYPE_STRING:
            option->value = coherentNewValue(value);
            break;

        default:
            printf("Unknown argument type: %d", type);
            return NULL;
    }

    return option;
}

const char *coherentOptToString(coherent_option_t *opt)
{
    return opt->value;
}

const char *coherentGetValueString(const char *name)
{
    coherent_option_t *opt;

    opt = findOption(name);
    if (opt == NULL)
        return NULL;

    return coherentOptToString(opt);
}

unsigned long long coherentOptToSize(coherent_option_t *opt)
{
    const char *c;
    char mult = 0;
    int rc;
    unsigned int i, n;
    unsigned long long res = 0;

    /* Reimplentation of sscanf(val->data, "%llu%c", &res, &mult), since
     * there is no standard on how to specify long long values */
    res = 0;
    for (rc = 0, c = opt->value; *c != '\0'; c++)
    {
        if (*c < '0' || *c > '9')
        {
            if (rc == 1)
            {
                rc = 2;
                mult = *c;
            }
            break;
        }
        rc = 1;
        res = res * 10 + *c - '0';
    }

    if (rc == 2)
    {
        for (n = 0; sizemods[n] != '\0'; n++)
            if (toupper(mult) == sizemods[n])
                break;
        if (sizemods[n] != '\0')
        {
            for (i = 0; i <= n; i++)
                res *= 1024;
        }
        else
            res = 0; /* Unknown size modifier */
    }

    return res;
}

unsigned long long coherentGetValueSize(const char *name)
{
    coherent_option_t *opt;

    opt = findOption(name);
    if (opt == NULL)
        return 0;

    return coherentOptToSize(opt);
}

static int coherentOptToFlag(coherent_option_t *opt)
{
    return !(opt->value == NULL);
}

int coherentGetValueFlag(const char *name)
{
    coherent_option_t *opt;

    opt = findOption(name);
    if (opt == NULL)
        return 0;

    return coherentOptToFlag(opt);
}

