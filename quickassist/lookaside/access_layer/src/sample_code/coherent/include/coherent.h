
#ifndef COHERENT_H
#define COHERENT_H

/* user space memory information structure */
typedef struct dev_mem_info_s
{
    /* Id of this block */
    uint32_t id;
    /* Node id for NUMA, shared b/w user/kernel */
    int64_t nodeId;
    /* Size of this block (bytes), shared b/w user/kernel */
    uint64_t size;
    /* Counter keeping track of number of allocations, user space only */
    uint32_t allocations;

    /* Physical address of the kmalloced area, shared b/w user/kernel */
    uint64_t phy_addr;
    /* Physical address of the kmalloced area */
    union
    {
        void *virt_addr; /* user space only */
        uint64_t padding_virt;
    };

    union
    {
        struct dev_mem_info_s *pPrev;
        uint64_t padding_pPrev;
    };
    union
    {
        struct dev_mem_info_s *pNext;
        uint64_t padding_pNext;
    };
} dev_mem_info_t;

#define ADD_ELEMENT_TO_HEAD_OF_LIST(elementtoadd, currentptr, headptr)         \
    do                                                                         \
    {                                                                          \
        if (NULL == currentptr)                                                \
        {                                                                      \
            currentptr = elementtoadd;                                         \
            elementtoadd->pNext = NULL;                                        \
            elementtoadd->pPrev = NULL;                                        \
        }                                                                      \
        else                                                                   \
        {                                                                      \
            elementtoadd->pNext = headptr;                                     \
            elementtoadd->pPrev = NULL;                                        \
            headptr->pPrev = elementtoadd;                                     \
        }                                                                      \
        headptr = elementtoadd;                                                \
    } while (0);

#define ADD_ELEMENT_TO_END_OF_LIST(elementtoadd, currentptr, headptr)          \
    do                                                                         \
    {                                                                          \
        if (NULL == currentptr)                                                \
        {                                                                      \
            currentptr = elementtoadd;                                         \
            elementtoadd->pNext = NULL;                                        \
            elementtoadd->pPrev = NULL;                                        \
            headptr = currentptr;                                              \
        }                                                                      \
        else                                                                   \
        {                                                                      \
            elementtoadd->pPrev = currentptr;                                  \
            currentptr->pNext = elementtoadd;                                  \
            elementtoadd->pNext = NULL;                                        \
            currentptr = elementtoadd;                                         \
        }                                                                      \
    } while (0);

#define REMOVE_ELEMENT_FROM_LIST(elementtoremove, currentptr, headptr)         \
    do                                                                         \
    {                                                                          \
        /*If the previous pointer is not NULL*/                                \
        if (NULL != elementtoremove->pPrev)                                    \
        {                                                                      \
            elementtoremove->pPrev->pNext = elementtoremove->pNext;            \
            if (elementtoremove->pNext)                                        \
            {                                                                  \
                elementtoremove->pNext->pPrev = elementtoremove->pPrev;        \
            }                                                                  \
            else                                                               \
            {                                                                  \
                currentptr = elementtoremove->pPrev;                           \
            }                                                                  \
        }                                                                      \
        else if (NULL != elementtoremove->pNext)                               \
        {                                                                      \
            elementtoremove->pNext->pPrev = NULL;                              \
            headptr = elementtoremove->pNext;                                  \
        }                                                                      \
        else                                                                   \
        {                                                                      \
            currentptr = NULL;                                                 \
            headptr = NULL;                                                    \
        }                                                                      \
    } while (0)

#define COHERENT_SAMPLE_MEM         "/dev/coherent"
#define COHERENT_SAMPLE_MEM_DMABUF  "/dev/coherent_dmabuf"

/* IOCTL number for use between the kernel and the user space application */
#define DEV_MEM_MAGIC 'm'
#define DEV_MEM_CMD_MEMALLOC (0)
#define DEV_MEM_CMD_MEMFREE  (1)

/* IOCTL commands for requesting kernel memory */
#define DEV_MEM_IOC_MEMALLOC                                                   \
    _IOWR(DEV_MEM_MAGIC, DEV_MEM_CMD_MEMALLOC, dev_mem_info_t)
#define DEV_MEM_IOC_MEMFREE                                                    \
    _IOWR(DEV_MEM_MAGIC, DEV_MEM_CMD_MEMFREE, dev_mem_info_t)

/* IOCTL number for use between the kernel and the user space application */
#define DEV_MEM_MAGIC_DMABUF 'd'
#define DEV_MEM_CMD_MEMALLOCDMABUF (0)
#define DEV_MEM_CMD_MEMFREEDMABUF  (1)

/* IOCTL commands for requesting kernel memory */
#define DEV_MEM_IOC_MEMALLOCDMABUF                                             \
    _IOWR(DEV_MEM_MAGIC_DMABUF, DEV_MEM_CMD_MEMALLOCDMABUF, dev_mem_info_t)

#define DEV_MEM_IOC_MEMFREEDMABUF                                              \
    _IOWR(DEV_MEM_MAGIC_DMABUF, DEV_MEM_CMD_MEMFREEDMABUF, dev_mem_info_t)

/* checksum, maybe stupid, but good enough :-) */
static inline unsigned short coherent_from32to16(unsigned int x)
{
    /* add up 16-bit and 16-bit for 16+c bit */
    x = (x & 0xffff) + (x >> 16);
    /* add up carry.. */
    x = (x & 0xffff) + (x >> 16);
    return x;
}

static inline unsigned int coherent_csum(const unsigned char *buff, int len)
{
    int odd;
    unsigned int result = 0;

    if (len <= 0)
        goto out;
    odd = 1 & (unsigned long)buff;
    if (odd)
    {
#ifdef __LITTLE_ENDIAN
        result += (*buff << 8);
#else
        result = *buff;
#endif
        len--;
        buff++;
    }
    if (len >= 2)
    {
        if (2 & (unsigned long)buff)
        {
            result += *(unsigned short *)buff;
            len -= 2;
            buff += 2;
        }
        if (len >= 4)
        {
            const unsigned char *end = buff + ((unsigned)len & ~3);
            unsigned int carry = 0;
            do
            {
                unsigned int w = *(unsigned int *)buff;
                buff += 4;
                result += carry;
                result += w;
                carry = (w > result);
            }
            while (buff < end);
            result += carry;
            result = (result & 0xffff) + (result >> 16);
        }
        if (len & 2)
        {
            result += *(unsigned short *)buff;
            buff += 2;
        }
    }
    if (len & 1)
#ifdef __LITTLE_ENDIAN
        result += *buff;
#else
        result += (*buff << 8);
#endif
    result = coherent_from32to16(result);
    if (odd)
        result = ((result >> 8) & 0xff) | ((result & 0xff) << 8);
out:
    return result;
}

#endif

