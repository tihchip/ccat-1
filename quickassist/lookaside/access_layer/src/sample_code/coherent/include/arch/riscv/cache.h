
#ifndef ASM_RISCV_CACHE_H_
#define ASM_RISCV_CACHE_H_

#define L1_CACHE_SHIFT      6

#define L1_CACHE_BYTES      (1 << L1_CACHE_SHIFT)

#define CACHE_LINE_SIZE     32

#endif /* ASM_RISCV_CACHE_H */
