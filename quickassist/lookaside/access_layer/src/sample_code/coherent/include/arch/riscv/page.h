
#ifndef ASM_RISCV_PAGE_H_
#define ASM_RISCV_PAGE_H_

/* align addr on a size boundary - adjust address up/down if needed */
#define _ALIGN_UP(addr, size)   (((addr)+((size)-1))&(~((size)-1)))
#define _ALIGN_DOWN(addr, size) ((addr)&(~((size)-1)))

/* align addr on a size boundary - adjust address up if needed */
#define _ALIGN(addr, size)  _ALIGN_UP(addr, size)

#endif /* ASM_RISCV_PAGE_H_ */
