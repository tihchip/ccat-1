
#include <stdlib.h>     /* exit, abort */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <fcntl.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "coherent_options.h"

extern int  coherentInit(void);
extern void coherentPrintRunMode(void);
extern void coherentRun(void);
extern void coherentReportCumulative(void);
extern void coherentDestroy(void);

#ifndef NAME
#define NAME                "coherent"
#endif
#ifndef VERSION
#define VERSION             "1.0"
#endif

#define MAX_ARGS            8

int quit; /* used only in interactive mode */

struct cmd_alias
{
    char *name;
    char *method;
    char *format;
};

static struct cmd_alias cmd_aliases[] =
{
    {"help",    "coherent.help",    0},
    {"exit",    "coherent.exit",    0},
    {"run",     "coherent.run",     0},
    {0, 0, 0}
};

static coherent_option_t builtins[] =
{
    COHERENT_OPT("direct",      NULL,  "off",   BOOL),
    COHERENT_OPT("block-size",  NULL,  "1K",    SIZE),
    COHERENT_OPT("total-size",  NULL,  "100G",  SIZE),
    COHERENT_OPT("scope",       NULL,  "local", STRING),
    COHERENT_OPT("oper",        NULL,  "write", STRING),
    COHERENT_OPT("access-mode", NULL,  "seq",   STRING),
    COHERENT_OPT("verify",      NULL,  "off",   BOOL),
};

enum complete_states
{
    COMPLETE_INIT,
    COMPLETE_CMD_NAME,
    COMPLETE_NOTHING
};

/* instead of rl_attempted_completion_over which is not present in
   some readline emulations, use attempted_completion_state */
static enum complete_states attempted_completion_state;
static int crt_param_no;

/* commands for which we complete the params to other method names */
char *complete_params_methods[] =
{
    0
};

/* readline command generator */
static char *coherentGenerator(const char *text, int state)
{
    static int idx;
    static int list; /* aliases, builtins */
    static int len;
    char *name;

    switch (attempted_completion_state)
    {
        case COMPLETE_INIT:
        case COMPLETE_NOTHING:
            return 0;
        case COMPLETE_CMD_NAME:
            if (state == 0)
            {
                /* init */
                idx = list = 0;
                len = strlen(text);
            }
            /* return next partial match */
            switch (list)
            {
                case 0: /* aliases*/
                    while ((name = cmd_aliases[idx].name))
                    {
                        idx++;
                        if (strncmp(name, text, len) == 0)
                            return strdup(name);
                    }
                    list++;
                    idx = 0;
                /* no break */
                case 1: /* builtins */
                    while ((name = builtins[idx].name))
                    {
                        idx++;
                        if (strncmp(name, text, len) == 0)
                            return strdup(name);
                    }
                    list++;
                    idx = 0;
                    /* no break */
            }
            break;
    }
    /* no matches */
    return 0;
}

char **coherentCompletion(const char *text, int start, int end)
{
    int i, j;
    int cmd_start, cmd_end, cmd_len;
    int whitespace;

    crt_param_no = 0;
    /* skip over whitespace at the beginning */
    for (j = 0; (j < start) && (rl_line_buffer[j] == ' ' ||
                                rl_line_buffer[j] == '\t'); j++);
    cmd_start = j;
    if (start == cmd_start)
    {
        /* complete cmd name at beginning */
        attempted_completion_state = COMPLETE_CMD_NAME;
    }
    else   /* or if this is a command for which we complete the parameters */
    {
        /* find first whitespace after command name*/
        for (; (j < start) && (rl_line_buffer[j] != ' ') &&
                (rl_line_buffer[j] != '\t'); j++);
        cmd_end = j;
        cmd_len = cmd_end - cmd_start;
        /* count params before the current one */
        whitespace = 1;
        for (; j < start; j++)
        {
            if (rl_line_buffer[j] != ' ' && rl_line_buffer[j] != '\t')
            {
                if (whitespace) crt_param_no++;
                whitespace = 0;
            }
            else
                whitespace = 1;
        }
        crt_param_no++;
        if (crt_param_no == 1)
        {
            for (i = 0; complete_params_methods[i]; i++)
            {
                if ((cmd_len == strlen(complete_params_methods[i])) &&
                        (strncmp(&rl_line_buffer[cmd_start],
                                 complete_params_methods[i],
                                 cmd_len) == 0))
                {
                    attempted_completion_state = COMPLETE_CMD_NAME;
                    goto end;
                }
            }
        }
        else if (crt_param_no == 2)
        {
        }
        attempted_completion_state = COMPLETE_NOTHING;
    }
end:
    return 0; /* let readline call usdmcmd_generator */
}

static char *coherentTrimWs(char *l)
{
    char *ret;

    for (; *l && ((*l == ' ') || (*l == '\t') || (*l == '\n') || (*l == '\r')); l++);
    ret = l;
    if (*ret == 0) return ret;
    for (l = l + strlen(l) - 1; (l > ret) &&
            ((*l == ' ') || (*l == '\t') || (*l == '\n') || (*l == '\r')); l--);
    *(l + 1) = 0;
    return ret;
}

static void coherentOptionsDefault(void)
{
    int i;
    const unsigned int num_op = sizeof(builtins) / sizeof(coherent_option_t);

    /* default */
    for (i = 0; i < num_op; i++)
    {
        if (!coherentFindOption(builtins[i].name))
        {
            coherentSetOption(builtins[i].name,
                              builtins[i].value,
                              builtins[i].type);
        }
    }
    return;
}

static int coherentOptionsScan(char *method)
{
    char *p;
    char *value = NULL;
    char *name = NULL;
    coherent_option_t *opt;

    name = strdup(method);
    if (name == NULL)
        return -1;

    p = strchr(name, '=');
    if (p)
    {
        *p = '\0';
        value = p + 1;
    }
    else
    {
        if (!strcmp("direct", name))
        {
            value = "on";
        }
        else
        {
            return -1;
        }
    }

    opt = coherentFindOption(name);
    if (opt)
    {
        if (opt->value)
            coherentFreeValue(opt->value);
        opt->value = coherentNewValue(value);
    }

    free(name);
    return 0;
}

static void coherentPrintHelp(void)
{
    printf("NAME\n");
    printf("  coherent -\n\n");

    printf("SYNOPSIS\n");
    printf("  coherent [run|help|exit] [options]\n\n");

    printf("DESCRIPTION\n");
    printf("  -\n\n");

    printf("GENERAL OPTIONS\n");
    printf("  run\n"
           "    run the test\n");
    printf("  help\n"
           "    print help\n");
    printf("  exit\n"
           "    end the coherent and exit\n\n");

    printf("MEMORY FLAGS\n");
    printf("  direct[=on|off]\n"
           "    Try to minimize cache effects of the I/O "
           "to this memory [off]\n\n");

    printf("MEMORY OPTIONS\n");
    printf("  block-size=SIZE\n"
           "    size of memory block for test [1K]\n");
    printf("  total-size=SIZE\n"
           "    total size of data to transfer [100G]\n");
    printf("  scope=STRING\n"
           "    memory access scope {local,numa,dmabuf} [local]\n");
    printf("  oper=STRING\n"
           "    type of memory operations {read,write,none} [write]\n");
    printf("  access-mode=STRING\n"
           "    memory access mode {seq,rnd} [seq]\n");
    return;
}

int coherentOptionsParse(char *line)
{
    char *p;
    char *method;
    int count = 0;

    coherentOptionsDefault();

    method = strtok(line, " \t");
    if (method == 0)
        goto error_no_method;

    if (!strcmp("exit", method))
    {
        quit = 1;
        return 1;
    }
    else if (!strcmp("help", method))
    {
        coherentPrintHelp();
        return 1;
    }
    else if (!strcmp("run", method))
    {
        /* run */
    }
    else
    {
        goto error_arg;
    }

    for (p = strtok(0, " \t"); p; p = strtok(0, " \t"))
    {
        if (count >= MAX_ARGS)
            goto error_too_many;

        if (-1 == coherentOptionsScan(p))
            goto error_arg;

        count++;
    }
    return 0;

error_no_method:
    printf("ERROR: no method name\n");
    return -1;

error_too_many:
    printf("ERROR: too many arguments (%d), no more than %d allowed\n",
           count, MAX_ARGS);
    return -1;

error_arg:
    printf("ERROR: bad argument %d: %s\n", count + 1, p);
    return -1;
}

/* runs a command represented in line */
static inline void coherent(char *line)
{
    /* initialize options */
    if (0 != coherentOptionsParse(line))
        goto end;

    /* initialize test */
    if (0 != coherentInit())
        goto end;

    /* print mode */
    coherentPrintRunMode();

    /* run test */
    coherentRun();

    /* cumulative */
    coherentReportCumulative();

end:
    coherentDestroy();
    return;
}

int main(int argc, char *argv[])
{
    char *line;
    char *l;

    /* initialize readline */
    /* allow conditional parsing of the ~/.inputrc file*/
    rl_readline_name = NAME;
    rl_completion_entry_function = coherentGenerator;
    rl_attempted_completion_function = coherentCompletion;

    while (!quit)
    {
        line = readline(NAME "> ");
        if (line == 0)              /* EOF */
            break;
        l = coherentTrimWs(line);   /* trim whitespace */
        if (*l)
        {
            add_history(l);
            coherent(l);
        }
        free(line);
        line = 0;
    }

    exit(0);
}

