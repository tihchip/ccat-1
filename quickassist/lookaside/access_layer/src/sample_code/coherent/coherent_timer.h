
#ifndef COHERENT_TIMER_H
#define COHERENT_TIMER_H

#include <sys/time.h>
#include <time.h>
#include <stdint.h>

#include "OsalOsTypes.h"

#define NS_PER_SEC 1000000000
#define US_PER_SEC 1000000
#define MS_PER_SEC 1000
#define NS_PER_MS (NS_PER_SEC / MS_PER_SEC)

/* Convert nanoseconds to seconds and vice versa */
#define NS2SEC(nsec) ((nsec) / (double) NS_PER_SEC)
#define SEC2NS(sec)  ((uint64_t) (sec) * NS_PER_SEC)

/* Convert nanoseconds to milliseconds and vice versa */
#define NS2MS(nsec) ((nsec) / (double) NS_PER_MS)
#define MS2NS(sec)  ((sec) * (uint64_t) NS_PER_MS)

/* Convert milliseconds to seconds and vice versa */
#define MS2SEC(msec) ((msec) / (double) MS_PER_SEC)
#define SEC2MS(sec) ((sec) * MS_PER_SEC)

/* Difference between two 'timespec' values in nanoseconds */
#define TIMESPEC_DIFF(a,b) (SEC2NS(a.tv_sec - b.tv_sec) +                      \
                            (a.tv_nsec - b.tv_nsec))

#define COHERENT_GETTIME(tsp)                                                  \
    do {                                                                       \
        struct timeval tv;                                                     \
        gettimeofday(&tv, NULL);                                               \
        (tsp)->tv_sec = tv.tv_sec;                                             \
        (tsp)->tv_nsec = tv.tv_usec * 1000;                                    \
    } while (0)

/* Timer structure definition */
typedef struct
{
    struct timespec time_start;
    struct timespec time_end;
    uint64_t        events;
    uint64_t        queue_time;
    uint64_t        min_time;
    uint64_t        max_time;
    uint64_t        sum_time;
} coherent_timer_t;

/* start timer */
static inline void coherentTimerStart(coherent_timer_t *t)
{
    COHERENT_GETTIME(&t->time_start);
}

/* stop timer */
static inline uint64_t coherentTimerStop(coherent_timer_t *t)
{
    COHERENT_GETTIME(&t->time_end);

    uint64_t elapsed = TIMESPEC_DIFF(t->time_end, t->time_start);

    t->events++;
    t->sum_time += elapsed;

    if (unlikely(elapsed < t->min_time))
    {
        t->min_time = elapsed;
    }

    if (unlikely(elapsed > t->max_time))
    {
        t->max_time = elapsed;
    }

    return elapsed;
}

/* get the current timer value in nanoseconds without affecting its state, i.e.
  is safe to be used concurrently on a shared timer. */
static inline uint64_t coherentTimerValue(coherent_timer_t *t)
{
    struct timespec ts;

    COHERENT_GETTIME(&ts);
    return TIMESPEC_DIFF(ts, t->time_start) + t->queue_time;
}

/* get total time for all events */
uint64_t coherentTimerSum(coherent_timer_t *);

/* get minimum time */
uint64_t coherentTimerMin(coherent_timer_t *);

/* get maximum time */
uint64_t coherentTimerMax(coherent_timer_t *);

/* get average time per event */
uint64_t coherentTimerAvg(coherent_timer_t *);

int coherentInitTimer(coherent_timer_t *t);

#endif
