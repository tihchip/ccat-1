
#ifndef _ICP_CCAT_FW_H_
#define _ICP_CCAT_FW_H_

#include "icp_ccat_hw.h"

#define CCAT_FIELD_SET(flags, val, bitpos, mask)                               \
    (flags) =                                                                  \
            (((flags) & (~((mask) << (bitpos)))) | (((val) & (mask)) << (bitpos)))

#define CCAT_FIELD_GET(flags, bitpos, mask) (((flags) >> (bitpos)) & (mask))

#define CCAT_FLAG_SET(flags, bitpos) (flags) = ((flags) | (1 << (bitpos)))

#define CCAT_FLAG_CLEAR(flags, bitpos) (flags) = ((flags) & (~(1 << (bitpos))))

#define CCAT_FLAG_GET(flags, bitpos) (((flags) >> (bitpos)) & 1)

/* Service type(bit 0~2) */
#define CCAT_COMN_PTR_SERVICE_TYPE_CIPHER   0x0
#define CCAT_COMN_PTR_SERVICE_TYPE_HASH     0x1
#define CCAT_COMN_PTR_SERVICE_TYPE_AUTH     0x2
#define CCAT_COMN_PTR_SERVICE_TYPE_AEAD     0x3
//#define CCAT_COMN_PTR_SERVICE_TYPE_IPSEC    0x4
#define CCAT_COMN_PTR_SERVICE_TYPE_PK       0x5
#define CCAT_COMN_PTR_SERVICE_TYPE_CTRL     0x6
#define CCAT_COMN_PTR_SERVICE_TYPE_BYPASS   0x7

/* Last(bit 3) */
#define CCAT_COMN_PTR_LAST_NULL             0x0
#define CCAT_COMN_PTR_LAST_LAST             0x1

/* First(bit 4) */
#define CCAT_COMN_PTR_FIRST_NULL            0x0
#define CCAT_COMN_PTR_FIRST_FIRST           0x1

/* Addr list(bit 5) */
#define CCAT_COMN_PTR_ADDR_TYPE_FLAT        0x0
/**< @ingroup icp_qat_fw_comn
 * Constant value indicating Src&Dst Buffer Pointer type is flat
 * If Batch and Pack mode is enabled, only applies to Destination buffer.*/

#define CCAT_COMN_PTR_ADDR_TYPE_SGL         0x1
/**< @ingroup icp_qat_fw_comn
 * Constant value indicating Src&Dst Buffer Pointer type is SGL type
 * If Batch and Pack mode is enabled, only applies to Destination buffer.*/

/* Cipher algorithm(bit 6~9) */
#define CCAT_COMN_PTR_CIPHER_ALG_NULL       0x0
#define CCAT_COMN_PTR_CIPHER_ALG_SM4        0x1
#define CCAT_COMN_PTR_CIPHER_ALG_SM7        0x2
#define CCAT_COMN_PTR_CIPHER_ALG_AES128     0x3
#define CCAT_COMN_PTR_CIPHER_ALG_AES192     0x4
#define CCAT_COMN_PTR_CIPHER_ALG_AES256     0x5
#define CCAT_COMN_PTR_CIPHER_ALG_DES        0x6
#define CCAT_COMN_PTR_CIPHER_ALG_3DES       0x7
#define CCAT_COMN_PTR_CIPHER_ALG_BASE64     0x8
#define CCAT_COMN_PTR_CIPHER_ALG_ZUC_EEA3   0x9

/* Cipher mode(bit 10~13) */
#define CCAT_COMN_PTR_CIPHER_MODE_NULL      0x0
#define CCAT_COMN_PTR_CIPHER_MODE_ECB       0x1
#define CCAT_COMN_PTR_CIPHER_MODE_CBC       0x2
#define CCAT_COMN_PTR_CIPHER_MODE_CFB       0x3
#define CCAT_COMN_PTR_CIPHER_MODE_OFB       0x4
#define CCAT_COMN_PTR_CIPHER_MODE_CTR       0x5
#define CCAT_COMN_PTR_CIPHER_MODE_XTS       0x6
#define CCAT_COMN_PTR_CIPHER_MODE_GCM       0x7
#define CCAT_COMN_PTR_CIPHER_MODE_CCM       0x8

/* Hash algorithm(bit14~17) */
#define CCAT_COMN_PTR_HASH_ALG_NULL         0x0
#define CCAT_COMN_PTR_HASH_ALG_SM3          0x1
#define CCAT_COMN_PTR_HASH_ALG_MD5          0x2
#define CCAT_COMN_PTR_HASH_ALG_SHA1         0x3
#define CCAT_COMN_PTR_HASH_ALG_SHA224       0x4
#define CCAT_COMN_PTR_HASH_ALG_SHA256       0x5
#define CCAT_COMN_PTR_HASH_ALG_SHA384       0x6
#define CCAT_COMN_PTR_HASH_ALG_SHA512       0x7
#define CCAT_COMN_PTR_HASH_ALG_GHASH        0x8

/* Auth mode(bit 18~20) */
#define CCAT_COMN_PTR_AUTH_MODE_NULL        0x0
#define CCAT_COMN_PTR_AUTH_MODE_HMAC        0x1
#define CCAT_COMN_PTR_AUTH_MODE_CBC_MAC     0x2
#define CCAT_COMN_PTR_AUTH_MODE_XCBC_MAC    0x3
#define CCAT_COMN_PTR_AUTH_MODE_GMAC        0x4
#define CCAT_COMN_PTR_AUTH_MODE_GCM         0x5
#define CCAT_COMN_PTR_AUTH_MODE_CCM         0x6
#define CCAT_COMN_PTR_AUTH_MODE_ZUC_EIA     0x7

/* Init(bit 21) */
#define CCAT_COMN_PTR_INIT_NULL             0x0
#define CCAT_COMN_PTR_INIT_INIT             0x1

/* Finish(bit 22) */
#define CCAT_COMN_PTR_FINISH_NULL           0x0
#define CCAT_COMN_PTR_FINISH_FINISH         0x1

/* Aead mode(bit 23~24) */
#define CCAT_COMN_PTR_AEAD_MODE_NULL        0x0
#define CCAT_COMN_PTR_AEAD_MODE_ETM         0x1
#define CCAT_COMN_PTR_AEAD_MODE_MTE         0x2

/* Aead tag(bit 25~26) */
#define CCAT_COMN_PTR_AEAD_TAG_16           0x0
#define CCAT_COMN_PTR_AEAD_TAG_12           0x1
#define CCAT_COMN_PTR_AEAD_TAG_8            0x2
#define CCAT_COMN_PTR_AEAD_TAG_4            0x3

/* Operation(bit 27) */
#define CCAT_COMN_PTR_OPERATION_ENCRYPT     0x0
#define CCAT_COMN_PTR_OPERATION_DECRYPT     0x1

/* ctx_ctrl mode(bit 28~29) */
#define CCAT_COMN_PTR_EX_CTX_NO_SAVE_WB     0x0
#define CCAT_COMN_PTR_EX_CTX_SAVE_NO_WB     0x1
#define CCAT_COMN_PTR_IN_CTX_SAVE_NO_WB     0x2
#define CCAT_COMN_PTR_IN_CTX_NO_SAVE_WB     0x3

/* key mode(bit 30~31) */
#define CCAT_COMN_PTR_KEY_EXT_DECRYPT_KEY   0x0
#define CCAT_COMN_PTR_KEY_EXT_ENCRYPT_KEY   0x1
#define CCAT_COMN_PTR_KEY_INT_ENCRYPT_KEY   0x2

/* ccat comm header0 bit pos */
#define CCAT_COMN_PTR_SERVICE_TYPE_BITPOS   0
#define CCAT_COMN_PTR_LAST_BITPOS           3
#define CCAT_COMN_PTR_FIRST_BITPOS          4
#define CCAT_COMN_PTR_SGL_TYPE_BITPOS       5
#define CCAT_COMN_PTR_CIPHER_ALG_BITPOS     6
#define CCAT_COMN_PTR_CIPHER_MODE_BITPOS    10
#define CCAT_COMN_PTR_HASH_ALG_BITPOS       14
#define CCAT_COMN_PTR_AUTH_MODE_BITPOS      18
#define CCAT_COMN_PTR_INIT_BITPOS           21
#define CCAT_COMN_PTR_FINISH_BITPOS         22
#define CCAT_COMN_PTR_AEAD_MODE_BITPOS      23
#define CCAT_COMN_PTR_AEAD_TAG_BITPOS       25
#define CCAT_COMN_PTR_OPERATION_BITPOS      27
#define CCAT_COMN_PTR_CTX_CTRL_BITPOS       28
#define CCAT_COMN_PTR_KEY_MODE_BITPOS       30

/* ccat comm header0 mask */
#define CCAT_COMN_PTR_SERVICE_TYPE_MASK     0x7
#define CCAT_COMN_PTR_LAST_MASK             0x1
#define CCAT_COMN_PTR_FIRST_MASK            0x1
#define CCAT_COMN_PTR_SGL_TYPE_MASK         0x1
#define CCAT_COMN_PTR_CIPHER_ALG_MASK       0xF
#define CCAT_COMN_PTR_CIPHER_MODE_MASK      0xF
#define CCAT_COMN_PTR_HASH_ALG_MASK         0xF
#define CCAT_COMN_PTR_AUTH_MODE_MASK        0x7
#define CCAT_COMN_PTR_INIT_MASK             0x1
#define CCAT_COMN_PTR_FINISH_MASK           0x1
#define CCAT_COMN_PTR_AEAD_MODE_MASK        0x3
#define CCAT_COMN_PTR_AEAD_TAG_MASK         0x3
#define CCAT_COMN_PTR_OPERATION_MASK        0x1
#define CCAT_COMN_PTR_CTX_CTRL_MASK         0x3
#define CCAT_COMN_PTR_KEY_MODE_MASK         0x3

#define ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_GET(hdr)                             \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_SERVICE_TYPE_BITPOS,                          \
                   CCAT_COMN_PTR_SERVICE_TYPE_MASK)

#define ICP_CCAT_FW_COMN_PTR_LAST_GET(hdr)                                     \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_LAST_BITPOS,                                  \
                   CCAT_COMN_PTR_LAST_MASK)

#define ICP_CCAT_FW_COMN_PTR_FIRST_GET(hdr)                                    \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_FIRST_BITPOS,                                 \
                   CCAT_COMN_PTR_FIRST_MASK)

#define ICP_CCAT_FW_COMN_PTR_SGL_TYPE_GET(hdr)                                 \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_SGL_TYPE_BITPOS,                              \
                   CCAT_COMN_PTR_SGL_TYPE_MASK)

#define ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_GET(hdr)                               \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_CIPHER_ALG_BITPOS,                            \
                   CCAT_COMN_PTR_CIPHER_ALG_MASK)

#define ICP_CCAT_FW_COMN_PTR_CIPHER_MODE_GET(hdr)                              \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_CIPHER_MODE_BITPOS,                           \
                   CCAT_COMN_PTR_CIPHER_MODE_MASK)

#define ICP_CCAT_FW_COMN_PTR_HASH_ALG_GET(hdr)                                 \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_HASH_ALG_BITPOS,                              \
                   CCAT_COMN_PTR_HASH_ALG_MASK)

#define ICP_CCAT_FW_COMN_PTR_AUTH_MODE_GET(hdr)                                \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_AUTH_MODE_BITPOS,                             \
                   CCAT_COMN_PTR_AUTH_MODE_MASK)

#define ICP_CCAT_FW_COMN_PTR_INIT_GET(hdr)                                     \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_INIT_BITPOS,                                  \
                   CCAT_COMN_PTR_INIT_MASK)

#define ICP_CCAT_FW_COMN_PTR_FINISH_GET(hdr)                                   \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_FINISH_BITPOS,                                \
                   CCAT_COMN_PTR_FINISH_MASK)

#define ICP_CCAT_FW_COMN_PTR_AEAD_MODE_GET(hdr)                                \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_AEAD_MODE_BITPOS,                             \
                   CCAT_COMN_PTR_AEAD_MODE_MASK)

#define ICP_CCAT_FW_COMN_PTR_AEAD_TAG_GET(hdr)                                 \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_AEAD_TAG_BITPOS,                              \
                   CCAT_COMN_PTR_AEAD_TAG_MASK)

#define ICP_CCAT_FW_COMN_PTR_OPERATION_GET(hdr)                                \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_OPERATION_BITPOS,                             \
                   CCAT_COMN_PTR_OPERATION_MASK)

#define ICP_CCAT_FW_COMN_PTR_CTX_CTRL_GET(hdr)                                 \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_CTX_CTRL_BITPOS,                              \
                   CCAT_COMN_PTR_CTX_CTRL_MASK)

#define ICP_CCAT_FW_COMN_PTR_KEY_MODE_GET(hdr)                                 \
    CCAT_FIELD_GET((hdr).header0,                                              \
                   CCAT_COMN_PTR_KEY_MODE_BITPOS,                              \
                   CCAT_COMN_PTR_KEY_MODE_MASK)

#define ICP_CCAT_FW_COMN_PTR_SERVICE_TYPE_SET(hdr, val)                        \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_SERVICE_TYPE_BITPOS,                          \
                   CCAT_COMN_PTR_SERVICE_TYPE_MASK)

#define ICP_CCAT_FW_COMN_PTR_LAST_SET(hdr, val)                                \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_LAST_BITPOS,                                  \
                   CCAT_COMN_PTR_LAST_MASK)

#define ICP_CCAT_FW_COMN_PTR_FIRST_SET(hdr, val)                               \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_FIRST_BITPOS,                                 \
                   CCAT_COMN_PTR_FIRST_MASK)

#define ICP_CCAT_FW_COMN_PTR_SGL_TYPE_SET(hdr, val)                            \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_SGL_TYPE_BITPOS,                              \
                   CCAT_COMN_PTR_SGL_TYPE_MASK)

#define ICP_CCAT_FW_COMN_PTR_CIPHER_ALG_SET(hdr, val)                          \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_CIPHER_ALG_BITPOS,                            \
                   CCAT_COMN_PTR_CIPHER_ALG_MASK)

#define ICP_CCAT_FW_COMN_PTR_CIPHER_MODE_SET(hdr, val)                         \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_CIPHER_MODE_BITPOS,                           \
                   CCAT_COMN_PTR_CIPHER_MODE_MASK)

#define ICP_CCAT_FW_COMN_PTR_HASH_ALG_SET(hdr, val)                            \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_HASH_ALG_BITPOS,                              \
                   CCAT_COMN_PTR_HASH_ALG_MASK)

#define ICP_CCAT_FW_COMN_PTR_AUTH_MODE_SET(hdr, val)                           \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_AUTH_MODE_BITPOS,                             \
                   CCAT_COMN_PTR_AUTH_MODE_MASK)

#define ICP_CCAT_FW_COMN_PTR_INIT_SET(hdr, val)                                \
        CCAT_FIELD_SET((hdr).header0,                                          \
                       val,                                                    \
                       CCAT_COMN_PTR_INIT_BITPOS,                              \
                       CCAT_COMN_PTR_INIT_MASK)

#define ICP_CCAT_FW_COMN_PTR_FINISH_SET(hdr, val)                              \
            CCAT_FIELD_SET((hdr).header0,                                      \
                           val,                                                \
                           CCAT_COMN_PTR_FINISH_BITPOS,                        \
                           CCAT_COMN_PTR_FINISH_MASK)

#define ICP_CCAT_FW_COMN_PTR_AEAD_MODE_SET(hdr, val)                           \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_AEAD_MODE_BITPOS,                             \
                   CCAT_COMN_PTR_AEAD_MODE_MASK)

#define ICP_CCAT_FW_COMN_PTR_AEAD_TAG_SET(hdr, val)                            \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_AEAD_TAG_BITPOS,                              \
                   CCAT_COMN_PTR_AEAD_TAG_MASK)

#define ICP_CCAT_FW_COMN_PTR_OPERATION_SET(hdr, val)                           \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_OPERATION_BITPOS,                             \
                   CCAT_COMN_PTR_OPERATION_MASK)

#define ICP_CCAT_FW_COMN_PTR_CTX_CTRL_SET(hdr, val)                            \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_CTX_CTRL_BITPOS,                              \
                   CCAT_COMN_PTR_CTX_CTRL_MASK)

#define ICP_CCAT_FW_COMN_PTR_KEY_MODE_SET(hdr, val)                            \
    CCAT_FIELD_SET((hdr).header0,                                              \
                   val,                                                        \
                   CCAT_COMN_PTR_KEY_MODE_BITPOS,                              \
                   CCAT_COMN_PTR_KEY_MODE_MASK)


/******************************************************************************/
/****************************** PK Define *************************************/
/******************************************************************************/
/* pk alg(bit5~12) */
#define PK_OP_RSA_KEYPAIR_GEN                               0x20
#define PK_OP_RSA_CRT_KEYPAIR_GEN                           0x21
#define PK_OP_RSA_ENCRYPT                                   0x22
#define PK_OP_RSA_DECRYPT                                   0x23
#define PK_OP_RSA_CRT_DECRYPT                               0x24
#define PK_OP_RSA_KEYPAIR_GEN_FROM_PQ                       0x25
#define PK_OP_RSA_KEYPAIR_GEN_FROM_PQE                      0x26
#define PK_OP_RSA_CRT_KEYPAIR_GEN_FROM_PQ                   0x27
#define PK_OP_RSA_CRT_KEYPAIR_GEN_FROM_PQE                  0x28
#define PK_OP_ECC_KEYPAIR_GEN                               0x30
#define PK_OP_ECC_PUBKEY_GEN_FROM_PRIKEY                    0x31
#define PK_OP_ECDSA_SIGN                                    0x32
#define PK_OP_ECDSA_VERIFY                                  0x33
#define PK_OP_ECDH_COMPUTE_KEY                              0x34
#define PK_OP_SM2_KEYPAIR_GEN                               0x40
#define PK_OP_SM2_PUBKEY_GEN_FROM_PRIKEY                    0x41
#define PK_OP_SM2_Z_GET                                     0x42
#define PK_OP_SM2_E_GET                                     0x43
#define PK_OP_SM2_SIGN                                      0x44
#define PK_OP_SM2_VERIFY                                    0x45
#define PK_OP_SM2_ENCRYPT                                   0x46
#define PK_OP_SM2_DECRYPT                                   0x47
#define PK_OP_SM2_KEY_EXCHANGE                              0x48
#define PK_OP_SM9_SIGN_MASTKEYPAIR_GEN                      0x50
#define PK_OP_SM9_SIGN_MASTPUBKEY_GEN_FROM_MASTPRIKEY       0x51
#define PK_OP_SM9_SIGN_USERPRIKEY_GEN                       0x52
#define PK_OP_SM9_SIGN                                      0x53
#define PK_OP_SM9_VERIFY                                    0x54
#define PK_OP_SM9_ENC_MASTKEYPAIR_GEN                       0x55
#define PK_OP_SM9_ENC_MASTPUBKEY_GEN_FROM_MASTPRIKEY        0x56
#define PK_OP_SM9_ENC_USERPRIKEY_GEN                        0x57
#define PK_OP_SM9_KEY_WRAP                                  0x58
#define PK_OP_SM9_KEY_UNWRAP                                0x59
#define PK_OP_SM9_ENCRYPT                                   0x5A
#define PK_OP_SM9_DECRYPT                                   0x5B
#define PK_OP_SM9_EXCKEY_MASTKEYPAIR_GEN                    0x5C
#define PK_OP_SM9_EXCKEY_MASTPUBKEY_GEN_FROM_MASTPRIKEY     0x5D
#define PK_OP_SM9_EXCKEY_USERPRIKEY_GEN                     0x5E
#define PK_OP_SM9_EXCKEY_TMPKEYPAIR_GEN                     0x5F
#define PK_OP_SM9_EXCKEY_TMPPUBKEY_GEN_FROM_TMPPRIKEY       0x60
#define PK_OP_SM9_KEY_EXCHANGE                              0x61
#define PK_OP_SM9_PAIRING_CALC                              0x62
#define PK_OP_DSA_SIGN                                      0x70
#define PK_OP_DSA_VERIFY                                    0x71
#define PK_OP_DSA_YPARAM_GEN                                0x72
#define PK_OP_DSA_PPARAM_GEN                                0x73
#define PK_OP_DSA_GPARAM_GEN                                0x74
#define PK_OP_DH_GENERATE_KEY                               0x78
#define PK_OP_DH_COMPUTE_KEY                                0x79
#define PK_OP_BIGNUM_ADD                                    0x80
#define PK_OP_BIGNUM_SUB                                    0x81
#define PK_OP_BIGNUM_MUL                                    0x82
#define PK_OP_BIGNUM_MODADD                                 0x83
#define PK_OP_BIGNUM_MODSUB                                 0x84
#define PK_OP_BIGNUM_MODMUL                                 0x85
#define PK_OP_BIGNUM_MODEXP                                 0x86
#define PK_OP_BIGNUM_MODINV                                 0x87
#define PK_OP_PRIME_TEST                                    0x88
#define PK_OP_POINT_ADD                                     0x90
#define PK_OP_POINT_MUL                                     0x91
#define PK_OP_POINT_MUL_BASE                                0x92
#define PK_OP_POINT_MUL_SHAMIR                              0x93
#define PK_OP_POINT_DOUBLE                                  0x94
#define PK_OP_POINT_VERIFY                                  0x95

/* ecc binary(bit13) */
/* sm2 order(bit14) */
/* sm2 and sm9 role(bit15) */
#define PK_SM2_SM9_ROLE_SPONSOR                             0x0
#define PK_SM2_SM9_9ROLE_RESPONSOR                          0x1

/* sm9 enc type(bit16) */
#define PK_SM9_ENC_KDF_STREAM_CIPHER                        0x0
#define PK_SM9_ENC_KDF_BLOCK_CIPHER                         0x1

/* sm9 padding type(bit17) */
#define PK_SM9_ENC_NO_PADDING                               0x0
#define PK_SM9_ENC_PKCS7_PADDING                            0x1

/* reserved(bit20~27) */
/* security(bit28) */
/* pin enable(bit29) */
/* key mode(bit30~31) */
#define PK_KEY_MODE_PLAIN                                   0x0
#define PK_KEY_MODE_CIPHER                                  0x1
#define PK_KEY_MODE_ID                                      0x2

/* pk header0 bit pos */
#define PK_SERVICE_TYPE_BIT_POS                             0
#define PK_FIRST_BIT_POS                                    3
#define PK_LAST_BIT_POS                                     4
#define PK_ALG_BIT_POS                                      5
#define PK_ECC_BINARY_BIT_POS                               13
#define PK_SM2_ORDER_BIT_POS                                14
#define PK_SM2_SM9_ROLE_BIT_POS                             15
#define PK_SM9_ENC_TYPE_BIT_POS                             16
#define PK_SM9_PADDING_TYPE_BIT_POS                         17
#define PK_SECURITY_BIT_POS                                 28
#define PK_PIN_ENABLE_BIT_POS                               29
#define PK_KEY_MODE_BIT_POS                                 30

/* pk header0 bits mask */
#define PK_SERVICE_TYPE_MASK                                0x7
#define PK_FIRST_MASK                                       0x1
#define PK_LAST_MASK                                        0x1
#define PK_ALG_MASK                                         0xFF
#define PK_ECC_BINARY_MASK                                  0x1
#define PK_SM2_ORDER_MASK                                   0x1
#define PK_SM2_SM9_ROLE_MASK                                0x1
#define PK_SM9_ENC_TYPE_MASK                                0x1
#define PK_SM9_PADDING_TYPE_MASK                            0x1
#define PK_SECURITY_MASK                                    0x1
#define PK_PIN_ENABLE_MASK                                  0x1
#define PK_KEY_MODE_MASK                                    0x3

#define ICP_CCAT_FW_PK_SERVICE_TYPE_GET(hdr)                                   \
    CCAT_FIELD_GET((hdr).header0, PK_SERVICE_TYPE_BIT_POS, PK_SERVICE_TYPE_MASK)

#define ICP_CCAT_FW_PK_FIRST_GET(hdr)                                          \
    CCAT_FIELD_GET((hdr).header0, PK_FIRST_BIT_POS, PK_FIRST_MASK

#define ICP_CCAT_FW_PK_LAST_GET(hdr)                                           \
    CCAT_FIELD_GET((hdr).header0, PK_FIRST_BIT_POS, PK_FIRST_MASK)

#define ICP_CCAT_FW_PK_ALG_GET(hdr)                                            \
    CCAT_FIELD_GET((hdr).header0, PK_ALG_BIT_POS, PK_ALG_MASK)

#define ICP_CCAT_FW_PK_ECC_BINARY_GET(hdr)                                     \
    CCAT_FIELD_GET((hdr).header0, PK_ECC_BINARY_BIT_POS, PK_ECC_BINARY_MASK)

#define ICP_CCAT_FW_PK_SM2_ORDER_GET(hdr)                                      \
    CCAT_FIELD_GET((hdr).header0, PK_SM2_ORDER_BIT_POS, PK_SM2_ORDER_MASK)

#define ICP_CCAT_FW_PK_SM2_SM9_ROLE_GET(hdr)                                   \
    CCAT_FIELD_GET((hdr).header0, PK_SM2_SM9_ROLE_BIT_POS, PK_SM2_SM9_ROLE_MASK)

#define ICP_CCAT_FW_PK_SM9_ENC_TYPE_GET(hdr)                                   \
    CCAT_FIELD_GET((hdr).header0, PK_SM9_ENC_TYPE_BIT_POS, PK_SM9_ENC_TYPE_MASK)

#define ICP_CCAT_FW_PK_SM9_PADDING_TYPE_GET(hdr)                               \
    CCAT_FIELD_GET((hdr).header0, PK_SM9_PADDING_TYPE_BIT_POS, PK_SM9_PADDING_TYPE_MASK)

#define ICP_CCAT_FW_PK_SECURITY_GET(hdr)                                       \
    CCAT_FIELD_GET((hdr).header0, PK_SECURITY_BIT_POS, PK_SECURITY_MASK)

#define ICP_CCAT_FW_PK_PIN_ENABLE_GET(hdr)                                     \
    CCAT_FIELD_GET((hdr).header0, PK_PIN_ENABLE_BIT_POS, PK_PIN_ENABLE_MASK)

#define ICP_CCAT_FW_PK_KEY_MODE_GET(hdr)                                       \
    CCAT_FIELD_GET((hdr).header0, PK_KEY_MODE_BIT_POS, PK_KEY_MODE_MASK)

#define ICP_CCAT_FW_PK_SERVICE_TYPE_SET(hdr, val)                              \
    CCAT_FIELD_SET((hdr).header0, val, PK_SERVICE_TYPE_BIT_POS, PK_SERVICE_TYPE_MASK)

#define ICP_CCAT_FW_PK_FIRST_SET(hdr, val)                                     \
    CCAT_FIELD_SET((hdr).header0, val, PK_FIRST_BIT_POS, PK_FIRST_MASK)

#define ICP_CCAT_FW_PK_LAST_SET(hdr, val)                                      \
    CCAT_FIELD_SET((hdr).header0, val, PK_LAST_BIT_POS, PK_LAST_MASK)

#define ICP_CCAT_FW_PK_ALG_SET(hdr, val)                                       \
    CCAT_FIELD_SET((hdr).header0, val, PK_ALG_BIT_POS, PK_ALG_MASK)

#define ICP_CCAT_FW_PK_ECC_BINARY_SET(hdr, val)                                \
    CCAT_FIELD_SET((hdr).header0, val, PK_ECC_BINARY_BIT_POS, PK_ECC_BINARY_MASK)

#define ICP_CCAT_FW_PK_SM2_ORDER_SET(hdr, val)                                 \
    CCAT_FIELD_SET((hdr).header0, val, PK_SM2_ORDER_BIT_POS, PK_SM2_ORDER_MASK)

#define ICP_CCAT_FW_PK_SM2_SM9_ROLE_SET(hdr, val)                              \
    CCAT_FIELD_SET((hdr).header0, val, PK_SM2_SM9_ROLE_BIT_POS, PK_SM2_SM9_ROLE_MASK)

#define ICP_CCAT_FW_PK_SM9_ENC_TYPE_SET(hdr, val)                              \
    CCAT_FIELD_SET((hdr).header0, val, PK_SM9_ENC_TYPE_BIT_POS, PK_SM9_ENC_TYPE_MASK)

#define ICP_CCAT_FW_PK_SM9_PADDING_TYPE_SET(hdr, val)                          \
    CCAT_FIELD_SET((hdr).header0, val, PK_SM9_PADDING_TYPE_BIT_POS, PK_SM9_PADDING_TYPE_MASK)

#define ICP_CCAT_FW_PK_SECURITY_SET(hdr, val)                                  \
    CCAT_FIELD_SET((hdr).header0, val, PK_SECURITY_BIT_POS, PK_SECURITY_MASK)

#define ICP_CCAT_FW_PK_PIN_ENABLE_SET(hdr, val)                                \
    CCAT_FIELD_SET((hdr).header0, val, PK_PIN_ENABLE_BIT_POS, PK_PIN_ENABLE_MASK)

#define ICP_CCAT_FW_PK_KEY_MODE_SET(hdr, val)                                  \
    CCAT_FIELD_SET((hdr).header0, val, PK_KEY_MODE_BIT_POS, PK_KEY_MODE_MASK)


/******************************************************************************/
/****************************** Ctrl Define ***********************************/
/******************************************************************************/
/* ctrl alg(bit5~12) */
#define CTRL_CMD_ID_RAND_GET                                0
#define CTRL_CMD_ID_KEY_GEN                                 1
#define CTRL_CMD_ID_KEY_INPUT                               2
#define CTRL_CMD_ID_KEY_OUTPUT                              3
#define CTRL_CMD_ID_KEY_UPDATE                              4
#define CTRL_CMD_ID_KEY_CLEAR                               5
#define CTRL_CMD_ID_KEY_BASE_ADDR_SET                       6
#define CTRL_CMD_ID_OTP_PROG                                7
#define CTRL_CMD_ID_OTP_READ                                8
#define CTRL_CMD_ID_SYM_RESET                               15
#define CTRL_CMD_ID_SLEEP                                   17
#define CTRL_CMD_ID_WAKE                                    18
#define CTRL_CMD_ID_TDC_TEMP_GET                            19

/* pk header0 bit pos */
#define CTRL_SERVICE_TYPE_BIT_POS                           0
#define CTRL_FIRST_BIT_POS                                  3
#define CTRL_LAST_BIT_POS                                   4
#define CTRL_KEY_TYPE_BIT_POS                               5
#define CTRL_RSA_BITS_BIT_POS                               8
#define CTRL_PRF_PROC_BIT_POS                               10
#define CTRL_RING_ID_BIT_POS                                14
#define CTRL_RESERVED0_BIT_POS                              16
#define CTRL_CMD_ID_BIT_POS                                 24

/* pk header0 bits mask */
#define CTRL_SERVICE_TYPE_MASK                              0x7
#define CTRL_FIRST_MASK                                     0x1
#define CTRL_LAST_MASK                                      0x1
#define CTRL_KEY_TYPE_MASK                                  0x7
#define CTRL_RSA_BITS_MASK                                  0x3
#define CTRL_PRF_PROC_MASK                                  0xF
#define CTRL_RING_ID_MASK                                   0x3
#define CTRL_RESERVED0_MASK                                 0xFF
#define CTRL_CMD_ID_MASK                                    0xFF

/* pk header1 bit pos */
#define CTRL_KEY_ID_BIT_POS                                 0
#define CTRL_OTP_OFFSET_BIT_POS                             0
#define CTRL_KEK_ID_BIT_POS                                 16

/* pk header1 bits mask */
#define CTRL_KEY_ID_MASK                                    0xFFFF
#define CTRL_OTP_OFFSET_MASK                                0xFFFF
#define CTRL_KEK_ID_MASK                                    0xFFFF

/* HSM-CTRL Descriptor Header Key type Bits enumeration */
#define CTRL_KEY_TYPE_CIPHER                                0
#define CTRL_KEY_TYPE_AUTH                                  1
#define CTRL_KEY_TYPE_SM2                                   2
#define CTRL_KEY_TYPE_ECC                                   3
#define CTRL_KEY_TYPE_RSA                                   4
#define CTRL_KEY_TYPE_RSA_CRT                               5

#define LAC_CHECK_CTRL_KEY_TYPE(type)                                          \
    do                                                                         \
    {                                                                          \
        if ((type != CTRL_KEY_TYPE_CIPHER) &&                                  \
            (type != CTRL_KEY_TYPE_AUTH) &&                                    \
            (type != CTRL_KEY_TYPE_SM2) &&                                     \
            (type != CTRL_KEY_TYPE_ECC) &&                                     \
            (type != CTRL_KEY_TYPE_RSA) &&                                     \
            (type != CTRL_KEY_TYPE_RSA_CRT))                                   \
        {                                                                      \
            LAC_INVALID_PARAM_LOG(#type " is invalid");                        \
            return CPA_STATUS_INVALID_PARAM;                                   \
        }                                                                      \
    } while (0)

#define ICP_CCAT_FW_CTRL_SERVICE_TYPE_SET(hdr, val)                            \
    CCAT_FIELD_SET((hdr).header0, val, CTRL_SERVICE_TYPE_BIT_POS, CTRL_SERVICE_TYPE_MASK)

#define ICP_CCAT_FW_CTRL_FIRST_SET(hdr, val)                                   \
    CCAT_FIELD_SET((hdr).header0, val, CTRL_FIRST_BIT_POS, CTRL_FIRST_MASK)

#define ICP_CCAT_FW_CTRL_LAST_SET(hdr, val)                                    \
    CCAT_FIELD_SET((hdr).header0, val, CTRL_LAST_BIT_POS, CTRL_LAST_MASK)

#define ICP_CCAT_FW_CTRL_CMD_ID_SET(hdr, val)                                  \
    CCAT_FIELD_SET((hdr).header0, val, CTRL_CMD_ID_BIT_POS, CTRL_CMD_ID_MASK)

#define ICP_CCAT_FW_CTRL_KEY_TYPE_SET(hdr, val)                                \
    CCAT_FIELD_SET((hdr).header0, val, CTRL_KEY_TYPE_BIT_POS, CTRL_KEY_TYPE_MASK)

#define ICP_CCAT_FW_CTRL_KEY_ID_SET(hdr, val)                                  \
    CCAT_FIELD_SET((hdr).header1, val, CTRL_KEY_ID_BIT_POS, CTRL_KEY_ID_MASK)

#define ICP_CCAT_FW_CTRL_KEK_ID_SET(hdr, val)                                  \
    CCAT_FIELD_SET((hdr).header1, val, CTRL_KEK_ID_BIT_POS, CTRL_KEK_ID_MASK)

#define ICP_CCAT_FW_CTRL_OTP_OFFSET_SET(hdr, val)                              \
    CCAT_FIELD_SET((hdr).header1, val, CTRL_OTP_OFFSET_BIT_POS, CTRL_OTP_OFFSET_MASK)

#endif /* _ICP_CCAT_FW_H_ */

