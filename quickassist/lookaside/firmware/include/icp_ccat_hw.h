/*
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 *
 *   GPL LICENSE SUMMARY
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 *
 *   Contact Information:
 *   Intel Corporation
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  version: QAT1.7.L.4.13.0-00009
 */

/**
 *****************************************************************************
 * @file icp_ccat_hw.h
 * @defgroup icp_ccat_hw_defs ICP CCAT HW definitions
 * @ingroup icp_ccat_hw
 * @description
 *      This file documents definitions for the CCAT HW
 *
 *****************************************************************************/

#ifndef _ICP_CCAT_HW_H_
#define _ICP_CCAT_HW_H_

#define ICP_QAT_HW_CHAIN_CONTEXT_HASH_OFFSET        80

/* Reserve enough space for max length cipher key */
#define ICP_QAT_HW_CIPHER_CONTEXT_KEY_SIZE          32
#define ICP_QAT_HW_CIPHER_CONTEXT_IV_OFFSET         64

/* AUTH contest offset index*/
#define ICP_QAT_HW_AUTH_CONTEXT_KEY_OFFSET          0
#define ICP_QAT_HW_AUTH_CONTEXT_MAC_OFFSET          64

/* AEAD contest offset index*/
#define ICP_QAT_HW_CHAIN_CONTEXT_SYM_KEY_OFFSET         0
#define ICP_QAT_HW_CHAIN_CONTEXT_STEP_SIZE_OFFSET       32
#define ICP_QAT_HW_CHAIN_CONTEXT_J0_OFFSET              64
#define ICP_QAT_HW_CHAIN_CONTEXT_MAC_KEY_OFFSET         80
#define ICP_QAT_HW_CHAIN_CONTEXT_DIGEST_OFFSET          144
#define ICP_QAT_HW_CHAIN_CONTEXT_PAD_TYPE_OFFSET        200
#define ICP_QAT_HW_CHAIN_CONTEXT_PAD_BYTE_OFFSET        204
#define ICP_QAT_HW_CHAIN_CONTEXT_AAD_BITS_OFFSET        208
#define ICP_QAT_HW_CHAIN_CONTEXT_AAD_ADDR_OFFSET        216

#endif /* _ICP_CCAT_HW_H_ */
