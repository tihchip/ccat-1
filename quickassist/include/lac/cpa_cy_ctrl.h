/***************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 * 
 *   GPL LICENSE SUMMARY
 * 
 *   Copyright(c) 2007-2023 Intel Corporation. All rights reserved.
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 * 
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 * 
 *   Contact Information:
 *   Intel Corporation
 * 
 *   BSD LICENSE
 * 
 *   Copyright(c) 2007-2023 Intel Corporation. All rights reserved.
 *   All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 *  version: QAT.L.4.22.0-00001
 *
 ***************************************************************************/

/*
 *****************************************************************************
 * Doxygen group definitions
 ****************************************************************************/

/**
 *****************************************************************************
 * @file cpa_cy_ecsm2.h
 *
 * @defgroup cpaCyEcsm2 Elliptic Curve SM2 (ECSM2) API
 *
 * @ingroup cpaCy
 *
 * @description
 *      These functions specify the API for Public Key Encryption
 *      (Cryptography) SM2 operations.
 *
 *      Chinese Public Key Algorithm based on Elliptic Curve Theory
 *
 * @note
 *      The naming, terms, and reference on SM2 elliptic curve, and their
 *      flow of algorithms inside this API header file are from the link
 *      below, please kindly refer to it for details.
 *      https://tools.ietf.org/html/draft-shen-sm2-ecdsa-02
 *
 *****************************************************************************/

#ifndef CPA_CY_CTRL_H_
#define CPA_CY_CTRL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "cpa_cy_common.h"

typedef void (*CpaCyCtrlSymResetCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean symResetStatus);

typedef void (*CpaCyCtrlRandGetCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaFlatBuffer *pRand);

typedef void (*CpaCyCtrlKeyBaseAddrSetCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean setStatus);

typedef void (*CpaCyCtrlKeyGenCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean keyGenStatus);

typedef void (*CpaCyCtrlKeyInputCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean keyInputStatus);

typedef void (*CpaCyCtrlKeyOutputCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaFlatBuffer *pKey);

typedef void (*CpaCyCtrlKeyUpdateCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean updateStatus);

typedef void (*CpaCyCtrlKeyClearCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean clearStatus);

typedef void (*CpaCyCtrlOtpProgCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean otpProgStatus);

typedef void (*CpaCyCtrlOtpReadCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaFlatBuffer *pOtp);

typedef void (*CpaCyCtrlSleepCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean sleepStatus);

typedef void (*CpaCyCtrlWakeCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaBoolean wakeStatus);

typedef void (*CpaCyCtrlTdcTempCbFunc)(void *pCallbackTag,
        CpaStatus status,
        void *pOpData,
        CpaFlatBuffer *pTdcTemp);



typedef struct _CpaCyCtrlRandGetOpData {
   Cpa32U randLength;
    /**< the rand length in bytes */
} CpaCyCtrlRandGetOpData;

typedef struct _CpaCyCtrlKeyBaseAddrSetOpData {
    Cpa32U keyType;
    /**< key type */
    Cpa32U keyBaseAddr;
    /**< the key base asdr */
    Cpa32U keyLen;
    /**< the length to the key */
} CpaCyCtrlKeyBaseAddrSetOpData;

typedef struct _CpaCyCtrlKeyGenOpData {
    Cpa32U keyType;
    /**< key type */
    Cpa32U keyId;
    /**< internel key id */
} CpaCyCtrlKeyGenOpData;

typedef struct _CpaCyCtrlKeyInputOpData {
    Cpa32U keyType;
    /**< key type */
    Cpa32U keyId;
    /**< internel key id */
    Cpa32U kekId;
    /**< internel kek id */
    CpaFlatBuffer key;
    /**< the key to update */
} CpaCyCtrlKeyInputOpData;

typedef struct _CpaCyCtrlKeyOutputOpData {
    Cpa32U keyType;
    /**< key type */
    Cpa32U keyId;
    /**< internel key id */
    Cpa32U kekId;
    /**< internel kek id */
} CpaCyCtrlKeyOutputOpData;

typedef struct _CpaCyCtrlKeyUpdateOpData {
    Cpa32U keyType;
    /**< key type */
    Cpa32U keyId;
    /**< internel key id */
    Cpa32U kekId;
    /**< internel kek id */
    CpaFlatBuffer key;
    /**< the key to update */
} CpaCyCtrlKeyUpdateOpData;

typedef struct _CpaCyCtrlKeyClearOpData {
    Cpa32U keyType;
    /**< key type */
    Cpa32U keyId;
    /**< internel key id */
} CpaCyCtrlKeyClearOpData;

typedef struct _CpaCyCtrlOtpProgOpData {
    Cpa32U offset;
    /**< Otp offset adderss */
    CpaFlatBuffer otpData;
    /**< Otp data */
} CpaCyCtrlOtpProgOpData;

typedef struct _CpaCyCtrlOtpReadOpData {
    Cpa32U offset;
    /**< Otp offset adderss */
} CpaCyCtrlOtpReadOpData;

/**
 *****************************************************************************
 * @file cpa_cy_ctrl.h
 * @ingroup cpaCyCtrl
 *      Cryptographic CTRL Statistics.
 * @description
 *      This structure contains statistics on the Cryptographic CTRL
 *      operations. Statistics are set to zero when the component is
 *      initialized, and are collected per instance.
 *
 ****************************************************************************/
typedef struct _CpaCyCtrlStats64 {
    Cpa64U numCtrlRandGetRequests;
    /**< Total number of CTRL Rand Get operation requests. */
    Cpa64U numCtrlRandGetRequestErrors;
    /**< Total number of CTRL Rand Get operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlRandGetCompleted;
    /**< Total number of CTRL Rand Get operation requests that
     * completed successfully. */
    Cpa64U numCtrlRandGetCompletedError;
    /**< Total number of CTRL Rand Get operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlRandGetCompletedOutputInvalid;
    /**< Total number of CTRL Rand Get
     * requests that could not be completed successfully due to an invalid
     * output. Note that this does not indicate an error. */

    Cpa64U numCtrlKeyGenRequests;
    /**< Total number of CTRL Key Generate operation requests. */
    Cpa64U numCtrlKeyGenRequestErrors;
    /**< Total number of CTRL Key Generate operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlKeyGenCompleted;
    /**< Total number of CTRL Key Generate operation requests that
     * completed successfully. */
    Cpa64U numCtrlKeyGenCompletedError;
    /**< Total number of CTRL Key Generate operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlKeyGenCompletedOutputInvalid;
    /**< Total number of CTRL Key Generate
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlKeyInputRequests;
    /**< Total number of CTRL Key Input operation requests. */
    Cpa64U numCtrlKeyInputRequestErrors;
    /**< Total number of CTRL Key Input operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlKeyInputCompleted;
    /**< Total number of CTRL Key Input operation requests that
     * completed successfully. */
    Cpa64U numCtrlKeyInputCompletedError;
    /**< Total number of CTRL Key Input operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlKeyInputCompletedOutputInvalid;
    /**< Total number of CTRL Key Input
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlKeyOutputRequests;
    /**< Total number of CTRL Key Output operation requests. */
    Cpa64U numCtrlKeyOutputRequestErrors;
    /**< Total number of CTRL Key Output operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlKeyOutputCompleted;
    /**< Total number of CTRL Key Output operation requests that
     * completed successfully. */
    Cpa64U numCtrlKeyOutputCompletedError;
    /**< Total number of CTRL Key Output operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlKeyOutputCompletedOutputInvalid;
    /**< Total number of CTRL Key Output
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlKeyUpdateRequests;
    /**< Total number of CTRL Key Update operation requests. */
    Cpa64U numCtrlKeyUpdateRequestErrors;
    /**< Total number of CTRL Key Update operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlKeyUpdateCompleted;
    /**< Total number of CTRL Key Update operation requests that
     * completed successfully. */
    Cpa64U numCtrlKeyUpdateCompletedError;
    /**< Total number of CTRL Key Update operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlKeyUpdateCompletedOutputInvalid;
    /**< Total number of CTRL Key Update
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlKeyClearRequests;
    /**< Total number of CTRL Key Clear operation requests. */
    Cpa64U numCtrlKeyClearRequestErrors;
    /**< Total number of CTRL Key Clear operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlKeyClearCompleted;
    /**< Total number of CTRL Key Clear operation requests that
     * completed successfully. */
    Cpa64U numCtrlKeyClearCompletedError;
    /**< Total number of CTRL Key Clear operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlKeyClearCompletedOutputInvalid;
    /**< Total number of CTRL Key Clear
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlKeyBaseAddrSetRequests;
    /**< Total number of CTRL Key Base Addr Set operation requests. */
    Cpa64U numCtrlKeyBaseAddrSetRequestErrors;
    /**< Total number of CTRL Key Base Addr Set operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlKeyBaseAddrSetCompleted;
    /**< Total number of CTRL Key Base Addr Set operation requests that
     * completed successfully. */
    Cpa64U numCtrlKeyBaseAddrSetCompletedError;
    /**< Total number of CTRL Key Base Addr Set operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlKeyBaseAddrSetCompletedOutputInvalid;
    /**< Total number of CTRL Key Base Addr Set
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlOtpProgRequests;
    /**< Total number of CTRL Otp Prog operation requests. */
    Cpa64U numCtrlOtpProgRequestErrors;
    /**< Total number of CTRL Otp Prog operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlOtpProgCompleted;
    /**< Total number of CTRL Otp Prog operation requests that
     * completed successfully. */
    Cpa64U numCtrlOtpProgCompletedError;
    /**< Total number of CTRL Otp Prog operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlOtpProgCompletedOutputInvalid;
    /**< Total number of CTRL Otp Prog
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlOtpReadRequests;
    /**< Total number of CTRL Otp Read operation requests. */
    Cpa64U numCtrlOtpReadRequestErrors;
    /**< Total number of CTRL Otp Read operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlOtpReadCompleted;
    /**< Total number of CTRL Otp Read operation requests that
     * completed successfully. */
    Cpa64U numCtrlOtpReadCompletedError;
    /**< Total number of CTRL Otp Read operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlOtpReadCompletedOutputInvalid;
    /**< Total number of CTRL Otp Read
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlSymResetRequests;
    /**< Total number of CTRL SYM Reset operation requests. */
    Cpa64U numCtrlSymResetRequestErrors;
    /**< Total number of CTRL SYM Reset operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlSymResetCompleted;
    /**< Total number of CTRL SYM Reset operation requests that
     * completed successfully. */
    Cpa64U numCtrlSymResetCompletedError;
    /**< Total number of CTRL SYM Reset operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlSymResetCompletedOutputInvalid;
    /**< Total number of CTRL SYM Reset
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlSleepRequests;
    /**< Total number of CTRL Sleep operation requests. */
    Cpa64U numCtrlSleepRequestErrors;
    /**< Total number of CTRL Sleep operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlSleepCompleted;
    /**< Total number of CTRL Sleep operation requests that
     * completed successfully. */
    Cpa64U numCtrlSleepCompletedError;
    /**< Total number of CTRL Sleep operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlSleepCompletedOutputInvalid;
    /**< Total number of CTRL Sleep
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlWakeRequests;
    /**< Total number of CTRL Wake operation requests. */
    Cpa64U numCtrlWakeRequestErrors;
    /**< Total number of CTRL Wake operation requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlWakeCompleted;
    /**< Total number of CTRL Wake operation requests that
     * completed successfully. */
    Cpa64U numCtrlWakeCompletedError;
    /**< Total number of CTRL Wake operation requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlWakeCompletedOutputInvalid;
    /**< Total number of CTRL Wake
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */

    Cpa64U numCtrlTdcTempRequests;
    /**< Total number of CTRL TDC Temperature Get operation requests. */
    Cpa64U numCtrlTdcTempRequestErrors;
    /**< Total number of CTRL TDC Temperature Get requests that
     * had an error and could not be processed. */
    Cpa64U numCtrlTdcTempCompleted;
    /**< Total number of CTRL TDC Temperature Get requests that
     * completed successfully. */
    Cpa64U numCtrlTdcTempCompletedError;
    /**< Total number of CTRL TDC Temperature Get requests that
     * could not be completed successfully due to errors. */
    Cpa64U numCtrlTdcTempCompletedOutputInvalid;
    /**< Total number of CTRL TDC Temperature Get
     * operation requests that could not be completed successfully due to an
     * invalid output. Note that this does not indicate an error. */
} CpaCyCtrlStats64;


CpaStatus
cpaCyCtrlSymReset(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlSymResetCbFunc pSymResetCb,
                void *pCallbackTag,
                CpaBoolean *pSymResetStatus);

CpaStatus
cpaCyCtrlRandGet(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlRandGetCbFunc pRandGetCb,
                void *pCallbackTag,
                const CpaCyCtrlRandGetOpData *pRandGetOpData,
                CpaFlatBuffer *pRandOutputData);

CpaStatus
cpaCyCtrlKeyBaseAddrSet(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlKeyBaseAddrSetCbFunc pKeyAddrCb,
                void *pCallbackTag,
                const CpaCyCtrlKeyBaseAddrSetOpData *pKeyAddrOpData,
                CpaBoolean *pKeyAddrStatus);

CpaStatus
cpaCyCtrlKeyGen(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlKeyGenCbFunc pKeyGenCb,
                void *pCallbackTag,
                const CpaCyCtrlKeyGenOpData *pKeyGenOpData,
                CpaBoolean *pKeyGenStatus);

CpaStatus
cpaCyCtrlKeyInput(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlKeyInputCbFunc pKeyInputCb,
                void *pCallbackTag,
                const CpaCyCtrlKeyInputOpData *pKeyInputeOpData,
                CpaBoolean *pInputStatus);

CpaStatus
cpaCyCtrlKeyOutput(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlKeyOutputCbFunc pKeyOutputCb,
                void *pCallbackTag,
                const CpaCyCtrlKeyOutputOpData *pKeyOutputOpData,
                CpaFlatBuffer *pKeyOutputData);

CpaStatus
cpaCyCtrlKeyUpdate(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlKeyUpdateCbFunc pKeyUpdateCb,
                void *pCallbackTag,
                const CpaCyCtrlKeyUpdateOpData *pKeyUpdateOpData,
                CpaBoolean *pUpdateStatus);

CpaStatus
cpaCyCtrlKeyClear(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlKeyClearCbFunc pKeyClearCb,
                void *pCallbackTag,
                const CpaCyCtrlKeyClearOpData *pKeyChearOpData,
                CpaBoolean *pClearStatus);

CpaStatus
cpaCyCtrlOtpProg(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlOtpProgCbFunc pOtpProgCb,
                void *pCallbackTag,
                const CpaCyCtrlOtpProgOpData *pOtpProgOpData,
                CpaBoolean *pOtpProgStatus);

CpaStatus
cpaCyCtrlOtpRead(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlOtpReadCbFunc pOtpReadCb,
                void *pCallbackTag,
                const CpaCyCtrlOtpReadOpData *pOtpReadOpData,
                CpaFlatBuffer *pOtpOutputData);

CpaStatus
cpaCyCtrlSleep(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlSleepCbFunc pSleepCb,
                void *pCallbackTag,
                CpaBoolean *pSleepStatus);

CpaStatus
cpaCyCtrlWake(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlWakeCbFunc pWakeCb,
                void *pCallbackTag,
                CpaBoolean *pWakeStatus);

CpaStatus
cpaCyCtrlGetTdcTemperature(const CpaInstanceHandle instanceHandle_in,
                const CpaCyCtrlTdcTempCbFunc pTempCb,
                void *pCallbackTag,
                CpaFlatBuffer *pTempOutputData);

CpaStatus
cpaCyCtrlQueryStats64(const CpaInstanceHandle instanceHandle_in,
                CpaCyCtrlStats64 *pCtrlStats);

#ifdef __cplusplus
} /* close the extern "C" { */
#endif

#endif /*CPA_CY_CTRL_H_*/