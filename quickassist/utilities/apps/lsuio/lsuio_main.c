/***************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 *
 *   GPL LICENSE SUMMARY
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 *
 *   Contact Information:
 *   Intel Corporation
 *
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  version: QAT1.7.L.4.13.0-00009
 *
 ****************************************************************************/
/*
 * This example shows how to use the driver ioctl interface for managing
 * the device-utilization feature
 *
 */
/*
*******************************************************************************
* Include public/global header files
*******************************************************************************
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>          /* Definition of AT_* constants */
#include <dirent.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define LS_UIO_MAX_NAME_SIZE    512
#define LS_UIO_MAX_NUM          255

#define LS_UIO_INVALID_SIZE     -1
#define LS_UIO_INVALID_ADDR     (~0)

#define LS_UIO_MMAP_NOT_DONE    0
#define LS_UIO_MMAP_OK          1
#define LS_UIO_MMAP_FAILED      2

/* This should be identical to the define in include/linux/uio_driver.h */
#define LS_MAX_UIO_MAPS         5

struct uio_map_t {
    unsigned long addr;
    int size;
    int mmap_result;
};

struct uio_dev_attr_t {
    char name[LS_UIO_MAX_NAME_SIZE];
    char value[LS_UIO_MAX_NAME_SIZE];
    struct uio_dev_attr_t *next;
};

struct uio_info_t {
    int uio_num;
    struct uio_map_t maps[LS_MAX_UIO_MAPS];
    unsigned long event_count;
    char name[LS_UIO_MAX_NAME_SIZE];
    char version[LS_UIO_MAX_NAME_SIZE];
    struct uio_dev_attr_t *dev_attrs;
    struct uio_info_t* next;            /* for linked list */
};

static int opt_mmap;
static int opt_verbose;
static int opt_uiodev;
static int opt_help;
static int uio_filter;

static void usage(int status);

/* The name the program was run with, stripped of any leading path. */
char *program_name;

static struct option const long_options[] = {
    {"help", no_argument, 0, 'h'},
    {"mmap", no_argument, 0, 'm'},
    {"uiodev", required_argument, 0, 'u'},
    {NULL, 0, NULL, 0}
};

static int uio_get_mem_size(struct uio_info_t *info, int map_num)
{
    int ret;
    char filename[64];
    info->maps[map_num].size = LS_UIO_INVALID_SIZE;
    sprintf(filename, "/sys/class/uio/uio%d/maps/map%d/size",
            info->uio_num, map_num);
    FILE *file = fopen(filename, "r");
    if (!file) return -1;
    ret = fscanf(file, "0x%x", &info->maps[map_num].size);
    fclose(file);
    if (ret < 0) return -2;
    return 0;
}

static int uio_get_mem_addr(struct uio_info_t *info, int map_num)
{
    int ret;
    char filename[64];
    info->maps[map_num].addr = LS_UIO_INVALID_ADDR;
    sprintf(filename, "/sys/class/uio/uio%d/maps/map%d/addr",
            info->uio_num, map_num);
    FILE *file = fopen(filename, "r");
    if (!file) return -1;
    ret = fscanf(file, "0x%lx", &info->maps[map_num].addr);
    fclose(file);
    if (ret < 0) return -2;
    return 0;
}

static int uio_get_event_count(struct uio_info_t *info)
{
    int ret;
    char filename[64];
    info->event_count = 0;
    sprintf(filename, "/sys/class/uio/uio%d/event", info->uio_num);
    FILE *file = fopen(filename, "r");
    if (!file) return -1;
    ret = fscanf(file, "%ld", &info->event_count);
    fclose(file);
    if (ret < 0) return -2;
    return 0;
}

static int line_from_file(char *filename, char *linebuf)
{
    char *s;
    int i;
    memset(linebuf, 0, LS_UIO_MAX_NAME_SIZE);
    FILE *file = fopen(filename, "r");
    if (!file) return -1;
    s = fgets(linebuf, LS_UIO_MAX_NAME_SIZE, file);
    if (!s) return -2;
    for (i = 0; (*s) && (i < LS_UIO_MAX_NAME_SIZE); i++) {
        if (*s == '\n') *s = 0;
        s++;
    }
    return 0;
}

static int uio_get_name(struct uio_info_t *info)
{
    char filename[64];
    sprintf(filename, "/sys/class/uio/uio%d/name", info->uio_num);

    return line_from_file(filename, info->name);
}

static int uio_get_version(struct uio_info_t *info)
{
    char filename[64];
    sprintf(filename, "/sys/class/uio/uio%d/version", info->uio_num);

    return line_from_file(filename, info->version);
}

static int uio_num_from_filename(char *name)
{
    enum scan_states { ss_u, ss_i, ss_o, ss_num, ss_err };
    enum scan_states state = ss_u;
    int i = 0, num = -1;
    char ch = name[0];
    while (ch && (state != ss_err)) {
        switch (ch) {
        case 'u':
            if (state == ss_u) state = ss_i;
            else state = ss_err;
            break;
        case 'i':
            if (state == ss_i) state = ss_o;
            else state = ss_err;
            break;
        case 'o':
            if (state == ss_o) state = ss_num;
            else state = ss_err;
            break;
        default:
            if ((ch >= '0') && (ch <= '9')
                && (state == ss_num)) {
                if (num < 0) num = (ch - '0');
                else num = (num * 10) + (ch - '0');
            } else state = ss_err;
        }
        i++;
        ch = name[i];
    }
    if (state == ss_err) num = -1;
    return num;
}

static struct uio_info_t* info_from_name(char *name, int filter_num)
{
    struct uio_info_t* info;
    int num = uio_num_from_filename(name);
    if (num < 0)
        return NULL;
    if ((filter_num >= 0) && (num != filter_num))
        return NULL;

    info = malloc(sizeof(struct uio_info_t));
    if (!info)
        return NULL;
    memset(info, 0, sizeof(struct uio_info_t));
    info->uio_num = num;

    return info;
}

static struct uio_info_t *uio_find_devices(int filter_num)
{
    struct dirent **namelist;
    struct uio_info_t *infolist = NULL, *infp, *last = NULL;
    int n;

    n = scandir("/sys/class/uio", &namelist, 0, alphasort);
    if (n < 0)
        return NULL;

    while (n--) {
        infp = info_from_name(namelist[n]->d_name, filter_num);
        free(namelist[n]);
        if (!infp)
            continue;
        if (!infolist)
            infolist = infp;
        else
            last->next = infp;
        last = infp;
    }
    free(namelist);

    return infolist;
}

static int decode_switches(int argc, char **argv)
{
    int opt, opt_index = 0;
    uio_filter = -1;

    while (1) {
        opt = getopt_long(argc, argv, "hmu:v", long_options, &opt_index);
        if (opt == EOF)
            break;
        switch (opt) {
        case 'm' :
            opt_mmap = 1;
            break;
        case 'u' :
            opt_uiodev = 1;
            uio_filter = atoi(optarg);
        case 'v' :
            opt_verbose = 1;
            break;
            break;
        case 'h' :
            opt_help = 1;
            break;
        }
    }

    return 0;
}

static int uio_get_all_info(struct uio_info_t *info)
{
    int i;
    if (!info)
        return -1;
    if ((info->uio_num < 0) || (info->uio_num > LS_UIO_MAX_NUM))
        return -1;
    for (i = 0; i < LS_MAX_UIO_MAPS; i++) {
        uio_get_mem_size(info, i);
        uio_get_mem_addr(info, i);
    }
    uio_get_event_count(info);
    uio_get_name(info);
    uio_get_version(info);
    return 0;
}

static int dev_attr_filter(char *filename)
{
    struct stat filestat;

    if (lstat(filename, &filestat))
        return 0;
    if (S_ISREG(filestat.st_mode))
        return 1;
    return 0;
}

static int uio_get_device_attributes(struct uio_info_t *info)
{
    struct dirent **namelist;
    struct uio_dev_attr_t *attr, *last = NULL;
    char fullname[512];
    int n;

    info->dev_attrs = NULL;
    sprintf(fullname, "/sys/class/uio/uio%d/device", info->uio_num);
    n = scandir(fullname, &namelist, 0, alphasort);
    if (n < 0)
        return -1;

    while (n--) {
        sprintf(fullname, "/sys/class/uio/uio%d/device/%s",
                info->uio_num, namelist[n]->d_name);
        if (!dev_attr_filter(fullname))
            continue;
        attr = malloc(sizeof(struct uio_dev_attr_t));
        if (!attr)
            return -1;
        strncpy(attr->name, namelist[n]->d_name, LS_UIO_MAX_NAME_SIZE);
        free(namelist[n]);
        if (line_from_file(fullname, attr->value)) {
            free(attr);
            continue;
        }

        if (!info->dev_attrs)
            info->dev_attrs = attr;
        else
            last->next = attr;
        attr->next = NULL;
        last = attr;
    }
    free(namelist);

    return 0;
}

static void uio_single_mmap_test(struct uio_info_t *info, int map_num)
{
    info->maps[map_num].mmap_result = LS_UIO_MMAP_NOT_DONE;
    if (info->maps[map_num].size <= 0)
        return;
    info->maps[map_num].mmap_result = LS_UIO_MMAP_FAILED;
    char dev_name[16];
    sprintf(dev_name, "/dev/uio%d", info->uio_num);
    int fd = open(dev_name, O_RDONLY);
    if (fd < 0)
        return;
    void *map_addr = mmap(NULL,
                          info->maps[map_num].size,
                          PROT_READ,
                          MAP_SHARED,
                          fd,
                          map_num * getpagesize());
    if (map_addr != MAP_FAILED) {
        info->maps[map_num].mmap_result = LS_UIO_MMAP_OK;
        munmap(map_addr, info->maps[map_num].size);
    }
    close(fd);

    return;
}

static void uio_mmap_test(struct uio_info_t *info)
{
    int map_num;
    for (map_num = 0; map_num < LS_MAX_UIO_MAPS; map_num++)
        uio_single_mmap_test(info, map_num);

    return;
}

static void show_device(struct uio_info_t *info)
{
    char dev_name[16];
    sprintf(dev_name, "uio%d", info->uio_num);
    printf("%s: name=%s, version=%s, events=%lu\n",
           dev_name, info->name, info->version, info->event_count);

    return;
}

static int show_map(struct uio_info_t *info, int map_num)
{
    if (info->maps[map_num].size <= 0)
        return -1;

    printf("\tmap[%d]: addr=0x%08lX, size=%d",
           map_num,
           info->maps[map_num].addr,
           info->maps[map_num].size);

    if (opt_mmap) {
        printf(", mmap test: ");
        switch (info->maps[map_num].mmap_result) {
        case LS_UIO_MMAP_NOT_DONE:
            printf("N/A");
            break;
        case LS_UIO_MMAP_OK:
            printf("OK");
            break;
        default:
            printf("FAILED");
        }
    }
    printf("\n");
    return 0;
}

static void show_dev_attrs(struct uio_info_t *info)
{
    struct uio_dev_attr_t *attr = info->dev_attrs;
    if (attr)
        printf("\tDevice attributes:\n");
    else
        printf("\t(No device attributes)\n");

    while (attr) {
        printf("\t%s=%s\n", attr->name, attr->value);
        attr = attr->next;
    }

    return;
}

static void show_maps(struct uio_info_t *info)
{
    int ret;
    int mi = 0;
    do {
        ret = show_map(info, mi);
        mi++;
    } while ((ret == 0) && (mi < LS_MAX_UIO_MAPS));

    return;
}

static void show_uio_info(struct uio_info_t *info)
{
    show_device(info);
    show_maps(info);
    if (opt_verbose) show_dev_attrs(info);

    return;
}

static void uio_free_dev_attrs(struct uio_info_t *info)
{
    struct uio_dev_attr_t *p1, *p2;
    p1 = info->dev_attrs;
    while (p1) {
        p2 = p1->next;
        free(p1);
        p1 = p2;
    }
    info->dev_attrs = NULL;

    return;
}

static void uio_free_info(struct uio_info_t *info)
{
    struct uio_info_t *p1, *p2;
    p1 = info;
    while (p1) {
        uio_free_dev_attrs(p1);
        p2 = p1->next;
        free(p1);
        p1 = p2;
    }
    return;
}

int main(int argc, char **argv)
{
    struct uio_info_t *info_list, *p = NULL;

    program_name = argv[0];

    decode_switches(argc, argv);

    if (opt_help)
        usage(0);

    info_list = uio_find_devices(uio_filter);
    if (!info_list)
        printf("No UIO devices found.\n");

    p = info_list;
    while (p) {
        uio_get_all_info(p);
        if (opt_verbose) uio_get_device_attributes(p);
        if (opt_mmap) uio_mmap_test(p);
        show_uio_info(p);
        p = p->next;
    }

    uio_free_info(info_list);
    return 0;
}

static void usage(int status)
{
    printf("%s - List UIO devices.\n", program_name);
    printf("Usage: %s [OPTIONS]\n", program_name);
    printf("\
Options:\n\
  -h, --help       display this help and exit\n\
  -m, --mmap       test if mmap() works for all mappings\n\
  -v, --verbose    also display device attributes\n\
");
    exit(status);
}

