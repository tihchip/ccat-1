/**
 * @file OsalServices.c (user space)
 *
 * @brief Implementation for Mem, Sleep and Log.
 *
 *
 * @par
 *   BSD LICENSE
 *
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  version: QAT1.7.L.4.13.0-00009
 */

#define __STDC_WANT_LIB_EXT1__ 1
#include <stdarg.h>
#include <stdio.h>
#include <ctype.h>
#include <bits/time.h>
#include <string.h>
#include <sched.h>
#include <time.h>
#include <sys/utsname.h>
#include <sys/time.h>
#include <errno.h>
#include "Osal.h"

#define OSAL_IOFILE struct _IO_FILE

#define OSAL_TIMEVAL_TOO_LARGE 0

/*********************
 * Log function
 *********************/
static char *traceHeaders[] = {"",
                               "[fatal] ",
                               "[error] ",
                               "[warn] ",
                               "[message] ",
                               "[debug1] ",
                               "[debug2] ",
                               "[debug3] ",
                               "[all] "
                              };

static CHAR osalModuleName[OSAL_MAX_MODULE_NAME_LENGTH] = "";

OSAL_PRIVATE UINT32 max_UINT32 = 0xFFFFFFFF;

/* by default trace all but debug message */
OSAL_PRIVATE int osalCurrLogLevel = OSAL_LOG_LVL_ALL;

#ifdef ICP_LOG_SYSLOG
OSAL_PRIVATE int osalCurrOutput = OSAL_LOG_OUTPUT_SYSLOG;
#else
OSAL_PRIVATE int osalCurrOutput = OSAL_LOG_OUTPUT_STD;
#endif

/* Writing Log */
INT32
osalLog(OsalLogLevel level, OsalLogDevice device, char *format, ...)
{
    OSAL_IOFILE *output_stream_s;
    int mask = 0;
    va_list args;
    /*
     * Return -1 for custom display devices
     */
    switch (device)
    {
        case OSAL_LOG_DEV_STDOUT:
            output_stream_s = stdout;
            break;
        case OSAL_LOG_DEV_STDERR:
            output_stream_s = stderr;
            break;
        default:
            printf("osalLog: only OSAL_LOG_DEV_STDOUT"
                   " and OSAL_LOG_DEV_STDERR are supported \n");
            return (OSAL_LOG_ERROR);
    }

    if ((level <= osalCurrLogLevel) && (level != OSAL_LOG_LVL_NONE))
    {
        if (OSAL_LOG_OUTPUT_SYSLOG == osalCurrOutput)
        {
            mask = setlogmask(0);
            mask = setlogmask(mask | LOG_MASK(LOG_INFO));
            if (level != OSAL_LOG_LVL_USER)
            {
                syslog(LOG_INFO, "%s", traceHeaders[level - 1]);
            }
            if (OSAL_OS_GET_STRING_LENGTH(osalModuleName,
                                          sizeof(osalModuleName)) != 0)
            {
                syslog(LOG_INFO, "%s :", osalModuleName);
            }

            va_start(args, format);
            vsyslog(LOG_INFO, format, args);
            va_end(args);
            return 0;
        }
        INT32 headerByteCount =
            (level == OSAL_LOG_LVL_USER)
            ? 0
            : fprintf(output_stream_s, "%s", traceHeaders[level - 1]);

        if (OSAL_OS_GET_STRING_LENGTH(osalModuleName, sizeof(osalModuleName)) !=
                0)
        {
            headerByteCount += fprintf(output_stream_s, "%s :", osalModuleName);
        }

        va_start(args, format);
        headerByteCount += vfprintf(output_stream_s, format, args);
        va_end(args);
        return headerByteCount;
    }
    else
    {
        return (OSAL_LOG_ERROR);
    }
}

OSAL_PUBLIC OSAL_STATUS osalStdLog(const char *arg_pFmtString, ...)
{
    OSAL_STATUS err = OSAL_SUCCESS;
    va_list argList;

    va_start(argList, arg_pFmtString);
    if (OSAL_OS_GET_STRING_LENGTH(osalModuleName, sizeof(osalModuleName)) != 0)
    {
        printf("%s :", osalModuleName);
    }

    vprintf(arg_pFmtString, argList);
    va_end(argList);

    return err;
}

/* Returns the old level that got overwritten */
OSAL_PUBLIC UINT32 osalLogLevelSet(UINT32 level)
{
    UINT32 oldLevel;

    /*
     * Check value first
     */
    if (level > OSAL_LOG_LVL_ALL)
    {
        osalLog(OSAL_LOG_LVL_MESSAGE,
                OSAL_LOG_DEV_STDOUT,
                "osalLogLevelSet: Log Level is between %d and %d \n",
                OSAL_LOG_LVL_NONE,
                OSAL_LOG_LVL_ALL);
        return OSAL_LOG_LVL_NONE;
    }
    oldLevel = osalCurrLogLevel;

    osalCurrLogLevel = level;

    return oldLevel;
}

OSAL_PUBLIC void osalLogModuleSet(const char *name)
{
    snprintf(osalModuleName, OSAL_MAX_MODULE_NAME_LENGTH, "%s", name);
}

OSAL_PUBLIC void osalLogOutputSet(UINT32 output)
{
    osalCurrOutput = output;
}

/* Print Hexdump */
enum
{
    DUMP_PREFIX_NONE,
    DUMP_PREFIX_ADDRESS,
    DUMP_PREFIX_OFFSET
};

const char hex_asc[] = "0123456789abcdef";
#define hex_asc_lo(x)    hex_asc[((x) & 0x0f)]
#define hex_asc_hi(x)    hex_asc[((x) & 0xf0) >> 4]

/**
 * vscnprintf - Format a string and place it in a buffer
 * @buf: The buffer to place the result into
 * @size: The size of the buffer, including the trailing null space
 * @fmt: The format string to use
 * @args: Arguments for the format string
 *
 * The return value is the number of characters which have been written into
 * the @buf not including the trailing '\0'. If @size is == 0 the function
 * returns 0.
 *
 * If you're not already dealing with a va_list consider using scnprintf().
 *
 * See the vsnprintf() documentation for format string extensions over C99.
 */
int vscnprintf(char *buf, size_t size, const char *fmt, va_list args)
{
    int i;

    i = vsnprintf(buf, size, fmt, args);

    if (likely(i < size))
        return i;
    if (size != 0)
        return size - 1;
    return 0;
}

/**
 * scnprintf - Format a string and place it in a buffer
 * @buf: The buffer to place the result into
 * @size: The size of the buffer, including the trailing null space
 * @fmt: The format string to use
 * @...: Arguments for the format string
 *
 * The return value is the number of characters written into @buf not including
 * the trailing '\0'. If @size is == 0 the function returns 0.
 */
int scnprintf(char *buf, size_t size, const char *fmt, ...)
{
    va_list args;
    int i;

    va_start(args, fmt);
    i = vscnprintf(buf, size, fmt, args);
    va_end(args);

    return i;
}

/**
 * osalHexdumpToBuffer - convert a blob of data to "hex ASCII" in memory
 * @buf: data blob to dump
 * @len: number of bytes in the @buf
 * @rowsize: number of bytes to print per line; must be 16 or 32
 * @groupsize: number of bytes to print at a time (1, 2, 4, 8; default = 1)
 * @linebuf: where to put the converted data
 * @linebuflen: total size of @linebuf, including space for terminating NUL
 * @ascii: include ASCII after the hex output
 *
 * osalHexdumpToBuffer() works on one "line" of output at a time, i.e.,
 * 16 or 32 bytes of input data converted to hex + ASCII output.
 *
 * Given a buffer of u8 data, osalHexdumpToBuffer() converts the input data
 * to a hex + ASCII dump at the supplied memory location.
 * The converted output is always NUL-terminated.
 *
 * E.g.:
 *   osalHexdumpToBuffer(frame->data, frame->len, 16, 1,
 *			linebuf, sizeof(linebuf), true);
 *
 * example output buffer:
 * 40 41 42 43 44 45 46 47 48 49 4a 4b 4c 4d 4e 4f  @ABCDEFGHIJKLMNO
 *
 * Return:
 * The amount of bytes placed in the buffer without terminating NUL. If the
 * output was truncated, then the return value is the number of bytes
 * (excluding the terminating NUL) which would have been written to the final
 * string if enough space had been available.
 */
void osalHexdumpToBuffer(const void *buf, size_t len, int rowsize,
                         int groupsize, char *linebuf, size_t linebuflen,
                         int ascii)
{
    const UINT8 *ptr = buf;
    UINT8 ch;
    int j, lx = 0;
    int ascii_column;

    if (rowsize != 16 && rowsize != 32)
        rowsize = 16;

    if (!len)
        goto nil;
    if (len > rowsize)		/* limit to one line at a time */
        len = rowsize;
    if ((len % groupsize) != 0)	/* no mixed size output */
        groupsize = 1;

    switch (groupsize)
    {
        case 8:
            {
                const UINT64 *ptr8 = buf;
                int ngroups = len / groupsize;

                for (j = 0; j < ngroups; j++)
                    lx += scnprintf(linebuf + lx, linebuflen - lx,
                                    "%s%16.16llx", j ? " " : "",
                                    (unsigned long long) * (ptr8 + j));
                ascii_column = 17 * ngroups + 2;
                break;
            }

        case 4:
            {
                const UINT32 *ptr4 = buf;
                int ngroups = len / groupsize;

                for (j = 0; j < ngroups; j++)
                    lx += scnprintf(linebuf + lx, linebuflen - lx,
                                    "%s%8.8x", j ? " " : "", *(ptr4 + j));
                ascii_column = 9 * ngroups + 2;
                break;
            }

        case 2:
            {
                const UINT16 *ptr2 = buf;
                int ngroups = len / groupsize;

                for (j = 0; j < ngroups; j++)
                    lx += scnprintf(linebuf + lx, linebuflen - lx,
                                    "%s%4.4x", j ? " " : "", *(ptr2 + j));
                ascii_column = 5 * ngroups + 2;
                break;
            }

        default:
            for (j = 0; (j < len) && (lx + 3) <= linebuflen; j++)
            {
                ch = ptr[j];
                linebuf[lx++] = hex_asc_hi(ch);
                linebuf[lx++] = hex_asc_lo(ch);
                linebuf[lx++] = ' ';
            }
            if (j)
                lx--;

            ascii_column = 3 * rowsize + 2;
            break;
    }
    if (!ascii)
        goto nil;

    while (lx < (linebuflen - 1) && lx < (ascii_column - 1))
        linebuf[lx++] = ' ';
    for (j = 0; (j < len) && (lx + 2) < linebuflen; j++)
    {
        ch = ptr[j];
        linebuf[lx++] = (isascii(ch) && isprint(ch)) ? ch : '.';
    }
nil:
    linebuf[lx++] = '\0';
}

/**
 * osalPrintHexdump - print a text hex dump to syslog for a binary blob of data
 * @prefix_str: string to prefix each line with;
 *  caller supplies trailing spaces for alignment if desired
 * @prefix_type: controls whether prefix of an offset, address, or none
 *  is printed (%DUMP_PREFIX_OFFSET, %DUMP_PREFIX_ADDRESS, %DUMP_PREFIX_NONE)
 * @rowsize: number of bytes to print per line; must be 16 or 32
 * @groupsize: number of bytes to print at a time (1, 2, 4, 8; default = 1)
 * @buf: data blob to dump
 * @len: number of bytes in the @buf
 * @ascii: include ASCII after the hex output
 *
 * Given a buffer of u8 data, osalPrintHexdump() prints a hex + ASCII dump
 * to the kernel log at the specified kernel log level, with an optional
 * leading prefix.
 *
 * osalPrintHexdump() works on one "line" of output at a time, i.e.,
 * 16 or 32 bytes of input data converted to hex + ASCII output.
 * osalPrintHexdump() iterates over the entire input @buf, breaking it into
 * "line size" chunks to format and print.
 *
 * E.g.:
 *   osalPrintHexdump("raw data: ", DUMP_PREFIX_ADDRESS,
 *          16, 1, frame->data, frame->len, true);
 *
 * Example output using %DUMP_PREFIX_OFFSET and 1-byte mode:
 * 0009ab42: 40 41 42 43 44 45 46 47 48 49 4a 4b 4c 4d 4e 4f  @ABCDEFGHIJKLMNO
 * Example output using %DUMP_PREFIX_ADDRESS and 4-byte mode:
 * ffffffff88089af0: 73727170 77767574 7b7a7978 7f7e7d7c  pqrstuvwxyz{|}~.
 */
void osalPrintHexdump(const char *prefix_str, int prefix_type,
                      int rowsize, int groupsize,
                      const void *buf, size_t len, int ascii)
{
    const UINT8 *ptr = buf;
    int i, linelen, remaining = len;
    //unsigned char linebuf[32 * 3 + 2 + 32 + 1];
    char linebuf[32 * 3 + 2 + 32 + 1];

    if (rowsize != 16 && rowsize != 32)
        rowsize = 16;

    for (i = 0; i < len; i += rowsize)
    {
        linelen = MIN(remaining, rowsize);
        remaining -= rowsize;

        osalHexdumpToBuffer(ptr + i, linelen, rowsize, groupsize,
                            linebuf, sizeof(linebuf), ascii);

        switch (prefix_type)
        {
            case DUMP_PREFIX_ADDRESS:
                printf("%s%p: %s\n", prefix_str, ptr + i, linebuf);
                break;
            case DUMP_PREFIX_OFFSET:
                printf("%s%.8x: %s\n", prefix_str, i, linebuf);
                break;
            default:
                printf("%s%s\n", prefix_str, linebuf);
                break;
        }
    }
}

OSAL_PUBLIC void osalHexdump(const char *note, const void *buf, size_t len)
{
    osalPrintHexdump(note, DUMP_PREFIX_OFFSET,
                     16, 1,
                     buf, len, 1);
}

/**************************************
 * Memory functions
 *************************************/

void *osalMemAlloc(UINT32 size)
{
    return (malloc(size));
}

void *osalMemAllocAtomic(UINT32 size)
{
    return (malloc(size));
}

/*
 * MemAlloc with Alignment API:
 * Make sure memory is aligned to alignment ( passed as last parameter)
 * This function is maintained for backward compatibiltiy
 * IDS team has requested not to touch this code, as this is in working
 condition.
 * another implementaion of this function can be found in API
 * "void * _OsalMemAllocAlligned(unsigned int size, unsigned int alignment)"
 *
 * return - Pointer to requested alligned memory.
 * in - space  (unknown....why?)
      - size need to malloc
 *    - allignement required ( 4, 8....256)
 *
 */
void *osalMemAllocAligned(UINT32 space, UINT32 size, UINT32 alignment)
{
    void *pMem = NULL;

    if (alignment < 1 || alignment > OSAL_MAX_ALIGNMENT)
    {
        osalLog(OSAL_LOG_LVL_ERROR,
                OSAL_LOG_DEV_STDOUT,
                "osalMemAllocAligned(): invalid alignment value %d \n",
                alignment);
        return (void *)NULL;
    }

    if (alignment == 1)
    {
        pMem = osalMemAlloc(size);
        return pMem;
    }

    if (posix_memalign(&pMem, alignment, size) != 0)
    {
        osalLog(OSAL_LOG_LVL_ERROR,
                OSAL_LOG_DEV_STDOUT,
                "osalMemAllocAlligned(): not able to align memory\n");
        return (void *)NULL;
    }
    return pMem;
}

void osalMemFree(void *ptr)
{
    OSAL_MEM_ASSERT(ptr != NULL);
    free(ptr);
}

/*
 * Copy count bytes from src to dest ,
 * returns pointer to the dest mem zone.
 */
void *osalMemCopy(void *dest, const void *src, UINT32 count)
{
    OSAL_MEM_ASSERT(dest != NULL);
    OSAL_MEM_ASSERT(src != NULL);
    return (memcpy(dest, src, count));
}

/*
 * Fills a memory zone with a given constant byte,
 * returns pointer to the memory zone.
 */
void *osalMemSet(void *ptr, UINT8 filler, UINT32 count)
{
    OSAL_MEM_ASSERT(ptr != NULL);
    return (memset(ptr, filler, count));
}

static void *osalMemZero(void *const ptr, const UINT32 count)
{
    UINT32 lim = 0;
    volatile unsigned char *volatile dstPtr = ptr;

    while (lim < count)
    {
        dstPtr[lim++] = '\0';
    }
    return (void *)dstPtr;
}

/*
 * Function for unoptimized calls.
 * Fills a memory zone with 0,
 * returns pointer to the memory zone.
 */
void *osalMemZeroExplicit(void *ptr, UINT32 count)
{
    OSAL_MEM_ASSERT(ptr != NULL);
#ifdef __STDC_LIB_EXT1__
    errno_t result =
        memset_s(ptr, sizeof(ptr), 0, count); /* Supported on C11 standard */
    OSAL_MEM_ASSERT(result == 0);
    return ptr;
#else
    return osalMemZero(ptr, count); /* Platform-independent secure memset */
#endif
}

OSAL_PUBLIC void osalMemAlignedFree(void *ptr)
{
    osalMemFree(ptr);
}

/**********************************************
 * Time module
 **********************************************/
/*
 *  Retrieve current system time.
 */
OSAL_PUBLIC OSAL_STATUS osalTimeGet(OsalTimeval *ptime)
{
    struct timeval tval;

    if (gettimeofday(&tval, NULL) == -1)
    {
        osalLog(OSAL_LOG_LVL_ERROR,
                OSAL_LOG_DEV_STDOUT,
                "osalTimeGet(): gettimeofday system call failed \n");

        return OSAL_FAIL;
    }

    ptime->secs = tval.tv_sec;

    /*
     * gettimeofday returns in terms of sec and uSec.
     * Convert it into sec and nanoseconds into OSAL type
     */
    ptime->nsecs = tval.tv_usec * OSAL_THOUSAND;

    return OSAL_SUCCESS;
}

OSAL_PUBLIC UINT64 osalTimestampGet(void)
{
    OsalTimeval ptime = {0};
    osalTimeGet(&ptime);
    return ((ptime.secs * OSAL_MILLION) + ptime.nsecs / OSAL_THOUSAND);
}

OSAL_PUBLIC UINT32 osalSysClockRateGet(void)
{
    return (HZ);
}

OSAL_PUBLIC void osalTicksToTimeval(UINT64 ticks, OsalTimeval *pTv)
{
    UINT32 tickPerSecs = 0;
    UINT32 nanoSecsPerTick = 0;
    /*
     * Reset the time value
     */

    pTv->secs = 0;
    pTv->nsecs = 0;

    tickPerSecs = osalSysClockRateGet();
    nanoSecsPerTick = OSAL_BILLION / tickPerSecs;

    /*
     * value less than 1 sec
     */

    if (tickPerSecs > ticks)   /* value less then 1 sec */
    {
        pTv->nsecs = ticks * nanoSecsPerTick;
    }
    else
    {
        pTv->secs = ticks / tickPerSecs;
        pTv->nsecs = (ticks % tickPerSecs) * nanoSecsPerTick;
    }
}

OSAL_PUBLIC UINT32 osalTimevalToTicks(OsalTimeval tv)
{
    UINT32 tickPerSecs = 0;
    UINT32 nanoSecsPerTick = 0;
    UINT32 maxSecs = 0;

    tickPerSecs = osalSysClockRateGet();
    nanoSecsPerTick = OSAL_BILLION / tickPerSecs;

    /*
     * Make sure we do not overflow
     */
    maxSecs = (max_UINT32 / tickPerSecs) - (tv.nsecs / OSAL_BILLION);

    if (maxSecs < tv.secs)
    {
        osalLog(OSAL_LOG_LVL_ERROR,
                OSAL_LOG_DEV_STDOUT,
                "osalTimevalToTicks(): Timeval too high. Max value allowed"
                " in seconds is %u < %llu\n",
                maxSecs,
                tv.secs);
        return OSAL_TIMEVAL_TOO_LARGE;
    }

    return ((tv.secs * tickPerSecs) + (tv.nsecs / nanoSecsPerTick));
}

/**************************************
 * Task services module
 *************************************/
/*
 * Sleep for the specified number of milliseconds
 */
OSAL_PUBLIC OSAL_STATUS osalSleep(UINT32 milliseconds)
{
    struct timespec resTime, remTime;
    INT32 status;
    UINT16 mil_rem;

    /* Divide by number of millisec per second */
    resTime.tv_sec = milliseconds / OSAL_THOUSAND;
    /* Multiply remainder by number of nanosecs per millisecond */
    mil_rem = milliseconds % OSAL_THOUSAND;
    resTime.tv_nsec = mil_rem * OSAL_MILLION;

    do
    {
        status = nanosleep(&resTime, &remTime);
        resTime.tv_sec = remTime.tv_sec;
        resTime.tv_nsec = remTime.tv_nsec;
    }
    while ((status != OSAL_SUCCESS) && (errno == EINTR));

    if (status != OSAL_SUCCESS)
    {
        osalLog(OSAL_LOG_LVL_ERROR,
                OSAL_LOG_DEV_STDOUT,
                "osalSleep():nanosleep() failed; errno=%d\n",
                errno);
        return OSAL_FAIL;
    }
    else
    {
        return OSAL_SUCCESS;
    }
}

OSAL_PUBLIC void osalYield(void)
{
    sched_yield();
}
