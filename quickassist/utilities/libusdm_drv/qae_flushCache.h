/***************************************************************************
 *
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 *   redistributing this file, you may do so under either license.
 * 
 *   GPL LICENSE SUMMARY
 * 
 *   Copyright(c) 2007-2020 Intel Corporation. All rights reserved.
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 * 
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *   The full GNU General Public License is included in this distribution
 *   in the file called LICENSE.GPL.
 * 
 *   Contact Information:
 *   Intel Corporation
 * 
 *   BSD LICENSE
 * 
 *   Copyright(c) 2007-2023 Intel Corporation. All rights reserved.
 *   All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 *  version: QAT1.7.L.4.13.0-00009
 *
 ***************************************************************************/
/**
 ***************************************************************************
 * @file qae_flushCache.h
 *
 * This file provides linux/FreeBSD memory allocation for quick assist API
 *
 ****************************************************************************/
#ifndef QAE_FLASH_CACHE_H_
#define QAE_FLASH_CACHE_H_

/*!
 * @file     core_feature_base.h
 * @brief    Base core feature API for Nuclei N/NX Core
 */
//#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup NMSIS_Core_Registers     Register Define and Type Definitions
 * \brief   Type definitions and defines for core registers.
 *
 * @{
 */
#ifndef __RISCV_XLEN
  /** \brief Refer to the width of an integer register in bits(either 32 or 64) */
  #ifndef __riscv_xlen
    #define __RISCV_XLEN    32
  #else
    #define __RISCV_XLEN    __riscv_xlen
  #endif
#endif /* __RISCV_XLEN */

/** \brief Type of Control and Status Register(CSR), depends on the XLEN defined in RISC-V */
#if __RISCV_XLEN == 32
  typedef uint32_t rv_csr_t;
#elif __RISCV_XLEN == 64
  typedef uint64_t rv_csr_t;
#else
  typedef uint32_t rv_csr_t;
#endif

/* NMSIS compiler specific defines */
/** \brief Pass information from the compiler to the assembler. */
#ifndef   __ASM
  #define __ASM                     __asm__
#endif

/** \brief Recommend that function should be inlined by the compiler. */
#ifndef   __INLINE
  #define __INLINE                  inline
#endif

/** \brief Define a static function that may be inlined by the compiler. */
#ifndef   __STATIC_INLINE
  #define __STATIC_INLINE           static inline
#endif

/** \brief Define a static function that should be always inlined by the compiler. */
#ifndef   __STATIC_FORCEINLINE
  #define __STATIC_FORCEINLINE      __attribute__((always_inline)) static inline
#endif

#ifdef USER_SPACE
#define STR(x)                    XSTR(x)
#define XSTR(x)                   #x
#define __STR(s)                  #s
#define STRINGIFY(s)              __STR(s)

/**
 * \brief CSR operation Macro for csrw instruction.
 * \details
 * Write the content of val to csr register
 * \param csr   CSR macro definition defined in
 *              \ref NMSIS_Core_CSR_Registers, eg. \ref CSR_MSTATUS
 * \param val   value to store into the CSR register
 */
#define __RV_CSR_WRITE(csr, val)                                               \
    do                                                                         \
    {                                                                          \
        rv_csr_t __v = (rv_csr_t)(val);                                        \
        __ASM volatile("csrw " STRINGIFY(csr) ", %0"                           \
                     :                                                         \
                     : "rK"(__v)                                               \
                     : "memory");                                              \
    } while (0);

/* === Nuclei CCM Registers === */
#define CSR_CCM_MBEGINADDR      0x7CB
#define CSR_CCM_MCOMMAND        0x7CC
#define CSR_CCM_MDATA           0x7CD
#define CSR_CCM_SUEN            0x7CE
#define CSR_CCM_SBEGINADDR      0x5CB
#define CSR_CCM_SCOMMAND        0x5CC
#define CSR_CCM_SDATA           0x5CD
#define CSR_CCM_UBEGINADDR      0x4CB
#define CSR_CCM_UCOMMAND        0x4CC
#define CSR_CCM_UDATA           0x4CD
#define CSR_CCM_FPIPE           0x4CF

/**
 * \brief Cache CCM Command Types
 */
typedef enum CCM_CMD {
    CCM_DC_INVAL = 0x0,                 /*!< Unlock and invalidate D-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_DC_WB = 0x1,                    /*!< Flush the specific D-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_DC_WBINVAL = 0x2,               /*!< Unlock, flush and invalidate the specific D-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_DC_LOCK = 0x3,                  /*!< Lock the specific D-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_DC_UNLOCK = 0x4,                /*!< Unlock the specific D-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_DC_WBINVAL_ALL = 0x6,           /*!< Unlock and flush and invalidate all the valid and dirty D-Cache lines */
    CCM_DC_WB_ALL = 0x7,                /*!< Flush all the valid and dirty D-Cache lines */
    CCM_DC_INVAL_ALL = 0x17,            /*!< Unlock and invalidate all the D-Cache lines */
    CCM_IC_INVAL = 0x8,                 /*!< Unlock and invalidate I-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_IC_LOCK = 0xb,                  /*!< Lock the specific I-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_IC_UNLOCK = 0xc,                /*!< Unlock the specific I-Cache line specified by CSR CCM_XBEGINADDR */
    CCM_IC_INVAL_ALL = 0xd              /*!< Unlock and invalidate all the I-Cache lines */
} CCM_CMD_Type;

/**
 * \brief  Flush several D-Cache lines specified by address in U-Mode
 * \details
 * This function flush several D-Cache lines specified
 * by the address and line count.
 * Command \ref CCM_DC_WB is written to CSR \ref CSR_CCM_UCOMMAND.
 * \remarks
 * This function must be executed in M/S/U-Mode only.
 * \param [in]    addr    start address to be flushed
 * \param [in]    size    cache size to be flushed
 */
__STATIC_FORCEINLINE void qaeFlushDCacheLines(unsigned long addr, unsigned long size)
{
	int cnt = (size + 31) >> 5;
    if (cnt > 0) {
        unsigned long i;
        __RV_CSR_WRITE(CSR_CCM_UBEGINADDR, addr);
        for (i = 0; i < cnt; i++) {
            __RV_CSR_WRITE(CSR_CCM_UCOMMAND, CCM_DC_WB);
        }
    }
}

/**
 * \brief  Flush and invalidate several D-Cache lines specified by address in U-Mode
 * \details
 * This function flush and invalidate several D-Cache lines specified
 * by the address and line count.
 * Command \ref CCM_DC_WBINVAL is written to CSR \ref CSR_CCM_UCOMMAND.
 * \remarks
 * This function must be executed in M/S/U-Mode only.
 * \param [in]    addr    start address to be flushed and invalidated
 * \param [in]    size    cache size to be flushed and invalidated
 */
__STATIC_FORCEINLINE void qaeFlushInvalDCacheLines(unsigned long addr, unsigned long size)
{
	int cnt = (size + 31) >> 5;
    if (cnt > 0) {
        unsigned long i;
        __RV_CSR_WRITE(CSR_CCM_UBEGINADDR, addr);
        for (i = 0; i < cnt; i++) {
            __RV_CSR_WRITE(CSR_CCM_UCOMMAND, CCM_DC_WBINVAL);
        }
    }
}
#endif

#ifdef KERNEL_SPACE
#include <asm/cache.h>
#include <asm/cacheflush.h>

/**
 * \brief  Flush several D-Cache lines specified by address in S-Mode
 * \details
 * This function flush several D-Cache lines specified
 * by the address and line count.
 * Command \ref CCM_DC_WB is written to CSR \ref CSR_CCM_UCOMMAND.
 * \remarks
 * This function must be executed in M/S/U-Mode only.
 * \param [in]    addr    start address to be flushed
 * \param [in]    size    cache size to be flushed
 */
__STATIC_FORCEINLINE void qaeFlushDCacheLines(unsigned long addr, unsigned long size)
{
    __flush_dcache_wback(addr, size);
}

/**
 * \brief  Flush and invalidate several D-Cache lines specified by address in S-Mode
 * \details
 * This function flush and invalidate several D-Cache lines specified
 * by the address and line count.
 * Command \ref CCM_DC_WBINVAL is written to CSR \ref CSR_CCM_UCOMMAND.
 * \remarks
 * This function must be executed in M/S/U-Mode only.
 * \param [in]    addr    start address to be flushed and invalidated
 * \param [in]    size    cache size to be flushed and invalidated
 */
__STATIC_FORCEINLINE void qaeFlushInvalDCacheLines(unsigned long addr, unsigned long size)
{
    //__flush_unalign_dcache_inv(addr, size);
    __flush_dcache_wback_inv(addr, size);
}
#endif


#ifdef __cplusplus
} /* close the extern "C" { */
#endif

#endif /* #ifndef QAE_FLASH_CACHE_H_ */